SELECT establish_dblink();

CREATE TABLE public.ati_ads_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_ads.menu em
    JOIN ati_ads.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_ads.ati_ads_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_ads_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_ads_combined_dataset
        ADD CONSTRAINT ati_ads_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_aes_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_aes.menu em
    JOIN ati_aes.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_aes.ati_aes_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_aes_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_aes_combined_dataset
        ADD CONSTRAINT ati_aes_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_ard_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_ard.menu em
    JOIN ati_ard.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_ard.ati_ard_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_ard_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_ard_combined_dataset
        ADD CONSTRAINT ati_ard_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_avs_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_avs.menu em
    JOIN ati_avs.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_avs.ati_avs_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_avs_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_avs_combined_dataset
        ADD CONSTRAINT ati_avs_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

REATE TABLE public.ati_cst_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_cst.menu em
    JOIN ati_cst.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_cst.ati_cst_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_cst_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_cst_combined_dataset
        ADD CONSTRAINT ati_cst_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_eec_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_eec.menu em
    JOIN ati_eec.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_eec.ati_eec_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_eec_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_eec_combined_dataset
        ADD CONSTRAINT ati_eec_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_evc_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_evc.menu em
    JOIN ati_evc.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_evc.ati_evc_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_evc_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_evc_combined_dataset
        ADD CONSTRAINT ati_evc_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_fms_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_fms.menu em
    JOIN ati_fms.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_fms.ati_fms_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_fms_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_fms_combined_dataset
        ADD CONSTRAINT ati_fms_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_ihm_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_ihm.menu em
    JOIN ati_ihm.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_ihm.ati_ihm_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_ihm_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_ihm_combined_dataset
        ADD CONSTRAINT ati_ihm_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_mes_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_mes.menu em
    JOIN ati_mes.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_mes.ati_mes_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_mes_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_mes_combined_dataset
        ADD CONSTRAINT ati_mes_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_obd_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_obd.menu em
    JOIN ati_obd.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_obd.ati_obd_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_obd_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_obd_combined_dataset
        ADD CONSTRAINT ati_obd_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_pmc_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_pmc.menu em
    JOIN ati_pmc.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_pmc.ati_pmc_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_pmc_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_pmc_combined_dataset
        ADD CONSTRAINT ati_pmc_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_scm_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_scm.menu em
    JOIN ati_scm.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_scm.ati_scm_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_scm_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_scm_combined_dataset
        ADD CONSTRAINT ati_scm_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_sms_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_sms.menu em
    JOIN ati_sms.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_sms.ati_sms_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_sms_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_sms_combined_dataset
        ADD CONSTRAINT ati_sms_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_tcs_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_tcs.menu em
    JOIN ati_tcs.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_tcs.ati_tcs_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_tcs_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_tcs_combined_dataset
        ADD CONSTRAINT ati_tcs_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.ati_vts_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM ati_vts.menu em
    JOIN ati_vts.submenu sm ON em.menu_id = sm.menu_id
    JOIN ati_vts.ati_vts_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'ati_vts_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.ati_vts_combined_dataset
        ADD CONSTRAINT ati_vts_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

