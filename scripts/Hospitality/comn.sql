SELECT establish_dblink();

CREATE TABLE public.hsi_cmd_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_cmd.menu em
    JOIN hsi_cmd.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_cmd.hsi_cmd_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_cmd_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_cmd_combined_dataset
        ADD CONSTRAINT hsi_cmd_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_crm_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_crm.menu em
    JOIN hsi_crm.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_crm.hsi_crm_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_crm_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_crm_combined_dataset
        ADD CONSTRAINT hsi_crm_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_ecm_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_ecm.menu em
    JOIN hsi_ecm.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_ecm.hsi_ecm_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_ecm_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_ecm_combined_dataset
        ADD CONSTRAINT hsi_ecm_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_wms_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_wms.menu em
    JOIN hsi_wms.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_wms.hsi_wms_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_wms_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_wms_combined_dataset
        ADD CONSTRAINT hsi_wms_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_swm_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_swm.menu em
    JOIN hsi_swm.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_swm.hsi_swm_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_swm_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_swm_combined_dataset
        ADD CONSTRAINT hsi_swm_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_sas_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_sas.menu em
    JOIN hsi_sas.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_sas.hsi_sas_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_sas_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_sas_combined_dataset
        ADD CONSTRAINT hsi_sas_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_pos_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_pos.menu em
    JOIN hsi_pos.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_pos.hsi_pos_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_pos_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_pos_combined_dataset
        ADD CONSTRAINT hsi_pos_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_pms_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_pms.menu em
    JOIN hsi_pms.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_pms.hsi_pms_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_pms_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_pms_combined_dataset
        ADD CONSTRAINT hsi_pms_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_orr_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_orr.menu em
    JOIN hsi_orr.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_orr.hsi_orr_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_orr_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_orr_combined_dataset
        ADD CONSTRAINT hsi_orr_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_obr_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_obr.menu em
    JOIN hsi_obr.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_obr.hsi_obr_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_obr_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_obr_combined_dataset
        ADD CONSTRAINT hsi_obr_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_ims_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table = 'departments' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_ims.menu em
    JOIN hsi_ims.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_ims.hsi_ims_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_ims_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_ims_combined_dataset
        ADD CONSTRAINT hsi_ims_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_hmm_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_hmm.menu em
    JOIN hsi_hmm.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_hmm.hsi_hmm_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_hmm_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_hmm_combined_dataset
        ADD CONSTRAINT hsi_hmm_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_haf_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_haf.menu em
    JOIN hsi_haf.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_haf.hsi_haf_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_haf_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_haf_combined_dataset
        ADD CONSTRAINT hsi_haf_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_gsc_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_gsc.menu em
    JOIN hsi_gsc.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_gsc.hsi_gsc_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_gsc_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_gsc_combined_dataset
        ADD CONSTRAINT hsi_gsc_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_gas_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_gas.menu em
    JOIN hsi_gas.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_gas.hsi_gas_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_gas_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_gas_combined_dataset
        ADD CONSTRAINT hsi_gas_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

CREATE TABLE public.hsi_etw_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    sm.submenu_action,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    eci.field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    eci.field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    eci.column_default,
    eci.field_required,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            0
        ELSE
            null
    END::integer AS field_min,
    CASE
        WHEN eci.column_name like '%_rating' THEN 
            10
        ELSE
            null
    END::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table = 'system_user' THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, reference_table), false, true, '')))[1]::text::int > 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    CASE
        WHEN eci.field_order <= 8 THEN NULL::text
        ELSE initcap(eci.table_name || ' '  || ((eci.field_order - 1) / 8)::text )
    END::varchar AS tab_name,
    CASE
        WHEN eci.field_order <= 8 THEN 0
        ELSE (eci.field_order - 1) / 8
    END::integer AS tab_no
FROM hsi_etw.menu em
    JOIN hsi_etw.submenu sm ON em.menu_id = sm.menu_id
    JOIN hsi_etw.hsi_etw_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'hsi_etw_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.hsi_etw_combined_dataset
        ADD CONSTRAINT hsi_etw_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;

