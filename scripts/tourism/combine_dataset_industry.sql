CREATE TABLE combined_dataset AS
SELECT 
    field_id, schema_name,industry_id,
    industry_code, industry_name,
    DENSE_RANK() OVER (ORDER BY system_code) AS system_id,
    system_code,
    system_name, system_description,
    DENSE_RANK() OVER (ORDER BY system_code, menu_code) AS menu_id,
    menu_name, menu_description, menu_code,
    menu_is_pinned, menu_element, menu_order_index,
    DENSE_RANK() OVER (ORDER BY system_code, menu_code, submenu_code) AS submenu_id,
    submenu_name, submenu_description, submenu_code, submenu_is_pinned,
    submenu_order_index,submenu_element,submenu_layout,
    submenu_parent_id,submenu_show_in_sidebar, submenu_action,
    table_name,table_description,column_name,field_type,field_order,
    field_label,field_placeholder,column_default,field_required,field_min,
    field_max,character_maximum_length,numeric_precision,reference_table,
    reference_column,field_multiple,form_group,form_ordering,
    field_label_length,field_parent,field_position,field_use_css,field_default_css,
    field_custom_css, field_component, field_response_choice, field_value, index_field,
    field_validator,field_function, tab_no,tab_name
FROM 
(
    SELECT * from public.toi_cfr_combined_dataset
    UNION ALL
    SELECT * from public.toi_cms_combined_dataset
    UNION ALL
    SELECT * from public.toi_crm_combined_dataset
    UNION ALL
    SELECT * from public.toi_ems_combined_dataset
    UNION ALL
    SELECT * from public.toi_gfr_combined_dataset
    UNION ALL
    SELECT * from public.toi_mps_combined_dataset
    UNION ALL
    SELECT * from public.toi_otp_combined_dataset
    UNION ALL
    SELECT * from public.toi_pms_combined_dataset
    UNION ALL
    SELECT * from public.toi_pos_combined_dataset
    UNION ALL
    SELECT * from public.toi_ppf_combined_dataset
    UNION ALL
    SELECT * from public.toi_rbs_combined_dataset
    UNION ALL
    SELECT * from public.toi_taa_combined_dataset
    UNION ALL
    SELECT * from public.toi_tic_combined_dataset
    UNION ALL
    SELECT * from public.toi_tms_combined_dataset
    UNION ALL
    SELECT * from public.toi_tss_combined_dataset
    UNION ALL
    SELECT * from public.toi_tto_combined_dataset
)