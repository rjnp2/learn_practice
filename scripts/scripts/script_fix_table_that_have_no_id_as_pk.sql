DO $$
DECLARE
	current_schema_name text;
	current_table_name text;
	current_column_names text[];
	modi_table_name text;
	query_text text;
BEGIN
    FOR current_schema_name, current_table_name, current_column_names IN (
        SELECT tc.table_schema, kcu.table_name, ARRAY_AGG(kcu.column_name) AS primary_key_columns
		FROM information_schema.table_constraints tc
		JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
		WHERE tc.constraint_type = 'PRIMARY KEY'
		AND tc.table_schema NOT IN ('information_schema', 'pg_catalog', 'public')
		AND kcu.column_name != 'id'
		AND kcu.table_name NOT IN ('menu', 'submenu')
		AND kcu.table_name NOT IN (
			SELECT relname FROM pg_stat_all_tables 
			WHERE n_live_tup > 0
			AND schemaname = tc.table_schema
		)
		GROUP BY tc.table_schema, kcu.table_name
		HAVING COUNT(kcu.column_name) = 1
		ORDER BY tc.table_schema, kcu.table_name
    )
    LOOP
		modi_table_name := current_schema_name || '.' || current_table_name;
		query_text := 'ALTER TABLE ' || modi_table_name || ' RENAME COLUMN ' || current_column_names[1] || ' TO id;';
		BEGIN
			RAISE NOTICE '%', query_text;	
			EXECUTE query_text;
		EXCEPTION
			WHEN others THEN
				RAISE NOTICE 'Error occur in Table % % %', current_schema_name, current_table_name, current_column_names[1];
		END;
		END LOOP;
END $$;
