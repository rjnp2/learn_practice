DROP VIEW IF EXISTS form_global cascade;

create or replace view form_global as 
select
id,
  name,
  jsonb_array_elements(options)->>'id' AS option_id,
  jsonb_array_elements(options)->>'name' AS option_name
FROM
  sch_20230822938eebb.custom_attributes;
