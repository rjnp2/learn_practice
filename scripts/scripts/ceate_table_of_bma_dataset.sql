SELECT establish_dblink();

CREATE TABLE public.bti_bma_combined_dataset AS
SELECT 
    ROW_NUMBER() OVER (ORDER BY em.menu_id, sm.submenu_id, eci.field_order) AS field_id,
    dsc.system_code AS schema_name,
    dsc.industry_id,
    dsc.industry_code,
    dsc.industry_name,
    dsc.system_id,
    dsc.system_code,
    dsc.system_name,
    dsc.system_description,
    em.menu_id,
    em.menu_name,
    em.menu_description,
    em.menu_code,
    em.menu_is_pinned,
    em.menu_element,
    dense_rank() OVER (PARTITION BY dsc.system_name ORDER BY em.menu_code)::integer AS menu_order_index,
    sm.submenu_id,
    sm.submenu_name,
    sm.submenu_description,
    sm.submenu_code,
    sm.submenu_is_pinned,
    dense_rank() OVER (PARTITION BY em.menu_name ORDER BY sm.submenu_code)::integer AS submenu_order_index,
    sm.submenu_element,
    sm.submenu_layout,
    sm.submenu_parent_id,
    sm.submenu_show_in_sidebar,
    eci.table_name,
    eci.table_description,
    eci.column_name,
    CASE
        WHEN eci.reference_table in ('users', 'employees') THEN 
            '1'
        ELSE
            eci.field_type
    END::text AS field_type,
    dense_rank() OVER (PARTITION BY eci.table_name ORDER BY eci.field_order)::integer AS field_order,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            regexp_replace(eci.field_label, ' Id', '', 'g')
        ELSE
            eci.field_label
    END::text AS field_label,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            CONCAT('Select', ' ', regexp_replace(eci.field_label, ' Id', '', 'g'))
        ELSE
            CONCAT('Enter', ' ', eci.field_label)
    END::text AS field_placeholder,
    CASE
        WHEN eci.reference_table != 'No' THEN 
            Null
        ELSE
            eci.column_default
    END::text AS column_default,
    CASE
        WHEN eci.column_default = 'CURRENT_TIMESTAMP' THEN 
            false
        ELSE
            eci.field_required
    END::boolean AS field_required,
    NULL::integer AS field_min,
    NULL::integer AS field_max,
    eci.character_maximum_length,
    eci.numeric_precision,
    eci.reference_table,
    eci.reference_column,
    false::boolean as field_multiple,
    0::text as form_group,
    0::integer as form_ordering,
    12::integer as field_label_length,
    null::integer as field_parent,
    'top'::text as field_position,
    'default_css'::text as field_use_css,
    1::integer as field_default_css,
    null::json as field_custom_css,
    'question'::text as field_component,
    CASE
        WHEN eci.reference_table in ('users', 'employees') THEN 
            'internal'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, table_name), false, true, '')))[1]::text::int = 0 THEN 
            'relation'
        WHEN eci.reference_table != 'No' and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, table_name), false, true, '')))[1]::text::int = 0 THEN 
            'global'
        ELSE
            'default'
    END::text AS field_response_choice,
    null::text as field_value,
    SPLIT_PART(eci.column_description, '//', 1) AS index_field,
    SPLIT_PART(eci.column_description, '//', 2) AS field_validator,
    SPLIT_PART(eci.column_description, '//', 3) AS field_function,
    eci.tab_no,
    eci.tab_name
FROM bti_bma.menu em
    JOIN bti_bma.submenu sm ON em.menu_id = sm.menu_id
    JOIN bti_bma.bti_bma_column_details eci ON sm.submenu_id = eci.subq
    JOIN public.dalfin_system_config dsc ON em.system_code::text = dsc.system_code::text
WHERE eci.column_name::name <> ALL (ARRAY['menu_id'::name, 'submenu_id'::name])
and em.menu_name != 'Dashboard'
and (eci.table_name not like '%role%' and eci.table_name not like '%permission%' and eci.table_name not like '%group%')
and eci.table_name not in ('users', 'employees')
and (eci.reference_table not like '%role%' and eci.reference_table not like '%permission%' and eci.reference_table not like '%group%')
ORDER BY em.menu_id, sm.submenu_id, eci.field_order;

DO $$ 
BEGIN
    IF NOT EXISTS (
        SELECT 1
        FROM information_schema.table_constraints 
        WHERE table_name = 'bti_bma_combined_dataset' 
          and table_schema = 'public'
          AND constraint_type = 'PRIMARY KEY' 
          AND constraint_name = 'field_id'
    ) THEN
        ALTER TABLE public.bti_bma_combined_dataset
        ADD CONSTRAINT bti_bma_combined_dataset_pk_field_id PRIMARY KEY (field_id);
    END IF;
END $$;
