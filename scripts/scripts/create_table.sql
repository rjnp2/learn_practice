CREATE TABLE hid_hms.lab_technician (
	id serial4 NOT NULL,
	"name" varchar(255) NOT NULL,
	"role" varchar(255) NULL,
	department_id int4 NULL,
	contact_info text NULL,
	hire_date date NULL,
	submenu_id int4 NULL,
	CONSTRAINT lab_technician_pkey PRIMARY KEY (id),
	CONSTRAINT fk_lab_technician_submenu FOREIGN KEY (submenu_id) REFERENCES hid_hms.submenu(submenu_id),
	CONSTRAINT lab_technician_department_id_fkey FOREIGN KEY (department_id) REFERENCES hid_hms.departments(id)
);

CREATE TABLE hid_hms.patient_admission (
	id serial4 NOT NULL,
	submenu_id int4 NULL DEFAULT 803,
	patient_id int4 NULL,
	start_date timestamp NULL,
	end_date timestamp NULL,
	data_collected text NULL,
	CONSTRAINT patient_admission_pkey PRIMARY KEY (id),
	CONSTRAINT patient_admission_patient_id_fkey FOREIGN KEY (patient_id) REFERENCES hid_hms.patients(id),
	CONSTRAINT patient_admission_submenu_id_fkey FOREIGN KEY (submenu_id) REFERENCES hid_hms.submenu(submenu_id)
);

CREATE TABLE hid_hms.pharmacies (
	id serial4 NOT NULL,
	"name" varchar(255) NOT NULL,
	"location" text NULL,
	contact_info text NULL,
	submenu_id int4 NULL,
	CONSTRAINT pharmacies_pkey PRIMARY KEY (id),
	CONSTRAINT fk_pharmacies_submenu FOREIGN KEY (submenu_id) REFERENCES hid_hms.submenu(submenu_id)
);

CREATE TABLE hid_mis.institution (
	id serial4 NOT NULL,
	"name" varchar(255) NOT NULL,
	"location" text NULL,
	contact_info text NULL,
	submenu_id int4 NULL,
	CONSTRAINT institution_pkey PRIMARY KEY (id),
	CONSTRAINT fk_institution_submenu FOREIGN KEY (submenu_id) REFERENCES hid_mis.submenu(submenu_id)
);

CREATE TABLE hid_lms.equipment (
	id int4 NOT NULL,
	supplier_id int4 NULL,
	equipment_supplied text NULL,
	contact_details text NULL,
	submenu_id int4 NULL,
	CONSTRAINT equipment_pkey PRIMARY KEY (id),
	CONSTRAINT fk_equipment_submenu FOREIGN KEY (submenu_id) REFERENCES hid_lms.submenu(submenu_id),
	CONSTRAINT fk_equipment_supplier FOREIGN KEY (supplier_id) REFERENCES hid_lms.suppliers(id)
);

CREATE TABLE hid_mis.users (
	id serial4 NOT NULL,
	"system_user" varchar NULL DEFAULT 'change to system_user'::character varying,
	address varchar NULL,
	data_of_birth date NULL,
	contact_info text NULL,
	submenu_id int4 NULL,
	CONSTRAINT users_pk PRIMARY KEY (id),
	CONSTRAINT users_submenu_fk FOREIGN KEY (submenu_id) REFERENCES hid_mis.submenu(submenu_id)
);

CREATE TABLE hid_prs.monitoring_device (
	id int4 NOT NULL DEFAULT nextval('hid_prs.monitoring_device_equipment_id_seq'::regclass),
	equipment_name text NOT NULL,
	manufacturer text NULL,
	purchase_date date NULL,
	warranty_expiry date NULL,
	submenu_id int4 NULL,
	CONSTRAINT monitoring_device_pkey PRIMARY KEY (id),
	CONSTRAINT fk_monitoring_device_submenu FOREIGN KEY (submenu_id) REFERENCES hid_prs.submenu(submenu_id)
);

CREATE TABLE hid_tis."content" (
	id serial4 NOT NULL,
	title varchar(255) NOT NULL,
	description text NULL,
	submenu_id int4 NULL,
	CONSTRAINT content_pkey PRIMARY KEY (id),
	CONSTRAINT fk_content_submenu FOREIGN KEY (submenu_id) REFERENCES hid_tis.submenu(submenu_id)
);