DO $$
DECLARE
    v_schema_name text;
   	num int:= 0;
    submenu_table_name text;
    column_exist boolean;
    max_submenu_id integer;
    query_text text;
    result_record record;
    menu_id integer;
    table_names text;
    submenu_id text;
    char_set TEXT = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    code_char text;
    menu_code text;
    menu_table_name text;
    current_table_name text;
begin
    FOR v_schema_name IN (
        SELECT schema_name FROM information_schema.schemata
        WHERE schema_name NOT IN  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
        ORDER BY schema_name
    )
    loop
        submenu_table_name := v_schema_name || '.submenu';
        menu_table_name := v_schema_name || '.menu';
        query_text := 'SELECT * FROM ' || submenu_table_name;

        FOR result_record IN EXECUTE query_text 
        LOOP
            submenu_id := result_record.submenu_id;
            menu_id := result_record.menu_id;
            query_text := 'SELECT 1 FROM information_schema.columns
                WHERE table_schema = $1
                AND column_name = $2
                AND column_default = $3
                having count(table_name) > 1;';
            num := num + 1;
            Execute query_text USING v_schema_name, 'submenu_id', submenu_id into column_exist;
            if column_exist then
            	
            	query_text := 'SELECT table_name FROM information_schema.columns
                WHERE table_schema = $1
                AND column_name = $2
                AND column_default = $3
				OFFSET 1';
			
				FOR table_names IN Execute query_text USING v_schema_name, 'submenu_id', submenu_id
        		loop
                    current_table_name := v_schema_name || '.' || table_names;
	        		table_names := REPLACE(INITCAP(table_names), '_', ' ');
	        		query_text := 'SELECT max(submenu_id) FROM '|| submenu_table_name||'
	                WHERE menu_id = '''||menu_id||'''';
                	Execute query_text into max_submenu_id; 
                	max_submenu_id := max_submenu_id + 1;
                
	        		query_text := 'SELECT menu_code FROM '||menu_table_name||' where menu_id = '||menu_id||';';
					EXECUTE query_text INTO menu_code;
	        		
	        		code_char := menu_code || '-' || SUBSTRING(char_set FROM ((max_submenu_id/1296) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM ((max_submenu_id/36) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM (max_submenu_id % LENGTH(char_set) + 1) FOR 1);
	        	
	        	    query_text := 'INSERT INTO ' || submenu_table_name || ' (submenu_id, submenu_name, menu_id, submenu_description, submenu_code) VALUES ($1, $2, $3, $4, $5)';
            	    EXECUTE query_text USING max_submenu_id, table_names, menu_id, table_names, code_char;
            	    RAISE NOTICE 'Added records % % % % % ', max_submenu_id, table_names, menu_id, table_names, code_char;
            	  	
            	    EXECUTE 'ALTER TABLE '||current_table_name||' ALTER COLUMN submenu_id SET DEFAULT '||max_submenu_id||';';
            	    raise notice 'Set default value of % with %', current_table_name, max_submenu_id;
           		end loop;
           	end if;
        END LOOP;
    END LOOP;
END $$;
