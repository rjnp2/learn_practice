DO $$ 
DECLARE
    v_schema_name TEXT;
    query_text TEXT;
    record_data record;
BEGIN
    FOR v_schema_name IN (
        SELECT schema_name FROM information_schema.schemata
        where schema_name not in  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
        and schema_name not like '%_temp_%'
        order by schema_name
    )
    LOOP
        query_text := 'SELECT submenu_code, COUNT(*) AS duplicate_count
            FROM '|| v_schema_name ||'.submenu
            GROUP BY submenu_code
            HAVING COUNT(submenu_code) > 1';

        FOR record_data IN execute query_text
        LOOP
            RAISE NOTICE 'schema: %, Submenu_code: %, Count: %', v_schema_name, record_data.submenu_code, record_data.duplicate_count;
        END LOOP;
    END LOOP;
END $$;
