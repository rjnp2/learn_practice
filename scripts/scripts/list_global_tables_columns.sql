SELECT table_catalog, table_schema, table_name, column_name
FROM information_schema.columns
where column_default is null
and table_name IN (
   SELECT tb.table_name
	FROM information_schema.tables as tb
	WHERE table_type = 'BASE TABLE'
	and table_name not in ('menu', 'submenu')
	and table_schema not in ('information_schema', 'pg_catalog', 'public')
	and table_name NOT IN (
		SELECT table_name
		FROM information_schema.columns
		WHERE column_name = 'submenu_id'
		and table_schema = tb.table_schema
	)
	and table_name in (
		SELECT relname FROM pg_stat_all_tables where n_live_tup > 0 and schemaname = tb.table_schema
	)
)
order by table_schema,table_name;