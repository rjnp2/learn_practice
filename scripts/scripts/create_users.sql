DO $$
DECLARE
    schema_name text;
BEGIN
    FOR schema_name IN
        SELECT s.schema_name FROM information_schema.schemata s
        WHERE s.schema_name NOT IN ('information_schema', 'pg_catalog', 'public','pg_toast')
        AND s.schema_name NOT IN (
            SELECT c.table_schema FROM information_schema.columns c
            WHERE c.column_name ILIKE '%username%' OR c.column_name ILIKE '%email%'  or c.column_default ilike '%change to system_user%'
        )
        ORDER BY s.schema_name
    LOOP
        EXECUTE 'CREATE TABLE ' || quote_ident(schema_name) || '.users (
            id serial4 NOT NULL,
            "system_user" varchar NULL DEFAULT ''change to system_user''::character varying,
            address varchar NULL,
            date_of_birth date NULL,
            contact_info text NULL,
            submenu_id int4 NULL DEFAULT 10010,
            CONSTRAINT users_pk PRIMARY KEY (id),
            CONSTRAINT users_submenu_fk FOREIGN KEY (submenu_id) REFERENCES ' || quote_ident(schema_name) || '.submenu(submenu_id)
        )';
    END LOOP;
END $$;