-- Retrieve all tables that lack the 'submenu_id' column and not global tables.
SELECT tb.table_catalog, tb.table_schema, tb.table_name
FROM information_schema.tables tb
WHERE tb.table_type = 'BASE TABLE'
and tb.table_name not in ('menu', 'submenu')
and tb.table_schema not in ('information_schema', 'pg_catalog', 'public')
and tb.table_schema = 'bti_agy'
and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, table_name), false, true, '')))[1]::text::int <= 0
and tb.table_name not IN (
	SELECT table_name
	FROM information_schema.columns
	WHERE column_name = 'submenu_id'
	and table_schema = tb.table_schema
)
order by table_schema, table_name;

