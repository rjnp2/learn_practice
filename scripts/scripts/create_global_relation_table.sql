CREATE TABLE dalfin.global_relation (
	id serial4 NOT NULL,
	database_name varchar NULL,
	schema_name varchar NULL,
	table_name varchar NULL,
	column_name varchar NULL,
	field_value varchar NULL,
	field_id int4 NULL,
	CONSTRAINT global_pk PRIMARY KEY (id)
);

CREATE TABLE dalfin.form_form_relation (
	id serial4 NOT NULL,
	database_name varchar NOT NULL,
	schema_name varchar NOT NULL,
	table_name varchar NOT NULL,
	column_name varchar NOT NULL,
	submenu_code varchar NOT NULL,
	field_id int4 NOT NULL,
	multiple bool NULL DEFAULT false,
	separator _text NULL,
	reference_column _int8 NULL,
	CONSTRAINT form_form_relation_pk PRIMARY KEY (id)
);