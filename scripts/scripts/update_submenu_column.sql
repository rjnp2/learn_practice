-- change column name of name/title to submenu_name, id to menu_id 
-- add new column if not exists
-- set submenu_code if null
DO $$
DECLARE
	v_schema_name text;
	mod_schema_name text;
	v_table_name text;
	v_column_name text;
	query_text text;
	submenu_table_name text;
	menu_table_name text;
	menu_code text;
	menu_id INTEGER;
	result_record record;
	column_exists boolean;
	char_set TEXT = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    code_char text;
begin
	FOR v_schema_name IN (
	   SELECT schema_name FROM information_schema.schemata
	   where schema_name not in  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
	   and schema_name not like '%_temp_%'
	   order by schema_name
	)
	loop
		mod_schema_name := REPLACE(UPPER(v_schema_name), '_', '-');
		raise notice 'Starting to work on the % schema.', mod_schema_name;
		submenu_table_name := v_schema_name || '.submenu';
		menu_table_name := v_schema_name || '.menu';

		-- change column name of id to submenu_id if exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''id''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists then
			raise notice 'Changing column name of id to submenu_id';
			query_text := 'ALTER TABLE '||submenu_table_name||' RENAME COLUMN "id" TO submenu_id;';
			EXECUTE query_text;
		end if;
		------------------

		-- change column name of name to submenu_name if exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''name''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists then
			raise notice 'Changing column name of name to submenu_name';
			query_text := 'ALTER TABLE '||submenu_table_name||' RENAME COLUMN "name" TO submenu_name;';
			EXECUTE query_text;
		end if;
		------------------

		-- change column name of label to submenu_name if exists
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''submenu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''label'';';
            
        EXECUTE query_text INTO column_exists;
        if column_exists then
            raise notice 'Changing column name of label to submenu_name';
            query_text := 'ALTER TABLE '||submenu_table_name||' RENAME COLUMN "label" TO submenu_name;';
            EXECUTE query_text;
        end if;
        ---------------------

		-- change column name of title to submenu_name if exists
		query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''submenu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''title'';';
            
        EXECUTE query_text INTO column_exists;
        if column_exists then
			raise notice 'Changing column name of title to submenu_name';
            query_text := 'ALTER TABLE '||submenu_table_name||' RENAME COLUMN "title" TO submenu_name;';
            EXECUTE query_text;
        end if;
		------------------

		-- change column name of description to submenu_description if exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''description''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists then
			raise notice 'Changing column name of description to submenu_description';
			query_text := 'ALTER TABLE '||submenu_table_name||' RENAME COLUMN "description" TO submenu_description;';
			EXECUTE query_text;
		end if;
		------------------
		
		
		-- change column name of description to submenu_description if exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''menu_ref''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists then
			raise notice 'Changing column name of menu_ref to submenu_description';
			query_text := 'ALTER TABLE '||submenu_table_name||' RENAME COLUMN "menu_ref" TO menu_id;';
			EXECUTE query_text;
		end if;
		------------------

		-- add new column menu_id if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''menu_id''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called menu_id';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN menu_id INTEGER;';
		end if;
		------------------
		
		-- add new column verified if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''verified''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called verified';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN verified INTEGER default 0;';
		end if;
		------------------

		-- add new column submenu_action if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''submenu_action''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called submenu_action';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN submenu_action text;';
		end if;
		------------------

		-- set value in menu_id if menu_id is null
		query_text := 'SELECT 1
            FROM '||submenu_table_name||'
            WHERE menu_id is null';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists then
			raise notice 'Updating value of menu_id after removing last two digit of submenu_id value where menu_id is null';
            query_text := 'SELECT submenu_id FROM '||submenu_table_name||' WHERE menu_id is null;';
        	FOR result_record IN execute query_text
			loop
				menu_id := LEFT(result_record.submenu_id::TEXT, LENGTH(result_record.submenu_id::TEXT) - 2);
				query_text := 'SELECT 1 FROM '||menu_table_name||' where menu_id = '||menu_id||';';
				EXECUTE query_text INTO column_exists;
				if column_exists then
					query_text := 'UPDATE '||submenu_table_name||' SET menu_id = ''' || menu_id ||''' WHERE submenu_id = '|| result_record.submenu_id || ';';
					raise notice '%', query_text;
					EXECUTE query_text;
				else
					raise notice 'Menu is not found for % submenu id.', result_record.submenu_id;
				end if;
			end loop;
		end if;
		------------------

		-- add new column submenu_code if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''submenu_code''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called submenu_code';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN submenu_code varchar(75);';
		end if;
		------------------

		-- set value in submenu_code if submenu_code is null
		query_text := 'SELECT 1
            FROM '||submenu_table_name||'
            WHERE submenu_code is null';
        
        EXECUTE query_text INTO column_exists;
        if column_exists is not null then
			raise notice 'Updating value of submenu_code with random value where submenu_code is null';
            query_text := 'SELECT submenu_id, menu_id FROM '||submenu_table_name||';';
        	FOR result_record IN execute query_text
            loop
				menu_id = result_record.menu_id;
				query_text := 'SELECT menu_code FROM '||menu_table_name||' where menu_id = '||menu_id||';';
				EXECUTE query_text INTO menu_code;

        		code_char := menu_code || '-' || SUBSTRING(char_set FROM ((result_record.submenu_id/1296) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM ((result_record.submenu_id/36) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM (result_record.submenu_id % LENGTH(char_set) + 1) FOR 1);
        		query_text := 'UPDATE '||submenu_table_name||' SET submenu_code = ''' || code_char ||''' WHERE submenu_id = '|| result_record.submenu_id || ';';
        		raise notice '%', query_text;
        		EXECUTE query_text;
            end loop;
        end if;
		------------------

		-- add new column submenu_is_pinned if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''submenu_is_pinned''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called submenu_is_pinned';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN submenu_is_pinned boolean default false;';
		end if;
		------------------

		-- add new column submenu_element if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''submenu_element''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called submenu_element';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN submenu_element varchar(75) default ''form'';';
		end if;
		------------------

		-- add new column submenu_layout if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''submenu_layout''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called submenu_layout';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN submenu_layout varchar(75) default ''800*1400'';';
		end if;
		------------------

		-- add new column submenu_parent_id if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''submenu_parent_id''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called submenu_parent_id';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN submenu_parent_id varchar(75);';
		end if;
		------------------

		-- add new column submenu_show_in_sidebar if not exists
		query_text := 'SELECT 1
	       FROM information_schema.columns
	       WHERE table_name = ''submenu''
	       AND table_schema = ''' || v_schema_name || '''
		   and column_name = ''submenu_show_in_sidebar''';
		 
		EXECUTE query_text INTO column_exists;
		if column_exists is null then
			raise notice 'Adding new column called submenu_show_in_sidebar';
			EXECUTE 'ALTER TABLE '||submenu_table_name||' ADD COLUMN submenu_show_in_sidebar boolean default false;';
		end if;
		------------------
		
	end loop;
end $$;