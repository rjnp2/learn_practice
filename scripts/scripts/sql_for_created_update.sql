select distinct column_name from information_schema.columns
where table_name IN (
	SELECT table_name
	FROM information_schema.tables
	WHERE table_type = 'BASE TABLE'
	and table_schema not in ('information_schema', 'pg_catalog', 'public')
	and table_name not in ('menu', 'submenu')
	and table_schema = information_schema.columns.table_schema 
)
and (column_name LIKE 'create%' OR column_name LIKE 'update%'); 