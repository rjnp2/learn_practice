SELECT table_schema, table_name
FROM information_schema.columns
WHERE column_name = 'submenu_id'
and column_default is null
and table_name NOT IN ('menu', 'submenu')
and table_schema not in ('information_schema', 'pg_catalog', 'public')
order by table_schema, table_name;
