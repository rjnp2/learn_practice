SELECT table_schema, table_name, (ARRAY_AGG(column_name)) AS primary_key_columns  from information_schema.columns
where table_name not in ('menu', 'submenu')
and table_schema not in ('information_schema', 'pg_catalog', 'public')
and table_name IN (
	SELECT table_name
	FROM information_schema.tables
	where table_type = 'BASE TABLE'
	and table_schema = information_schema.columns.table_schema
	and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, table_name), false, true, '')))[1]::text::int = 0
)
AND EXISTS (
	SELECT *
	FROM information_schema.table_constraints AS tc
	JOIN information_schema.key_column_usage AS kcu
	ON tc.constraint_name = kcu.constraint_name
	WHERE tc.constraint_type = 'PRIMARY KEY'
	 and kcu.column_name = information_schema.columns.column_name
	 and tc.table_schema = information_schema.columns.table_schema
	 and kcu.table_name = information_schema.columns.table_name
)
GROUP BY table_schema, table_name
HAVING COUNT(column_name) > 1
order by table_schema, table_name;

