DO $$
DECLARE
    query_text text;
begin
	FOR query_text IN (
		SELECT 'ALTER TABLE ' || table_schema || '.' || table_name || ' ALTER COLUMN ' || column_name || ' SET NOT NULL;'
		FROM information_schema.columns
		WHERE table_name IN (
			SELECT tb.table_name
			FROM information_schema.tables as tb
			WHERE table_type = 'BASE TABLE'
		)
		AND table_schema NOT IN ('information_schema', 'pg_catalog', 'public')
		AND table_name NOT IN ('menu', 'submenu')
		AND is_nullable = 'YES'
		ORDER BY table_schema, table_name, ordinal_position
	)
	loop
	raise notice '%', query_text;
	EXECUTE query_text;
	end loop;
end $$;