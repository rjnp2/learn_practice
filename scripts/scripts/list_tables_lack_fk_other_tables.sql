SELECT table_schema, table_name, column_name from information_schema.columns
where column_name LIKE '%_id' 
and column_name not in('menu_id','submenu_id')
and table_name not like '%_dataset'
and table_schema not in ('information_schema', 'pg_catalog', 'public')
AND NOT EXISTS (
	SELECT 1
	FROM information_schema.table_constraints AS tc
	JOIN information_schema.key_column_usage AS kcu
	ON tc.constraint_name = kcu.constraint_name
	WHERE tc.constraint_type in ('PRIMARY KEY', 'FOREIGN KEY')
	AND kcu.table_name = information_schema.columns.table_name
	and tc.table_schema = information_schema.columns.table_schema 
	AND kcu.column_name = information_schema.columns.column_name
	)
and table_name not in (
	SELECT relname FROM pg_stat_all_tables where n_live_tup > 0
)
order by table_schema, table_name asc;
