// Script to add fk of submenu_id column
DO $$
DECLARE
    current_table_name text;
    current_schema_name text;
BEGIN
    -- Add the submenu_id column to tables that meet the specified conditions
    for current_schema_name, current_table_name IN (
        SELECT table_schema, table_name from information_schema.columns
        where column_name = 'submenu_id'
        and table_name not in ('menu', 'submenu')
        and table_schema not in ('information_schema', 'pg_catalog', 'public')
        and table_schema != 'public'
        AND NOT EXISTS (
            SELECT 1
            FROM information_schema.table_constraints AS tc
            JOIN information_schema.key_column_usage AS kcu
            ON tc.constraint_name = kcu.constraint_name
            WHERE tc.constraint_type = 'FOREIGN KEY'
            AND kcu.table_name = information_schema.columns.table_name
            AND kcu.column_name = information_schema.columns.column_name
        )
        order by table_schema, table_name
    )
    Loop
        RAISE NOTICE 'ALTER TABLE %.% ADD CONSTRAINT fk_%_submenu FOREIGN KEY (submenu_id) REFERENCES submenu(submenu_id);', current_schema_name, current_table_name, current_table_name;   
        EXECUTE 'ALTER TABLE '||current_schema_name||'.'||current_table_name||' ADD CONSTRAINT fk_' || current_table_name || '_submenu FOREIGN KEY (submenu_id) REFERENCES '||current_schema_name||'.submenu(submenu_id);';
    END LOOP;
END $$;