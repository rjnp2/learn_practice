select table_name, column_name, udt_name  from information_schema.columns
where table_schema = 'bti_bma'
and column_name != 'id'
and table_name IN (
   SELECT tb.table_name
	FROM information_schema.tables as tb
	WHERE table_type = 'BASE TABLE'
	and table_name not in ('menu', 'submenu','combined_dataset')
	and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, table_name), false, true, '')))[1]::text::int = 0
)
and column_name not in ('created_at','created_by','created_date','update_time','update_timestamp','updated_at',
    'updated_by','created_by_user_id','created_on','created_timestamp','createdat','update_date',
    'updated_by_user_id','updated_on','updated_timestamp','updatedat')
order by table_name, ordinal_position ;