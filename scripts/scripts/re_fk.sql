DO $$ 
DECLARE 
    table_name text;
    schema_name text;
    constraint_name text;
BEGIN
    FOR schema_name, table_name, constraint_name IN 
        SELECT con.table_schema, con.table_name, con.constraint_name
        FROM information_schema.table_constraints con
        JOIN information_schema.key_column_usage col 
            ON con.constraint_name = col.constraint_name
        WHERE con.constraint_type = 'FOREIGN KEY'
          AND con.table_schema = 'fsh_fms'
          AND col.column_name = 'submenu_id'
    loop
	    table_name := schema_name || '.' || table_name;
        begin
	       EXECUTE 'ALTER TABLE ' || table_name || ' DROP CONSTRAINT ' || constraint_name;
	    EXCEPTION
            WHEN others then
        END;
        raise notice '% %', table_name, constraint_name;
    END LOOP;
END $$;
