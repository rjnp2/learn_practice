SELECT tc.table_schema, kcu.table_name, (ARRAY_AGG(kcu.column_name)) AS primary_key_columns
FROM information_schema.table_constraints tc
JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
WHERE tc.constraint_type = 'PRIMARY KEY'
AND tc.table_schema NOT IN ('information_schema', 'pg_catalog', 'public')
AND kcu.column_name != 'id'
AND kcu.table_name NOT IN ('menu', 'submenu')
AND kcu.table_name NOT IN (
    SELECT relname FROM pg_stat_all_tables 
    WHERE n_live_tup > 0
    AND schemaname = tc.table_schema
)
GROUP BY tc.table_schema, kcu.table_name
HAVING COUNT(kcu.column_name) = 1
ORDER BY tc.table_schema, kcu.table_name;