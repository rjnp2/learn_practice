DO $$ 
DECLARE
    v_schema_name text;
    menu text;
    menu_record RECORD;
    column_names text;
    v_column_name text;
BEGIN
    FOR v_schema_name IN (
        SELECT schema_name FROM information_schema.schemata
        WHERE schema_name NOT IN ('information_schema', 'pg_catalog', 'public', 'pg_toast')
        AND schema_name LIKE 'aoi\_%'
        ORDER BY schema_name
    )
    LOOP
        menu := v_schema_name || '.menu';
        column_names := ''; 
       
        RAISE NOTICE 'schema name: %', v_schema_name;
        FOR v_column_name IN (
            SELECT column_name 
            FROM information_schema.columns 
            WHERE table_schema = v_schema_name 
            AND table_name = 'menu'
            ORDER BY ordinal_position
        )
        LOOP
            column_names := column_names || v_column_name || ', ';
        END LOOP;        
        column_names := LEFT(column_names, LENGTH(column_names) - 2);
        RAISE NOTICE 'Column Names: %', column_names;

        RAISE NOTICE 'Menu records:';
        FOR menu_record IN EXECUTE 'SELECT * FROM ' || menu
        LOOP
            RAISE NOTICE '%', menu_record;
        END LOOP;
    END LOOP;
END $$;
