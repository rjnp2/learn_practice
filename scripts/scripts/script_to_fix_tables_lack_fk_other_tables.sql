-- Script to add submenu_id column
DO $$
DECLARE
    current_table_name text;
    current_schema_name text;
    current_column_name text;
    foreign_table_name text;
    plural_foreign_table_name text;
    primary_name text;
	query_text text;
	column_exists boolean;
BEGIN
    FOR current_schema_name, current_table_name, current_column_name IN (
        SELECT table_schema, table_name, column_name from information_schema.columns
		where column_name LIKE '%_id' 
		and column_name not in('menu_id','submenu_id')
		and table_name not like '%_dataset'
		and udt_name like 'int%'
		and table_schema not in ('information_schema', 'pg_catalog', 'public')
		AND NOT EXISTS (
			SELECT 1
			FROM information_schema.table_constraints AS tc
			JOIN information_schema.key_column_usage AS kcu
			ON tc.constraint_name = kcu.constraint_name
			WHERE tc.constraint_type in ('PRIMARY KEY', 'FOREIGN KEY')
			AND kcu.table_name = information_schema.columns.table_name
			and tc.table_schema = information_schema.columns.table_schema 
			AND kcu.column_name = information_schema.columns.column_name
			)
		and table_name not in (
			SELECT relname FROM pg_stat_all_tables where n_live_tup > 0
		)
		order by table_schema, table_name asc
    )
    Loop
		foreign_table_name := REPLACE(LOWER(current_column_name), '_id', '');
		plural_foreign_table_name := foreign_table_name || 's';

		query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name IN ('''||foreign_table_name||''', '''||plural_foreign_table_name||''')
            AND table_schema = ''' || current_schema_name || ''';';
		
		EXECUTE query_text INTO column_exists;
        if column_exists then
			query_text := 'SELECT kcu.column_name, tc.table_name
				FROM information_schema.table_constraints tc
				JOIN information_schema.key_column_usage kcu
				ON tc.constraint_name = kcu.constraint_name
				WHERE tc.table_schema = '''||current_schema_name||'''
				AND tc.constraint_type = ''PRIMARY KEY''
				AND tc.table_name IN ('''||foreign_table_name||''', '''||plural_foreign_table_name||''');';

			EXECUTE query_text INTO primary_name, foreign_table_name;
			raise notice '% % %', primary_name, foreign_table_name, query_text;
			BEGIN
				EXECUTE 'ALTER TABLE '||current_schema_name||'.'||current_table_name||' ADD CONSTRAINT fk_' || current_table_name || '_' || foreign_table_name || '_id FOREIGN KEY ('||current_column_name||') REFERENCES '||current_schema_name||'.'||foreign_table_name||'('||primary_name||');';
				RAISE NOTICE 'Table found of % % %', current_schema_name, current_table_name, current_column_name;
			EXCEPTION
				WHEN others then
					RAISE NOTICE 'Error occur in Table % % %', current_schema_name, current_table_name, current_column_name;
			END;
		else
			RAISE NOTICE 'Table not found of % % %', current_schema_name, current_table_name, current_column_name;
        end if;
    END LOOP;
END $$;

				
			
			