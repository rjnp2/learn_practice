DO $$
DECLARE
    v_schema_name text;
    submenu_table_name text;
    query_text text;
    result_record record;
    table_name text;
    column_exists text;
BEGIN
 FOR v_schema_name IN (
   SELECT schema_name FROM information_schema.schemata
   where schema_name not in  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
   and schema_name not like '%temp%'
   order by schema_name
  )
  loop
     submenu_table_name := v_schema_name || '.submenu';
     query_text := 'SELECT * FROM ' || submenu_table_name ||' where menu_id = 100';
    
     FOR result_record IN EXECUTE query_text
     loop
	     query_text = 'SELECT table_name from information_schema.columns
				where column_name = ''submenu_id'' 
				and table_schema = '''|| v_schema_name || '''
				and column_default = '''|| result_record.submenu_id || '''';
		 execute query_text into table_name;
		 
		 table_name := (v_schema_name || '.' || table_name)::regclass::oid;
		 query_text := 'SELECT pg_description.description
	        FROM pg_description
	        WHERE pg_description.objoid = ('''|| table_name  || ''')
	        and objsubid = 0
	        LIMIT 1';
	     execute query_text into table_name;
		 execute 'update '|| submenu_table_name ||' set submenu_description = '''|| table_name ||''' where submenu_id = '''|| result_record.submenu_id ||'''';
	END LOOP;
  END LOOP;
END $$;

