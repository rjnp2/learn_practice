SELECT table_schema, table_name
FROM information_schema.tables as tb
WHERE table_type = 'BASE TABLE'
and table_name not in ('menu', 'submenu')
and table_schema not in ('information_schema', 'pg_catalog', 'public')
and table_name in (
	select table_name from information_schema.columns
	where column_name ILIKE '%username%' OR column_name ILIKE '%email%'
)
and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, table_name), false, true, '')))[1]::text::int = 0
order by table_schema,table_name;
