DO $$ 
DECLARE
    old_schema_name text;
    new_schema_name text;
BEGIN
    FOR old_schema_name IN (
        SELECT schema_name FROM information_schema.schemata
        where schema_name not in ('information_schema', 'pg_catalog', 'public', 'pg_toast')
        and schema_name not LIKE 'agr\_%'
        order by schema_name
    )
    loop
        new_schema_name := 'agr_' || old_schema_name;
        EXECUTE 'ALTER SCHEMA ' || old_schema_name || ' RENAME TO ' || new_schema_name;
        raise notice 'alter schema name of % to %', old_schema_name, new_schema_name;
    END LOOP;
END $$;
