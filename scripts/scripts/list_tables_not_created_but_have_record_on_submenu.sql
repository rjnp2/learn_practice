DO $$
DECLARE
    v_schema_name text;
    submenu_table_name text;
    query_text text;
    result_record record;
    submenu_id text;
    column_exists boolean;
	num int:= 0;
begin
	RAISE NOTICE '| S.No | Schema Name | Submenu Name | Submenu id |';
    FOR v_schema_name IN (
        SELECT schema_name FROM information_schema.schemata
        where schema_name not in  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
        order by schema_name
    )
    loop
        submenu_table_name := v_schema_name || '.submenu';
        query_text := 'SELECT * FROM ' || submenu_table_name;

        FOR result_record IN EXECUTE query_text loop
            submenu_id := result_record.submenu_id;
            column_exists := EXISTS (
	            SELECT *
	            FROM information_schema.columns
	            WHERE table_schema = v_schema_name
	            AND column_name = 'submenu_id'
	            and column_default = submenu_id
            );
            IF not column_exists then
            	num := num + 1;
                RAISE NOTICE '| % | % | % | % |', num, v_schema_name, result_record.submenu_name, submenu_id;
            END IF;
        END LOOP;
    END LOOP;
END $$;