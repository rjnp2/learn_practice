DO $$
DECLARE
    v_schema_name text;
   	num int:= 0;
    submenu_table_name text;
    column_exist boolean;
    query_text text;
    result_record record;
    submenu_id text;
begin
	RAISE NOTICE '| S.No | Schema Name | submenu_name |';
    FOR v_schema_name IN (
        SELECT schema_name FROM information_schema.schemata
        WHERE schema_name NOT IN  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
        ORDER BY schema_name
    )
    loop
        submenu_table_name := v_schema_name || '.submenu';
        query_text := 'SELECT * FROM ' || submenu_table_name;

        FOR result_record IN EXECUTE query_text 
        LOOP
            submenu_id := result_record.submenu_id;
            query_text := 'SELECT 1 FROM information_schema.columns
                WHERE table_schema = $1
                AND column_name = $2
                AND column_default = $3
                having count(table_name) > 1;';
            num := num + 1;
            Execute query_text USING v_schema_name, 'submenu_id', submenu_id into column_exist;
            if column_exist then
           		RAISE NOTICE '| % | % |', v_schema_name, result_record.submenu_name;
           	end if;
        END LOOP;
    END LOOP;
END $$;
