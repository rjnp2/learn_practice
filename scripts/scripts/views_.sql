DO $$
DECLARE
    view_name RECORD;
    query_text text;
BEGIN
    FOR view_name IN
        SELECT table_schema, table_name
        FROM information_schema.views
        WHERE table_name ilike 'form_%'
    LOOP
        query_text := 'DROP VIEW IF EXISTS ' || quote_ident(view_name.table_schema) ||'.'|| quote_ident(view_name.table_name) || ' CASCADE';
        raise notice '%', query_text;
        EXECUTE query_text;
    END LOOP;
END $$;