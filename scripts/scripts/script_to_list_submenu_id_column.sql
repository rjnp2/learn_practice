-- Script to add submenu_id column
DO $$
DECLARE
    current_table_name text;
    current_schema_name text;
BEGIN
    -- Add the submenu_id column to tables that meet the specified conditions
    FOR current_schema_name, current_table_name IN (
        SELECT tb.table_schema, tb.table_name
        FROM information_schema.tables tb
        WHERE tb.table_type = 'BASE TABLE'
        and tb.table_name not in ('menu', 'submenu')
        and tb.table_schema not in ('information_schema', 'pg_catalog', 'public')
        and tb.table_name not IN (
            SELECT table_name
            FROM information_schema.columns
            WHERE column_name = 'submenu_id'
            and table_schema = tb.table_schema
        )
        and table_name not in ( SELECT relname FROM pg_stat_all_tables where n_live_tup > 0
        )
        order by table_schema, table_name
    )
    Loop
        RAISE NOTICE 'ALTER TABLE %.% ADD COLUMN submenu_id INTEGER;', current_schema_name, current_table_name;
        EXECUTE 'ALTER TABLE '||current_schema_name||'.'||current_table_name||' ADD COLUMN submenu_id INTEGER;';
        RAISE NOTICE 'ALTER TABLE %.% ADD CONSTRAINT fk_%_submenu FOREIGN KEY (submenu_id) REFERENCES submenu(submenu_id);', current_schema_name, current_table_name, current_table_name;      
        EXECUTE 'ALTER TABLE '||current_schema_name||'.'||current_table_name||' ADD CONSTRAINT fk_' || current_table_name || '_submenu FOREIGN KEY (submenu_id) REFERENCES '||current_schema_name||'.submenu(submenu_id);';
    END LOOP;
END $$;