DO $$
DECLARE
    current_table_name text;
    current_schema_name text;
    submenu_table_name text;
    submenu_id integer;
    submenu_name text;
    submenu_code text;
    menu_id integer = 100;
    menu_code text = 'BTI-AGY-XYZ';
    column_exists boolean;
    char_set TEXT = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    query_text TEXT;
BEGIN
    -- Add the submenu_id column to tables that meet the specified conditions
    for current_schema_name, current_table_name IN (
        SELECT table_schema, table_name
        FROM information_schema.columns
        WHERE column_name = 'submenu_id'
        and column_default is null
        and table_name NOT IN ('menu', 'submenu')
        and table_schema not in ('information_schema', 'pg_catalog', 'public')
        order by table_schema, table_name
    )
    loop
	    submenu_name := TRIM(current_table_name);
        submenu_name := REPLACE(INITCAP(submenu_name), '_', ' ');
        submenu_table_name := current_schema_name || '.submenu';
        current_table_name := current_schema_name || '.' || current_table_name;

        query_text := 'SELECT 1 FROM '||submenu_table_name||' where menu_id = '||menu_id||';';

        EXECUTE query_text INTO column_exists;
        raise notice '%', column_exists;
        if column_exists then
            query_text := 'SELECT Max(submenu_id) FROM '||submenu_table_name||' where menu_id = '||menu_id||';';
            EXECUTE query_text INTO submenu_id;
            submenu_id := submenu_id + 1;
        else
            submenu_id := 10001;

        end if;
        submenu_code := menu_code || '-' || SUBSTRING(char_set FROM ((submenu_id/1296) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM ((submenu_id/36) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM (submenu_id % LENGTH(char_set) + 1) FOR 1);
        RAISE NOTICE '% % % % % %', current_schema_name, submenu_id, submenu_name, menu_id, submenu_name, submenu_code;   

        query_text := 'INSERT INTO ' || submenu_table_name || ' (submenu_id, submenu_name, menu_id, submenu_description, submenu_code) VALUES ($1, $2, $3, $4, $5)';
        EXECUTE query_text USING submenu_id, submenu_name, menu_id, submenu_name, submenu_code;

        EXECUTE 'ALTER TABLE '||current_table_name||' ALTER COLUMN submenu_id SET DEFAULT '||submenu_id||';';
    END LOOP;
END $$;