SELECT schema_name FROM information_schema.schemata
where schema_name not in ('information_schema', 'pg_catalog', 'public', 'pg_toast')
and schema_name not LIKE 'agr\_%'
order by schema_name;