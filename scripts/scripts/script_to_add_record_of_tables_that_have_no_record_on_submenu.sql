DO $$
DECLARE
  v_schema_name text;
  submenu_table_name text;
  menu_id integer;
  query_text text;
  result_record record;
  submenu_id integer;
  submenu_name text;
  v_table_name text;
  column_exists text;
  menu_code text;
  menu_table_name text;
  char_set TEXT = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  code_char text;
BEGIN
 FOR v_schema_name IN (
   SELECT schema_name FROM information_schema.schemata
   where schema_name not in  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
   order by schema_name
  )
  loop
     submenu_table_name := v_schema_name || '.submenu';
     menu_table_name := v_schema_name || '.menu';

     query_text := 'SELECT table_name, column_default 
       FROM information_schema.columns
       WHERE column_name = ''submenu_id''
       AND table_name NOT IN (''menu'', ''submenu'')
       AND table_schema = ''' || v_schema_name || '''
	   AND column_default IS NOT null
	   order by table_name';
    
     FOR result_record IN EXECUTE query_text
     loop
	     submenu_id := result_record.column_default;
         v_table_name := v_schema_name || '.' || result_record.table_name;
         submenu_name := TRIM(result_record.table_name);
         submenu_name := REPLACE(INITCAP(submenu_name), '_', ' ');

	     query_text := 'SELECT 1
	          FROM '|| submenu_table_name ||'
	          WHERE submenu_id = ''' || submenu_id|| '''';
	         
	     EXECUTE query_text INTO column_exists;
	     if column_exists is NULL then
	     	  menu_id := LEFT(submenu_id::TEXT, LENGTH(submenu_id::TEXT) - 2);
          submenu_id := submenu_id::integer;
        	query_text := 'SELECT 1 FROM '||menu_table_name||' where menu_id = '||menu_id||';';

          EXECUTE query_text INTO column_exists;
	        if column_exists then
            query_text := 'SELECT menu_code FROM '||menu_table_name||' where menu_id = '||menu_id||';';
            EXECUTE query_text INTO menu_code;
           
            code_char := menu_code || '-' || SUBSTRING(char_set FROM ((submenu_id/1296) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM ((submenu_id/36) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM (submenu_id % LENGTH(char_set) + 1) FOR 1);
            
            query_text := 'INSERT INTO ' || submenu_table_name || ' (submenu_id, submenu_name, menu_id, submenu_description, submenu_code) VALUES ($1, $2, $3, $4, $5)';
            RAISE NOTICE 'Added records % %', v_schema_name, submenu_id;
            EXECUTE query_text USING submenu_id, submenu_name, menu_id, submenu_name, code_char;
          else
          	EXECUTE 'ALTER TABLE '||v_table_name||' ALTER COLUMN submenu_id DROP DEFAULT;';
            RAISE NOTICE 'Reset default value % % %', v_schema_name, submenu_id, v_table_name;
          end if;
	     end if;
	    
     end loop;
  END LOOP;
END $$;