DO $$
DECLARE
    result_record record;
    query_text text;
BEGIN
    for result_record IN (
       SELECT
            id,
            (
                SELECT
                    jsonb_agg(jsonb_build_object('id',mod_option->>'id', 'name', mod_option->>'field_value'))
                FROM
                    jsonb_array_elements(options) AS mod_option
            ) AS transformed_options
        FROM
            sch_202401224fb5a50.custom_attributes
        WHERE
            (
                SELECT
                    jsonb_agg(jsonb_build_object('name', mod_option->>'field_value'))
                FROM
                    jsonb_array_elements(options) AS mod_option
                WHERE
                    mod_option->>'field_value' IS NOT NULL
            ) IS NOT NULL
    )
    loop
        query_text :=  'update sch_202401224fb5a50.custom_attributes set options = $1 where id =$2';
        Execute query_text USING result_record.transformed_options, result_record.id;
        raise notice '%', query_text;
    END LOOP;
END $$;