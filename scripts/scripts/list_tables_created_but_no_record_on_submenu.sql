DO $$
DECLARE
    v_schema_name text;
    submenu_table_name text;
    query_text text;
    result_record record;
    submenu_id text;
    column_exists text;
BEGIN
 FOR v_schema_name IN (
   SELECT schema_name FROM information_schema.schemata
   where schema_name not in  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
   order by schema_name
  )
  loop
     submenu_table_name := v_schema_name || '.submenu';
     query_text := 'SELECT table_name, column_default 
       FROM information_schema.columns
       WHERE column_name = ''submenu_id''
       AND table_name NOT IN (''menu'', ''submenu'')
       AND table_schema = ''' || v_schema_name || '''
		AND column_default IS NOT null
		order by table_name';
    
     FOR result_record IN EXECUTE query_text
     loop
	      submenu_id := result_record.column_default;
	     if submenu_id is NULL then
	    	RAISE NOTICE 'schema name: %, table name: %, default value: %', v_schema_name, result_record.table_name, submenu_id;
	    else
	      query_text := 'SELECT 1
	          FROM '|| submenu_table_name ||'
	          WHERE submenu_id = ''' || submenu_id|| '''';
	     EXECUTE query_text INTO column_exists;
	     if column_exists is NULL then
	    	RAISE NOTICE 'schema name: %, table name: %, default value: %', v_schema_name, result_record.table_name, submenu_id;
	     end if;
	    end if;
     end loop;
  END LOOP;
END $$;