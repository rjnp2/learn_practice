DROP TYPE IF EXISTS address cascade;
DROP VIEW IF EXISTS form_1184 cascade;

CREATE TYPE address AS (
	id integer,
    "88" date,
    "89" TEXT,
    "90" TEXT
);

create or replace view sch_20230822938eebb.form_1184 as 
select 
(jsonb_populate_record).id,
(jsonb_populate_record)."88",
(jsonb_populate_record)."89",
(jsonb_populate_record)."90"
from 
( 
	SELECT
	    jsonb_populate_record(
	        null::address,
	        (SELECT 
	            (SELECT jsonb_object_agg(jdata->>'id', jdata->'value')
	            FROM jsonb_array_elements(t.fields) AS jdata
	            ) || jsonb_build_object('id', id) as aggrs
	    )
	)
	FROM sch_20230822938eebb.inspection t
	WHERE template_id = '1184'
);
  