DO $$
DECLARE
    v_schema_name text;
   	num int:= 0;
    submenu_table_name text;
    v_table_record record;
    v_submenu_name text;
    v_table_name text;
    query_text text;
    result_record record;
    submenu_id text;
begin
	RAISE NOTICE '| S.No | Schema Name | Submenu Name | Table Name |';
    FOR v_schema_name IN (
        SELECT schema_name FROM information_schema.schemata
        WHERE schema_name NOT IN  ('information_schema', 'pg_catalog', 'public', 'pg_toast')
        ORDER BY schema_name
    )
    loop
        submenu_table_name := v_schema_name || '.submenu';
        query_text := 'SELECT * FROM ' || submenu_table_name;

        FOR result_record IN EXECUTE query_text 
        LOOP
            submenu_id := result_record.submenu_id;
            query_text := 'SELECT table_name FROM information_schema.columns
                WHERE table_schema = $1
                AND column_name = $2
                AND column_default = $3;';
           
            FOR v_table_record IN EXECUTE query_text USING v_schema_name, 'submenu_id', submenu_id
            LOOP
                v_submenu_name := TRIM(result_record.submenu_name);
                v_submenu_name := REPLACE(LOWER(v_submenu_name), '-', '_');
                v_submenu_name := REPLACE(v_submenu_name, ' & ', ' ');
                v_submenu_name := REPLACE(v_submenu_name, ' and ', ' ');
                v_submenu_name := REPLACE(v_submenu_name, ' ', '_');
                v_submenu_name := REPLACE(v_submenu_name, '_', '');

                v_table_name := TRIM(v_table_record.table_name);
                v_table_name := REPLACE(LOWER(v_table_name), '-', '_');
                v_table_name := REPLACE(v_table_name, '&', ' ');
                v_table_name := REPLACE(v_table_name, 'and', ' ');
                v_table_name := REPLACE(v_table_name, ' ', '_');
                v_table_name := REPLACE(v_table_name, '_', '');
               
                IF position(v_submenu_name IN v_table_name) = 0
                then
                	num := num + 1;
                    RAISE NOTICE '| % | % | % | % |', num, v_schema_name, result_record.submenu_name, v_table_record.table_name;                
                END IF;
            END LOOP; 
        END LOOP;
    END LOOP;
END $$;
