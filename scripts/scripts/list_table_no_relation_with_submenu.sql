SELECT table_schema, table_name from information_schema.columns
where column_name = 'submenu_id'
and table_name not in ('menu', 'submenu')
and table_schema not in ('information_schema', 'pg_catalog', 'public')
and table_schema != 'public'
AND NOT EXISTS (
	SELECT 1
	FROM information_schema.table_constraints AS tc
	JOIN information_schema.key_column_usage AS kcu
	ON tc.constraint_name = kcu.constraint_name
	WHERE tc.constraint_type = 'FOREIGN KEY'
	AND kcu.table_name = information_schema.columns.table_name
	AND kcu.column_name = information_schema.columns.column_name
	)
order by table_schema, table_name;