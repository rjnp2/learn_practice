-- change column name of name/title to menu_name, id to menu_id 
-- add new column if not exists
-- set system_code if null
-- set menu_code if null
-- create miscellaneous if not exists
DO $$
DECLARE
    v_schema_name text;
    mod_schema_name text;
   	v_menu_code text;
    query_text text;
    menu_table_name text;
    result_record record;
    column_exists boolean;
    char_set TEXT = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    code_char text;
begin
	FOR v_schema_name IN (
        SELECT schema_name FROM information_schema.schemata
        where schema_name not in ('information_schema', 'pg_catalog', 'public', 'pg_toast')
        and schema_name not like '%_temp_%'
        order by schema_name
	)
	loop
        mod_schema_name := REPLACE(UPPER(v_schema_name), '_', '-');
        raise notice 'Starting to work on the % schema.', mod_schema_name;
        menu_table_name := v_schema_name || '.menu';

        -- change column name of id to menu_id if exists
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''id'';';
            
        EXECUTE query_text INTO column_exists;
        if column_exists then
            raise notice 'Changing column name of id to menu_id';
            query_text := 'ALTER TABLE '||menu_table_name||' RENAME COLUMN "id" TO menu_id;';
            EXECUTE query_text;
        end if;
        ------------------

        -- change column name of name to menu_name if exists
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''name'';';
            
        EXECUTE query_text INTO column_exists;
        if column_exists then
            raise notice 'Changing column name of name to menu_name';
            query_text := 'ALTER TABLE '||menu_table_name||' RENAME COLUMN "name" TO menu_name;';
            EXECUTE query_text;
        end if;
        ---------------------

        -- change column name of label to menu_name if exists
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''label'';';
            
        EXECUTE query_text INTO column_exists;
        if column_exists then
            raise notice 'Changing column name of label to menu_name';
            query_text := 'ALTER TABLE '||menu_table_name||' RENAME COLUMN "label" TO menu_name;';
            EXECUTE query_text;
        end if;
        ---------------------

        -- change column name of title to menu_name if exists
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''title'';';
            
        EXECUTE query_text INTO column_exists;
        if column_exists then
            raise notice 'Changing column name of title to menu_name';
            query_text := 'ALTER TABLE '||menu_table_name||' RENAME COLUMN "title" TO menu_name;';
            EXECUTE query_text;
        end if;
        ---------------------

        -- change column name of description to menu_description if exists
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''description'';';
            
        EXECUTE query_text INTO column_exists;
        if column_exists then
            raise notice 'Changing column name of name to menu_name';
            query_text := 'ALTER TABLE '||menu_table_name||' RENAME COLUMN "description" TO menu_description;';
            EXECUTE query_text;
        end if;
        ---------------------

        -- add new column system_code if not exists and set system_code if null 
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''system_code''';
            
        EXECUTE query_text INTO column_exists;
        if column_exists is null then
            raise notice 'Adding new column called system_code with default value %', mod_schema_name;
            EXECUTE 'ALTER TABLE '||menu_table_name||' ADD COLUMN system_code VARCHAR(75) default '''||mod_schema_name||''';';
        end if;

        query_text := 'SELECT 1
            FROM '||menu_table_name||'
            WHERE system_code IS NULL;';
            
        EXECUTE query_text INTO column_exists;
        if column_exists then
            raise notice 'Updating value of system_code with % value where system_code is null', mod_schema_name;
            query_text := 'UPDATE '||menu_table_name||'
                SET system_code = '''||mod_schema_name||'''';
		    execute query_text;
        end if;
        ---------------------

        -- add new column menu_code if not exists and set menu_code if null 
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''menu_code''';
            
        EXECUTE query_text INTO column_exists;
        if column_exists is null then
            raise notice 'Adding new column called menu_code';
            EXECUTE 'ALTER TABLE '||menu_table_name||' ADD COLUMN menu_code VARCHAR(75);';
        end if;
        
        query_text := 'SELECT 1
            FROM '||menu_table_name||'
            WHERE menu_code is null';
        
        EXECUTE query_text INTO column_exists;
        if column_exists is not null then
            raise notice 'Updating value of menu_code with random value where menu_code is null';
            query_text := 'SELECT menu_id FROM '||menu_table_name||';';
        	FOR result_record IN execute query_text
            loop
        		code_char := mod_schema_name || '-' || SUBSTRING(char_set FROM ((result_record.menu_id/1296) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM ((result_record.menu_id/36) % LENGTH(char_set) + 1) FOR 1) || SUBSTRING(char_set FROM (result_record.menu_id % LENGTH(char_set) + 1) FOR 1);
        		query_text := 'UPDATE '||menu_table_name||' SET menu_code = ''' || code_char ||''' WHERE menu_id = '|| result_record.menu_id || ';';
                raise notice '%', query_text;
                EXECUTE query_text;
            end loop;
        end if;
        ---------------------

        -- add new column menu_is_pinned if not exists
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''menu_is_pinned''';
            
        EXECUTE query_text INTO column_exists;
        if column_exists is null then
            raise notice 'Adding new column called menu_is_pinned';
            EXECUTE 'ALTER TABLE '||menu_table_name||' ADD COLUMN menu_is_pinned boolean default false;';
        end if;
        ---------------------

        -- add new column menu_element if not exists
        query_text := 'SELECT 1
            FROM information_schema.columns
            WHERE table_name = ''menu''
            AND table_schema = ''' || v_schema_name || '''
            and column_name = ''menu_element''';
            
        EXECUTE query_text INTO column_exists;
        if column_exists is null then
            raise notice 'Adding new column called menu_element';
            EXECUTE 'ALTER TABLE '||menu_table_name||' ADD COLUMN menu_element VARCHAR(75) default ''submenu'';';
        end if;
        ---------------------
       
       -- create miscellaneous records if not exists
       query_text := 'SELECT 1
            FROM '||menu_table_name||'
            WHERE menu_name = ''Miscellaneous''';
        
        v_menu_code := mod_schema_name || '-XYZ';
        EXECUTE query_text INTO column_exists;
        if column_exists is null then
            raise notice 'Adding new record with menu_name Miscellaneous';
            EXECUTE 'INSERT INTO ' || menu_table_name || ' (menu_id, menu_name, menu_description, system_code, menu_code) VALUES (''100'', ''Miscellaneous'', ''Miscellaneous menu'', ''' || mod_schema_name || ''', ''' || v_menu_code || ''')';
        end if;
		---------------------

	end loop;
end $$;