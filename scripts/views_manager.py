import random
import string

from sqlalchemy import text

from technical.utility.views_tables import get_column_details


schema_code = "public"


def generate_random_string(length=8, generated_strings=None):
    if generated_strings is None:
        generated_strings = []
    count = 0
    characters = string.ascii_letters
    random_string = "".join(random.choice(characters) for _ in range(length))
    while random_string in generated_strings:
        random_string = "".join(random.choice(characters) for _ in range(length))
        count += 1
        if count > 20:
            count = 0
            length += 1
    return random_string


def sql_create_global_view(schema_name):
    query_text = f"""
    DROP VIEW IF EXISTS {schema_name}.form_global cascade;

    create or replace view {schema_name}.form_global as 
    select
    id::text,
    name,
    jsonb_array_elements(options)->>'id'::text AS option_id,
    jsonb_array_elements(options)->>'name' AS option_name,
    false::boolean as deleted
    FROM
    {schema_code}.custom_attributes;
    """
    return text(query_text)


def sql_create_user_view(schema_name):
    query_text = f"""
    DROP VIEW IF EXISTS {schema_name}.form_organization_user cascade;

    create or replace view {schema_name}.form_organization_user as 
    select distinct ou.id, ou.user_id, u.full_name
    from {schema_code}.organization__user ou 
    join public."user" u 
    on ou.user_id = u.id;

    DROP VIEW IF EXISTS {schema_name}.form_department cascade;

    create or replace view {schema_name}.form_department as 
    select d.id, d.name
    from {schema_code}.department d;
    """
    return text(query_text)


def sql_combine_view(db, schema_name, field, view_id):
    # schema_name = schema_code
    view_name = f"package_{view_id}"
    final_raw_query = []

    join_query = ""
    selected_fields = []
    other_selected_fields = []
    raw_fields = set()
    raw_form = set()
    raw_table = set()
    generated_strings = set()

    field = sorted(
        field,
        key=lambda x: len(x.get("joiner", [])) if x.get("joiner") is not None else 0,
    )
    field = [i for i in field if i["fields"]]
    is_first = True

    for data in field:
        form_id = data["form_id"]

        if form_id not in ["form_department", "form_organization_user", "form_global"]:
            tabl_name = f"jt_{form_id}_"
            form_id = f"table_{form_id}"
            original_form_id = form_id
            query_text = get_column_details(form_name=form_id, schema_name=schema_name)
            old_column_data = db.execute(query_text).fetchall()
            final_data = []
            junction_query = []

            for i in old_column_data:
                if i[0] not in data["fields"]:
                    continue

                if i[0] == "created_by":
                    i = "us.full_name as created_by"
                    junction_query.append(
                        f"left join public.user us on {original_form_id}.created_by = us.id"
                    )

                elif i[0] == "updated_by":
                    i = "us2.full_name as updated_by"
                    junction_query.append(
                        f"left join public.user us2 on {original_form_id}.updated_by = us2.id"
                    )

                elif i[3] == "_text":
                    i = f'unnest({original_form_id}."{i[0]}") as "{i[0]}"'

                else:
                    i = f'{original_form_id}."{i[0]}" as "{i[0]}"'

                final_data.append(i)

            raw_query = f"""SELECT table_name
                FROM information_schema.tables
                WHERE table_name ILIKE '{tabl_name}%'
                and table_schema = '{schema_name}';
            """

            for i in db.execute(raw_query).fetchall():
                jt_org_name = i[0]
                fiel_name = i[0].replace(tabl_name, "")
                i = f'{jt_org_name}.table_1 as "{fiel_name}"'

                if fiel_name not in data["fields"]:
                    continue

                final_data.append(i)
                junction_query.append(
                    f"left join {schema_name}.{jt_org_name} {jt_org_name} on {original_form_id}.id = {jt_org_name}.table_1"
                )

            final_data = ", ".join(final_data)
            junction_query = "\n ".join(junction_query)
            if is_first:
                where_query = f"where deleted=false and is_draft=false"
            else:
                where_query = ""

            form_id = f"temp_{form_id}"
            if form_id in raw_table:
                random_string = generate_random_string(
                    generated_strings=generated_strings
                )
                generated_strings.add(random_string)
                form_id = f"{random_string}_{form_id}"
            raw_table.add(form_id)

            final_raw_query.append(
                f"""{form_id} AS (
                SELECT {final_data}
                FROM {schema_name}.{original_form_id}
                {junction_query}
                {where_query})"""
            )
        else:
            original_form_id = form_id

        is_first = False

        if original_form_id in raw_form:
            random_string = generate_random_string(generated_strings=generated_strings)
            generated_strings.add(random_string)
            form_alias = f"v_{random_string}___{original_form_id}"

        else:
            form_alias = f"v_{original_form_id}"

        raw_form.add(original_form_id)

        for data_field in data["fields"]:

            joiner = data["joiner"]
            from_ = joiner.get("from", "0")
            alias = f"{original_form_id}__{from_}__{data_field}"
            if alias in raw_fields:
                random_string = generate_random_string(
                    generated_strings=generated_strings
                )
                generated_strings.add(random_string)
                alias = f"{random_string}__{alias}"

            if alias.startswith("table_"):
                alias = "form_" + alias[len("table_") :]
            raw_fields.add(alias)

            if data_field in [
                "created_by",
                "updated_by",
                "created_at",
                "updated_at",
            ]:
                other_selected_fields.append(f'{form_alias}."{data_field}" as {alias}')
            else:
                selected_fields.append(f'{form_alias}."{data_field}" as {alias}')

        joiner = data["joiner"]
        if not joiner:
            join_query += f"{form_id} {form_alias}"
            continue

        to = joiner["to"]
        if to.isdigit():
            to = f'{form_alias}."{to}"'
        else:
            to = f"{form_alias}.{to}"

        nested_form_id = joiner["form_id"]

        if nested_form_id not in [
            "form_department",
            "form_organization_user",
            "form_global",
        ]:
            nested_form_id = f"v_table_{nested_form_id}"
            table_details_data = f"{form_id}"

        else:
            table_details_data = f" {schema_name}.{form_id}"

        from_ = joiner["from"]
        if from_.isdigit():
            from_ = f'{nested_form_id}."{from_}"'
        else:
            from_ = f"{nested_form_id}.{from_}"

        if form_id == "form_global":
            type_ = str(joiner["type"])
            to += f" and {form_alias}.id = '{type_}'"

        join_query += f""" left outer join {table_details_data} {form_alias}
        on {from_} = {to} 
        """

    selected_fields = selected_fields + other_selected_fields
    selected_fields = ", ".join(selected_fields)
    join_query = join_query.strip()

    final_raw_query = ",\n ".join(final_raw_query)
    selected_fields = selected_fields.replace("as table_", "as form_")

    final_raw_query = f"""
    DROP VIEW IF EXISTS public.{view_name} cascade;
        create or replace view public.{view_name}
        AS WITH
        {final_raw_query}
        select {selected_fields} from 
        {join_query};
    """

    return text(final_raw_query)
