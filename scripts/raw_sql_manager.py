from sqlalchemy import text

from technical.configurations.exceptions import BadRequest_400
from technical.utility.views_tables import get_column_data


def create_raw_query(db, chart_obj, schema_name, global_filters=[]):
    package_id = chart_obj.package_id
    field_data = chart_obj.field_data
    chart_type = chart_obj.chart_type.lower()
    filters = field_data.get("filters", []) or []
    limit = field_data.get("limit", 5) or 5
    try:
        limit = int(limit)
    except:
        limit = 5

    package_id = f"package_{package_id}"
    raw_query = get_column_data(table_name=package_id, schema_name=schema_name)
    data = db.execute(raw_query).fetchall()
    if not data:
        raise BadRequest_400(
            message="Package views was not created properly, please update or save this package."
        )

    data = [i[0] for i in data]
    for global_filter in global_filters:
        operand = global_filter.get("operand")
        if not operand:
            raise BadRequest_400(message="Operand is missing in global_filters")

        operator = global_filter.get("operator")
        if not operator:
            raise BadRequest_400(message="operator is missing in global_filters")

        value = global_filter.get("value")
        if not value:
            raise BadRequest_400(message="value is missing in global_filters")

        operand = operand.lower().strip()
        status = global_filter.get("status", "").lower().strip()

        if status == "global" and operand == "created_at":
            created_at_columns = [i for i in data if "created_at" in i]
            if created_at_columns:
                filters.append(
                    {
                        "operand": f"DATE({created_at_columns[0]})",
                        "value": value,
                        "operator": operator,
                    }
                )

        elif operand in data:
            filters.append(global_filter)
        else:
            return "no_filter"

    condition_data = set()
    for filter_ in filters:
        operand = filter_["operand"].lower().strip()
        operator = filter_["operator"].lower().strip()
        value = filter_["value"]

        if operator.lower() == "in":
            if type(value) != list:
                raise BadRequest_400(message='Value must be in list for "In" operator.')

            value_ = []
            for i in value:
                if isinstance(i, str):
                    i = f"'{i}'"
                else:
                    i = str(i)
                value_.append(i)

            value = ",".join(value_)
            condi = f"{operand} in ({value})"

        elif operator.lower() == "not_in":
            if type(value) != list:
                raise BadRequest_400(message='Value must be in list for "In" operator.')

            value_ = []
            for i in value:
                if isinstance(i, str):
                    i = f"'{i}'"
                else:
                    i = str(i)
                value_.append(i)

            value = ",".join(value_)
            condi = f"{operand} not in ({value})"

        elif operator.lower() == "like":
            condi = f"{operand} like '%{value}%'"

        elif operator.lower() == "not_like":
            condi = f"{operand} not like '%{value}%'"

        elif operator.lower() == "=":
            if isinstance(value, str):
                value = f"'{value}'"

            condi = f"{operand} = {value}"

        elif operator.lower() == "!=":
            if isinstance(value, str):
                value = f"'{value}'"
            condi = f"{operand} != {value}"

        elif operator.lower() == ">":
            if isinstance(value, str):
                value = f"'{value}'"
            condi = f"{operand} > {value}"

        elif operator.lower() == "<":
            if isinstance(value, str):
                value = f"'{value}'"
            condi = f"{operand} < {value}"

        elif operator.lower() == ">=":
            if isinstance(value, str):
                value = f"'{value}'"
            condi = f"{operand} >= {value}"

        elif operator.lower() == "<=":
            if isinstance(value, str):
                value = f"'{value}'"
            condi = f"{operand} <= {value}"

        else:
            continue
        condition_data.add(condi)

    if condition_data:
        condi = "\n and ".join(condition_data)
        condi = condi.strip()
        condi = "where " + condi.strip()

    else:
        condi = ""

    package_id = f"{schema_name}.{package_id}"
    if chart_type == "scorecard":
        operation = field_data.get("operation", "count") or "count"
        if hasattr(operation, "value"):
            operation = operation.value

        x_axis = field_data.get("x_axis", ["id"])[0]
        query_text = f"""
            select {operation}({x_axis}) as value 
            from {package_id}
            {condi}
        """

    elif chart_type == "table":
        x_axis = field_data.get(
            "x_axis",
            [
                "id",
            ],
        )
        order = field_data.get("sorting", x_axis[0]) or x_axis[0]
        x_axis = ", \n".join(set(x_axis))

        query_text = f"""
            select {x_axis} 
            from {package_id}
            {condi}
            order by {order}
            limit {limit};
        """

    elif chart_type in (
        "piechart",
        "doughnut",
        "donutpluspie",
        "semidonut",
        "barchart",
        "columnchart",
        "rotatedbarchart",
        "radialbarchart",
        "stackedcolumn",
        "negativebarchart",
        "multidata",
        "treemap",
        "funnel",
    ):
        operation = field_data.get("operation", "sum") or "sum"
        if hasattr(operation, "value"):
            operation = operation.value

        x_axis = field_data.get("x_axis", ["id"])[0]

        y_axis = set(field_data.get("y_axis", ["id"]))
        y_axis = [f"{operation}({i}) as {i}" for i in y_axis]
        y_axis = ", \n".join(y_axis)

        query_text = f"""
            select {x_axis}, 
            {y_axis} 
            from {package_id}
            {condi}
            group by {x_axis};
        """

    elif chart_type in ("gauge",):
        x_axis = field_data.get("x_axis", ["id"])[0]
        query_text = f"""
            select MIN({x_axis}) as min_value, 
            AVG({x_axis}) as avg_value, MAX({x_axis}) as max_value 
            from {package_id}
            {condi};
        """

    elif chart_type in ("columnrange",):
        x_axis = field_data.get("x_axis", ["id"])[0]
        y_axis = field_data.get("y_axis", ["id"])[0]
        query_text = f"""
            select {x_axis}, 
            MIN({y_axis}) as min_value, 
            AVG({y_axis}) as avg_value, 
            MAX({y_axis}) as max_value 
            from {package_id}
            {condi}
            group by {x_axis}
            order by {x_axis};
        """

    elif chart_type in ("timeseries",):
        interval = field_data.get("interval", "").lower()
        x_axis = field_data.get("x_axis", ["id"])
        group_by = ""
        y_axis = field_data.get("y_axis", ["id"])

        operation = field_data.get("operation", "avg") or "avg"
        if hasattr(operation, "value"):
            operation = operation.value

        date_formatter = {
            "year-wise": "YYYY",
            "month-wise": "YYYY-MM",
            "date-wise": "YYYY-MM-DD",
            "hour-wise": "YYYY-MM-DD-hh24",
            "minute-wise": "YYYY-MM-DD-hh24-mi",
        }

        formater = date_formatter.get(interval)
        if formater:
            group_by = f"GROUP BY TO_CHAR({x_axis[0]}, '{formater}')"
            x_axis = [
                f"TO_CHAR({x_axis[0]}, '{formater}') as {x_axis[0]}",
            ]
            y_axis = [f"{operation}({i}) as {i}" for i in y_axis]

        x_axis = ", \n".join(set(x_axis + y_axis))

        query_text = f"""
            select {x_axis} 
            from {package_id}
            {condi}
            {group_by}
        """

    elif chart_type in ("timeseries",):
        interval = field_data.get("interval", "").lower()
        x_axis = field_data.get("x_axis", ["id"])[0]

        date_formatter = {
            "year-wise": "YYYY",
            "month-wise": "YYYY-MM",
            "date-wise": "YYYY-MM-DD",
            "hour-wise": "YYYY-MM-DD-HH",
            "minute-wise": "YYYY-MM-DD-HH-MM",
        }

        formater = date_formatter.get(interval)
        if formater:
            group_by = f"TO_CHAR({x_axis}, '{formater}')"
            x_axis = (f"TO_CHAR({x_axis}, '{formater}') as {x_axis}",)
        else:
            group_by = x_axis

        x_axis = [
            x_axis,
        ] + field_data.get("y_axis", ["id"])

        x_axis = ",".join(set(x_axis))

        query_text = f"""
            select {x_axis} from {package_id}
            {condi}
            GROUP BY {group_by}
        """

    elif chart_type in (
        "linechart",
        "areachart",
        "spider",
        "smoothlinechart",
        "scatterplot",
        "bubbleplot",
        "bellcurve",
        "heatmap",
    ):
        x_axis = field_data.get("x_axis", ["id"]) + field_data.get("y_axis", ["id"])
        x_axis = ", \n".join(set(x_axis))

        query_text = f"""
            select {x_axis} 
            from {package_id}
            {condi}
        """

    else:
        query_text = f"""
        select * 
        from {package_id}
        where 1 = 0;
        """

    with open("query_text.txt", "w") as f:
        f.write(query_text.strip())
        f.write("\n")
    return text(query_text)
