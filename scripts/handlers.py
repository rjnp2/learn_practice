import base64
import json
import re
import zlib
from datetime import datetime
from math import ceil
from typing import List

from fastapi import Header
from sentry_sdk import capture_exception
from sqlalchemy import func, or_, text
from sqlalchemy.orm import Session

from technical.common.system_control import system_access

from technical.configurations.permission import (
    ValidateFormPermission,
    instance_all_permission,
)
from technical.endpoints.master_services.user import models as user_orm
from technical.endpoints.org_services.form import models as form_orm
from technical.endpoints.org_services.form_access import models as access_orm
from technical.endpoints.org_services.inspection.functions import ENCRYPTION_KEY
from technical.endpoints.org_services.inspection.mapper import (
    CURRENT_USER,
    action_functions,
    constants_dict,
)
from technical.endpoints.org_services.custom_attributes.models import CustomAttributes

from technical.endpoints.org_services.invoice import models as invoice_orm
from technical.endpoints.org_services.relation_attributes.models import (
    RelationAttributes,
)
from technical.utility.subdomain_manager import get_db_domain

from technical.endpoints.org_services.user_log.org_log import OrganizationAuditLog
from technical.endpoints.tenant_services.organization import models as org_orm
from technical.endpoints.tenant_services.organization_user.models import (
    OrganizationUser,
    OrganizationUserRole,
)
from technical.endpoints.tenant_services.user_role.models import UserRole
from technical.imports.handler_import import *
from technical.settings.settings import settings_instance
from technical.utility.views_tables import (
    check_table_exist,
    get_column_data,
    get_column_details,
    get_data_details,
    get_deleted_data_count,
    sql_update_record,
    sql_insert_record_junction,
    sql_delete_record_junction,
)

# Default Imports
from .repository import TemplatesDataSQLRepository
from .schema import *

# Initialize Global Variables
access_model = access_orm.AccessRule
model_user = access_orm.DataAccessUser
router = InferringRouter()
repo = TemplatesDataSQLRepository()
lower = ["template", "user"]
exclude = ["inspection_status", "user"]
headers = {"uid": "Id", "created_at": "Start Date", "updated_at": "Modified Date"}
date_format = "%Y-%m-%dT%H:%M"


schema_public_code = "public"


def raise_invalid_input(message=None):
    if message:
        raise BadRequest_400(message=message)
    raise BadRequest_400(message=ErrorCode.INVALID_INPUT)


# Utility Global Functions
def is_valid_datetime(value):
    try:
        datetime.strptime(value, date_format)
        return True
    except ValueError:
        return False


def is_valid_date(value):
    try:
        datetime.strptime(value, "%Y-%m-%d")
        return True
    except ValueError:
        return False


def input_to_json(input):
    json = {}
    for key, value in input.items():
        nested_keys = key.split(".")
        current_dict = json
        for nested_key in nested_keys[:-1]:
            if nested_key not in current_dict:
                current_dict[nested_key] = {}
            current_dict = current_dict[nested_key]
        current_dict[nested_keys[-1]] = value
    return json


def get_data_list(db, current_user, organization__user_id, system_id):
    organization_user = organization__user_id
    if organization_user:
        system_roles = db.query(UserRole).filter(UserRole.system_id == system_id).all()
        system_roles_ids = [x.id for x in system_roles]
        role = db.query(OrganizationUserRole).filter(
            OrganizationUserRole.org_user_id == organization_user,
            OrganizationUserRole.role_id.in_(system_roles_ids),
        )
        role_id = [role.role_id for role in role.all()]
    else:
        role_id = [0]

    access_objs = (
        db.query(access_model.form_id, access_model.row_id, access_model.id)
        .outerjoin(model_user)
        .filter(access_model.organization_id == current_user.org_id)
        .filter(access_model.deleted == False)
        .where(
            or_(
                access_model.owner == organization_user,
                access_model.data_available_type == 2,
                access_model.groups.in_(role_id),
                model_user.user_id.in_([organization_user]),
            )
        )
        .all()
    )
    form = list({obj[0] for obj in access_objs})
    row = list({obj[1] for obj in access_objs})
    access = list({obj[2] for obj in access_objs})
    return form, row, access


def ValidateAccessRuleData(db, current_user, query, system_id):
    organization__user_id = (
        db.query(OrganizationUser.id)
        .filter(OrganizationUser.user_id == current_user.id)
        .first()
    )
    if organization__user_id:
        form, row, access = get_data_list(
            db, current_user, organization__user_id.id, system_id
        )
    # query = query.filter(
    #     and_(
    #         orm.Inspection.deleted == False,
    #         or_(
    #             orm.Inspection.id.in_(row),
    #             orm.Inspection.user_id == current_user.id,
    #         ),
    #     )
    # )
    query = query
    # for x in query:
    #     x.access_rule = [
    #         y for y in x.access_rule if y.id in access and y.deleted == False
    #     ]
    return query, access


def try_int(value):
    if value != float("-inf"):
        try:
            return int(value)
        except (ValueError, TypeError):
            return value
    return value


def filter_mapper_data(
    form,
    filter_data,
    schema_name,
    all_field_data=[],
):
    comparison_methods = {
        "is-less-than": "<",
        "less than": "<",
        "less than or equal to": "<=",
        "is-less-or-equals-to": "<=",
        "is-equals-to": "=",
        "is-same-as": "=",
        "is-not-equal-to": "!=",
        "is-not-same-as": "!=",
        "greater than": ">",
        "is-greater-than": ">",
        "greater than or equal to": ">=",
        "is-greater-or-equals-to": ">=",
    }
    condition = []
    having = []
    junction = []
    table = f"table_{form.id}"
    for key, value in filter_data.items():
        if key.lower() not in all_field_data:
            continue

        if key.lower() in ["created_by", "updated_by"]:
            key = f"f_{key}"

        key = f'"{key}"'
        if value["method"] == "equal to":
            start = value.get("start")
            if isinstance(start, list):
                value_ = []
                for i in start:
                    i = f"'{i}'"
                    value_.append(i)

                value = ",".join(value_)

                colu_fiel = all_field_data.get(key.replace('"', ""), {})

                if colu_fiel.get("response_choice", "").lower() == "table":
                    condition.append(f"{key} in ({value})")

                elif colu_fiel.get("response_choice", "").startswith(
                    ("relation", "internal")
                ):
                    jt = f"jt_{form.id}_{key}_filter".replace('"', "")
                    having.append(
                        f"ARRAY[{value}]::bigint[] @> ARRAY_AGG(DISTINCT {jt}.table_2)::bigint[]"
                    )
                    junction.append(
                        f"join {schema_name}.{jt} {jt} on {table}.id = {jt}.table_1"
                    )
                    # que = f"{key} && ARRAY[{value}]::bigint[]".replace('"', "")
                    # condition.append(que)

                elif colu_fiel.get("response_choice", "").startswith("global"):
                    que = f"{key} && ARRAY[{value}]".replace('"', "")
                    condition.append(que)

                else:
                    que = f"{key} in ({value})".replace('"', "")
                    condition.append(que)

            else:
                if isinstance(start, str):
                    start = f"'{start}'"
                condition.append(f"{key} = {start}")

        elif value["method"] == "between":
            if value.get("start"):
                start = value.get("start")
                if isinstance(start, str):
                    start = f"'{start}'"
                condition.append(f"{key} >= {start}")

            if value.get("end"):
                end = value.get("end")
                if isinstance(end, str):
                    end = f"'{end}'"
                condition.append(f"{key} <= {end}")

        else:
            method = value["method"]
            operation = comparison_methods.get(method) or "="

            start = value.get("start").strip()
            if isinstance(start, str):
                start = f"'{start}'"

            condition.append(f"{key} {operation} {start}")

    condition = " and ".join(condition)
    having = " and ".join(having)
    return condition, having, junction


def get_mapping_data(
    db,
    form,
    schema_name,
    query,
    page,
    size,
    value,
    selected_id=[],
    multiple=True,
    filters={},
    head=None,
):
    form_name = f"table_{form.id}"

    raw_query = check_table_exist(table_name=form_name, schema_name=schema_name)
    exist = db.execute(raw_query).fetchone()[0]
    if not exist:
        raise BadRequest_400(
            message="Table is not properly created, Please contact to admin to publish this form from Brain Grid."
        )

    # raw_query = get_column_data(
    #     table_name=form_name,
    #     schema_name=schema_name,
    # )
    # all_field_data = [i[0] for i in db.execute(raw_query).fetchall()]

    selected_fields = [
        f"{form_name}.id as id",
    ]
    selected_id = [i for i in selected_id if i]
    single_data = False

    form_fields = form.fields
    form_fields.append(
        {"id": "uid", "response_choice": "default", "type": "number"},
    )
    junction_query = []
    if multiple and query.multiple and query.reference_column:
        reference_column = query.reference_column or []
        reference_column = [str(i).lower() for i in reference_column]

        field_type = {
            str(i["id"]).lower(): i
            for i in form_fields
            if str(i.get("id", "")).lower() in reference_column and not i.get("deleted")
        }

        error_h = False
        if not field_type:
            field_type = {
                "uid": {"id": "uid", "response_choice": "default", "type": "number"},
            }
            error_h = True

        field_type_relation = [
            value["response_choice"]
            for _, value in field_type.items()
            if value["response_choice"] == "relation"
        ]
        if field_type_relation or error_h:
            selected_fields.append(f"{form_name}.uid as uid")
            separator = None
            single_data = True

        else:
            separator = query.separator
            field_type = list(field_type.values())
            for field in field_type:
                field_id = str(field["id"]).lower()

                if field_id == "uid":
                    selected_fields.append(f"{form_name}.uid as uid")

                elif field["response_choice"] == "global":
                    global_form_alias = f"t_{form.id}_g_{field_id}"
                    unnest_global_key = f"unnest_{form.id}_{field_id}"

                    junction_query.append(
                        f"""
                        LEFT JOIN LATERAL UNNEST({form_name}."{field_id}") AS {unnest_global_key}(id) ON true
                        LEFT JOIN {schema_name}.form_global {global_form_alias} 
                        ON {unnest_global_key}.id = {global_form_alias}.option_id 
                        and {field["type"]}::text = {global_form_alias}.id::text
                    """
                    )

                    selected_fields.append(
                        f'''STRING_AGG(distinct {global_form_alias}.option_name::varchar, ', ') AS "{field_id}"'''
                    )

                elif (
                    field["response_choice"] == "internal"
                    and str(field.get("type")) == "1"
                ):
                    alias = f"jt_{form.id}_{field_id}"
                    organization_user_alias = f"ou_{form.id}_{field_id}"
                    puser_alias = f"pu_{form.id}_{field_id}"

                    junction_query.append(
                        f"""
                        left join {schema_name}.{alias} {alias}
                        on {form_name}.id = {alias}.table_1 

                        left join public.organization__user {organization_user_alias} 
                        on {alias}.table_2 = {organization_user_alias}.id
                        
                        left join public.user {puser_alias} 
                        on {organization_user_alias}.user_id = {puser_alias}.id
                    """
                    )
                    selected_fields.append(
                        f'''STRING_AGG({puser_alias}.full_name::varchar, ', ') as "{field_id}"'''
                    )

                elif (
                    field["response_choice"] == "internal"
                    and str(field.get("type")) == "2"
                ):
                    alias = f"jt_{form.id}_{field_id}"
                    dep_alias = f"ou_{form.id}_{field_id}"

                    junction_query.append(
                        f"""
                        left join {schema_name}.{alias} {alias} 
                        on {form_name}.id = {alias}.table_1 
                        left join {schema_public_code}.department {dep_alias} 
                        on {alias}.table_2 = {dep_alias}.id
                    """
                    )
                    selected_fields.append(
                        f'''STRING_AGG({dep_alias}.name::varchar, ', ') as "{field_id}"'''
                    )

                else:
                    selected_fields.append(f'{form_name}."{field_id}" as "{field_id}"')

    else:
        separator = None
        field_id = str(query.field).lower()

        field = [
            i
            for i in form_fields
            if str(i.get("id", "")).lower() == field_id and not i.get("deleted")
        ]

        if not field:
            field_id = "uid"
            field = [
                {"id": "uid", "response_choice": "default", "type": "number"},
            ]

        field = field[0]
        if field_id == "uid" or field.get("id") == "uid":
            selected_fields.append(f"{form_name}.uid as uid")
            single_data = True

        elif field.get("response_choice") == "relation":
            selected_fields.append(f"{form_name}.uid as uid")
            single_data = True

        elif field["response_choice"] == "global":
            junction_query.append(
                f"""
                LEFT JOIN LATERAL UNNEST({form_name}."{field_id}") AS global_field_id(id) ON true
                LEFT JOIN {schema_name}.form_global global_form_alias 
                ON global_field_id.id = global_form_alias.option_id 
                and {field["type"]}::text = global_form_alias.id::text
            """
            )

            selected_fields.append(
                f'''STRING_AGG(distinct global_form_alias.option_name::varchar, ', ') AS "{field_id}"'''
            )

        elif field["response_choice"] == "internal" and str(field.get("type")) == "1":
            alias = f"jt_{form.id}_{field_id}"
            junction_query.append(
                f"""
                left join {schema_name}.{alias} {alias}
                on {form_name}.id = {alias}.table_1 
                left join public.organization__user organization__user 
                on {alias}.table_2 = organization__user.id

                left join public.user puser 
                on organization__user.user_id = puser.id
            """
            )
            selected_fields.append(
                f'''STRING_AGG(puser.full_name::varchar, ', ') as "{field_id}"'''
            )

        elif field["response_choice"] == "internal" and str(field.get("type")) == "2":
            alias = f"jt_{form.id}_{field_id}"
            junction_query.append(
                f"""
                left join {schema_name}.{alias} {alias} on {form_name}.id = {alias}.table_1 
                left join {schema_public_code}.department department on {alias}.table_2 = department.id
            """
            )
            selected_fields.append(
                f'''STRING_AGG(department.name::varchar, ', ') as "{field_id}"'''
            )

        else:
            selected_fields.append(f'{form_name}."{field_id}" as "{field_id}"')

    groupby = ", ".join(
        [i.split(" as ")[0] for i in selected_fields if "STRING_AGG(" not in i]
    )
    selected_fields = ", ".join(selected_fields)
    junction_query = " ".join(junction_query)
    offset = size * (page - 1)

    if value:
        value = [f"'{i}'" for i in value]
        value = ",".join(value)
        raw_query = f"""
            select {selected_fields} from {schema_name}.{form_name}
            {junction_query}
            where {form_name}.id in ({value})
            group by {groupby};
        """
        raw_query_count = f"""
            select COUNT(*) from {schema_name}.{form_name}
            where {form_name}.id in ({value});
        """

    elif selected_id:
        selected_id = [f"'{i}'" for i in selected_id]
        selected_id = ",".join(selected_id)
        raw_query = f"""
            select {selected_fields} from {schema_name}.{form_name}
            {junction_query}
            group by {groupby}
            order by (
                case 
                    when {form_name}.id in ({selected_id}) then 0
                    else 1
                end
            )
            limit {size} offset {offset};
        """
        raw_query_count = f"""
            select COUNT(id) from {schema_name}.{form_name};
        """

    else:
        condition_data, having_data, junc_data = filter_mapper_data(
            form=form,
            filter_data=filters,
            all_field_data=head,
            schema_name=schema_name,
        )
        if condition_data:
            condition_data = f" and {condition_data}"

        if having_data:
            having_data = f"having {having_data}"
        if junc_data:
            junc_data = junc_data[0]
        else:
            junc_data = ""
        raw_query = f"""
            select {selected_fields} from {schema_name}.{form_name}
            {junc_data}
            {junction_query}
            where {form_name}.deleted = false
            {condition_data}
            group by {groupby}
            {having_data}
            limit {size} offset {offset};
        """
        raw_query_count = f"""
            select COUNT(*) from {schema_name}.{form_name}
        """

    total = db.execute(text(raw_query_count)).fetchone()[0]
    pages = ceil(total / size) if total is not None else None

    data = db.execute(text(raw_query)).fetchall()
    data = jsonable_encoder(data)

    final_data = {}
    for i in data:
        key_id = str(i.pop("id"))

        if single_data:
            value = i.pop("uid")[0]

        else:
            value = list(
                i.values(),
            )
            if separator:
                value = [
                    value[0] or "-empty-",
                ] + [
                    str(sep or "-empty-") + str(val or "-empty-")
                    for val, sep in zip(value[1:], separator)
                ]
                value = "".join(([str(i or "-empty-") for i in value]))
            else:
                value = ",".join(([str(i or "-empty-") for i in value]))

        final_data[key_id] = value

    return {
        "items": final_data,
        "total": total,
        "page": page,
        "size": size,
        "pages": pages,
        "form": form.id,
        "form_perms": [f"add_{form.code}_form", f"edit_{form.code}_form"],
    }


def get_raw_query_form(
    template_id,
    current_user,
    tenant_code,
    is_ml_data=False,
    db: Session = Depends(get_db),
):
    junction_ids = []
    junctions = []
    junction_fields = []
    form = (
        db.query(
            form_orm.Form.id,
            form_orm.Form.name,
            form_orm.Form.description,
            form_orm.Form.code,
            form_orm.Form.element,
            form_orm.Form.defaults,
            form_orm.Form.system_id,
            func.jsonb_path_query_array(
                form_orm.Form.fields,
                '$[*] ? (@.component == "question")',
                path_ops="lax",
            ).label("fields"),
        )
        .filter(form_orm.Form.id == template_id, form_orm.Form.deleted == False)
        .first()
    )
    if not form:
        raise BadRequest_400(message="Id doesn't Exist")

    if form.element == "dashboard":
        return (
            None,
            None,
            None,
            None,
            None,
            None,
        )

    form_name = f"table_{template_id}"
    raw_query = check_table_exist(table_name=form_name, schema_name=tenant_code)
    exist = db.execute(raw_query).fetchone()[0]
    if not exist:
        raise BadRequest_400(
            message="Table is not properly created, Please contact to admin to publish this form from Brain Grid."
        )
    defaults = form.defaults
    role_base_defaults = {}
    extra_dict = {}
    if defaults:
        defaults = defaults["data"]
        user_obj = (
            db.query(OrganizationUser)
            .filter(OrganizationUser.user_id == current_user.id)
            .first()
        )

        role_obj = (
            db.query(OrganizationUserRole)
            .filter(OrganizationUserRole.org_user_id == user_obj.id)
            .all()
        )
        if role_obj:
            role_id = [i.role_id for i in role_obj]
            try:
                role_base_defaults = [i for i in defaults if i.get("role") in role_id]
                role_base_defaults = role_base_defaults[0] if role_base_defaults else {}

                try:
                    created_by_self = role_base_defaults.get("created_by_self")
                    if created_by_self:
                        extra_dict["created_by_self"] = created_by_self
                except:
                    pass

                try:
                    associated_with_column = role_base_defaults.get(
                        "associated_with_column", []
                    )
                    if associated_with_column:
                        associated_with_column = associated_with_column[0]
                        key = associated_with_column.get("key")
                        if key:
                            extra_dict["associated_with_column"] = key
                except:
                    pass
            except Exception as e:
                capture_exception(e)
                role_base_defaults = {}

    head = {
        "id": {
            "label": "Main_ID",
            "type": "text",
            "order": 1000,
            "response_choice": "hidden",
        },
        "uid": {
            "label": "ID",
            "type": "text",
            "order": 0,
            "response_choice": "table",
        },
        "created_by": {
            "label": "Created By",
            "order": 9991,
            "type": "text",
            "response_choice": "table",
        },
        "created_at": {
            "label": "Created At",
            "type": "datetime",
            "order": 9992,
            "response_choice": "table",
        },
        "updated_by": {
            "label": "Modified By",
            "type": "text",
            "order": 9993,
            "response_choice": "table",
        },
        "updated_at": {
            "label": "Modified At",
            "type": "datetime",
            "order": 9994,
            "response_choice": "table",
        },
    }

    junction_fields.append("table_.id as id")
    junction_fields.append("table_.uid as uid")
    junction_fields.append("table_.created_at as created_at")
    junction_fields.append("table_.updated_at as updated_at")
    junction_fields.append("table_.created_by as f_created_by")
    junction_fields.append("table_.updated_by as f_updated_by")

    cal = {}
    raw_query = get_column_data(
        table_name=f"table_{template_id}",
        schema_name=tenant_code,
    )
    all_field_data = [i[0] for i in db.execute(raw_query).fetchall()]
    tabl_name = f"jt_{template_id}_"
    raw_query = f"""SELECT table_name
            FROM information_schema.tables
            WHERE table_name ILIKE '{tabl_name}%'
            and table_schema = '{tenant_code}';
        """
    all_field_data.extend(
        [i[0].replace(tabl_name, "") for i in db.execute(raw_query).fetchall()]
    )
    all_field_data = set(all_field_data)

    for i in form.fields:
        if (
            str(i["type"]).lower()
            in ["static_image", "custom", "table", "repeating-element"]
            or i.get("isTable")
            or (i.get("deleted") and i["deleted"] == True)
            or str(i.get("id", "")) not in all_field_data
        ):
            continue

        if i["response_choice"] not in ["reverse-relation"]:
            lower_id = str(i["id"]).lower()

            if is_ml_data and i["response_choice"].startswith(["relation", "internal"]):
                continue

            elif i["response_choice"].startswith("global"):
                global_form_alias = f"t_{template_id}_g_{lower_id}"
                unnest_global_key = f"unnest_{template_id}_{lower_id}"

                junction_fields.append(
                    f'''STRING_AGG(distinct {global_form_alias}.option_name::varchar, ', ') AS "{lower_id}"'''
                )
                junction_fields.append(f"""table_."{lower_id}" AS data_{lower_id}""")

                junctions.append(
                    f"""
                    LEFT JOIN LATERAL UNNEST(table_."{lower_id}") AS {unnest_global_key}(id) ON true
                    LEFT JOIN {tenant_code}.form_global {global_form_alias} 
                    ON {unnest_global_key}.id = {global_form_alias}.option_id 
                    and {i["type"]}::text = {global_form_alias}.id::text
                """
                )

            elif (
                i["response_choice"].startswith("internal")
                and str(i.get("type")) == "1"
            ):
                junction_tb_alias = f"jt_{form.id}_{lower_id}"

                org_user_alias = f"ou_{form.id}_{lower_id}"
                public_user_alias = f"pu_{form.id}_{lower_id}"

                junctions.append(
                    f"""
                        left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                        on table_.id = {junction_tb_alias}.table_1 

                        left join public.organization__user {org_user_alias} 
                        on {junction_tb_alias}.table_2 = {org_user_alias}.id

                        left join public.user {public_user_alias} 
                        on {org_user_alias}.user_id = {public_user_alias}.id
                    """
                )
                junction_fields.append(
                    f"""
                    STRING_AGG(distinct {public_user_alias}.full_name::varchar, ', ') as "{lower_id}"
                """
                )
                junction_fields.append(
                    f"""Array_agg(distinct {junction_tb_alias}.table_2) as data_{lower_id}"""
                )

            elif (
                i["response_choice"].startswith("internal")
                and str(i.get("type")) == "2"
            ):
                junction_tb_alias = f"jt_{form.id}_{lower_id}"
                dep_alias = f"dep_{form.id}_{lower_id}"

                junctions.append(
                    f"""
                    left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                    on table_.id = {junction_tb_alias}.table_1 

                    left join {schema_public_code}.department {dep_alias} 
                    on {junction_tb_alias}.table_2 = {dep_alias}.id
                """
                )
                junction_fields.append(
                    f'''STRING_AGG(distinct {dep_alias}.name::varchar, ', ') as "{lower_id}"'''
                )
                junction_fields.append(
                    f"""
                    Array_agg(distinct {junction_tb_alias}.table_2) as data_{lower_id}
                """
                )

            elif i["response_choice"].startswith("relation"):
                junction_tb_alias = f"jt_{form.id}_{lower_id}"

                try:
                    id_ = int(i["type"])
                    relat_query = (
                        db.query(RelationAttributes)
                        .filter(
                            RelationAttributes.id == id_,
                            RelationAttributes.system_id == form.system_id,
                        )
                        .first()
                    )

                    related_form = db.query(form_orm.Form).get(relat_query.form_id)
                except:
                    junctions.append(
                        f"""
                        left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                        on table_.id = {junction_tb_alias}.table_1 
                    """
                    )
                    junction_fields.append(
                        f'''STRING_AGG(distinct {junction_tb_alias}.table_2::varchar, ', ') as "{lower_id}"'''
                    )
                    junction_fields.append(
                        f'''Array_agg(distinct {junction_tb_alias}.table_2) as "data_{lower_id}"'''
                    )
                    continue

                fk_tb_alias = f"fk_{form.id}_{lower_id}"

                form_fields = related_form.fields
                if (
                    relat_query.multiple
                    and relat_query.reference_column
                    and relat_query.separator
                ):
                    reference_column = relat_query.reference_column or []
                    reference_column = [str(i).lower() for i in reference_column]

                    field_type = {
                        str(i["id"]).lower(): i
                        for i in form_fields
                        if str(i.get("id", "")).lower() in reference_column
                        and not i.get("deleted")
                    }

                    error_h = False
                    if not field_type:
                        field_type = {
                            "uid": {
                                "id": "uid",
                                "response_choice": "default",
                                "type": "number",
                            },
                        }
                        error_h = True

                    field_type_relation = [
                        value["response_choice"]
                        for _, value in field_type.items()
                        if value["response_choice"].startswith("relation")
                    ]
                    if field_type_relation or error_h:
                        junctions.append(
                            f"""
                            left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                            on table_.id = {junction_tb_alias}.table_1 

                            left join {relat_query.schema_code}.table_{relat_query.form_id} {fk_tb_alias}
                            on {junction_tb_alias}.table_2 = {fk_tb_alias}.id 
                        """
                        )
                        junction_fields.append(
                            f'''STRING_AGG(distinct {fk_tb_alias}.uid::varchar, ', ') as "{lower_id}"'''
                        )
                        junction_fields.append(
                            f'''Array_agg(distinct {junction_tb_alias}.table_2) as "data_{lower_id}"'''
                        )

                    else:
                        field_type = list(field_type.values())[:3]
                        field_type_len = len(field_type) - 1 or 1
                        separator = relat_query.separator[:field_type_len]
                        nested_junctions = [
                            f"""
                                left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                                on table_.id = {junction_tb_alias}.table_1 
                                
                                left join {relat_query.schema_code}.table_{relat_query.form_id} {fk_tb_alias}
                                on {junction_tb_alias}.table_2 = {fk_tb_alias}.id 
                            """,
                        ]

                        nested_selected_fields = []

                        for field in field_type:
                            field_id = str(field["id"]).lower()

                            if field_id == "uid":
                                nested_selected_fields.append(
                                    f"""STRING_AGG(distinct {fk_tb_alias}.uid::varchar, ', ')"""
                                )

                            elif field["response_choice"].startswith("global"):
                                # nested_selected_fields.append(
                                #     f"""STRING_AGG(distinct {fk_tb_alias}.uid::varchar, ', ')"""
                                # )
                                global_form_alias = f"t_{form.id}_g_{field_id}"
                                unnest_global_key = f"unnest_{form.id}_{field_id}"

                                nested_junctions.append(
                                    f"""
                                        LEFT JOIN LATERAL UNNEST({fk_tb_alias}."{field_id}") AS {unnest_global_key}(id) ON true

                                        LEFT JOIN {tenant_code}.form_global {global_form_alias}
                                        ON {unnest_global_key}.id = {global_form_alias}.option_id
                                        and {field["type"]}::text = {global_form_alias}.id::text
                                """
                                )
                                nested_selected_fields.append(
                                    f"""STRING_AGG(distinct {global_form_alias}.option_name::varchar, ', ')"""
                                )

                            elif (
                                field["response_choice"].startswith("internal")
                                and str(field.get("type")) == "1"
                            ):
                                relation_user_alias = f"jt_{related_form.id}_{field_id}"

                                org_user_alias = f"ou_{related_form.id}_{field_id}"
                                public_user_alias = f"pu_{related_form.id}_{field_id}"

                                nested_junctions.append(
                                    f"""
                                        left join {tenant_code}.{relation_user_alias} {relation_user_alias}
                                        on {fk_tb_alias}.id = {relation_user_alias}.table_1 

                                        left join public.organization__user {org_user_alias} 
                                        on {relation_user_alias}.table_2 = {org_user_alias}.id

                                        left join public.user {public_user_alias} 
                                        on {org_user_alias}.user_id = {public_user_alias}.id
                                    """
                                )
                                nested_selected_fields.append(
                                    f"""STRING_AGG({public_user_alias}.full_name::varchar, ', ')"""
                                )

                            elif (
                                field["response_choice"].startswith("internal")
                                and str(field.get("type")) == "2"
                            ):

                                relation_dep_alias = f"jt_{related_form.id}_{field_id}"
                                dep_alias = f"dep_{related_form.id}_{field_id}"

                                nested_junctions.append(
                                    f"""
                                        left join {tenant_code}.{relation_dep_alias} {relation_dep_alias}
                                        on {fk_tb_alias}.id = {relation_dep_alias}.table_1 

                                        left join {schema_public_code}.department {dep_alias} 
                                        on {relation_dep_alias}.table_2 = {dep_alias}.id
                                    """
                                )

                                nested_selected_fields.append(
                                    f"""STRING_AGG({dep_alias}.name::varchar, ', ')"""
                                )

                            else:
                                nested_selected_fields.append(
                                    f"""STRING_AGG(distinct {fk_tb_alias}."{field_id}"::varchar, ', ')::text"""
                                )

                        nested_junctions = "\n ".join(nested_junctions)
                        junctions.append(nested_junctions)

                        nested_selected_fields = [nested_selected_fields[0]] + [
                            f"""'{str(sep)}' , {nested_selected_field}"""
                            for nested_selected_field, sep in zip(
                                nested_selected_fields[1:], separator
                            )
                        ]
                        nested_selected_fields = ", ".join(nested_selected_fields)
                        junction_fields.append(
                            f'''CONCAT({nested_selected_fields}) as "{lower_id}"'''
                        )
                        junction_fields.append(
                            f'''Array_agg(distinct {junction_tb_alias}.table_2) as "data_{lower_id}"'''
                        )

                else:
                    field_id = str(relat_query.field).lower()

                    field = [
                        nest_key
                        for nest_key in form_fields
                        if str(nest_key.get("id", "")).lower() == field_id
                        and not i.get("deleted")
                    ]
                    if not field:
                        field_id = "uid"
                        field = [
                            {
                                "id": "uid",
                                "response_choice": "default",
                                "type": "number",
                            },
                        ]

                    field = field[0]
                    if field_id == "uid" or field.get("id") == "uid":
                        junctions.append(
                            f"""
                            left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                            on table_.id = {junction_tb_alias}.table_1 

                            left join {relat_query.schema_code}.table_{relat_query.form_id} {fk_tb_alias}
                            on {junction_tb_alias}.table_2 = {fk_tb_alias}.id 
                        """
                        )
                        junction_fields.append(
                            f'''STRING_AGG(distinct {fk_tb_alias}.uid::varchar, ', ') as "{lower_id}"'''
                        )
                        junction_fields.append(
                            f'''Array_agg(distinct {junction_tb_alias}.table_2) as "data_{lower_id}"'''
                        )

                    elif field.get("response_choice") == "relation":
                        junctions.append(
                            f"""
                            left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                            on table_.id = {junction_tb_alias}.table_1 

                            left join {relat_query.schema_code}.table_{relat_query.form_id} {fk_tb_alias}
                            on {junction_tb_alias}.table_2 = {fk_tb_alias}.id 
                        """
                        )
                        junction_fields.append(
                            f'''STRING_AGG(distinct {fk_tb_alias}.uid::varchar, ', ') as "{lower_id}"'''
                        )
                        junction_fields.append(
                            f'''Array_agg(distinct {junction_tb_alias}.table_2) as "data_{lower_id}"'''
                        )

                    elif field["response_choice"] == "global":
                        global_form_alias = f"t_{related_form.id}_g_{field_id}"
                        unnest_global_key = f"unnest_{related_form.id}_{field_id}"

                        junctions.append(
                            f"""
                            left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                            on table_.id = {junction_tb_alias}.table_1 

                            left join {relat_query.schema_code}.table_{relat_query.form_id} {fk_tb_alias}
                            on {junction_tb_alias}.table_2 = {fk_tb_alias}.id 

                            LEFT JOIN LATERAL UNNEST({fk_tb_alias}."{field_id}") AS {unnest_global_key}(id) ON true

                            LEFT JOIN {tenant_code}.form_global {global_form_alias} 
                            ON {unnest_global_key}.id = {global_form_alias}.option_id 
                            and {field["type"]}::text = {global_form_alias}.id::text
                        """
                        )

                        junction_fields.append(
                            f'''STRING_AGG(distinct {global_form_alias}.option_name::varchar, ', ') as "{lower_id}"'''
                        )
                        junction_fields.append(
                            f"""Array_agg(distinct {junction_tb_alias}.table_2) as data_{lower_id}"""
                        )

                    elif (
                        field["response_choice"] == "internal"
                        and str(field.get("type")) == "1"
                    ):
                        relation_user_alias = f"jt_{related_form.id}_{field_id}"

                        org_user_alias = f"ou_{related_form.id}_{field_id}"
                        public_user_alias = f"pu_{related_form.id}_{field_id}"

                        junctions.append(
                            f"""
                            left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                            on table_.id = {junction_tb_alias}.table_1 

                            left join {tenant_code}.table_{relat_query.form_id} {fk_tb_alias}
                            on {junction_tb_alias}.table_2 = {fk_tb_alias}.id 

                            left join {tenant_code}.{relation_user_alias} {relation_user_alias}
                            on {fk_tb_alias}.id = {relation_user_alias}.table_1 

                            left join public.organization__user {org_user_alias} 
                            on {relation_user_alias}.table_2 = {org_user_alias}.id

                            left join public.user {public_user_alias} 
                            on {org_user_alias}.user_id = {public_user_alias}.id
                        """
                        )
                        junction_fields.append(
                            f'''STRING_AGG(distinct {public_user_alias}.full_name::varchar, ', ') as "{lower_id}"'''
                        )
                        junction_fields.append(
                            f'''Array_agg(distinct {junction_tb_alias}.table_2) as "data_{lower_id}"'''
                        )

                    elif (
                        field["response_choice"] == "internal"
                        and str(field.get("type")) == "2"
                    ):
                        relation_dep_alias = f"jt_{related_form.id}_{field_id}"
                        dep_alias = f"dep_{related_form.id}_{field_id}"

                        junctions.append(
                            f"""
                            left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                            on table_.id = {junction_tb_alias}.table_1 

                            left join {tenant_code}.table_{relat_query.form_id} {fk_tb_alias}
                            on {junction_tb_alias}.table_2 = {fk_tb_alias}.id 

                            left join {tenant_code}.{relation_dep_alias} {relation_dep_alias}
                            on {fk_tb_alias}.id = {relation_dep_alias}.table_1 

                            left join {schema_public_code}.department {dep_alias} 
                            on {relation_dep_alias}.table_2 = {dep_alias}.id
                        """
                        )
                        junction_fields.append(
                            f'''STRING_AGG(distinct {dep_alias}.name::varchar, ', ') as "{lower_id}"'''
                        )
                        junction_fields.append(
                            f'''Array_agg(distinct {junction_tb_alias}.table_2) as "data_{lower_id}"'''
                        )

                    else:
                        junctions.append(
                            f"""
                            left join {tenant_code}.{junction_tb_alias} {junction_tb_alias}
                            on table_.id = {junction_tb_alias}.table_1 

                            left join {relat_query.schema_code}.table_{relat_query.form_id} {fk_tb_alias}
                            on {junction_tb_alias}.table_2 = {fk_tb_alias}.id 
                        """
                        )
                        junction_fields.append(
                            f'''STRING_AGG(distinct {fk_tb_alias}."{field_id}"::varchar, ', ') as "{lower_id}"'''
                        )
                        junction_fields.append(
                            f'''Array_agg(distinct {junction_tb_alias}.table_2) as "data_{lower_id}"'''
                        )

            else:
                junction_fields.append(f'''table_."{lower_id}" as "{lower_id}"''')

            head[lower_id] = {
                "label": re.sub(
                    r"([a-z])([A-Z])", r"\1 \2", re.sub(r"_", " ", i["label"])
                ).title(),
                "column_name": i["column_name"] or i["label"].lower().replace(" ", "_"),
                "type": i["type"],
                "response_choice": i["response_choice"],
                "multiple": i.get("multiple"),
                "order": i.get("order"),
            }

            if form.element == "calendar" and i["index_field"]:
                for form_elem in i["index_field"].split(","):
                    cal[form_elem.strip()] = f'{i["id"]}'

    return (
        form,
        head,
        cal,
        role_base_defaults,
        extra_dict,
        junction_ids,
        junctions,
        junction_fields,
    )


def get_raw_query_head(
    template_id,
    current_user,
    tenant_code,
    is_ml_data=False,
    db: Session = Depends(get_db),
):

    form = (
        db.query(
            form_orm.Form.id,
            form_orm.Form.name,
            form_orm.Form.description,
            form_orm.Form.code,
            form_orm.Form.element,
            form_orm.Form.defaults,
            form_orm.Form.system_id,
            func.jsonb_path_query_array(
                form_orm.Form.fields,
                '$[*] ? (@.component == "question")',
                path_ops="lax",
            ).label("fields"),
        )
        .filter(form_orm.Form.id == template_id, form_orm.Form.deleted == False)
        .first()
    )
    if not form:
        raise BadRequest_400(message="Id doesn't Exist")

    if form.element == "dashboard":
        return (
            None,
            None,
            None,
            None,
            None,
            None,
        )

    form_name = f"table_{template_id}"
    raw_query = check_table_exist(table_name=form_name, schema_name=tenant_code)
    exist = db.execute(raw_query).fetchone()[0]
    if not exist:
        raise BadRequest_400(
            message="Table is not properly created, Please contact to admin to publish this form from Brain Grid."
        )
    head = {
        "id": {
            "label": "Main_ID",
            "type": "text",
            "order": 1000,
            "response_choice": "hidden",
        },
        "uid": {
            "label": "ID",
            "type": "text",
            "order": 0,
            "response_choice": "table",
        },
        "created_by": {
            "label": "Created By",
            "order": 9991,
            "type": "text",
            "response_choice": "table",
        },
        "created_at": {
            "label": "Created At",
            "type": "datetime",
            "order": 9992,
            "response_choice": "table",
        },
        "updated_by": {
            "label": "Modified By",
            "type": "text",
            "order": 9993,
            "response_choice": "table",
        },
        "updated_at": {
            "label": "Modified At",
            "type": "datetime",
            "order": 9994,
            "response_choice": "table",
        },
    }

    cal = {}
    raw_query = get_column_data(
        table_name=f"table_{template_id}",
        schema_name=tenant_code,
    )
    all_field_data = [i[0] for i in db.execute(raw_query).fetchall()]

    tabl_name = f"jt_{template_id}_"
    raw_query = f"""SELECT table_name
            FROM information_schema.tables
            WHERE table_name ILIKE '{tabl_name}%'
            and table_schema = '{tenant_code}';
        """
    all_field_data.extend(
        [i[0].replace(tabl_name, "") for i in db.execute(raw_query).fetchall()]
    )
    all_field_data = set(all_field_data)

    for i in form.fields:
        if (
            str(i["type"]).lower()
            in ["static_image", "custom", "table", "repeating-element"]
            or i.get("isTable")
            or (i.get("deleted") and i["deleted"] == True)
            or str(i.get("id", "")) not in all_field_data
        ):
            continue

        if i["response_choice"] not in ["reverse-relation"]:
            lower_id = str(i["id"]).lower()

            head[lower_id] = {
                "label": re.sub(
                    r"([a-z])([A-Z])", r"\1 \2", re.sub(r"_", " ", i["label"])
                ).title(),
                "column_name": i["column_name"] or i["label"].lower().replace(" ", "_"),
                "type": i["type"],
                "response_choice": i["response_choice"],
                "multiple": i.get("multiple"),
                "order": i.get("order"),
            }

            if form.element == "calendar" and i["index_field"]:
                for form_elem in i["index_field"].split(","):
                    cal[form_elem.strip()] = f'{i["id"]}'

    return form, head


@cbv(router=router)
class InspectionAPIView:
    def __init__(
        self,
        request: Request,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ) -> None:
        self.tenant_code = get_tenant_code(db, current_user, request)
        self.db_code = get_db_domain(request=request)
        if (
            request.method != "GET"
            and request.scope["path"]
            != "/amodify api, i wapi/v1/templates-data/mapping"
        ):
            ValidateFormPermission(db, current_user, request)
        system_access(current_user, request, db)
        self.organization = current_user.org_id
        self.current_user = current_user
        if not self.organization:
            raise HTTPException(400, detail={"message": "Access Denied"})

    def filter_raw_inspection(
        self,
        filter_data,
        template_id,
        user_id=None,
        search_query=None,
        archived=None,
        all_field_data=[],
        is_draft=False,
        role_base_defaults={},
        extra_dict={},
        current_user=None,
    ):
        comparison_methods = {
            "is-less-than": "<",
            "less than": "<",
            "less than or equal to": "<=",
            "is-less-or-equals-to": "<=",
            "is-equals-to": "=",
            "is-same-as": "=",
            "is-not-equal-to": "!=",
            "is-not-same-as": "!=",
            "greater than": ">",
            "is-greater-than": ">",
            "greater than or equal to": ">=",
            "is-greater-or-equals-to": ">=",
        }
        condition = []

        display_conditions_mapping = role_base_defaults.get(
            "display_conditions_mapping", []
        )
        display_conditions = role_base_defaults.get("display_conditions", [])
        display_conditions = {
            i.get("tid"): i for i in display_conditions if i.get("tid")
        }

        for display_conditions_mapp in display_conditions_mapping:
            gate = display_conditions_mapp.get("gate")
            groups = display_conditions_mapp.get("group", [])

            default_condition = []

            for group in groups:
                display_condition = display_conditions.get(group)
                if display_condition:
                    value = display_condition.get("value")

                    column = display_condition.get("column").lower()
                    if column not in all_field_data:
                        continue

                    column = f'"{column}"'
                    relation = display_condition.get("relation", "").lower()

                    if relation in ["is-one-of", "is-not-one-of"]:
                        if isinstance(value, list):
                            value = ",".join([f"'{i}'" for i in value])
                            if relation == "is-one-of":
                                default_condition.append(
                                    f"data_{column} && ARRAY[{value}]"
                                )
                            else:
                                default_condition.append(
                                    f"NOT (data_{column} && ARRAY[{value}])"
                                )

                    elif relation == "between":
                        if isinstance(value, list):
                            value_ = []
                            for i in value:
                                i = f"'{i}'"
                                value_.append(i)

                            start = value_[0]
                            end = value_[0]
                            default_condition.append(f"{column} >= {start}")
                            default_condition.append(f"{column} <= {end}")

                    else:
                        if isinstance(value, list):
                            value = f"'{value[0]}'"

                        elif isinstance(value, str):
                            value = f"'{value}'"

                        operation = comparison_methods.get(relation) or "="
                        default_condition.append(f"{column} {operation} {value}")

            if gate.lower() == "or" and default_condition:
                if len(default_condition) > 1:
                    default_condition = " or ".join(default_condition)
                    default_condition = f"({default_condition})"
                else:
                    default_condition = default_condition[0]
                condition.append(default_condition)

            elif gate.lower() == "and" and default_condition:
                if len(default_condition) > 1:
                    default_condition = " and ".join(default_condition)
                    default_condition = f"({default_condition})"
                else:
                    default_condition = default_condition[0]
                condition.append(default_condition)

        if search_query and all_field_data:
            search_query = search_query.lower().strip()
            search_query = f"::text ilike '%{search_query}%'"

            search_condition = []
            for fields_li, value in all_field_data.items():
                # if fields_li in [
                #     "created_at",
                #     "created_by",
                #     "updated_at",
                #     "updated_by",
                # ]:
                #     continue

                # if str(value.get("type", "")).lower() not in [
                #     "number",
                #     "number-int",
                #     "text",
                #     "email",
                #     "url",
                #     "telephone",
                #     "text_area",
                # ] and not str(value.get("response_choice", "")).lower().startswith(('relation', 'global','internal')):
                #     continue

                search_condition.append(f'"{fields_li}"{search_query}')

            if search_condition:
                search_condition = " or ".join(search_condition)
                search_condition = f"({search_condition})"
                condition.append(search_condition)

        if user_id:
            condition.append(f"user_id = {user_id}")

        if archived == "true":
            condition.append(f"deleted = true")
        else:
            condition.append(f"deleted = false")

        if is_draft:
            condition.append(f"is_draft = true")
        else:
            condition.append(f"is_draft = false")

        if "created_by_self" in extra_dict:
            current_user_id = f"{current_user.id}"
            condition.append(f"created_by = {current_user_id}")

        associated_with_column = extra_dict.get("associated_with_column")
        if associated_with_column:
            current_user_id = f"'{current_user.id}'"
            associated_with_column = f"data_{associated_with_column}"
            condition.append(f"{associated_with_column} @> ARRAY[{current_user_id}]")

        for key, value in filter_data.items():
            if key.lower() not in all_field_data:
                continue

            if key.lower() in ["created_by", "updated_by"]:
                key = f"f_{key}"

            key = f'"{key}"'
            if value["method"] == "equal to":
                start = value.get("start")
                if isinstance(start, list):
                    value_ = []
                    for i in start:
                        i = f"'{i}'"
                        value_.append(i)

                    value = ",".join(value_)

                    colu_fiel = all_field_data.get(key.replace('"', ""), {})

                    if colu_fiel.get("response_choice", "").lower() == "table":
                        condition.append(f"{key} in ({value})")

                    elif colu_fiel.get("response_choice", "").startswith(
                        ("relation", "internal")
                    ):
                        que = f"data_{key} && ARRAY[{value}]::bigint[]".replace('"', "")
                        condition.append(que)

                    elif colu_fiel.get("response_choice", "").startswith("global"):
                        que = f"data_{key} && ARRAY[{value}]".replace('"', "")
                        condition.append(que)

                    else:
                        que = f"{key} in ({value})".replace('"', "")
                        condition.append(que)

                else:
                    if isinstance(start, str):
                        start = f"'{start}'"
                    condition.append(f"{key} = {start}")

            elif value["method"] == "between":
                if value.get("start"):
                    start = value.get("start")
                    if isinstance(start, str):
                        start = f"'{start}'"
                    condition.append(f"{key} >= {start}")

                if value.get("end"):
                    end = value.get("end")
                    if isinstance(end, str):
                        end = f"'{end}'"
                    condition.append(f"{key} <= {end}")

            elif value["method"] == "calendar_between":
                start = value["start"]
                if isinstance(start, str):
                    start = f"'{start}'"

                end = value["end"]
                if isinstance(end, str):
                    end = f"'{end}'"

                end_date_field = value["end_date_field"]
                end_date_field = f'"{end_date_field}"'
                condition.append(
                    f"{key} BETWEEN {start} and {end} or {end_date_field} BETWEEN {start} and {end}"
                )

            else:
                method = value["method"]
                operation = comparison_methods.get(method) or "="

                start = value.get("start").strip()
                if isinstance(start, str):
                    start = f"'{start}'"

                condition.append(f"{key} {operation} {start}")

        condition = " and ".join(condition)
        return condition

    @router.post("/meta/")
    async def update_meta(
        self,
        schema: UpdateMetaSchema,
        id: int,
        system_id: int = Query(None),
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = await repo.update_meta(
                db=db,
                schema=schema,
                id=id,
                current_user=current_user,
                tenant_code=self.tenant_code,
                system_id=system_id,
            )
        return JSONResponse(
            {
                "message": "Data saved successfully",
                "data": data,
            }
        )

    @router.post("/")
    async def post(
        self,
        host: Request,
        templates: Schema,
        background_tasks: BackgroundTasks,
        template_id: int = Query(None),
        system_id: int = Query(None),
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = await repo.save(
                db=db,
                templates=templates,
                background_tasks=background_tasks,
                current_user=current_user,
                organization=self.organization,
                tenant_code=self.tenant_code,
                db_code=self.db_code,
                system_id=system_id,
            )
            log = OrganizationAuditLog(
                user_id=current_user.id,
                module_id=None,
                module_name="Window Data",
                form_id=templates.template_id,
                form_data_id=data.get("id"),
                system_id=system_id,
                organization_id=self.organization,
                action="CREATE",
            )
            ip = (
                host.headers.get("X-Forwarded-For")
                or host.headers.get("X-Real-IP")
                or host.client.host
            )
            background_tasks.add_task(log.log_saver, ip, db)
        return JSONResponse(
            {
                "message": "Data saved successfully",
                "data": data,
            }
        )

    @router.put("/{id}")
    async def put(
        self,
        host: Request,
        templates: Schema,
        background_tasks: BackgroundTasks,
        id: int,
        template_id: int = Query(None),
        system_id: int = Query(None),
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = {}
            data = await repo.update(
                db=db,
                templates=templates,
                id=id,
                background_tasks=background_tasks,
                current_user=current_user,
                organization=self.organization,
                tenant_code=self.tenant_code,
                system_id=system_id,
                db_code=self.db_code,
            )
            log = OrganizationAuditLog(
                user_id=current_user.id,
                module_id=None,
                module_name="Window Data",
                form_id=templates.template_id,
                form_data_id=id,
                system_id=system_id,
                organization_id=self.organization,
                action="UPDATE",
            )
            ip = (
                host.headers.get("X-Forwarded-For")
                or host.headers.get("X-Real-IP")
                or host.client.host
            )
            background_tasks.add_task(log.log_saver, ip, db)
            return JSONResponse(
                {"message": "TemplatesData saved successfully", "data": data}
            )

    @router.get("/mapping", response_model=typing.Any)
    async def get_mappings(
        self,
        id: str = Query(...),
        value: typing.List[str] = Query([]),
        selected_id: typing.List[int] = Query([]),
        system_id: int = Query(...),
        page: int = 1,
        size: int = 10,
        db: Session = Depends(get_db),
        filters: str = None,
    ):
        if filters:
            try:
                filters = json.loads(filters)
            except:
                raise BadRequest_400(message="There is issues in filter")
        else:
            filters = {}
        try:
            id = int(id)
        except:
            return {
                "form_perms": [],
                "items": [],
                "page": 1,
                "pages": 1,
                "size": 1,
                "total": 0,
            }

        with schema_context(self.tenant_code, db):
            query = (
                db.query(RelationAttributes)
                .filter(
                    RelationAttributes.id == id,
                    RelationAttributes.system_id == system_id,
                )
                .first()
            )

            try:
                value = [int(i) for i in value if i]
            except:
                raise HTTPException(
                    400,
                    detail={"message": "Value must be integer."},
                )

            try:
                selected_id = [int(i) for i in selected_id if i]
            except:
                raise HTTPException(
                    400,
                    detail={"message": "Selected id must be integer."},
                )

            if not query:
                raise HTTPException(
                    400,
                    detail={
                        "message": f"Relation attributes doesnot exist with this {id} id."
                    },
                )

            form = db.query(form_orm.Form).get(query.form_id)
            if not form:
                raise HTTPException(
                    400,
                    detail={
                        "message": "Form doesnot exist with this relation attributes data."
                    },
                )
            form, head = get_raw_query_head(
                template_id=form.id,
                current_user=self.current_user,
                is_ml_data=False,
                tenant_code=query.schema_code,
                db=db,
            )
            data = get_mapping_data(
                db=db,
                form=form,
                schema_name=query.schema_code,
                query=query,
                selected_id=selected_id,
                value=value,
                page=page,
                size=size,
                filters=filters,
                head=head,
            )

            return data

    @router.get("/modify/")
    async def get_modify(
        self,
        db: Session = Depends(get_db),
        filters: str = None,
        q: str = None,
        user_id: int = None,
        template_id: int = Query(...),
        system_id: int = Query(...),
        column_id: list[str] = Query(None),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
        archived: str = None,
        page: int = 1,
        size: int = 10,
        is_draft: bool = False,
        is_ml_data: bool = False,
        sortBy: str = "created_at",
    ):
        if filters:
            try:
                filters = json.loads(filters)
            except:
                raise BadRequest_400(message="There is issues in filter")
        else:
            filters = {}
        with schema_context(self.tenant_code, db):
            (
                form,
                head,
                cal,
                role_base_defaults,
                extra_dict,
                junction_ids,
                junctions,
                junction_fields,
            ) = get_raw_query_form(
                template_id=template_id,
                current_user=current_user,
                is_ml_data=is_ml_data,
                tenant_code=get_current_tenant(db),
                db=db,
            )

            if form is None:
                return {
                    "items": [],
                    "total": 0,
                    "page": 1,
                    "size": 1,
                    "pages": 1,
                    "headers": [],
                    "exclude": [],
                    "info": {},
                }

            order_by = "asc" if "-" in sortBy else "desc"
            sortBy = sortBy.replace("-", "").lower()
            sortBy = sortBy if sortBy in head else "created_at"

            offset = size * (page - 1)
            junction_query = ""

            junctions = "\n ".join(junctions)
            junction_query += junctions

            selected_data = junction_fields
            selected_data.append(f"us.full_name as created_by")
            selected_data.append(f"us2.full_name as updated_by")
            selected_data.append(f"table_.meta as meta")
            selected_data.append(f"table_.is_draft as is_draft")
            selected_data.append(f"table_.deleted as deleted")

            group_by = ", ".join(
                [
                    i.lower().split(" as ")[0]
                    for i in selected_data
                    if "string_agg(" not in i.lower() and "array_agg(" not in i.lower()
                ]
            )

            condition_data = self.filter_raw_inspection(
                filter_data=filters,
                user_id=user_id,
                search_query=q,
                archived=archived,
                all_field_data=head,
                is_draft=is_draft,
                role_base_defaults=role_base_defaults,
                extra_dict=extra_dict,
                current_user=current_user,
                template_id=template_id,
            )
            if condition_data:
                condition_data = f"where {condition_data}"

            selected_data = ", ".join(selected_data)
            if column_id:
                column_id = [f'"{i}"' for i in column_id]
                column_id = ", ".join(["id", "uid"] + column_id)
            else:
                column_id = "*"
            raw_query = f"""
                WITH aggregated_data AS (
                    select {selected_data} from {self.tenant_code}.table_{template_id} table_
                    left join public.user us on table_.created_by = us.id
                    left join public.user us2 on table_.updated_by = us2.id
                    {junction_query}
                    group by {group_by}
                )
                SELECT {column_id}
                FROM aggregated_data
                {condition_data}
                order by "{sortBy}" {order_by}
                limit {size} offset {offset};
            """

            with open("query_text.txt", "w") as f:
                f.write(raw_query.strip())
                f.write("\n")

            raw_query = text(raw_query)
            data = db.execute(raw_query).fetchall()
            data = jsonable_encoder(data)

            raw_query = f"""
                WITH aggregated_data AS (
                    select {selected_data} from {self.tenant_code}.table_{template_id} table_
                    left join public.user us on table_.created_by = us.id
                    left join public.user us2 on table_.updated_by = us2.id
                    {junction_query}
                    group by {group_by}
                )
                SELECT COUNT(*)
                FROM aggregated_data
                {condition_data};
            """
            total = db.execute(raw_query).fetchone()[0]
            pages = ceil(total / size) if total is not None else None

            raw_query = get_deleted_data_count(
                table_name=f"table_{template_id}", schema_name=self.tenant_code
            )
            archived_count = db.execute(raw_query).fetchone()[0]

            return {
                "items": data,
                "total": total,
                "page": page,
                "size": size,
                "pages": pages,
                "headers": head,
                "exclude": exclude,
                "info": {
                    "archived_count": archived_count,
                    "form_name": form.name,
                    "description": form.description,
                    "form_element": form.element,
                    "calendar_info": cal if cal != {} else None,
                    "perms": instance_all_permission(
                        db, self.current_user, form.code, system_id
                    ),
                },
            }

    @router.get("/actions", response_model=CustomPage[typing.Any])
    async def get_actions(
        self,
        db: Session = Depends(get_db),
        key: str = None,
        value: str = None,
        user_id: int = None,
        window_central: bool = False,
        template_id: int = Query(None),
        system_id: int | None = Query(None),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            query = (
                db.query(form_orm.Form.id, form_orm.Form.actions)
                .filter(form_orm.Form.id == template_id)
                .first()
            )
            actions_data_obj = query.actions.get("data")

            org_admin = (
                db.query(org_orm.Organization)
                .filter(
                    org_orm.Organization.user_id == current_user.id,
                    org_orm.Organization.is_active == True,
                    org_orm.Organization.deleted == False,
                )
                .first()
            )

            if window_central:
                actions_data = actions_data_obj

            elif org_admin:
                actions_data = actions_data_obj

            else:
                user_obj = (
                    db.query(OrganizationUser)
                    .filter(OrganizationUser.user_id == current_user.id)
                    .first()
                )
                role_obj = (
                    db.query(OrganizationUserRole)
                    .filter(OrganizationUserRole.org_user_id == user_obj.id)
                    .all()
                )
                role_ids = [x.role_id for x in role_obj]
                if actions_data_obj:
                    action_list = []
                    actions_data = actions_data_obj
                    for action in actions_data:
                        if action.get("default") == True:
                            action_list.append(action)
                        else:
                            if action.get("config"):
                                if action.get("config").get("roles") and any(
                                    x in role_ids
                                    for x in action.get("config").get("roles")
                                ):
                                    action_list.append(action)
                    actions_data = action_list
                else:
                    actions_data = []

            # actions_data_new=[]
            for a in actions_data:
                if a.get("code", "") in ["EDIT_ITEM", "DELETE_ITEM", "VIEW_ITEM"]:
                    a["enabled"] = True
                    a["default"] = True
                # enabled_value = a.get('enabled', None)
                # if enabled_value == True:
                #     actions_data_new.append(a)
                # elif enabled_value == False:
                #     continue
                # else:
                #     actions_data_new.append(a)

            return paginate(actions_data)

    @router.get("/defaults", response_model=CustomPage[typing.Any])
    async def get_defaults(
        self,
        db: Session = Depends(get_db),
        template_id: int = Query(None),
        system_id: int = Query(None),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            query = (
                db.query(form_orm.Form).filter(form_orm.Form.id == template_id).first()
            )
            if query:
                defaults_data = query.defaults
                if defaults_data:
                    defaults_data = defaults_data.get("data")
                else:
                    defaults_data = []
            else:
                defaults_data = []
            return paginate(defaults_data)

    @router.get("/raw-data", status_code=400)
    async def get_raw_data(
        self,
        db: Session = Depends(get_db),
        template_id: int = Query(None),
        size: int = 10,
        page: int = 0,
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        url = "Please use /templates-data/modify/?template_id={template_id}&system_id={system_id}&is_ml_data=true&is_draft=falsepage=1&size=20 api."
        return {"message": url}

    @router.get("/{id}")
    async def get_details(
        self,
        id: int,
        db: Session = Depends(get_db),
        template_id: int = Query(...),
        system_id: int = Query(...),
        encrypted: List[str] = Query([]),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            form = (
                db.query(
                    func.jsonb_path_query_array(
                        form_orm.Form.fields,
                        '$[*] ? (@.component == "question" && (@.response_choice starts with "relation" || @.response_choice starts with "internal"))',
                        path_ops="lax",
                    ).label("fields"),
                )
                .filter(
                    form_orm.Form.id == template_id,
                    form_orm.Form.deleted == False,
                    form_orm.Form.system_id == system_id,
                )
                .first()
            )
            if not form:
                raise BadRequest_400(message="Id doesn't Exist")

            form_name = f"table_{template_id}"
            raw_query = get_data_details(
                table_name=form_name, schema_name=self.tenant_code, record_id=id
            )

            instance = db.execute(raw_query).fetchone()
            if instance:
                data = jsonable_encoder(instance)

                for i in form.fields:
                    if i["response_choice"].startswith(("relation", "internal")):
                        alias = f"jt_{template_id}_{i['id']}"
                        query_text = f"select Array_agg(DISTINCT table_2) from {self.tenant_code}.{alias} where table_1 = {id} group by table_1;"
                        dat = db.execute(query_text).fetchone()
                        data[i["id"]] = dat[0] if dat else []

                for key in encrypted:
                    if key in data and type(data[key]) == str:
                        try:
                            data[key] = (
                                ENCRYPTION_KEY.decrypt(data[key].encode())
                                .decode()
                                .replace('"', "")
                            )
                        except:
                            continue
                return data
            else:
                raise BadRequest_400(
                    message={"message": "Id doesn't exists"},
                )

    @router.delete("/")
    async def delete(
        self,
        schema: BulkDeleteSchema,
        background_tasks: BackgroundTasks,
        template_id: int = Query(None),
        system_id: int = Query(None),
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = await repo.bulk_delete(
                db=db,
                schema=schema,
                background_tasks=background_tasks,
                current_user=current_user,
                organization=self.organization,
                tenant_code=self.tenant_code,
                db_code=self.db_code,
            )
        return jsonable_encoder({"message": "Deleted successfully", "data": data})

    @router.post("/restore/")
    async def restore(
        self,
        schema: BulkDeleteSchema,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = await repo.restore_data(
                db=db,
                schema=schema,
                tenant_code=self.tenant_code,
                current_user=current_user,
                db_code=self.db_code,
            )
        return jsonable_encoder({"message": "Restored successfully", "data": data})

    @router.get("/csv-format/{id}")
    async def csv_format(
        self,
        id: int,
        db: Session = Depends(get_db),
        template_id: int = Query(None),
        system_id: int = Query(None),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            headers = {}
            form_obj = (
                db.query(
                    form_orm.Form.id,
                    func.jsonb_path_query_array(
                        form_orm.Form.fields,
                        '$[*] ? (@.component == "question")',
                        path_ops="lax",
                    ).label("fields"),
                )
                .filter(form_orm.Form.id == id)
                .first()
            )
            if form_obj:
                for i in form_obj.fields:
                    if not i.get("response_choice", "").startswith(
                        ("relation", "internal", "global", "multiple", "signature")
                    ):
                        id = i["id"]
                        label = re.sub(r"([a-z])([A-Z])", r"\1 \2", i["label"])
                        headers[f'{i["id"]}'] = f"{label} | {id}"

            df = pd.DataFrame(columns=list(headers.values()))
            stream = io.StringIO()
            df.to_csv(stream, index=False)
            response = StreamingResponse(
                iter([stream.getvalue()]), media_type="text/csv"
            )
            response.headers["Content-Disposition"] = (
                "attachment; filename=bulk_upload.csv"
            )
            return response

    @staticmethod
    def populate_data(data_list, last_data_dict):
        last_data_copy = last_data_dict.copy()
        for data_dict in data_list:
            label = data_dict.get("label")
            if label in last_data_copy:
                data_dict["value"] = last_data_copy[label]
        return data_list

    @router.post("/import-data/")
    async def upload_csv(
        self,
        host: Request,
        templates: typing.List[Schema],
        background_tasks: BackgroundTasks,
        template_id: int = Query(None),
        system_id: int = Query(None),
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            await repo.bulk_save(
                db=db,
                templates=templates,
                background_tasks=background_tasks,
                current_user=current_user,
                organization=self.organization,
                tenant_code=self.tenant_code,
                system_id=system_id,
            )
            db.commit()
        return JSONResponse({"message": "Data saved successfully"})

    @router.get("/export-csv/{id}")
    async def modify_export_csv(
        self,
        id: int,
        template_id: int = Query(None),
        filters: str = None,
        system_id: int = Query(None),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
        fields: typing.List[str] = Query(None),
        db: Session = Depends(get_db),
    ):
        if filters:
            try:
                filters = json.loads(filters)
            except:
                raise BadRequest_400(message="There is issues in filter")
        else:
            filters = {}

        with schema_context(self.tenant_code, db):
            (
                form,
                head,
                _,
                role_base_defaults,
                extra_dict,
                _,
                junctions,
                junction_fields,
            ) = get_raw_query_form(
                template_id=template_id,
                current_user=current_user,
                tenant_code=self.tenant_code,
                db=db,
            )

            if form is None:
                return {"message": "could dump data to csv."}

            selected_data = junction_fields
            selected_data.append(f"us.full_name as created_by")
            selected_data.append(f"us2.full_name as updated_by")
            selected_data.append(f"table_.meta as meta")
            selected_data.append(f"table_.is_draft as is_draft")
            selected_data.append(f"table_.deleted as deleted")

            group_by = ", ".join(
                [
                    i.lower().split(" as ")[0]
                    for i in selected_data
                    if "string_agg(" not in i.lower() and "array_agg(" not in i.lower()
                ]
            )

            condition_data = self.filter_raw_inspection(
                filter_data=filters,
                all_field_data=head,
                role_base_defaults=role_base_defaults,
                extra_dict=extra_dict,
                current_user=current_user,
                template_id=template_id,
            )
            if condition_data:
                condition_data = f"where {condition_data}"

            junction_query = ""
            junctions = "\n ".join(junctions)
            junction_query += junctions

            selected_data = ", ".join(selected_data)
            raw_query = f"""
                WITH aggregated_data AS (
                    select {selected_data} from {self.tenant_code}.table_{template_id} table_
                    left join public.user us on table_.created_by = us.id
                    left join public.user us2 on table_.updated_by = us2.id
                    {junction_query}
                    group by {group_by}
                )
                SELECT *
                FROM aggregated_data
                {condition_data}
                order by id asc;
            """

            raw_query = text(raw_query)
            data = db.execute(raw_query).fetchall()
            data = jsonable_encoder(data)

            data = pd.DataFrame(data)

            columns = {
                key.lower(): value["label"].title() for key, value in head.items()
            }
            data = data.rename(columns=columns)
            fields = [i.title() for i in fields if i in data.columns or i == "ID"]
            data = data[fields]

            stream = io.StringIO()
            data.to_csv(stream, index=False)

            response = StreamingResponse(
                iter([stream.getvalue()]), media_type="text/csv"
            )
            response.headers["Content-Disposition"] = (
                f"attachment; filename={form.name}.csv"
            )
            response.headers["Content-Type"] = "text/csv"
            return response

    @router.patch("/update-data/{id}")
    async def update_data(
        self,
        id: int,
        data: typing.List[UpdateSchema],
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            raw_query = get_column_details(
                form_name=f"table_{id}", schema_name=self.tenant_code
            )
            field_data = db.execute(raw_query).fetchall()
            field_data = {i[0]: i for i in field_data}

            for row in data:
                raw_query = get_data_details(
                    table_name=f"table_{id}",
                    schema_name=self.tenant_code,
                    record_id=row.id,
                )
                obj = db.execute(raw_query).fetchone()

                existing_junction_data = []
                if row.response_choice.startswith(("relation", "internal")):
                    junc_tbl = f"jt_{row.template_id}_{row.label}"
                    alias_id = row.id
                    tbl_query = f"""
                        SELECT ARRAY_AGG(tbl.table_2) AS aggr_{alias_id}
                        FROM {self.tenant_code}.{junc_tbl} tbl
                        WHERE tbl.table_1 = {alias_id};
                    """
                    data = db.execute(tbl_query).fetchone()
                    data = jsonable_encoder(data)
                    existing_junction_data.append(data)

                    changed_data = [{"id": row.id, "value": row.value}]
                    create, delete = TemplatesDataSQLRepository.separate_data(
                        changed_data, existing_junction_data
                    )
                    if create:
                        column_data_junc = ", ".join(["table_1", "table_2"])
                        for data in create:
                            junction_table = f"jt_{row.template_id}_{row.label}"
                            for val in data.get("value"):
                                value_data = f"({row.id}, {val})"
                                raw_query = sql_insert_record_junction(
                                    table_name=junction_table,
                                    schema_name=self.tenant_code,
                                    cols=column_data_junc,
                                    vals=value_data,
                                )
                                db.execute(raw_query)
                    if delete:
                        for data in delete:
                            junction_table = f"jt_{row.template_id}_{row.label}"
                            for val in data.get("value"):
                                raw_query = sql_delete_record_junction(
                                    table_name=junction_table,
                                    schema_name=self.tenant_code,
                                    record_id=row.id,
                                    parent_id=val,
                                )
                                db.execute(raw_query)
                else:
                    if obj:
                        obj = jsonable_encoder(obj)
                        row_label = str(row.label).lower()
                        column_data = []
                        value_data = []

                        if row_label in obj:
                            value = row.value

                            field_dat = field_data[row_label]

                            if field_dat[3].lower() == "_text":
                                if not value:
                                    value = "{-}"
                                else:
                                    value = (
                                        "{"
                                        + ",".join([str(i) for i in value if i])
                                        + "}"
                                    )

                            elif field_dat[3].lower() == "int8":
                                try:
                                    value = int(value)
                                except:
                                    raise HTTPException(
                                        400,
                                        detail={
                                            "message": f"Value of {row_label} data must be integer."
                                        },
                                    )

                            elif field_dat[3].lower() == "float8":
                                try:
                                    value = float(value)
                                except:
                                    raise HTTPException(
                                        400,
                                        detail={
                                            "message": f"Value of {row_label} data must be float."
                                        },
                                    )

                            elif field_dat[3].lower() == "boolean":
                                try:
                                    value = bool(value)
                                except:
                                    raise HTTPException(
                                        400,
                                        detail={
                                            "message": f"Value of {row_label} data must be boolean."
                                        },
                                    )

                            elif field_dat[3].lower() == "timestamptz":
                                try:
                                    value = datetime.datetime.fromisoformat(
                                        value.replace("Z", "+00:00")
                                    )
                                    value = str(value)
                                except:
                                    raise HTTPException(
                                        400,
                                        detail={
                                            "message": f"Value of {row_label} data must be datetime with format %Y-%m-%d %H:%M:%S."
                                        },
                                    )

                            elif field_dat[3].lower() == "date":
                                try:
                                    value = datetime.datetime.strptime(
                                        value, "%Y-%m-%d"
                                    ).date()
                                    value = str(value)
                                except:
                                    raise HTTPException(
                                        400,
                                        detail={
                                            "message": f"Value of {row_label} data must be datetime with format %Y-%m-%d."
                                        },
                                    )

                            elif field_dat[3].lower() == "time":
                                try:
                                    value = value.split(":")
                                    if len(value) == 3:
                                        value = value[:-1]
                                    value = ":".join(value)
                                    value = datetime.datetime.strptime(
                                        value, "%H:%M"
                                    ).time()
                                    value = str(value)
                                except:
                                    raise HTTPException(
                                        400,
                                        detail={
                                            "message": f"Value of {row_label} data must be time."
                                        },
                                    )

                            elif isinstance(value, list):
                                value = json.dumps(value)

                            elif isinstance(value, dict):
                                value = json.dumps(value)

                            column_data.append(row_label)
                            value_data.append(value)

                        if not column_data:
                            continue

                        my_dict = {
                            f"val{k}".replace('"', ""): v
                            for k, v in zip(column_data, value_data)
                        }
                        if not my_dict:
                            raise BadRequest_400(
                                message="You can save int, float, str and bool data only from here."
                            )

                        my_dict["data_id"] = row.id
                        column_dat = [f":val{i}".replace('"', "") for i in column_data]
                        set_clause = [
                            f'"{i}" = {j}' for i, j in zip(column_data, column_dat)
                        ]
                        set_clause = ", ".join(set_clause)

                        raw_query = sql_update_record(
                            form_id=id,
                            schema_name=self.tenant_code,
                            set_clause=set_clause,
                        )
                        db.execute(raw_query, my_dict)

                    else:
                        raise BadRequest_400(
                            message=f"{row.id} Id doesnot exist in table."
                        )

            db.commit()
            return JSONResponse({"message": "Data updated successfully"})

    @router.post("/action-executer/")
    async def action_executer(
        self,
        data: ActionExecuterSchema,
        background_tasks: BackgroundTasks,
        db: Session = Depends(get_db),
        master_db: Session = Depends(get_master_db),
    ):
        with schema_context(self.tenant_code, db):
            form_obj = db.query(form_orm.Form).get(data.form_id)
            junc_data_types = [
                {
                    "response_choice": field.get("response_choice"),
                    "type": field.get("type"),
                    "id": field.get("id"),
                }
                for field in form_obj.fields
                if field.get("response_choice")
                and field.get("response_choice").startswith(("relation", "internal"))
            ]
            if form_obj.actions:
                action_obj = data.action.get("action")
                for action in action_obj:
                    caller = [
                        await tf.call(
                            db=db,
                            master_db=master_db,
                            action=data.action,
                            action_data=data,
                            junc_data_types=junc_data_types,
                            current_user=self.current_user,
                            organization=self.organization,
                            tenant_code=self.tenant_code,
                            background_tasks=background_tasks,
                        )
                        for tf in action_functions
                        if action.get("action") in tf.tags
                        and (CURRENT_USER & (2 ** constants_dict[action.get("action")]))
                        != 0
                    ]
                    db.commit()
                    return caller
            else:
                raise BadRequest_400(message=ErrorCode.ACCESS_DENIED)

    @router.post("/generate-invoice/")
    async def generate_invoice(
        self,
        data: GenerateInvoiceSchema,
        background_tasks: BackgroundTasks,
        system_id: int = Query(None),
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            code = "INV-" + str(uuid.uuid4().hex)[:4].upper()
            amount = 0
            invoice_data = []
            invoice_selection = data.mapping.get("invoice_selection", "quantity")

            for x in data.items:
                t_obj = {}

                status_key = data.mapping.get("status")

                if not status_key:
                    raise BadRequest_400(
                        message="It seems there is an issue with the status field, as it is neither mapped nor empty."
                    )

                inspection_id = x.get("id")
                if not inspection_id:
                    raise BadRequest_400(
                        message="It seems there is id missing in items."
                    )

                raw_query = get_data_details(
                    table_name=f"table_{data.template_id}",
                    schema_name=self.tenant_code,
                    record_id=inspection_id,
                )
                obj = db.execute(raw_query).fetchone()

                raw_query = get_column_details(
                    form_name=f"table_{data.template_id}", schema_name=self.tenant_code
                )
                field_data = db.execute(raw_query).fetchall()
                field_data = {i[0]: i for i in field_data}

                if obj:
                    obj = jsonable_encoder(obj)
                    status_key = status_key.lower()
                    if status_key not in obj:
                        raise_invalid_input(
                            message=f"It appears that the {status_key} field is not found in the inspection data or data doesnot exist."
                        )

                    status_data = field_data.get(status_key)
                    if not status_data or status_data[3] != "bool":
                        raise_invalid_input(
                            message=f"It appears that the {status_key} field is not found in the inspection data or field is not type of checkbox or switch."
                        )

                    if status_data == True:
                        uid = x.get("uid")
                        raise_invalid_input(
                            message=f"For the ID {uid}, an invoice has already been generated."
                        )

                    my_dict = {"valstatus_key": True, "data_id": inspection_id}
                    set_clause = f"{status_key} =:valstatus_key"

                    raw_query = sql_update_record(
                        form_id=data.template_id,
                        schema_name=self.tenant_code,
                        set_clause=set_clause,
                    )

                    db.execute(raw_query, my_dict)
                    db.flush()

                else:
                    uid = x.get("uid")
                    raise BadRequest_400(
                        message=f"It seems there is an {uid} field is not found with the ID  as it does not exist in the inspection records."
                    )

                if invoice_selection == "quantity":
                    rate_key = data.mapping.get("rate")
                    rate = x.get(rate_key)
                    if not rate:
                        raise_invalid_input(
                            message=f"It appears that the {rate_key} field is not found in the item data."
                        )

                    try:
                        rate = int(rate)
                    except:
                        raise_invalid_input(message=f"Rate of item must be integer.")

                    t_obj["rate"] = rate
                    t_obj["rate_mapper"] = rate_key

                    quantity_key = data.mapping.get("quantity")
                    quantity = x.get(quantity_key)
                    if not quantity:
                        raise_invalid_input(
                            message=f"It appears that the {quantity_key} field is not found in the item data."
                        )

                    try:
                        quantity = int(quantity)
                    except:
                        raise_invalid_input(
                            message=f"quantity of item must be integer."
                        )
                    t_obj["quantity"] = quantity
                    t_obj["quantity_mapper"] = quantity_key

                    amount_calc = rate * quantity
                    t_obj["amount"] = amount_calc
                    t_obj["amount_mapper"] = None
                    amount += amount_calc

                elif invoice_selection == "amount":
                    amount_key = data.mapping.get("amount")
                    amount_calc = x.get(amount_key)
                    if not amount_calc:
                        raise_invalid_input(
                            message=f"It appears that the {amount_key} field is not found in the item data."
                        )

                    try:
                        amount_calc = int(amount_calc)
                    except:
                        raise_invalid_input(message=f"amount of item must be integer.")
                    t_obj["amount"] = amount_calc
                    t_obj["amount_mapper"] = amount_key

                    t_obj["quantity"] = 1
                    t_obj["quantity_mapper"] = amount_key

                    t_obj["rate"] = amount_calc
                    t_obj["rate_mapper"] = amount_key
                    amount += amount_calc

                else:
                    raise_invalid_input(message=f"Invalid invoice selection.")

                for key, value in data.mapping.items():
                    if key not in ["amount", "quantity", "rate", "invoice_selection"]:
                        try:
                            t_obj[key] = (
                                x.get(value)
                                if value in x
                                else raise_invalid_input(
                                    message=f"It appears that the {value} field is not found in the item data."
                                )
                            )
                            key = f"{key}_mapper"
                            t_obj[key] = value
                        except:
                            continue

                invoice_data.append(t_obj)

            invoice_data = invoice_orm.Invoice(
                invoice_number=code,
                net_amount=amount,
                total_amount=amount,
                system_id=system_id,
                invoice_selection=invoice_selection,
                organization_id=self.organization,
                form_id=data.individual_form_id,
                data_id=data.parent_instance_id,
                items=invoice_data,
                created_by=self.current_user.id,
                invoice_sent=False,
                is_paid=False,
                is_cancelled=False,
            )
            db.add(invoice_data)
            data = db.commit()
            return JSONResponse({"message": "Invoice Succesfully Generated."})

    @router.post("/decrypt/")
    async def decrypt_action_data(
        self,
        decrypt_schema: DecryptSchema,
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        decrypted_fields = {}
        for key, value in decrypt_schema.fields.items():
            try:
                decrypted_value = ENCRYPTION_KEY.decrypt(value.encode()).decode()
                decrypted_fields[key] = json.loads(decrypted_value)
            except Exception as e:
                decrypted_fields[key] = value
        return decrypted_fields


@router.get("/share-data/")
async def share_data(db: Session = Depends(get_db), authorization: str = Header(None)):
    if authorization is None or not authorization.startswith("Bearer "):
        raise HTTPException(status_code=401, detail="Invalid authorization header")
    token = authorization.split(" ")[1]
    try:
        decrypted_data = ENCRYPTION_KEY.decrypt(token.encode())
    except Exception as e:
        raise BadRequest_400(message=ErrorCode.ACCESS_DENIED)

    dictionary = json.loads(decrypted_data.decode())
    schema = dictionary["sch"]
    table_id = dictionary["table_id"]
    row_id = dictionary["row_id"]
    system_id = dictionary["system_id"]

    with schema_context(schema, db):
        raw_query = get_data_details(
            table_name=f"table_{table_id}",
            schema_name=schema,
            record_id=row_id,
        )
        obj = db.execute(raw_query).fetchone()
        if not obj:
            raise HTTPException(
                400,
                detail={"message": f"Id doesn't exists"},
            )

        obj = jsonable_encoder(obj)
        form = (
            db.query(form_orm.Form)
            .filter(form_orm.Form.id == table_id, form_orm.Form.system_id == system_id)
            .first()
        )
        for i in form.fields:
            if i.get("response_choice", "").startswith(("relation", "internal")):
                alias = f"jt_{table_id}_{i['id']}"
                query_text = f"select Array_agg(table_2) from {schema}.{alias} where table_1 = {row_id} group by table_1;"
                obj[i["id"]] = db.execute(query_text).fetchone()[0] or []

        form = jsonable_encoder(form)

        if settings_instance.UAT_PRODUCTION:
            query_json_string = json.dumps(obj)
            compressed_data = zlib.compress(query_json_string.encode("utf-8"))
            compressed_data_base64 = base64.b64encode(compressed_data).decode("utf-8")
            query_fields = compressed_data_base64

            form_json_string = json.dumps(form["fields"])
            compressed_data = zlib.compress(form_json_string.encode("utf-8"))
            compressed_data_base64 = base64.b64encode(compressed_data).decode("utf-8")
            form_fields = compressed_data_base64

            form["fields"] = form_fields
            return {"query": query_fields, "form_id": form}
        else:
            return {"query": obj, "form_id": form}


@router.get("/mapping/public/", response_model=typing.Any)
async def get_mappings_public(
    id: int = Query(),
    page: int = Query(1),
    size: int = Query(5),
    value: typing.List[int] = Query([]),
    db: Session = Depends(get_db),
    authorization: str = Header(None),
):
    if authorization is None or not authorization.startswith("Bearer "):
        raise HTTPException(status_code=401, detail="Invalid authorization header")
    token = authorization.split(" ")[1]
    try:
        decrypted_data = ENCRYPTION_KEY.decrypt(token.encode())
    except Exception as e:
        raise BadRequest_400(message=ErrorCode.ACCESS_DENIED)
    dictionary = json.loads(decrypted_data.decode())
    schema = dictionary["sch"]
    table_id = dictionary["table_id"]
    row_id = dictionary["row_id"]
    org_id = dictionary["org"]
    system_id = dictionary["system_id"]

    with schema_context(schema, db):
        query = (
            db.query(RelationAttributes)
            .filter(
                RelationAttributes.id == id,
                RelationAttributes.system_id == system_id,
            )
            .first()
        )

        value = [i for i in value if i]
        if not query:
            raise HTTPException(
                400,
                detail={
                    "message": "Something wrong with the relations. Check for Invalid Field Mappings"
                },
            )

        form = db.query(form_orm.Form).get(query.form_id)
        if not form:
            raise HTTPException(
                400,
                detail={
                    "message": "Something wrong with the relations. Check for Invalid Field Mappings"
                },
            )

        return get_mapping_data(
            db=db,
            form=form,
            schema_name=schema,
            query=query,
            value=value,
            page=page,
            size=size,
        )
