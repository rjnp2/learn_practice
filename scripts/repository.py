import base64
import datetime
import json
import os
import zlib

from cryptography.fernet import Fernet
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, subqueryload

from technical.configurations.exceptions import BadRequest_400
from technical.configurations.sequence import seq_function
from technical.endpoints.org_services.form import models as form_orm
from technical.endpoints.org_services.inspection.mapper import (
    action_functions,
)
from technical.endpoints.workflow_services.trigger.transerve_workflow import (
    transerve_workflow_using_bfs,
)
from technical.endpoints.workflow_services.trigger.models import Trigger
from technical.settings.settings import settings_instance

from technical.utility.views_tables import (
    get_data_details,
    sql_insert_record,
    sql_insert_record_junction,
    sql_update_record,
    sql_delete_record_junction,
)
from .schema import *

ENCRYPTION_KEY = Fernet("AzdixRZ7QVn0eDdXcjjVg7HSCnS51X-0FdMSps7kEaE=")


class TemplatesDataSQLRepository:
    def __init__(self) -> None:
        pass

    @staticmethod
    def transform_b(b):
        transformed_b = []
        for item in b:
            for key, value in item.items():
                if key.startswith("aggr_"):
                    key = key.removeprefix("aggr_")
                    transformed_b.append({"id": key, "value": value})
        return transformed_b

    @staticmethod
    def separate_data(new, old):
        old = TemplatesDataSQLRepository.transform_b(old)
        create = []
        delete = []
        for item_old in old:
            found = False
            for item_new in new:
                if item_old["id"] == item_new["id"]:
                    found = True
                    if item_old["value"] is None and item_new["value"] is not None:
                        create.append(item_new)
                    elif item_old["value"] != item_new["value"]:
                        delete.append(item_old)
                        create.append(item_new)
                    break
            if not found and item_old["value"] is not None:
                delete.append(item_old)

        for item_new in new:
            found = False
            for item_old in old:
                if item_new["id"] == item_old["id"]:
                    found = True
                    break
            if not found:
                create.append(item_new)
        for create_item in create:
            id_to_remove = create_item["id"]
            values_to_remove = create_item["value"]
            for delete_item in delete:
                if delete_item["id"] == id_to_remove:
                    create_item["value"] = [
                        value
                        for value in values_to_remove
                        if value not in delete_item["value"]
                    ]
                    delete_item["value"] = [
                        value
                        for value in delete_item["value"]
                        if value not in values_to_remove
                    ]
        create = [item for item in create if item["value"]]
        delete = [item for item in delete if item["value"]]
        return create, delete

    @staticmethod
    async def action_executer(
        background_tasks,
        actions,
        fields,
        db,
        current_user,
        organization,
        tenant_code,
        system_id,
        id=None,
        form_id="",
    ):
        new_prepare_data = {}
        new_prepare_data["action"] = actions
        new_prepare_data["key"] = "row-cxshrfPvyW"
        new_prepare_data["code"] = "custom-row-MlwRENViODoTO"
        new_prepare_data["name"] = "Custom"
        new_prepare_data["type"] = "table"
        new_prepare_data["enabled"] = True
        new_prepare_data["data_format"] = None
        new_prepare_data["description"] = ""
        action_data_formatted = {}
        action_data_formatted["action"] = new_prepare_data
        action_data_formatted["system_id"] = system_id

        if form_id:
            action_data_formatted["form_id"] = form_id

        if hasattr(fields, "template_id"):
            action_data_formatted["form_id"] = fields.template_id

        if id:
            action_data_formatted["inspection_id"] = id

        schema_validate = ActionExecuterSchema(**action_data_formatted)
        for action in actions:
            if (
                "actions" in action
                and len(action["actions"]) == 1
                and list(action["actions"].keys())[0]
                in ["NOTIFY", "SEND_MAIL", "CHANGE_VALUE", "ADD_VALUE"]
            ):
                action["action"] = "DYNAMIC_ACTION"
                print("++++++++++++++++++++++++++++++++++???????????????????????????")
                caller = [
                    await tf.call(
                        db=db,
                        action=new_prepare_data,
                        action_data=schema_validate,
                        current_user=current_user,
                        organization=organization,
                        tenant_code=tenant_code,
                        background_tasks=background_tasks,
                    )
                    for tf in action_functions
                    if action.get("action") in tf.tags
                    # and (CURRENT_USER & (2 ** constants_dict[action.get("code")])) != 0
                ]
                break
            else:
                action["action"] = "DYNAMIC_ACTION"
                caller = [
                    await tf.call(
                        db=db,
                        action=new_prepare_data,
                        action_data=schema_validate,
                        current_user=current_user,
                        organization=organization,
                        tenant_code=tenant_code,
                        background_tasks=background_tasks,
                    )
                    for tf in action_functions
                    if action.get("action") in tf.tags
                    # and (CURRENT_USER & (2 ** constants_dict[action.get("code")])) != 0
                ]

    def temp_conversion(self, x, seq):
        x["value"] = seq
        return x

    def data_convert_validation(self, field_data, final_data):
        column_data = []
        value_data = []
        junction_table_data = []

        for field_dat in field_data:
            if type(field_dat) != dict:
                raise HTTPException(
                    400,
                    detail={
                        "message": f"Type of field_data must be list of object/dict."
                    },
                )

            value = field_dat.get("value")
            field_id = str(field_dat.get("id")).lower()

            final_data_id = final_data.get(field_id, {})
            label = final_data_id.get("label") or field_id
            required = final_data_id.get("required", False)
            deleted = final_data_id.get("deleted", False)
            response_choice = field_dat.get("response_choice").lower()

            if deleted:
                if response_choice.startswith(("global")):
                    field_dat["value"] = "{-}"
                continue

            if value in [None, "", []] and required:
                raise HTTPException(
                    400,
                    detail={"message": f"value of {label} is required."},
                )

            elif value in ["", "", None, []]:
                if response_choice.startswith(("global")):
                    field_dat["value"] = "{-}"

                continue

            field_type = str(field_dat.get("type")).lower()
            if response_choice.startswith(("global")):
                value = ",".join([str(i) for i in value if i]) or "-"
                value = "{" + value + "}"

            elif response_choice.startswith(("internal", "relation")):
                data = {
                    "id": field_dat.get("id"),
                    "value": field_dat.get("value"),
                }
                junction_table_data.append(data)

            elif field_type == "number":
                try:
                    value = float(value)
                except:
                    raise HTTPException(
                        400,
                        detail={"message": f"Value of {label} data must be float."},
                    )

            elif field_type == "number-int":
                try:
                    value = int(value)
                except:
                    raise HTTPException(
                        400,
                        detail={"message": f"Value of {label} data must be integer."},
                    )

            elif field_type in ["checkbox", "switch"]:
                try:
                    value = bool(value)
                except:
                    raise HTTPException(
                        400,
                        detail={"message": f"Value of {label} data must be boolean."},
                    )

            elif field_type == "datetime":
                try:
                    value = datetime.datetime.fromisoformat(
                        value.replace("Z", "+00:00")
                    )
                    value = str(value)
                except:
                    raise HTTPException(
                        400,
                        detail={
                            "message": f"Value of {label} data must be datetime with format %Y-%m-%d %H:%M:%S."
                        },
                    )

            elif field_type == "date":
                try:
                    value = datetime.datetime.strptime(value, "%Y-%m-%d").date()
                    value = str(value)
                except:
                    raise HTTPException(
                        400,
                        detail={
                            "message": f"Value of {label} data must be datetime with format %Y-%m-%d."
                        },
                    )

            elif field_type == "time":
                try:
                    value = value.split(":")
                    if len(value) == 3:
                        value = value[:-1]
                    value = ":".join(value)
                    value = datetime.datetime.strptime(value, "%H:%M").time()
                    value = str(value)
                except:
                    raise HTTPException(
                        400,
                        detail={"message": f"Value of {label} data must be time."},
                    )

            if isinstance(value, list):
                value = json.dumps(value)

            elif isinstance(value, dict):
                value = json.dumps(value)

            field_dat["value"] = value
            field_id = f'"{field_id}"'
            if not response_choice.startswith(("internal", "relation")):
                column_data.append(field_id)
                value_data.append(value)

        return field_data, column_data, value_data, junction_table_data

    def get_form_fields(self, id, db):
        form_field = db.query(form_orm.Form).get(id)
        if not form_field:
            raise HTTPException(
                400,
                detail={"message": "Template id not found."},
            )

        final_data = {}
        for i in form_field.fields:
            id = i.get("id")
            final_data[id] = {
                "label": i.get("label"),
                "deleted": i.get("deleted", False),
                "required": bool(i.get("required")),
            }
        return final_data, form_field.name

    def check_trigger_condition(self, instance, condition_config):
        conditions_mapping = condition_config.get("conditions_mapping", [])
        conditions = condition_config.get("conditions", [])

        combined_result_data = []
        for conditions_map in conditions_mapping:
            gate = conditions_map["gate"]
            groups = conditions_map["group"]

            result_data = []
            for group in groups:
                condition_data = conditions[group]

                column = condition_data["column"]
                instance_value = instance.get(column)
                if instance_value is None:
                    raise BadRequest_400(
                        message=f"{column} column doesnot exit in this form.",
                    )

                value = condition_data["value"]
                operator = condition_data["operator"].lower().strip().replace("_", " ")
                if operator not in [
                    "=",
                    "!=",
                    "in",
                    "not in",
                    "like",
                    "not like",
                    ">",
                    ">=",
                    "<",
                    "<=",
                    "between",
                ]:
                    continue

                if operator == "like":
                    result_data.append(str(value) in str(instance_value))

                elif operator == "not like":
                    result_data.append(str(value) not in str(instance_value))

                elif operator == "between":
                    start_item = value[0]
                    end_item = value[-1]
                    result_data.append(
                        instance_value <= start_item and instance_value >= end_item
                    )
                else:
                    result_data.append(eval(f'"{instance_value}" {operator} {value}'))

            if gate.lower() == "and":
                result_data = all(result_data)

            elif gate.lower() == "or":
                result_data = any(result_data)

            combined_result_data.append(result_data)

        return all(combined_result_data)

    async def save(
        self,
        templates: Schema,
        db: Session,
        background_tasks,
        current_user,
        organization,
        tenant_code,
        db_code,
        system_id,
    ):
        # if something changed here, please look into action_repository save function too
        seq = await seq_function(templates.template_id, db, current_user)
        if settings_instance.UAT_PRODUCTION:
            compressed_data = base64.b64decode(templates.fields)
            decompressed_data = zlib.decompress(compressed_data)
            original_string = decompressed_data.decode("utf-8")
            template_fields = json.loads(original_string)
        else:
            template_fields = templates.fields

        final_data, form_name = self.get_form_fields(id=templates.template_id, db=db)

        fields, column_data, value_data, junction_table_data = (
            self.data_convert_validation(
                field_data=template_fields,
                final_data=final_data,
            )
        )

        column_data.extend(["uid", "meta", "is_draft", "user_id", "created_by"])
        value_data.extend(
            [
                seq,
                json.dumps(templates.meta),
                templates.is_draft,
                current_user.id,
                current_user.id,
            ]
        )
        my_dict = {
            f"val{k}".replace('"', ""): v for k, v in zip(column_data, value_data)
        }

        value_d = [f":val{i}".replace('"', "") for i in column_data]
        value_d = ", ".join(value_d)
        value_d = f"({value_d})"

        column_data = ", ".join(column_data)
        raw_query = sql_insert_record(
            form_id=templates.template_id,
            schema_name=tenant_code,
            cols=column_data,
            vals=value_d,
        )
        templates_obj = db.execute(raw_query, my_dict)
        value = jsonable_encoder(templates_obj.fetchone())
        db.flush()

        if junction_table_data:
            column_data_junc = ", ".join(["table_1", "table_2"])
            for data in junction_table_data:
                value[data.get("id")] = data.get("value", [])

                for val in data.get("value"):
                    value_data = f'({value["id"]}, {val})'
                    junction_table = "jt_{0}_{1}".format(
                        templates.template_id, data.get("id")
                    )
                    raw_query = sql_insert_record_junction(
                        table_name=junction_table,
                        schema_name=tenant_code,
                        cols=column_data_junc,
                        vals=value_data,
                    )
                    db.execute(raw_query)

        triggers = (
            db.query(Trigger)
            .join(Trigger.workflow_obj)
            .where(
                Trigger.form_id == templates.template_id,
                Trigger.trigger_type == "TRG_DB",
                Trigger.trigger_method == "add",
                Trigger.source == "dyanmic_table",
                Trigger.deleted == False,
                Trigger.is_enabled == True,
            )
            .options(subqueryload(Trigger.workflow_obj))
            .all()
        )

        trigger_message = f'"{current_user.login_id}" has successfully created data for the {form_name} form.'
        if triggers:

            for trigger in triggers:
                trigger_type = trigger.trigger_type
                trigger_id = trigger.id

                value["trigger_id"] = f"{trigger_type}_{trigger_id}"
                event_dict = {"start": value}

                workflow_obj = jsonable_encoder(trigger.workflow_obj)
                if workflow_obj["deleted"]:
                    continue

                log_data, status = await transerve_workflow_using_bfs(
                    workflow_instance=workflow_obj,
                    start_node="start",
                    db=db,
                    schema_name=db_code,
                    event_dict=event_dict,
                    trigger_id=trigger.id,
                    trigger_message=trigger_message,
                    current_user_id=current_user.id,
                )
                if not status:
                    raise HTTPException(
                        400,
                        detail=log_data,
                    )

        db.commit()
        return value

    async def update(
        self,
        templates: Schema,
        id: int,
        organization,
        db: Session,
        background_tasks,
        current_user,
        tenant_code,
        db_code,
        system_id,
    ):
        final_data, form_name = self.get_form_fields(id=templates.template_id, db=db)
        if settings_instance.UAT_PRODUCTION:
            compressed_data = base64.b64decode(templates.fields)
            decompressed_data = zlib.decompress(compressed_data)
            original_string = decompressed_data.decode("utf-8")
            template_fields = json.loads(original_string)
        else:
            template_fields = templates.fields

        fields, column_data, value_data, junction_table_data = (
            self.data_convert_validation(
                field_data=template_fields,
                final_data=final_data,
            )
        )
        existing_junction_data = []
        for junc_table in junction_table_data:
            tbl_name = f"jt_{templates.template_id}_{junc_table.get('id')}"
            alias_id = junc_table.get("id")
            tbl_query = f"""
                SELECT ARRAY_AGG(tbl.table_2::TEXT) AS aggr_{alias_id}
                FROM {tenant_code}.{tbl_name} tbl
                WHERE tbl.table_1 = {id};
            """
            data = db.execute(tbl_query).fetchone()
            data = jsonable_encoder(data)
            existing_junction_data.append(data)

        create, delete = self.separate_data(junction_table_data, existing_junction_data)

        raw_query = get_data_details(
            table_name=f"table_{templates.template_id}",
            schema_name=tenant_code,
            record_id=id,
        )
        inspection_data = db.execute(raw_query).fetchone()
        if inspection_data:
            inspection_data = jsonable_encoder(inspection_data)
        else:
            raise HTTPException(
                400,
                detail={"message": f"Id doesn't exists"},
            )
        column_data.extend(["meta", "is_draft", "updated_by"])
        value_data.extend(
            [json.dumps(templates.meta), templates.is_draft, current_user.id]
        )

        my_dict = {
            f"val{k}".replace('"', ""): v for k, v in zip(column_data, value_data)
        }
        my_dict["data_id"] = id

        column_dat = [f":val{i}".replace('"', "") for i in column_data]
        set_clause = [f"{i} = {j}" for i, j in zip(column_data, column_dat)]
        set_clause = ", ".join(set_clause)

        raw_query = sql_update_record(
            form_id=templates.template_id,
            schema_name=tenant_code,
            set_clause=set_clause,
        )
        templates_obj = db.execute(raw_query, my_dict)
        value = jsonable_encoder(templates_obj.fetchone())

        if create:
            column_data_junc = ", ".join(["table_1", "table_2"])
            for data in create:
                value[data.get("id")] = data.get("value", [])

                junction_table = "jt_{0}_{1}".format(
                    templates.template_id, data.get("id")
                )
                for val in data.get("value"):
                    value_data = f"({id}, {val})"
                    raw_query = sql_insert_record_junction(
                        table_name=junction_table,
                        schema_name=tenant_code,
                        cols=column_data_junc,
                        vals=value_data,
                    )
                    db.execute(raw_query)

        if delete:
            for data in delete:
                junction_table = "jt_{0}_{1}".format(
                    templates.template_id, data.get("id")
                )
                for val in data.get("value"):
                    raw_query = sql_delete_record_junction(
                        table_name=junction_table,
                        schema_name=tenant_code,
                        record_id=id,
                        parent_id=val,
                    )
                    db.execute(raw_query)

        if templates.delete_files:
            for path in templates.delete_files:
                base_path = os.path.join(os.getcwd())
                path = os.path.join(base_path, path)
                if os.path.exists(path):
                    os.remove(path)

        triggers = (
            db.query(Trigger)
            .join(Trigger.workflow_obj)
            .where(
                Trigger.form_id == templates.template_id,
                Trigger.trigger_type == "TRG_DB",
                Trigger.trigger_method == "mod",
                Trigger.source == "dyanmic_table",
                Trigger.deleted == False,
                Trigger.is_enabled == True,
            )
            .options(subqueryload(Trigger.workflow_obj))
            .all()
        )

        trigger_message = f'"{current_user.login_id}" has successfully updated data for the {form_name} form.'
        if triggers:
            for trigger in triggers:
                trigger_type = trigger.trigger_type
                trigger_id = trigger.id

                value["trigger_id"] = f"{trigger_type}_{trigger_id}"
                event_dict = {"start": value}

                workflow_obj = jsonable_encoder(trigger.workflow_obj)
                if workflow_obj["deleted"]:
                    continue

                log_data, status = await transerve_workflow_using_bfs(
                    workflow_instance=workflow_obj,
                    start_node="start",
                    db=db,
                    schema_name=db_code,
                    event_dict=event_dict,
                    trigger_id=trigger.id,
                    trigger_message=trigger_message,
                    current_user_id=current_user.id,
                )
                if not status:
                    raise HTTPException(
                        400,
                        detail=log_data,
                    )

        db.commit()
        return value

    async def update_meta(
        self,
        schema: UpdateMetaSchema,
        id: int,
        db: Session,
        current_user,
        tenant_code,
        system_id,
    ):
        raw_query = get_data_details(
            table_name=f"table_{schema.template_id}",
            schema_name=tenant_code,
            record_id=id,
        )
        inspection_data = db.execute(raw_query).fetchone()
        if inspection_data:
            inspection_data = jsonable_encoder(inspection_data)
        else:
            raise HTTPException(
                400,
                detail={"message": f"Id doesn't exists"},
            )

        column_data = ["meta", "updated_by"]
        value_data = [json.dumps(schema.meta), current_user.id]

        my_dict = {
            f"val{k}".replace('"', ""): v for k, v in zip(column_data, value_data)
        }
        my_dict["data_id"] = id

        column_dat = [f":val{i}".replace('"', "") for i in column_data]
        set_clause = [f"{i} = {j}" for i, j in zip(column_data, column_dat)]
        set_clause = ", ".join(set_clause)

        raw_query = sql_update_record(
            form_id=schema.template_id,
            schema_name=tenant_code,
            set_clause=set_clause,
        )

        templates_obj = db.execute(raw_query, my_dict)
        db.commit()
        data = jsonable_encoder(templates_obj.fetchone())
        return data

    async def restore_data(
        self,
        schema: BulkDeleteSchema,
        db: Session,
        tenant_code,
        db_code,
        current_user,
    ):
        triggers = (
            db.query(Trigger)
            .join(Trigger.workflow_obj)
            .where(
                Trigger.form_id == schema.template_id,
                Trigger.trigger_type == "TRG_DB",
                Trigger.trigger_method == "restore",
                Trigger.source == "dyanmic_table",
                Trigger.deleted == False,
                Trigger.is_enabled == True,
            )
            .options(subqueryload(Trigger.workflow_obj))
            .all()
        )

        final_data, form_name = self.get_form_fields(id=schema.template_id, db=db)

        for id in schema.config_ids:
            raw_query = get_data_details(
                table_name=f"table_{schema.template_id}",
                schema_name=tenant_code,
                record_id=id,
            )

            inspection_data = db.execute(raw_query).fetchone()
            if inspection_data:
                inspection_data = jsonable_encoder(inspection_data)
            else:
                raise HTTPException(
                    400,
                    detail={"message": f"Id doesn't exists"},
                )

            my_dict = {"valdeleted": False, "data_id": id}
            set_clause = "deleted =:valdeleted"

            raw_query = sql_update_record(
                form_id=schema.template_id,
                schema_name=tenant_code,
                set_clause=set_clause,
            )

            db.execute(raw_query, my_dict)

            if not triggers:
                continue

            trigger_message = f'"{current_user.login_id}" has successfully restored row {id} for the {form_name} form.'
            inspection_data["deleted"] = False

            for trigger in triggers:
                trigger_type = trigger.trigger_type
                trigger_id = trigger.id

                inspection_data["trigger_id"] = f"{trigger_type}_{trigger_id}"
                event_dict = {"start": inspection_data}

                workflow_obj = jsonable_encoder(trigger.workflow_obj)
                if workflow_obj["deleted"]:
                    continue

                log_data, status = await transerve_workflow_using_bfs(
                    workflow_instance=workflow_obj,
                    start_node="start",
                    db=db,
                    schema_name=db_code,
                    event_dict=event_dict,
                    trigger_id=trigger.id,
                    trigger_message=trigger_message,
                    current_user_id=current_user.id,
                )
                if not status:
                    raise HTTPException(
                        400,
                        detail=log_data,
                    )

        db.commit()

    async def bulk_delete(
        self,
        schema: BulkDeleteSchema,
        db: Session,
        background_tasks,
        current_user,
        organization,
        tenant_code,
        db_code,
    ):
        triggers = (
            db.query(Trigger)
            .join(Trigger.workflow_obj)
            .where(
                Trigger.form_id == schema.template_id,
                Trigger.trigger_type == "TRG_DB",
                Trigger.trigger_method == "del",
                Trigger.source == "dyanmic_table",
                Trigger.deleted == False,
                Trigger.is_enabled == True,
            )
            .options(subqueryload(Trigger.workflow_obj))
            .all()
        )

        final_data, form_name = self.get_form_fields(id=schema.template_id, db=db)
        for id in schema.config_ids:
            raw_query = get_data_details(
                table_name=f"table_{schema.template_id}",
                schema_name=tenant_code,
                record_id=id,
            )

            inspection_data = db.execute(raw_query).fetchone()
            if inspection_data:
                inspection_data = jsonable_encoder(inspection_data)
            else:
                raise HTTPException(
                    400,
                    detail={"message": f"Id doesn't exists"},
                )

            my_dict = {"valdeleted": True, "data_id": id}
            set_clause = "deleted =:valdeleted"

            raw_query = sql_update_record(
                form_id=schema.template_id,
                schema_name=tenant_code,
                set_clause=set_clause,
            )

            db.execute(raw_query, my_dict)

            if not triggers:
                continue

            inspection_data["deleted"] = True

            trigger_message = f'"{current_user.login_id}" has successfully restored row {id} for the {form_name} form.'

            for trigger in triggers:
                trigger_type = trigger.trigger_type
                trigger_id = trigger.id

                inspection_data["trigger_id"] = f"{trigger_type}_{trigger_id}"
                event_dict = {"start": inspection_data}

                workflow_obj = jsonable_encoder(trigger.workflow_obj)
                if workflow_obj["deleted"]:
                    continue

                log_data, status = await transerve_workflow_using_bfs(
                    workflow_instance=workflow_obj,
                    start_node="start",
                    db=db,
                    schema_name=db_code,
                    event_dict=event_dict,
                    trigger_id=trigger.id,
                    trigger_message=trigger_message,
                    current_user_id=current_user.id,
                )
                if not status:
                    raise HTTPException(
                        400,
                        detail=log_data,
                    )

        db.commit()

    async def bulk_save(
        self,
        templates: typing.List[Schema],
        db: Session,
        background_tasks,
        current_user,
        organization,
        tenant_code,
        system_id,
    ):
        for i in templates:
            seq = await seq_function(i.template_id, db, current_user)
            if settings_instance.UAT_PRODUCTION:
                compressed_data = base64.b64decode(i.fields)
                decompressed_data = zlib.decompress(compressed_data)
                original_string = decompressed_data.decode("utf-8")
                template_fields = json.loads(original_string)
            else:
                template_fields = i.fields

            final_data, form_name = self.get_form_fields(id=i.template_id, db=db)
            fields, column_data, value_data, junction_table_data = (
                self.data_convert_validation(
                    field_data=template_fields,
                    final_data=final_data,
                )
            )

            column_data.extend(["uid", "meta", "is_draft", "user_id", "created_by"])
            value_data.extend(
                [
                    seq,
                    json.dumps(i.meta),
                    i.is_draft,
                    current_user.id,
                    current_user.id,
                ]
            )
            my_dict = {
                f"val{k}".replace('"', ""): v for k, v in zip(column_data, value_data)
            }

            value_d = [f":val{i}".replace('"', "") for i in column_data]
            value_d = ", ".join(value_d)
            value_d = f"({value_d})"

            column_data = ", ".join(column_data)

            raw_query = sql_insert_record(
                form_id=i.template_id,
                schema_name=tenant_code,
                cols=column_data,
                vals=value_d,
            )
            templates_obj = db.execute(raw_query, my_dict)
            value = jsonable_encoder(templates_obj.fetchone())
            if junction_table_data:
                column_data_junc = ", ".join(["table_1", "table_2"])
                for data in junction_table_data:
                    value[data.get("id")] = data.get("value", [])

                    for val in data.get("value"):
                        value_data = f'({value["id"]}, {val})'
                        junction_table = "jt_{0}_{1}".format(
                            templates.template_id, data.get("id")
                        )
                        raw_query = sql_insert_record_junction(
                            table_name=junction_table,
                            schema_name=tenant_code,
                            cols=column_data_junc,
                            vals=value_data,
                        )
                        db.execute(raw_query)
            db.flush()
