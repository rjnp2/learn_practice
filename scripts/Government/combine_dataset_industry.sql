CREATE TABLE combined_dataset AS
SELECT 
    field_id, schema_name,industry_id,
    industry_code, industry_name,
    DENSE_RANK() OVER (ORDER BY system_code) AS system_id,
    system_code,
    system_name, system_description,
    DENSE_RANK() OVER (ORDER BY system_code, menu_code) AS menu_id,
    menu_name, menu_description, menu_code,
    menu_is_pinned, menu_element, menu_order_index,
    DENSE_RANK() OVER (ORDER BY system_code, menu_code, submenu_code) AS submenu_id,
    submenu_name, submenu_description, submenu_code, submenu_is_pinned,
    submenu_order_index,submenu_element,submenu_layout,
    submenu_parent_id,submenu_show_in_sidebar, submenu_action,
    table_name,table_description,column_name,field_type,field_order,
    field_label,field_placeholder,column_default,field_required,field_min,
    field_max,character_maximum_length,numeric_precision,reference_table,
    reference_column,field_multiple,form_group,form_ordering,
    field_label_length,field_parent,field_position,field_use_css,field_default_css,
    field_custom_css, field_component, field_response_choice, field_value, index_field,
    field_validator,field_function, tab_no,tab_name
FROM 
(
    SELECT * from public.gov_edm_combined_dataset
    UNION ALL
    SELECT * from public.gov_ems_combined_dataset
    UNION ALL
    SELECT * from public.gov_fmb_combined_dataset
    UNION ALL
    SELECT * from public.gov_gis_combined_dataset
    UNION ALL
    SELECT * from public.gov_gss_combined_dataset
    UNION ALL
    SELECT * from public.gov_hph_combined_dataset
    UNION ALL
    SELECT * from public.gov_hrm_combined_dataset
    UNION ALL
    SELECT * from public.gov_lep_combined_dataset
    UNION ALL
    SELECT * from public.gov_prd_combined_dataset
    UNION ALL
    SELECT * from public.gov_rce_combined_dataset
    UNION ALL
    SELECT * from public.gov_tms_combined_dataset
    UNION ALL
    SELECT * from public.gov_ums_combined_dataset
    UNION ALL
    SELECT * from public.gov_vem_combined_dataset
)
