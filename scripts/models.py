import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

from technical.settings.database import Base, BaseModel


class Inspection(BaseModel, Base):
    __tablename__ = "inspection"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    uid = sa.Column(sa.String, nullable=True, unique=True)
    template_id = sa.Column(sa.ForeignKey("public.form.id"), nullable=False)
    inspection_status = sa.Column(sa.ForeignKey("general_status.id"), nullable=True)
    user_id = sa.Column(sa.ForeignKey("public.user.id"), nullable=False)
    fields = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )
    meta = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default={},
        server_default="{}",
    )

    is_draft = sa.Column(sa.Boolean, nullable=False, default=False)
    created_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=False)
    updated_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)
    template = sa.orm.relationship("Form", foreign_keys=[template_id])
    user = sa.orm.relationship("User", foreign_keys=[user_id])
    inspection_status_obj = sa.orm.relationship(
        "GeneralStatus", foreign_keys=[inspection_status]
    )

    access_rule = sa.orm.relationship("AccessRule", back_populates="form_inspection")
