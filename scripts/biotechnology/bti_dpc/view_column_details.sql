CREATE OR REPLACE VIEW bti_dpc.bti_dpc_column_details AS 
SELECT
    c1.table_schema :: varchar,
    (
        SELECT DISTINCT c2.column_default AS column_default
        FROM information_schema.columns c2
        WHERE c2.table_schema = c1.table_schema
          AND c2.table_name = c1.table_name
          AND c2.column_name = 'submenu_id'
          AND c2.table_name <> 'submenu'
    )::integer AS subq,
    c1.table_name::varchar,
    (CASE
        WHEN c1.column_default like '%change to system_user%' then 'system_user'
        else c1.column_name
    end) ::varchar as column_name,
    (CASE
        WHEN c1.column_default like '%change to system_user%' then 'System User'
        WHEN fk.reference_table is not null THEN initcap(regexp_replace(c1.column_name, '_id', '', 'g'))
        else initcap(regexp_replace(c1.column_name, '_', ' ', 'g'))
    end)::text AS field_label,
    dense_rank() OVER (PARTITION BY c1.table_name ORDER BY c1.ordinal_position)::integer AS field_order,
     (CASE
        WHEN c1.column_default like '%change to system_user%' then NULL
        else c1.column_default
    end)::text AS column_default,
    CASE
        WHEN c1.column_default = 'CURRENT_TIMESTAMP' THEN 
            false
        ELSE
            true
    END::boolean AS field_required,
    case
        WHEN c1.column_default like '%change to system_user%' then '1'
        WHEN c1.column_name LIKE '%_color' THEN 'color_picker'
        WHEN c1.column_name ILIKE '%email%' THEN 'email'
        WHEN c1.column_name ILIKE '%file' OR
             c1.column_name ILIKE '%document' OR
             c1.column_name ILIKE '%script' OR
             c1.column_name ILIKE '%qr_code' OR
             c1.column_name ILIKE '%icon' THEN 'file'
        WHEN c1.column_name ILIKE '%image' OR
             c1.column_name ILIKE '%barcode' OR
             c1.column_name ILIKE '%picture' OR
             c1.column_name ILIKE '%snapshot' OR
             c1.column_name ILIKE '%icon' THEN 'image'
        WHEN c1.column_name ILIKE '%phone' OR
             c1.column_name ILIKE '%phone_number' OR
             c1.column_name ILIKE '%contact' OR
             c1.column_name ILIKE '%contract_number' THEN 'telephone'
        WHEN c1.column_name ILIKE '%link' OR
             c1.column_name ILIKE '%url' OR
             c1.column_name ILIKE '%file_path' OR
             c1.column_name ILIKE '%profile_path' OR
             c1.column_name ILIKE '%image_path' OR
             c1.column_name ILIKE '%storage_location' OR
             c1.column_name ILIKE '%document_path' THEN 'url'
        WHEN c1.udt_name IN ('smallint', 'integer', 'bigint', 'decimal', 'numeric', 'real', 'double precision', 'integer[]', 'int8', 'int4', 'numeric', 'float8', '_float*') THEN 'number'
        WHEN c1.udt_name IN ('character varying', 'varchar', 'character', 'char', 'varchar[]', 'bit', 'bit varying', 'uuid', 'hstore', 'daterange', 'bpchar', 'interval', 'varcha', '_char', 'r') THEN 'text'
        WHEN c1.udt_name IN ('text[]', 'text', '_text') THEN 'text_area'
        WHEN c1.udt_name = 'bytea' THEN 'file'
        WHEN c1.udt_name IN ('date', '_date', '_date') THEN 'date'
        WHEN c1.udt_name IN ('time', '_time') THEN 'time'
        WHEN c1.udt_name IN ('timestamp', 'timestamptz') THEN 'datetime'
        WHEN c1.udt_name IN ('boolean', 'bool', '_bool') THEN 'checkbox'
        WHEN c1.udt_name = 'enum' THEN 'checkbox'
        WHEN c1.udt_name IN ('json', 'jsonb') THEN 'text_area'
        WHEN c1.udt_name = 'xml' THEN 'text_area'
        ELSE c1.udt_name
    END::text AS field_type,
    c1.character_maximum_length::varchar,
    c1.numeric_precision::integer,
    (CASE
        WHEN c1.column_default like '%change to system_user%' then 'system_user'
        else COALESCE(fk.reference_table, 'No')
    end)::varchar AS reference_table,
    (CASE
        WHEN c1.column_default like '%change to system_user%' then 'id'
        else COALESCE(fk.reference_column, 'No')
    end)::varchar AS reference_column,
    CASE
        WHEN c1.ordinal_position <= 8 THEN NULL::text
        ELSE initcap(regexp_replace((c1.table_name || ' ') || split_part(initcap(regexp_replace(c1.column_name, '_', ' ', 'g')), ' ', 1), '_', ' ', 'g'))
    END::varchar AS tab_name,
    CASE
        WHEN c1.ordinal_position <= 8 THEN 0
        ELSE (c1.ordinal_position - 1) / 8
    END::integer AS tab_no,
    (
        SELECT pg_description.description
        FROM pg_description
        WHERE pg_description.objoid = (c1.table_schema || '.' || c1.table_name)::regclass::oid
        and objsubid = 0
        LIMIT 1
    )::text AS table_description,
    (
        SELECT pg_description.description
        FROM pg_description
        WHERE pg_description.objoid = (c1.table_schema || '.' || c1.table_name)::regclass::oid
        and objsubid = c1.ordinal_position::integer
        LIMIT 1
    )::text AS column_description
FROM information_schema.columns c1
LEFT JOIN (SELECT DISTINCT tc.table_schema AS schema_name,
    tc.table_name,
    kcu.column_name,
    ccu.table_name AS reference_table,
    ccu.column_name AS reference_column
    FROM information_schema.table_constraints tc
    JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
    JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name
    WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_schema = 'bti_dpc'
    ) fk ON c1.table_schema = 'bti_dpc'
AND c1.table_name = fk.table_name AND c1.column_name = fk.column_name
WHERE c1.table_schema = 'bti_dpc' AND c1.column_name <> 'id'
and c1.table_schema NOT IN ('information_schema', 'pg_catalog', 'public')
and c1.table_name IN (
    SELECT tb.table_name
    FROM information_schema.tables as tb
    WHERE table_type = 'BASE TABLE'
    and table_schema = c1.table_schema
)
and c1.column_name not in ('created_at','created_by','created_date','update_time','update_timestamp','updated_at',
    'updated_by','created_by_user_id','created_on','created_timestamp','createdat','update_date',
    'updated_by_user_id','updated_on','updated_timestamp','updatedat', 'last_login','lastactivity')
and (column_default IS NULL or c1.column_default not like '%need to remove%')
and (c1.table_name not like '%role%' and c1.table_name not like '%permission%' and c1.table_name not like '%group%')
and c1.table_name not in ('audit_logs','user_activity_logs', 'recent_activities', 'audit_trail', 'audit_trail_logs')
and (fk.reference_table is null or (fk.reference_table not like '%role%' and fk.reference_table not like '%permission%' and fk.reference_table not like '%group%'))
ORDER BY table_name, subq, field_order;