from collections import OrderedDict

import pandas as pd
import psycopg2

# from database_query.ssh_db import SSH
from sshtunnel import SSHTunnelForwarder

cred = {
    "host": "localhost",
    "database": "Dalfin_Configuration",
    "user": "admin",
    "password": "CQ9irmBF8l7lxxJ",
    "port": "5433",
    "ssh_host": "64.23.209.39",
    "ssh_username": "root",
    "ssh_password": "/Users/rajanpandey/.ssh/id_braintip",
}


class SSH:
    def __init__(self, database=None) -> None:
        if database:
            tunnel = SSHTunnelForwarder(
                (cred.get("ssh_host"), 22),  # Replace with your SSH server details
                ssh_username=cred.get("ssh_username"),
                ssh_pkey=cred.get("ssh_password"),
                remote_bind_address=(
                    "localhost",
                    5433,
                ),  # Replace with your PostgreSQL server details
            )
            # PostgreSQL connection setup
            tunnel.start()

            self.conn = psycopg2.connect(
                user="admin",
                password="CQ9irmBF8l7lxxJ",
                host="127.0.0.1",
                port=tunnel.local_bind_port,
                database=database or cred.get("database"),
            )
        else:
            self.conn = psycopg2.connect(
                user="postgres",
                password="postgres",
                host="127.0.0.1",
                port=5433,
                database=database,
            )

        self.cursor = self.conn.cursor()

    def display_data(self):
        self.cursor.execute("select * from dalfin.system_model order by -id;")
        data = self.cursor.fetchall()
        columns = [desc[0] for desc in self.cursor.description]
        df = pd.DataFrame(data, columns=columns)
        return df

    def get_table_name(self):
        self.cursor.execute(
            "select distinct table_name from information_schema.tables where table_schema = 'public' and table_type = 'BASE TABLE' and table_name like '%_combined_dataset'"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def system_code(self, industry_name):
        self.cursor.execute(
            f"select distinct system_code from dalfin.system_code where industry_name='{industry_name}';"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def get_category(self):
        self.cursor.execute(
            "select distinct industry_name from dalfin.system_code order by industry_name;"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def get_data(self, query):
        self.cursor.execute(query)
        data = self.cursor.fetchall()
        column_names = [desc[0] for desc in self.cursor.description]
        self.conn.commit()
        return data, column_names

    def get_single_data(self, query):
        self.cursor.execute(query)
        data = self.cursor.fetchone()
        column_names = [desc[0] for desc in self.cursor.description]
        self.conn.commit()
        return data, column_names

    def insert_data(self, query, insert_data):
        self.cursor.execute(query, insert_data)
        self.conn.commit()

    def update_bulk_data(self, query, insert_data):
        self.cursor.execute(query, insert_data)

    def insert_data_with_return_id(self, query, insert_data):
        self.cursor.execute(query, insert_data)
        id = self.cursor.fetchone()[0]
        self.conn.commit()
        return id

    def close_db(self):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()


def get_save_global_data(database, schema_name=None):
    # schema_name = schema_name.lower().replace('-', '_')
    # and table_schema = '{schema_name}'
    ssh = SSH(database=database.strip())
    final_data = []

    sql_query = f"""
    SELECT table_catalog, table_schema, table_name, (ARRAY_AGG(column_name))::text[] AS primary_key_columns
    FROM information_schema.columns
    where table_name IN (
    SELECT tb.table_name
        FROM information_schema.tables as tb
        WHERE table_type = 'BASE TABLE'
        and table_name not in ('menu', 'submenu')
        and table_schema not in ('information_schema', 'pg_catalog', 'public')
        and table_schema = information_schema.columns.table_schema
        and (xpath('/row/cnt/text()', query_to_xml(format('SELECT COUNT(*) AS cnt FROM %I.%I', table_schema, table_name), false, true, '')))[1]::text::int > 0
    )
    AND not EXISTS (
		SELECT *
		FROM information_schema.table_constraints AS tc
		JOIN information_schema.key_column_usage AS kcu
		ON tc.constraint_name = kcu.constraint_name
		WHERE tc.constraint_type = 'PRIMARY KEY'
		 and kcu.column_name = information_schema.columns.column_name
		 and tc.table_schema = information_schema.columns.table_schema
		 and kcu.table_name = information_schema.columns.table_name
	)
    GROUP BY table_catalog, table_schema, table_name
    order by table_schema,table_name;
    """
    data, _ = list(ssh.get_data(query=sql_query))

    for dat in data:
        if not dat:
            continue

        database = dat[0].lower().strip()
        schema_name = dat[1].lower().strip()
        table_name = dat[2].lower().strip()
        column_name = dat[3][0]

        sql_query = f"select {column_name} from {schema_name}.{table_name}"
        results, _ = list(ssh.get_data(query=sql_query))

        name = [
            table_name.split("_")[0],
        ] + column_name.split("_")
        name = OrderedDict.fromkeys(name).keys()

        dat = {
            "database": database,
            "schema_name": schema_name,
            "table_name": table_name,
            "column_name": " ".join(name).strip().title(),
        }
        results = [{**dat, **dict(zip(["field_value"], row))} for row in results]
        final_data.extend(results)

    print(f"Global record of", len(final_data))
    ssh.close_db()
    ssh = SSH(database="Dalfin_Configuration")
    for result in final_data:
        # Define your SQL query
        sql_query = f"""SELECT 1 FROM dalfin.global_relation
        WHERE database_name = '{result['database']}'
        AND schema_name = '{result['schema_name']}'
        AND table_name = '{result['table_name']}'
        AND column_name = '{result['column_name']}'
        AND field_value = '{result['field_value']}';"""

        results, _ = list(ssh.get_single_data(query=sql_query))
        if results:
            continue

        # Define your SQL query
        sql_query = f"""SELECT 1 FROM dalfin.global_relation
        WHERE database_name = '{result['database']}'
        AND schema_name = '{result['schema_name']}'
        AND table_name = '{result['table_name']}'
        AND column_name = '{result['column_name']}';"""

        results, _ = list(ssh.get_single_data(query=sql_query))
        if results:
            sql_query = f"""SELECT MAX(field_id) AS max_field_id FROM dalfin.global_relation
            WHERE database_name = '{result['database']}'
            AND schema_name = '{result['schema_name']}'
            AND table_name = '{result['table_name']}'
            AND column_name = '{result['column_name']}';"""

            results, _ = list(ssh.get_single_data(query=sql_query))
            max_field_id = results[0]
        else:
            sql_query = (
                "SELECT MAX(field_id) AS max_field_id FROM dalfin.global_relation"
            )
            results, _ = list(ssh.get_single_data(query=sql_query))
            max_field_id = results[0] or 0
            max_field_id += 1

        sql_query = """
            INSERT INTO dalfin.global_relation (database_name, schema_name, table_name, column_name, field_value, field_id)
            VALUES (%s, %s, %s, %s, %s, %s);
        """

        insert_data = (
            result["database"],
            result["schema_name"],
            result["table_name"],
            result["column_name"],
            result["field_value"],
            max_field_id,
        )
        ssh.insert_data(query=sql_query, insert_data=insert_data)

    ssh.close_db()


if __name__ == "__main__":
    get_save_global_data(database="Telecommunications")
