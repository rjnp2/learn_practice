from cgi import test
import random
import string
from itertools import zip_longest

import numpy as np
import pandas as pd
import psycopg2

# from database_query.ssh_db import SSH
from sshtunnel import SSHTunnelForwarder

cred = {
    "host": "localhost",
    "database": "Dalfin_Configuration",
    "user": "admin",
    "password": "CQ9irmBF8l7lxxJ",
    "port": "5433",
    "ssh_host": "64.23.209.39",
    "ssh_username": "root",
    "ssh_password": "/Users/igloo/.ssh/id_braintip",
}


class SSH:
    def __init__(self, database=None) -> None:
        # if database is None:
        #     tunnel = SSHTunnelForwarder(
        #         (cred.get("ssh_host"), 22),  # Replace with your SSH server details
        #         ssh_username=cred.get("ssh_username"),
        #         ssh_pkey=cred.get("ssh_password"),
        #         remote_bind_address=(
        #             "localhost",
        #             5433,
        #         ),  # Replace with your PostgreSQL server details
        #     )
        #     # PostgreSQL connection setup
        #     tunnel.start()

        #     self.conn = psycopg2.connect(
        #         user="admin",
        #         password="admin",
        #         host="127.0.0.1",
        #         port=tunnel.local_bind_port,
        #         database= database or cred.get("database"),
        #     )
        # else:
        #     self.conn = psycopg2.connect(
        #         user="postgres",
        #         password="postgres",
        #         host="127.0.0.1",
        #         port=5433,
        #         database= database,
        #     )

        tunnel = SSHTunnelForwarder(
            (cred.get("ssh_host"), 22),  # Replace with your SSH server details
            ssh_username=cred.get("ssh_username"),
            ssh_pkey=cred.get("ssh_password"),
            remote_bind_address=(
                "localhost",
                5433,
            ),  # Replace with your PostgreSQL server details
        )
        # PostgreSQL connection setup
        tunnel.start()

        self.conn = psycopg2.connect(
            user="admin",
            password="CQ9irmBF8l7lxxJ",
            host="127.0.0.1",
            port=tunnel.local_bind_port,
            database=database or cred.get("database"),
        )

        self.cursor = self.conn.cursor()

    def display_data(self):
        self.cursor.execute("select * from dalfin.system_model order by -id;")
        data = self.cursor.fetchall()
        columns = [desc[0] for desc in self.cursor.description]
        df = pd.DataFrame(data, columns=columns)
        return df

    def get_table_name(self):
        self.cursor.execute(
            "select distinct table_name from information_schema.tables where table_schema = 'public' and table_type = 'BASE TABLE' and table_name like '%_combined_dataset'"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def system_code(self, industry_name):
        self.cursor.execute(
            f"select distinct system_code from dalfin.system_code where industry_name='{industry_name}';"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def get_category(self):
        self.cursor.execute(
            "select distinct industry_name from dalfin.system_code order by industry_name;"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def get_data(self, query):
        self.cursor.execute(query)
        data = self.cursor.fetchall()
        column_names = [desc[0] for desc in self.cursor.description]
        self.conn.commit()
        return data, column_names

    def get_single_data(self, query):
        self.cursor.execute(query)
        data = self.cursor.fetchone()
        column_names = [desc[0] for desc in self.cursor.description]
        self.conn.commit()
        return data, column_names

    def insert_data(self, query, insert_data):
        self.cursor.execute(query, insert_data)
        self.conn.commit()

    def update_bulk_data(self, query, insert_data):
        self.cursor.execute(query, insert_data)

    def insert_data_with_return_id(self, query, insert_data):
        self.cursor.execute(query, insert_data)
        id = self.cursor.fetchone()[0]
        self.conn.commit()
        return id

    def close_db(self):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()


def find_submenu_and_create_relation(data, table_name, cursor, relation_data):
    submenu_data = data[data["table_name"] == table_name]
    if submenu_data.empty:
        return None

    submenu_data = submenu_data.sort_values(by="field_order")
    submenu_dat = submenu_data[submenu_data["field_type"] == "text"]
    if not submenu_dat.empty:
        submenu_dict = submenu_dat.iloc[0].to_dict()
    else:
        submenu_dict = submenu_data.iloc[0].to_dict()

    multiple = False
    separator = []
    reference_column = []
    if submenu_dict["field_response_choice"] not in ("default", "internal"):
        multiple = True
        separator = ["|", "|"]  # " | "
        reference_column = submenu_data["field_id"].head(3).tolist()

    insert_query = "INSERT INTO dalfin.form_form_relation (database_name, schema_name, table_name, column_name, submenu_code, field_id, multiple, separator, reference_column) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING id"
    system_code = submenu_dict["system_code"].lower().replace("-", "_")
    submenu_code = submenu_dict["submenu_code"].lower().replace("-", "_")
    table_name = submenu_dict["table_name"].lower()

    name = f"{table_name.title().replace('_', ' ')} {submenu_dict['field_label']}"
    data_to_insert = (
        submenu_dict["industry_name"].lower(),
        system_code,
        table_name,
        name,
        submenu_code,
        submenu_dict["field_id"],
        multiple,
        separator,
        reference_column,
    )
    inserted_id = cursor.insert_data_with_return_id(insert_query, data_to_insert)
    relation_data[f"{system_code}-{table_name}"] = inserted_id
    return inserted_id


def generate_random_string(length=8, generated_strings=None):
    if generated_strings is None:
        generated_strings = []
    characters = string.ascii_letters + string.digits
    random_string = "".join(random.choice(characters) for _ in range(length))
    while random_string in generated_strings:
        random_string = "".join(random.choice(characters) for _ in range(length))
    return random_string


def check_index_field(value):
    if not value:
        return None

    value = value.strip().lower().split(",")
    index_field = [
        i.strip()
        for i in value
        if i.strip() in ["title", "hover", "start_date", "end_date", "link"]
    ]
    return ",".join(index_field)


def check_field_validator(inti_value, group_data):
    if not inti_value:
        return None

    value = inti_value.strip().lower().split(",")
    if value[0] not in [">", "<", ">=", "<=", "="]:
        print(inti_value, "operator")
        return f"{inti_value}:invalid"

    operand = value[1]
    if operand != "now":
        operand = [
            i["field_id"]
            for i in group_data
            if (operand == i["column_name"] or operand == str(i["field_id"]))
        ]
        operand = str(operand[0]) if operand else None

    if not operand:
        table = group_data[0].get("table_name")
        print(inti_value, table)
        return f"{inti_value}:invalid"

    return ",".join([value[0], operand])


def check_field_function(inti_value, group_data):
    if not inti_value:
        return None

    value = inti_value.lower().split(",")
    if value[0] not in ["sum", "sub", "multiply"]:
        print(inti_value)
        return f"{inti_value}:invalid"

    operand_1 = value[1]
    operand_1 = [
        i["field_id"]
        for i in group_data
        if (operand_1 == i["column_name"] or operand_1 == str(i["field_id"]))
    ]
    operand_1 = str(operand_1[0]) if operand_1 else None

    if not operand_1:
        print(inti_value)
        return f"{inti_value}:invalid"

    operand_2 = value[1]
    operand_2 = [
        i["field_id"]
        for i in group_data
        if (operand_2 == i["column_name"] or operand_2 == str(i["field_id"]))
    ]
    operand_2 = str(operand_2[0]) if operand_2 else None

    if not operand_2:
        print(inti_value)
        return f"{inti_value}:invalid"

    return ",".join([value[0], operand_1, operand_2])


def set_value_from_relation(element, global_data, relation_data, data, ssh_dalfin):
    found = False
    reference_table = element["reference_table"].lower()
    system_code = element["system_code"].replace("-", "_").lower()

    if reference_table == element["table_name"]:
        print("circular_relation", reference_table, element["table_name"])
        input_val = input("What to do: return or continue?")
        if input_val == "return":
            element["field_type"] = f"circular_relation-{element['field_type']}"
            return element

    if element["field_response_choice"] == "global":
        valu = f"{system_code}-{reference_table}"
        valu = global_data.get(valu)
        if valu:
            valu = str(valu)
            found = True
            field_type = valu
    else:
        valu = f"{system_code}-{reference_table}"
        valu = relation_data.get(valu) or find_submenu_and_create_relation(
            data, reference_table, ssh_dalfin, relation_data
        )

        if valu:
            valu = str(valu)
            found = True
            field_type = valu

    if not found:
        print(
            system_code, element["table_name"], element["column_name"], reference_table
        )
        field_type = f"not_found-{element['field_type']}"

    element["field_type"] = field_type
    return element


def get_update_data(database=None, table_name=None):
    tables = {
        "Aerospace": [
            "aoi_amf_combined_dataset",
            "aoi_arc_combined_dataset",
            "aoi_atc_combined_dataset",
            "aoi_atd_combined_dataset",
            "aoi_ats_combined_dataset",
            "aoi_fca_combined_dataset",
            "aoi_fdb_combined_dataset",
            "aoi_fpo_combined_dataset",
            "aoi_plm_combined_dataset",
            "aoi_smp_combined_dataset",
        ],
        "Agriculture": [
            "agr_afm_combined_dataset",
            "agr_cmp_combined_dataset",
            "agr_csa_combined_dataset",
            "agr_fea_combined_dataset",
            "agr_fms_combined_dataset",
            "agr_ims_combined_dataset",
            "agr_isc_combined_dataset",
            "agr_lms_combined_dataset",
            "agr_mas_combined_dataset",
            "agr_pas_combined_dataset",
            "agr_rsd_combined_dataset",
            "agr_sec_combined_dataset",
            "agr_wfm_combined_dataset",
        ],
        "Automotive": [
            "ati_ads_combined_dataset",
            "ati_aes_combined_dataset",
            "ati_ard_combined_dataset",
            "ati_avs_combined_dataset",
            "ati_cst_combined_dataset",
            "ati_eec_combined_dataset",
            "ati_evc_combined_dataset",
            "ati_fms_combined_dataset",
            "ati_ihm_combined_dataset",
            "ati_mes_combined_dataset",
            "ati_obd_combined_dataset",
            "ati_pmc_combined_dataset",
            "ati_scm_combined_dataset",
            "ati_sms_combined_dataset",
            "ati_tcs_combined_dataset",
            "ati_vts_combined_dataset",
        ],
        "Biotechnology": [
            "bti_bda_combined_dataset",
            "bti_bma_combined_dataset",
            "bti_bsm_combined_dataset",
            "bti_cts_combined_dataset",
            "bti_ddc_combined_dataset",
            "bti_dpc_combined_dataset",
            "bti_eln_combined_dataset",
            "bti_emp_combined_dataset",
            "bti_lar_combined_dataset",
            "bti_lms_combined_dataset",
            "bti_pds_combined_dataset",
            "bti_pms_combined_dataset",
            "bti_qms_combined_dataset",
            "bti_rac_combined_dataset",
            "bti_scm_combined_dataset",
        ],
        "Commerce": [
            "cmi_agy_combined_dataset",
            "cmi_cms_combined_dataset",
            "cmi_crm_combined_dataset",
            "cmi_dma_combined_dataset",
            "cmi_ecp_combined_dataset",
            "cmi_ims_combined_dataset",
            "cmi_oms_combined_dataset",
            "cmi_pim_combined_dataset",
            "cmi_pms_combined_dataset",
            "cmi_pos_combined_dataset",
            "cmi_rrm_combined_dataset",
            "cmi_scm_combined_dataset",
            "cmi_slm_combined_dataset",
            "cmi_wms_combined_dataset",
        ],
        "Construction": [
            "cri_dmc_combined_dataset",
            "cri_eam_combined_dataset",
            "cri_ecm_combined_dataset",
            "cri_ess_combined_dataset",
            "cri_fms_combined_dataset",
            "cri_mms_combined_dataset",
            "cri_pms_combined_dataset",
            "cri_qms_combined_dataset",
            "cri_sms_combined_dataset",
            "cri_sts_combined_dataset",
        ],
        "Education": [
            "edu_alp_combined_dataset",
            "edu_apa_combined_dataset",
            "edu_att_combined_dataset",
            "edu_cms_combined_dataset",
            "edu_cmt_combined_dataset",
            "edu_css_combined_dataset",
            "edu_eps_combined_dataset",
            "edu_erm_combined_dataset",
            "edu_erp_combined_dataset",
            "edu_fms_combined_dataset",
            "edu_lbs_combined_dataset",
            "edu_lms_combined_dataset",
            "edu_sep_combined_dataset",
            "edu_sis_combined_dataset",
        ],
        "Electronics": [
            "eli_ecl_combined_dataset",
            "eli_edc_combined_dataset",
            "eli_ems_combined_dataset",
            "eli_erm_combined_dataset",
            "eli_erp_combined_dataset",
            "eli_ers_combined_dataset",
            "eli_esd_combined_dataset",
            "eli_ess_combined_dataset",
            "eli_far_combined_dataset",
            "eli_iom_combined_dataset",
            "eli_mes_combined_dataset",
            "eli_plm_combined_dataset",
            "eli_scm_combined_dataset",
            "eli_scv_combined_dataset",
            "eli_tms_combined_dataset",
            "eli_qci_combined_dataset",
        ],
        "Energy": [
            "eri_ami_combined_dataset",
            "eri_bms_combined_dataset",
            "eri_crr_combined_dataset",
            "eri_ead_combined_dataset",
            "eri_ebc_combined_dataset",
            "eri_eep_combined_dataset",
            "eri_eia_combined_dataset",
            "eri_epd_combined_dataset",
            "eri_erm_combined_dataset",
            "eri_esb_combined_dataset",
            "eri_etm_combined_dataset",
            "eri_gmd_combined_dataset",
            "eri_pmc_combined_dataset",
            "eri_res_combined_dataset",
            "eri_twm_combined_dataset",
        ],
        "Environmental_Management": [
            "enm_cmr_combined_dataset",
            "enm_ecs_combined_dataset",
            "enm_edm_combined_dataset",
            "enm_eia_combined_dataset",
            "enm_emi_combined_dataset",
            "enm_ems_combined_dataset",
            "enm_era_combined_dataset",
            "enm_gis_combined_dataset",
            "enm_sms_combined_dataset",
            "enm_wms_combined_dataset",
            "enm_wrm_combined_dataset",
        ],
        "Fashion": [
            "fsh_fec_combined_dataset",
            "fsh_fmp_combined_dataset",
            "fsh_fms_combined_dataset",
            "fsh_fps_combined_dataset",
            "fsh_fra_combined_dataset",
            "fsh_fsv_combined_dataset",
            "fsh_ftf_combined_dataset",
            "fsh_plm_combined_dataset",
            "fsh_rms_combined_dataset",
            "fsh_scm_combined_dataset",
            "fsh_erp_combined_dataset",
            "fsh_fcw_combined_dataset",
        ],
        "Finance": [
            "fsi_acc_combined_dataset",
            "fsi_afs_combined_dataset",
            "fsi_awm_combined_dataset",
            "fsi_bcs_combined_dataset",
            "fsi_cbf_combined_dataset",
            "fsi_crm_combined_dataset",
            "fsi_csr_combined_dataset",
            "fsi_fpa_combined_dataset",
            "fsi_ips_combined_dataset",
            "fsi_lom_combined_dataset",
            "fsi_omb_combined_dataset",
            "fsi_ppp_combined_dataset",
            "fsi_rmc_combined_dataset",
            "fsi_rrc_combined_dataset",
            "fsi_tcm_combined_dataset",
            "fsi_trs_combined_dataset",
        ],
        "Food": [
            "fdi_crm_combined_dataset",
            "fdi_fdo_combined_dataset",
            "fdi_flm_combined_dataset",
            "fdi_fpm_combined_dataset",
            "fdi_frs_combined_dataset",
            "fdi_fsc_combined_dataset",
            "fdi_fst_combined_dataset",
            "fdi_fwr_combined_dataset",
            "fdi_ims_combined_dataset",
            "fdi_mpn_combined_dataset",
            "fdi_qcs_combined_dataset",
            "fdi_rfs_combined_dataset",
            "fdi_rms_combined_dataset",
            "fdi_scm_combined_dataset",
            "fdi_ses_combined_dataset",
        ],
        "Government": [
            "gov_edm_combined_dataset",
            "gov_ems_combined_dataset",
            "gov_fmb_combined_dataset",
            "gov_gis_combined_dataset",
            "gov_gss_combined_dataset",
            "gov_hph_combined_dataset",
            "gov_hrm_combined_dataset",
            "gov_lep_combined_dataset",
            "gov_prd_combined_dataset",
            "gov_rce_combined_dataset",
            "gov_tms_combined_dataset",
            "gov_ums_combined_dataset",
            "gov_vem_combined_dataset",
        ],
        "Health": [
            "hid_cms_combined_dataset",
            "hid_ehr_combined_dataset",
            "hid_eps_combined_dataset",
            "hid_hie_combined_dataset",
            "hid_hms_combined_dataset",
            "hid_lims_combined_dataset",
            "hid_mis_combined_dataset",
            "hid_pms_combined_dataset",
            "hid_prs_combined_dataset",
            "hid_rcs_combined_dataset",
            "hid_tis_combined_dataset",
        ],
        "Hospitality": [
            "hsi_cmd_combined_dataset",
            "hsi_crm_combined_dataset",
            "hsi_ecm_combined_dataset",
            "hsi_etw_combined_dataset",
            "hsi_gas_combined_dataset",
            "hsi_gsc_combined_dataset",
            "hsi_haf_combined_dataset",
            "hsi_hmm_combined_dataset",
            "hsi_ims_combined_dataset",
            "hsi_obr_combined_dataset",
            "hsi_orr_combined_dataset",
            "hsi_pms_combined_dataset",
            "hsi_pos_combined_dataset",
            "hsi_sas_combined_dataset",
            "hsi_swm_combined_dataset",
            "hsi_wms_combined_dataset",
        ],
        "IT": [
            "iti_erp_combined_dataset",
            "iti_its_combined_dataset",
            "iti_kmd_combined_dataset",
            "iti_lam_combined_dataset",
            "iti_lms_combined_dataset",
            "iti_pmc_combined_dataset",
        ],
        "Insurance": [
            "ins_acs_combined_dataset",
            "ins_bpp_combined_dataset",
            "ins_cms_combined_dataset",
            "ins_fds_combined_dataset",
            "ins_irs_combined_dataset",
            "ins_pas_combined_dataset",
            "ins_rcs_combined_dataset",
            "ins_ris_combined_dataset",
            "ins_rms_combined_dataset",
            "ins_uws_combined_dataset",
        ],
        "Legal": [
            "leg_bts_combined_dataset",
            "leg_cms_combined_dataset",
            "leg_cos_combined_dataset",
            "leg_cps_combined_dataset",
            "leg_crs_combined_dataset",
            "leg_dma_combined_dataset",
            "leg_eds_combined_dataset",
            "leg_ipm_combined_dataset",
            "leg_lrs_combined_dataset",
            "leg_pms_combined_dataset",
            "leg_prs_combined_dataset",
        ],
        "Manufacturing": [
            "mfi_dmc_combined_dataset",
            "mfi_erp_combined_dataset",
            "mfi_ess_combined_dataset",
            "mfi_ims_combined_dataset",
            "mfi_mes_combined_dataset",
            "mfi_mms_combined_dataset",
            "mfi_plm_combined_dataset",
            "mfi_qms_combined_dataset",
            "mfi_rcr_combined_dataset",
            "mfi_scm_combined_dataset",
            "mfi_sfm_combined_dataset",
            "mfi_tws_combined_dataset",
        ],
        "Maritime": [
            "mar_cms_combined_dataset",
            "mar_cmt_combined_dataset",
            "mar_emr_combined_dataset",
            "mar_fms_combined_dataset",
            "mar_mcs_combined_dataset",
            "mar_mir_combined_dataset",
            "mar_mns_combined_dataset",
            "mar_msc_combined_dataset",
            "mar_pma_combined_dataset",
            "mar_pmt_combined_dataset",
            "mar_sls_combined_dataset",
            "mar_vms_combined_dataset",
        ],
        "Media": [
            "mde_brd_combined_dataset",
            "mde_cce_combined_dataset",
            "mde_cms_combined_dataset",
            "mde_crm_combined_dataset",
            "mde_dam_combined_dataset",
            "mde_let_combined_dataset",
            "mde_mpb_combined_dataset",
            "mde_pmc_combined_dataset",
            "mde_rrm_combined_dataset",
            "mde_smm_combined_dataset",
        ],
        "Mining": [
            "mni_edd_combined_dataset",
            "mni_emc_combined_dataset",
            "mni_ems_combined_dataset",
            "mni_fam_combined_dataset",
            "mni_ims_combined_dataset",
            "mni_mer_combined_dataset",
            "mni_mpd_combined_dataset",
            "mni_mpo_combined_dataset",
            "mni_msc_combined_dataset",
            "mni_omp_combined_dataset",
            "mni_rcr_combined_dataset",
            "mni_sss_combined_dataset",
            "mni_twm_combined_dataset",
        ],
        "Pet": [
            "pet_aft_combined_dataset",
            "pet_lpa_combined_dataset",
            "pet_nds_combined_dataset",
            "pet_par_combined_dataset",
            "pet_phw_combined_dataset",
            "pet_pmc_combined_dataset",
            "pet_snc_combined_dataset",
            "pet_tbm_combined_dataset",
        ],
        "Petroleum": [
            "pri_cms_combined_dataset",
            "pri_dwm_combined_dataset",
            "pri_eia_combined_dataset",
            "pri_ems_combined_dataset",
            "pri_erc_combined_dataset",
            "pri_erm_combined_dataset",
            "pri_hap_combined_dataset",
            "pri_lim_combined_dataset",
            "pri_mam_combined_dataset",
            "pri_pis_combined_dataset",
            "pri_pos_combined_dataset",
            "pri_rpp_combined_dataset",
            "pri_scm_combined_dataset",
            "pri_tms_combined_dataset",
            "pri_twm_combined_dataset",
        ],
        "Publishing": [
            "pub_cce_combined_dataset",
            "pub_cms_combined_dataset",
            "pub_dam_combined_dataset",
            "pub_ecc_combined_dataset",
            "pub_eic_combined_dataset",
            "pub_pgc_combined_dataset",
            "pub_pod_combined_dataset",
            "pub_psm_combined_dataset",
            "pub_rms_combined_dataset",
        ],
        "RealEstate": [
            "rei_crm_combined_dataset",
            "rei_eaf_combined_dataset",
            "rei_eam_combined_dataset",
            "rei_eia_combined_dataset",
            "rei_eip_combined_dataset",
            "rei_ldc_combined_dataset",
            "rei_lms_combined_dataset",
            "rei_pim_combined_dataset",
            "rei_pms_combined_dataset",
            "rei_ptc_combined_dataset",
            "rei_pvs_combined_dataset",
            "rei_rem_combined_dataset",
            "rei_tls_combined_dataset",
            "rei_tme_combined_dataset",
        ],
        "Retail": [
            "rti_cfr_combined_dataset",
            "rti_clr_combined_dataset",
            "rti_crm_combined_dataset",
            "rti_ems_combined_dataset",
            "rti_eps_combined_dataset",
            "rti_esl_combined_dataset",
            "rti_esw_combined_dataset",
            "rti_ims_combined_dataset",
            "rti_lps_combined_dataset",
            "rti_oms_combined_dataset",
            "rti_pos_combined_dataset",
            "rti_rrm_combined_dataset",
            "rti_scm_combined_dataset",
            "rti_teo_combined_dataset",
            "rti_wms_combined_dataset",
        ],
        "Sports": [
            "spr_apa_combined_dataset",
            "spr_ems_combined_dataset",
            "spr_esm_combined_dataset",
            "spr_fes_combined_dataset",
            "spr_fts_combined_dataset",
            "spr_ltm_combined_dataset",
            "spr_sbf_combined_dataset",
            "spr_snd_combined_dataset",
            "spr_tms_combined_dataset",
            "spr_tvm_combined_dataset",
            "spr_wts_combined_dataset",
        ],
        "Telecommunications": [
            "tel_brm_combined_dataset",
            "tel_bss_combined_dataset",
            "tel_bug_combined_dataset",
            "tel_crm_combined_dataset",
            "tel_mns_combined_dataset",
            "tel_nms_combined_dataset",
            "tel_nss_combined_dataset",
            "tel_oss_combined_dataset",
            "tel_sdp_combined_dataset",
            "tel_tem_combined_dataset",
            "tel_ucs_combined_dataset",
        ],
        "Tourism": [
            "toi_cfr_combined_dataset",
            "toi_cms_combined_dataset",
            "toi_crm_combined_dataset",
            "toi_ems_combined_dataset",
            "toi_gfr_combined_dataset",
            "toi_mps_combined_dataset",
            "toi_otp_combined_dataset",
            "toi_pms_combined_dataset",
            "toi_pos_combined_dataset",
            "toi_ppf_combined_dataset",
            "toi_rbs_combined_dataset",
            "toi_taa_combined_dataset",
            "toi_tic_combined_dataset",
            "toi_tms_combined_dataset",
            "toi_tss_combined_dataset",
            "toi_tto_combined_dataset",
        ],
        "Transportation": [
            "tri_drs_combined_dataset",
            "tri_eis_combined_dataset",
            "tri_eld_combined_dataset",
            "tri_fbs_combined_dataset",
            "tri_fcc_combined_dataset",
            "tri_fms_combined_dataset",
            "tri_fts_combined_dataset",
            "tri_gns_combined_dataset",
            "tri_mrs_combined_dataset",
            "tri_pie_combined_dataset",
            "tri_pis_combined_dataset",
            "tri_prs_combined_dataset",
            "tri_scs_combined_dataset",
            "tri_tcm_combined_dataset",
            "tri_tms_combined_dataset",
            "tri_vts_combined_dataset",
        ],
    }

    tested = True
    for key, value in tables.items():
        database = key
        database_ = database
        for table_name in value:
            table_name = table_name.lower()
            print(database_, table_name)
            ssh_dalfin = SSH()
            final_data = []

            ssh = SSH(database=database_)
            sql_query = f"select schema_name, field_id, industry_name, menu_code, system_code, submenu_code, table_name, column_name, field_type, field_order, field_label, reference_table, reference_column, field_response_choice, index_field, field_validator, field_function from public.{table_name};"
            results, column_names = ssh.get_data(sql_query)
            data = [dict(zip(column_names, row)) for row in results]

            if not tested:
                try:
                    system_code = data[0]["system_code"].lower().replace("-", "_")
                except:
                    print("there is no data for this system.")
                    input_val = input("What to do: return or continue?")
                    if input_val == "return":
                        return
                    system_code = ""

                database = database.lower()
                sql_query = f"SELECT schema_name, table_name, field_id FROM dalfin.global_relation where database_name = '{database}' and schema_name='{system_code}' order by field_id"
                results, column_names = ssh_dalfin.get_data(sql_query)
                global_data = [dict(zip(column_names, row)) for row in results]
                global_data = {
                    f"{i['schema_name']}-{i['table_name']}": i["field_id"]
                    for i in global_data
                }

                sql_query = f"SELECT id, schema_name, table_name FROM dalfin.form_form_relation where database_name = '{database}' and schema_name='{system_code}' order by id;"
                results, column_names = ssh_dalfin.get_data(sql_query)
                relation_data = [dict(zip(column_names, row)) for row in results]
                relation_data = {
                    f"{i['schema_name']}-{i['table_name']}": i["id"]
                    for i in relation_data
                }

            data = pd.DataFrame(data)
            data.replace({np.nan: None}, inplace=True)

            if tested:
                table_name_mod = table_name.replace("_combined_dataset", "")
                schema_name = data["schema_name"].unique()
                schema_name = [
                    i
                    for i in schema_name
                    if "_".join(i.split("-")[:2]).lower() != table_name_mod
                ]
                if schema_name:
                    print("schema_name", schema_name)

                system_code = data["system_code"].unique()
                system_code = [
                    i
                    for i in system_code
                    if "_".join(i.split("-")[:2]).lower() != table_name_mod
                ]
                if system_code:
                    print("system_code", system_code)

                menu_code = data["menu_code"].unique()
                menu_code = [
                    i
                    for i in menu_code
                    if "_".join(i.split("-")[:2]).lower() != table_name_mod
                ]
                if menu_code:
                    print("menu_code", menu_code)

                submenu_code = data["submenu_code"].unique()
                submenu_code = [
                    i
                    for i in submenu_code
                    if "_".join(i.split("-")[:2]).lower() != table_name_mod
                ]
                if submenu_code:
                    print("submenu_code", submenu_code)

                continue

            group_data = data.groupby("submenu_code")

            for _, group_data in group_data:
                group_data = group_data.sort_values(by="field_order")
                group_data = group_data.to_dict(orient="records")
                first_element, second_element = group_data[::2], group_data[1::2]

                generated_strings = set()
                for lists in zip_longest(first_element, second_element, fillvalue=None):
                    first_element = lists[0]
                    second_element = lists[1]

                    first_element_type = first_element["field_type"]
                    first_element["index_field"] = check_index_field(
                        first_element.get("index_field")
                    )
                    first_element["field_validator"] = check_field_validator(
                        first_element.get("field_validator"), group_data
                    )
                    first_element["field_function"] = check_field_function(
                        first_element.get("field_function"), group_data
                    )

                    if second_element:
                        second_element_type = second_element["field_type"]
                        second_element["index_field"] = check_index_field(
                            second_element.get("index_field")
                        )
                        second_element["field_validator"] = check_field_validator(
                            second_element.get("field_validator"), group_data
                        )
                        second_element["field_function"] = check_field_function(
                            second_element.get("field_function"), group_data
                        )
                    else:
                        second_element_type = None

                    if (
                        first_element["reference_table"].lower() != "no"
                        and first_element["field_response_choice"].lower() != "internal"
                    ):
                        first_element = set_value_from_relation(
                            first_element, global_data, relation_data, data, ssh_dalfin
                        )

                    if (
                        second_element
                        and second_element["reference_table"].lower() != "no"
                        and second_element["field_response_choice"].lower()
                        != "internal"
                    ):
                        second_element = set_value_from_relation(
                            second_element, global_data, relation_data, data, ssh_dalfin
                        )

                    random_string = generate_random_string(
                        generated_strings=generated_strings
                    )
                    generated_strings.add(random_string)

                    if first_element_type in ["text_area", "richtext"]:
                        first_element["field_label_length"] = 12
                        first_element["form_group"] = None

                    if second_element_type and second_element_type in [
                        "text_area",
                        "richtext",
                    ]:
                        second_element["field_label_length"] = 12
                        second_element["form_group"] = None

                    if first_element_type not in ["text_area", "richtext"]:
                        first_element["field_label_length"] = 6
                        first_element["form_group"] = random_string

                    if second_element_type and second_element_type not in [
                        "text_area",
                        "richtext",
                    ]:
                        second_element["field_label_length"] = 6
                        second_element["form_group"] = random_string

                    final_data.append(first_element)
                    if second_element:
                        final_data.append(second_element)

            ssh_dalfin.close_db()

            for final_dat in final_data:
                update_query = f"UPDATE public.{table_name} SET field_type = %s, field_order = %s, field_response_choice = %s, field_label_length = %s, form_group = %s, index_field = %s, field_validator = %s, field_function = %s WHERE field_id = %s"
                data_to_update = (
                    final_dat["field_type"],
                    final_dat["field_order"],
                    final_dat["field_response_choice"],
                    final_dat["field_label_length"],
                    final_dat["form_group"],
                    final_dat["index_field"],
                    final_dat["field_validator"],
                    final_dat["field_function"],
                    final_dat["field_id"],
                )
                ssh.update_bulk_data(update_query, data_to_update)
            ssh.close_db()


if __name__ == "__main__":
    get_update_data()
