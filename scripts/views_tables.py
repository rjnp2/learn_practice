from sqlalchemy import text
from technical.imports.handler_import import BadRequest_400
from datetime import datetime
from technical.imports.handler_import import HTTPException
from datetime import datetime, timedelta, timezone

schema_code = "public"

data_type_mapper = {
    "number": "float8",
    "number-int": "int8",
    "text_area": "TEXT",
    "text_editor": "TEXT",
    "checkbox": "boolean",
    "switch": "boolean",
    "datetime": "timestamptz",
    "image": "jsonb",
    "file": "jsonb",
    "signature": "varchar",
    "date": "date",
    "time": "time",
}

convert_data_type_mapper = {
    "float8": "float8",
    "int8": "int8",
    "TEXT": "TEXT",
    "boolean": "boolean",
    "varchar": "varchar",
}


def get_default_value(column_type, default_value, label):
    if column_type == "jsonb":
        default_value = "default '[]'::jsonb"
        update_data = "'[]'"

    elif column_type == "_text":
        if default_value:
            default_value = "{" + ",".join([str(i) for i in default_value if i]) + "}"
        else:
            default_value = "'{-}'"

        update_data = f"'{default_value}'"
        default_value = f"default '{default_value}'"

    elif column_type.lower() == "boolean" and default_value:
        try:
            default_value = bool(default_value)
            default_value = f"default {default_value}"
            update_data = f"'{default_value}'".lower()
        except:
            raise HTTPException(
                400,
                detail={"message": f"Value of {label} data must be boolean."},
            )

    elif column_type.lower() == "timestamptz" and default_value:
        if default_value.lower() == "now":
            default_value = f"default CURRENT_TIMESTAMP"
            current_utc_timestamp = str(datetime.now(timezone.utc))
            update_data = f"'{current_utc_timestamp}'"

        elif default_value.lower() == "yesterday":
            default_value = f"default CURRENT_TIMESTAMP - INTERVAL '1 day'"
            yesterday_utc_timestamp = datetime.now(timezone.utc) - timedelta(days=1)
            yesterday_utc_timestamp = str(yesterday_utc_timestamp)
            update_data = f"'{yesterday_utc_timestamp}'"

        elif default_value.lower() == "tomorrow":
            default_value = f"default CURRENT_TIMESTAMP + INTERVAL '1 day'"
            yesterday_utc_timestamp = datetime.now(timezone.utc) + timedelta(days=1)
            yesterday_utc_timestamp = str(yesterday_utc_timestamp)
            update_data = f"'{yesterday_utc_timestamp}'"

        else:
            try:
                default_value = datetime.fromisoformat(
                    default_value.replace("Z", "+00:00")
                )
                default_value = str(default_value)
                update_data = f"'{default_value}'"
                default_value = f"default '{default_value}'"
            except:
                raise HTTPException(
                    400,
                    detail={
                        "message": f"Value of {label} data must be datetime with format %Y-%m-%d %H:%M:%S."
                    },
                )

    elif column_type.lower() == "date" and default_value:
        if default_value.lower() == "now":
            default_value = f"default CURRENT_DATE"
            current_utc_timestamp = str(datetime.now(timezone.utc).date())
            update_data = f"'{current_utc_timestamp}'"

        else:
            try:
                default_value = datetime.strptime(default_value, "%Y-%m-%d").date()
                default_value = str(default_value)
                update_data = f"'{default_value}'"
                default_value = f"default '{default_value}'"
            except:
                raise HTTPException(
                    400,
                    detail={
                        "message": f"Value of {label} data must be datetime with format %Y-%m-%d."
                    },
                )

    elif column_type.lower() == "time" and default_value:
        if default_value.lower() == "now":
            default_value = f"default CURRENT_TIME"
            current_utc_timestamp = str(datetime.now(timezone.utc).time())
            update_data = f"'{current_utc_timestamp}'"

        else:
            try:
                default_value = default_value.split(":")
                if len(default_value) == 3:
                    default_value = default_value[:-1]
                default_value = ":".join(default_value)
                default_value = datetime.strptime(default_value, "%H:%M").time()
                default_value = str(default_value)
                update_data = f"'{default_value}'"
                default_value = f"default '{default_value}'"
            except:
                raise HTTPException(
                    400,
                    detail={"message": f"Value of {label} data must be time."},
                )

    elif column_type.lower() == "int8" and default_value:
        try:
            default_value = int(default_value)
            update_data = default_value
            default_value = f"default {default_value}"
        except:
            raise HTTPException(
                400,
                detail={"message": f"Value of {label} data must be integer."},
            )

    elif column_type.lower() == "float8" and default_value:
        try:
            default_value = float(default_value)
            update_data = default_value
            default_value = f"default {default_value}"
        except:
            raise HTTPException(
                400,
                detail={"message": f"Value of {label} data must be float."},
            )

    elif default_value:
        update_data = f"'{default_value}'"
        default_value = f"default '{default_value}'"

    return default_value, update_data


def sql_create_tables(schema_name, form_id, field_data):
    column_data = []
    index_data = []
    relation_data = []

    for dat in field_data:
        if dat.get("type", "") == "table":
            field_name = str(dat["id"]).lower()
            column_data.append(f'"{field_name}" jsonb NULL')
            continue

        elif dat.get("type", "") == "repeating-element":
            field_name = str(dat["id"]).lower()
            column_data.append(f'"{field_name}" jsonb NULL')
            continue

        elif dat.get("is-repeating-element") == "yes":
            field_name = str(dat["id"]).lower()
            column_data.append(f'"{field_name}" jsonb NULL')
            continue

        if (
            dat.get("component", "") != "question"
            or dat.get("isTable") == True
            or not dat.get("id")
        ):
            continue

        field_name = str(dat["id"]).lower()
        required = dat.get("required", False)

        if required:
            required = "NOT NULL"
        else:
            required = "NULL"

        if dat["response_choice"].lower() == "default":
            field_type = str(dat["type"]).lower()
            column_type = data_type_mapper.get(field_type, "varchar")

        elif dat["response_choice"].startswith(("internal", "relation")):
            relation_data.append(
                {
                    "form_to": form_id,
                    "field_from": "id",
                    "field_to": field_name,
                    "type": str(dat["type"]).lower(),
                    "response_choice": dat["response_choice"],
                }
            )
            continue

        else:
            column_type = "_text"

        if column_type == "jsonb":
            default_value = "default '[]'::jsonb"
        elif column_type == "_text":
            default_value = "default '{-}'"
        else:
            default_value = ""

        if column_type in ["boolean", "timestamptz", "date"]:
            index_data.append(
                f'CREATE INDEX system_table_{form_id}_{field_name}_idx ON {schema_name}.table_{form_id} ("{field_name}");'
            )

        column_data.append(
            f""""{field_name}" {column_type} {default_value} {required}"""
        )

    # if not column_data:
    #     raise BadRequest_400(message="Field is empty.")

    column_data.append("meta jsonb DEFAULT '{}'::jsonb NULL")
    column_data = ",".join(column_data)

    index_data.extend(
        [
            f"CREATE INDEX system_table_{form_id}_user_id_idx ON {schema_name}.table_{form_id} (user_id);",
            f"CREATE INDEX system_table_{form_id}_created_by_idx ON {schema_name}.table_{form_id} (created_by);",
            f"CREATE INDEX system_table_{form_id}_updated_by_idx ON {schema_name}.table_{form_id} (updated_by);",
            f"CREATE INDEX system_table_{form_id}_deleted_idx ON {schema_name}.table_{form_id} (deleted);",
            f"CREATE INDEX system_table_{form_id}_is_draft_idx ON {schema_name}.table_{form_id} (is_draft);",
            f"CREATE INDEX system_table_{form_id}_created_at_idx ON {schema_name}.table_{form_id} (created_at);",
            f"CREATE INDEX system_table_{form_id}_updated_at_idx ON {schema_name}.table_{form_id} (updated_at);",
        ]
    )
    index_data = " ".join(index_data)

    query_text = f"""
    CREATE TABLE {schema_name}.table_{form_id} (
        id serial4 NOT NULL,
        uid varchar NOT NULL UNIQUE,
        {column_data},
        user_id int8 NOT NULL,
        created_by int8 NULL,
        updated_by int8 NULL,
        created_at timestamptz DEFAULT now() NOT NULL,
        updated_at timestamptz DEFAULT now() NOT NULL,
        deleted bool DEFAULT false NOT NULL,
        is_draft bool DEFAULT false NOT NULL,

        CONSTRAINT table_{form_id}_pkey PRIMARY KEY (id),
        CONSTRAINT table_{form_id}_created_by_fkey FOREIGN KEY (created_by) REFERENCES public."user"(id),
        CONSTRAINT table_{form_id}_updated_by_fkey FOREIGN KEY (updated_by) REFERENCES public."user"(id),
        CONSTRAINT table_{form_id}_user_id_fkey FOREIGN KEY (user_id) REFERENCES public."user"(id)
    );
    {index_data}
    """
    return text(query_text), relation_data


def create_fk_tables(schema_name, relation_data, db):
    query_list = []
    for relation_dat in relation_data:
        field_to = relation_dat["field_to"]

        schema_code = schema_name
        if (
            relation_dat["response_choice"].startswith(("internal"))
            and relation_dat["type"] == "1"
        ):
            form_from = "organization__user"
            form_to = f'table_{relation_dat["form_to"]}'
            comment = f"Junction table for organization__user with id and {form_to} with id for {field_to} field."

        elif (
            relation_dat["response_choice"].startswith(("internal"))
            and relation_dat["type"] == "2"
        ):
            form_from = f"department"
            form_to = f'table_{relation_dat["form_to"]}'
            comment = f"Junction table for department with id and {form_to} with id for {field_to} field."

        elif relation_dat["response_choice"].startswith(("relation")):
            rel_id = int(relation_dat["type"])
            query_text = f"""
                select schema_code, form_id from public.relation_attributes
                where id = {rel_id}
            """
            schema_code, relation_data = db.execute(query_text).fetchone()
            form_from = f"table_{relation_data}"
            form_to = f'table_{relation_dat["form_to"]}'

            comment = f"Junction table for {form_from} with id and {form_to} with id for {field_to} field."

        else:
            continue

        table_name = f"j{form_to}_{field_to}".replace("table_", "t_")
        query_text = f"""
            SELECT EXISTS (
                select 1 FROM information_schema.tables WHERE table_schema = '{schema_name}' AND table_name = '{table_name}'
            )
        """
        exist = db.execute(query_text).fetchone()[0]
        if exist:
            continue
        if relation_dat["response_choice"].startswith(("internal")):
            query_text = f"""
            CREATE TABLE {schema_name}.{table_name} (
                table_1 int8 NOT NULL, 
                table_2 int8 NOT NULL, 

                CONSTRAINT table_1_fkey FOREIGN KEY ("table_1") REFERENCES {schema_name}."{form_to}"(id),
                CONSTRAINT table_2_fkey FOREIGN KEY ("table_2") REFERENCES public."{form_from}"(id)
            );
            
            COMMENT ON TABLE {schema_name}.{table_name} IS '{comment}';
            """
        else:
            query_text = f"""
            CREATE TABLE {schema_name}.{table_name} (
                table_1 int8 NOT NULL, 
                table_2 int8 NOT NULL, 

                CONSTRAINT table_1_fkey FOREIGN KEY ("table_1") REFERENCES {schema_name}."{form_to}"(id),
                CONSTRAINT table_2_fkey FOREIGN KEY ("table_2") REFERENCES {schema_code}."{form_from}"(id)
            );
            
            COMMENT ON TABLE {schema_name}.{table_name} IS '{comment}';
            """

        query_text = text(query_text)
        query_list.append(query_text)

    return query_list


def permanent_delete_columns(table, schema_name, deleted_items):
    table_name = f"table_{table}"
    query_list = {}
    for deleted_item in deleted_items:
        column_name = deleted_item.get("id")
        if not column_name:
            continue

        column_name = str(column_name).lower()
        column_name_ = f'"{column_name}"'
        if (
            deleted_item.get("response_choice", "")
            .lower()
            .startswith(("default", "global"))
        ):
            query_text = f"""
                ALTER TABLE {schema_name}.{table_name} DROP COLUMN IF EXISTS {column_name_};
            """

        elif deleted_item["response_choice"].startswith(("internal", "relation")):
            junction_table = f"jt_{table}_{column_name}"
            query_text = f"""
                DROP TABLE IF EXISTS {schema_name}.{junction_table};
            """
        else:
            continue

        query_list[column_name] = text(query_text)

    return query_list


def get_column_details(form_name: int, schema_name: str):
    raw_query = f"""
        select lower(column_name), column_default, is_nullable, udt_name from information_schema.columns
        where table_schema = '{schema_name}'
        and table_name = '{form_name}';
    """
    return text(raw_query)


def sql_update_tables(schema_name, form_id, field_data, old_column_data):
    column_data = []
    relation_data = []

    for dat in field_data:
        if dat.get("type", "") == "table":
            field_name = str(dat["id"]).lower()
            column_details = old_column_data.get(field_name)

            if column_details is None:
                column_data.append(
                    f"""ALTER TABLE {schema_name}.table_{form_id} ADD COLUMN "{field_name}" jsonb default '[]'::jsonb NULL"""
                )
            continue

        elif dat.get("type", "") == "repeating-element":
            field_name = str(dat["id"]).lower()
            column_details = old_column_data.get(field_name)

            if column_details is None:
                column_data.append(
                    f"""ALTER TABLE {schema_name}.table_{form_id} ADD COLUMN "{field_name}" jsonb default '[]'::jsonb NULL"""
                )
            continue

        elif dat.get("is-repeating-element") == "yes":
            field_name = str(dat["id"]).lower()
            column_details = old_column_data.get(field_name)

            if column_details is None:
                column_data.append(
                    f"""ALTER TABLE {schema_name}.table_{form_id} ADD COLUMN "{field_name}" jsonb default '[]'::jsonb NULL"""
                )
            continue

        if (
            dat.get("component", "") != "question"
            or dat.get("is_table") == True
            or not dat.get("id")
        ):
            continue

        field_name = str(dat["id"]).lower()
        field_type = str(dat["type"]).lower()
        required = dat.get("required", False)

        if required:
            required_ = "NOT NULL"
            required = "NO"
        else:
            required_ = "NULL"
            required = "YES"

        if dat["response_choice"].lower() == "default":
            column_type = data_type_mapper.get(field_type, "varchar")

        else:
            column_type = "_text"

        column_details = old_column_data.get(field_name)

        if column_details is None:
            if dat["response_choice"].startswith(("internal", "relation")):
                relation_data.append(
                    {
                        "form_to": form_id,
                        "field_from": "id",
                        "field_to": field_name,
                        "type": str(dat["type"]).lower(),
                        "response_choice": dat["response_choice"],
                    }
                )
                continue

            if column_type == "jsonb":
                default_value = "default '[]'::jsonb"
            elif column_type == "_text":
                default_value = "default '{-}'"
            else:
                default_value = ""

            # if required_ == "NOT NULL" and not default_value:
            #     raise BadRequest_400(
            #         message=f'Database won"t allow to add {label} column with {field_type} type without default value of it is required.'
            #     )

            column_data.append(
                f"""ALTER TABLE {schema_name}.table_{form_id} ADD COLUMN "{field_name}" {column_type} {default_value} NULL;"""
            )

            if column_type in ["boolean", "timestamptz", "date"]:
                column_data.append(
                    f'CREATE INDEX system_table_{form_id}_{field_name}_idx ON {schema_name}.table_{form_id} ("{field_name}");'
                )

        elif dat.get("deleted") and column_details[2] == "NO":
            column_data.append(
                f'ALTER TABLE {schema_name}.table_{form_id} ALTER COLUMN "{field_name}" DROP NOT NULL;'
            )

        elif column_type.lower() != column_details[3].lower():
            convert_ = convert_data_type_mapper.get(column_type)
            if convert_ is None:
                label = dat.get("label")
                raise BadRequest_400(
                    message=f'Database won"t allow to convert {label} to {column_type} type from {column_details[3]} type.'
                )

            column_data.append(
                f'ALTER TABLE {schema_name}.table_{form_id} ALTER COLUMN "{field_name}" SET DATA TYPE {column_type} using "{field_name}"::{convert_};'
            )

        elif required.lower() != column_details[2].lower():
            if required_ == "NULL":
                column_data.append(
                    f'ALTER TABLE {schema_name}.table_{form_id} ALTER COLUMN "{field_name}" DROP NOT NULL;'
                )
            # else:
            #     default_value = dat.get("value", "")
            #     if not default_value:
            #         raise BadRequest_400(
            #             message=f'Database won"t allow to add {label} column with {field_type} type without default value of it is required.'
            #         )

            #     default_value, update_data = get_default_value(
            #         column_type=column_type, default_value=default_value, label=label
            #     )

            #     column_data.append(
            #         f"""
            #         UPDATE {schema_name}.table_{form_id} SET "{field_name}" = {update_data} WHERE "{field_name}" IS NULL;
            #         """
            #     )
            #     column_data.append(
            #         f"""
            #         ALTER TABLE {schema_name}.table_{form_id} ALTER COLUMN "{field_name}" SET {default_value};
            #         """
            #     )
            #     column_data.append(
            #         f"""
            #         ALTER TABLE {schema_name}.table_{form_id} ALTER COLUMN "{field_name}" SET NOT NULL;
            #         """
            #     )

    if not column_data:
        return None, relation_data

    return column_data, relation_data


def get_deleted_data_count(
    table_name: int,
    schema_name: str,
):
    raw_query = f"""
        select count(*) from {schema_name}.{table_name}
        where deleted=true;
    """
    return text(raw_query)


def check_table_exist(table_name: int, schema_name: str):
    raw_query = f"""
        SELECT EXISTS (
            select 1 from information_schema.columns
            where table_schema = '{schema_name}'
            and table_name = '{table_name}'  
        )   
    """
    return text(raw_query)


def get_column_data(
    table_name: int,
    schema_name: str,
):
    raw_query = f"""
        select LOWER(column_name) from information_schema.columns
        where table_schema = '{schema_name}'
        and table_name = '{table_name}';
    """
    return text(raw_query)


def get_data(table_name: int, schema_name: str, condition: str = ""):
    raw_query = f"""
        select table_.*, us.full_name as _created_by, us2.full_name as _updated_by from {schema_name}.{table_name} table_
        left join public.user us on table_.created_by = us.id
        left join public.user us2 on table_.updated_by = us2.id
        {condition};
    """
    return text(raw_query)


def count_data(table_name: str, schema_name: str, condition_data: str = ""):
    raw_query = f"""
        select count(*) from {schema_name}.{table_name} table_
        {condition_data};
    """
    return text(raw_query)


def get_data_details(table_name: int, schema_name: str, record_id: int):
    raw_query = f"""
        select * from {schema_name}.{table_name}
        where id = {record_id};
    """
    return text(raw_query)


def sql_insert_record(schema_name, form_id, cols, vals):
    query_text = f"""
    INSERT INTO {schema_name}.table_{form_id} ({cols}) VALUES {vals} RETURNING *;
    """
    return text(query_text)


def sql_insert_record_junction(schema_name, table_name, cols, vals):
    query_text = f"""
    INSERT INTO {schema_name}.{table_name} ({cols}) VALUES {vals} RETURNING *;
    """
    return text(query_text)


def sql_update_record_junction(schema_name, table_name, set_clause):
    query_text = f"""
    UPDATE {schema_name}.{table_name} set {set_clause} WHERE id = :data_id RETURNING *;
    """
    return text(query_text)


def sql_delete_record_junction(schema_name, table_name, record_id, parent_id):
    query_text = f"""
    DELETE FROM {schema_name}.{table_name} WHERE table_1 = {record_id} AND table_2 = {parent_id};
    """
    return text(query_text)


def get_data_junction(table_name: int, schema_name: str, condition: str = ""):
    raw_query = f"""
        select table_.* from {schema_name}.{table_name} table_
        {condition};
    """
    return text(raw_query)


def sql_update_record(schema_name, form_id, set_clause):
    query_text = f"""
    UPDATE {schema_name}.table_{form_id} set {set_clause} WHERE id = :data_id RETURNING *;
    """
    return text(query_text)


def sql_delete_record(schema_name, form_id, record_id):
    query_text = f"""
    UPDATE {schema_name}.table_{form_id} set deleted=true WHERE id in ({record_id});
    """
    return text(query_text)


def sql_duplicate_record(schema_name, form_id, cols, record_id, seq=None):
    query_text = f"""
    INSERT INTO {schema_name}.table_{form_id} ("uid", {cols})
    SELECT '{seq}', {cols}
    FROM {schema_name}.table_{form_id} 
    WHERE id = {record_id}
    RETURNING *;
    """
    return text(query_text)


def get_package_data(
    package_id: int,
    schema_name: str,
    limit: int = 1,
    offset: int = 0,
):
    raw_query = f"""
        select * from {schema_code}.{package_id}
        limit {limit} offset {offset};
    """
    return text(raw_query)
