CREATE TABLE combined_dataset AS
SELECT 
    field_id, schema_name,industry_id,
    industry_code, industry_name,
    DENSE_RANK() OVER (ORDER BY system_code) AS system_id,
    system_code,
    system_name, system_description,
    DENSE_RANK() OVER (ORDER BY system_code, menu_code) AS menu_id,
    menu_name, menu_description, menu_code,
    menu_is_pinned, menu_element, menu_order_index,
    DENSE_RANK() OVER (ORDER BY system_code, menu_code, submenu_code) AS submenu_id,
    submenu_name, submenu_description, submenu_code, submenu_is_pinned,
    submenu_order_index,submenu_element,submenu_layout,
    submenu_parent_id,submenu_show_in_sidebar, submenu_action,
    table_name,table_description,column_name,field_type,field_order,
    field_label,field_placeholder,column_default,field_required,field_min,
    field_max,character_maximum_length,numeric_precision,reference_table,
    reference_column,field_multiple,form_group,form_ordering,
    field_label_length,field_parent,field_position,field_use_css,field_default_css,
    field_custom_css, field_component, field_response_choice, field_value, index_field,
    field_validator,field_function, tab_no,tab_name
FROM 
(
    SELECT * from public.rti_cfr_combined_dataset
    UNION ALL
    SELECT * from public.rti_clr_combined_dataset
    UNION ALL
    SELECT * from public.rti_crm_combined_dataset
    UNION ALL
    SELECT * from public.rti_ems_combined_dataset
    UNION ALL
    SELECT * from public.rti_eps_combined_dataset
    UNION ALL
    SELECT * from public.rti_esl_combined_dataset
    UNION ALL
    SELECT * from public.rti_esw_combined_dataset
    UNION ALL
    SELECT * from public.rti_ims_combined_dataset
    UNION ALL
    SELECT * from public.rti_lps_combined_dataset
    UNION ALL
    SELECT * from public.rti_oms_combined_dataset
    UNION ALL
    SELECT * from public.rti_pos_combined_dataset
    UNION ALL
    SELECT * from public.rti_rrm_combined_dataset
    UNION ALL
    SELECT * from public.rti_scm_combined_dataset
    UNION ALL
    SELECT * from public.rti_teo_combined_dataset
    UNION ALL
    SELECT * from public.rti_wms_combined_dataset
)