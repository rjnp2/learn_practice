import json
import random
import string
from datetime import datetime
from itertools import zip_longest

import numpy as np
import pandas as pd
import psycopg2

# from database_query.ssh_db import SSH
from sshtunnel import SSHTunnelForwarder

Validator_mapper_data = {
    "=": "is-equals-to",
    ">=": "is-greater-or-equals-to",
    ">": "is-greater-than",
    "<=": "is-less-or-equals-to",
    "<": "is-less-than",
}


cred = {
    "host": "localhost",
    "database": "Dalfin_Configuration",
    "user": "admin",
    "password": "CQ9irmBF8l7lxxJ",
    "port": "5433",
    "ssh_host": "64.23.209.39",
    "ssh_username": "root",
    "ssh_password": "/Users/rajanpandey/.ssh/id_rsa",
}


class SSH:
    def __init__(self, database=None) -> None:
        # if database is None:
        #     tunnel = SSHTunnelForwarder(
        #         (cred.get("ssh_host"), 22),  # Replace with your SSH server details
        #         ssh_username=cred.get("ssh_username"),
        #         ssh_pkey=cred.get("ssh_password"),
        #         remote_bind_address=(
        #             "localhost",
        #             5433,
        #         ),  # Replace with your PostgreSQL server details
        #     )
        #     # PostgreSQL connection setup
        #     tunnel.start()

        #     self.conn = psycopg2.connect(
        #         user="admin",
        #         password="admin",
        #         host="127.0.0.1",
        #         port=tunnel.local_bind_port,
        #         database= database or cred.get("database"),
        #     )
        # else:
        #     self.conn = psycopg2.connect(
        #         user="postgres",
        #         password="postgres",
        #         host="127.0.0.1",
        #         port=5433,
        #         database= database,
        #     )

        tunnel = SSHTunnelForwarder(
            (cred.get("ssh_host"), 22),  # Replace with your SSH server details
            ssh_username=cred.get("ssh_username"),
            ssh_pkey=cred.get("ssh_password"),
            remote_bind_address=(
                "localhost",
                5433,
            ),  # Replace with your PostgreSQL server details
        )
        # PostgreSQL connection setup
        tunnel.start()

        self.conn = psycopg2.connect(
            user="admin",
            password="CQ9irmBF8l7lxxJ",
            host="127.0.0.1",
            port=tunnel.local_bind_port,
            database=database or cred.get("database"),
        )

        self.cursor = self.conn.cursor()

    def display_data(self):
        self.cursor.execute("select * from dalfin.system_model order by -id;")
        data = self.cursor.fetchall()
        columns = [desc[0] for desc in self.cursor.description]
        df = pd.DataFrame(data, columns=columns)
        return df

    def get_table_name(self):
        self.cursor.execute(
            "select distinct table_name from information_schema.tables where table_schema = 'public' and table_type = 'BASE TABLE' and table_name like '%_combined_dataset'"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def system_code(self, industry_name):
        self.cursor.execute(
            f"select distinct system_code from dalfin.system_code where industry_name='{industry_name}';"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def get_category(self):
        self.cursor.execute(
            "select distinct industry_name from dalfin.system_code order by industry_name;"
        )
        data = self.cursor.fetchall()
        return [x[0] for x in data]

    def get_data(self, query):
        self.cursor.execute(query)
        data = self.cursor.fetchall()
        column_names = [desc[0] for desc in self.cursor.description]
        self.conn.commit()
        return data, column_names

    def get_single_data(self, query):
        self.cursor.execute(query)
        data = self.cursor.fetchone()
        column_names = [desc[0] for desc in self.cursor.description]
        self.conn.commit()
        return data, column_names

    def insert_data(self, query, insert_data):
        self.cursor.execute(query, insert_data)
        self.conn.commit()

    def update_bulk_data(self, query, insert_data):
        self.cursor.execute(query, insert_data)

    def insert_data_with_return_id(self, query, insert_data):
        self.cursor.execute(query, insert_data)
        id = self.cursor.fetchone()[0]
        self.conn.commit()
        return id

    def close_db(self):
        self.conn.commit()
        self.cursor.close()
        self.conn.close()


def generate_random_string(length=8, generated_strings=None):
    if generated_strings is None:
        generated_strings = []
    characters = string.ascii_letters + string.digits
    random_string = "".join(random.choice(characters) for _ in range(length))
    while random_string in generated_strings:
        random_string = "".join(random.choice(characters) for _ in range(length))
    return random_string


def get_data_in_json():
    ssh_dalfin = SSH()
    final_data = {}

    sql_query = "select * from dalfin.combined_dataset;"
    results, column_names = ssh_dalfin.get_data(sql_query)
    data = [dict(zip(column_names, row)) for row in results]
    data = pd.DataFrame(data)
    data.replace({np.nan: None}, inplace=True)

    submenu_code_mapper = {
        i["submenu_code"]: i["submenu_id"]
        for i in data[["submenu_id", "submenu_code"]].to_dict(orient="records")
    }
    schema_name = [
        i.lower().replace("-", "_") for i in data["system_code"].to_dict().values()
    ]

    # Selecting specific columns
    selected_columns = ["industry_id", "industry_code", "industry_name"]
    dataframe = data[selected_columns]
    dataframe = dataframe.drop_duplicates()
    column_name_mapping = {
        "industry_id": "id",
        "industry_code": "code",
        "industry_name": "name",
    }
    dataframe.rename(columns=column_name_mapping, inplace=True)
    dataframe = dataframe.to_dict(orient="records")
    final_data["industry_json"] = dataframe
    print("Completed parse industry data")

    # Selecting specific columns
    selected_columns = [
        "system_id",
        "system_code",
        "system_name",
        "system_description",
        "industry_id",
    ]
    dataframe = data[selected_columns]
    dataframe = dataframe.drop_duplicates()
    column_name_mapping = {
        "system_id": "id",
        "system_code": "code",
        "system_name": "name",
        "system_description": "description",
    }
    dataframe.rename(columns=column_name_mapping, inplace=True)
    dataframe['auto'] = True
    dataframe = dataframe.to_dict(orient="records")
    final_data["system_obj"] = dataframe
    print("Completed parse system data")

    # Selecting specific columns
    selected_columns = [
        "menu_id",
        "menu_code",
        "menu_name",
        "menu_description",
        "menu_is_pinned",
        "menu_element",
        "system_id",
    ]
    dataframe = data[selected_columns]
    dataframe = dataframe.drop_duplicates()
    dataframe = dataframe.sort_values(by="menu_id")
    dataframe["order_index"] = range(1, len(dataframe) + 1)
    dataframe["menu_is_pinned"] = (
        dataframe["menu_is_pinned"].replace("", None).fillna(False).astype(bool)
    )
    column_name_mapping = {
        "menu_id": "id",
        "menu_code": "code",
        "menu_name": "name",
        "menu_description": "description",
        "menu_is_pinned": "is_pinned",
        "menu_element": "element",
    }
    dataframe.rename(columns=column_name_mapping, inplace=True)
    dataframe['auto'] = True
    dataframe = dataframe.to_dict(orient="records")
    final_data["menu_obj"] = dataframe
    print("Completed parse menu data")

    # Selecting specific columns
    selected_columns = [
        "submenu_id",
        "submenu_code",
        "submenu_name",
        "submenu_description",
        "submenu_element",
        "submenu_layout",
        "submenu_is_pinned",
        "submenu_parent_id",
        "submenu_show_in_sidebar",
        "submenu_action",
        "menu_id",
        "field_id",
        "column_name",
        "field_component",
        "submenu_order_index",
        "field_response_choice",
        "field_type",
        "field_order",
        "column_default",
        "field_required",
        "character_maximum_length",
        "numeric_precision",
        "tab_no",
        "field_label",
        "field_min",
        "field_max",
        "tab_name",
        "field_placeholder",
        "field_multiple",
        "field_value",
        "form_group",
        "form_ordering",
        "field_label_length",
        "table_name",
        "field_parent",
        "field_position",
        "field_default_css",
        "reference_table",
        "reference_column",
        "field_custom_css",
        "field_use_css",
        "index_field",
        "field_validator",
        "field_function",
    ]
    dataframe = data[selected_columns]
    dataframe = dataframe.groupby("submenu_code")
    form_obj = []
    field_type = []
    for _, group_data in dataframe:
        nest_data = group_data[
            [
                "submenu_id",
                "submenu_code",
                "submenu_name",
                "submenu_description",
                "submenu_element",
                "submenu_layout",
                "submenu_is_pinned",
                "submenu_parent_id",
                "submenu_show_in_sidebar",
                "menu_id",
                "submenu_order_index",
                "submenu_action",
            ]
        ]
        column_name_mapping = {
            "submenu_id": "id",
            "submenu_code": "code",
            "submenu_name": "name",
            "submenu_description": "description",
            "submenu_is_pinned": "is_pinned",
            "submenu_element": "element",
            "submenu_layout": "layout",
            "submenu_parent_id": "parent_id",
            "submenu_show_in_sidebar": "show_in_sidebar",
            "submenu_action": "action",
            "submenu_order_index": "order_index",
        }
        nest_data = nest_data.drop_duplicates()
        nest_data.rename(columns=column_name_mapping, inplace=True)
        nest_data['auto'] = True
        nest_data = nest_data.to_dict(orient="records")[0]

        code = nest_data["code"]
        nest_data["is_pinned"] = bool(nest_data["is_pinned"])
        nest_data["show_in_sidebar"] = bool(nest_data["show_in_sidebar"])
        nest_data["layout"] = "830*1350"
        nest_data["parent_id"] = submenu_code_mapper.get(nest_data["parent_id"], None)

        fields = group_data[
            [
                "field_id",
                "column_name",
                "field_component",
                "field_response_choice",
                "field_type",
                "field_order",
                "column_default",
                "field_required",
                "character_maximum_length",
                "numeric_precision",
                "tab_no",
                "field_label",
                "field_min",
                "field_max",
                "tab_name",
                "field_placeholder",
                "field_multiple",
                "field_value",
                "form_group",
                "form_ordering",
                "field_label_length",
                "field_parent",
                "field_position",
                "field_default_css",
                "table_name",
                "field_custom_css",
                "field_use_css",
                "index_field",
                "reference_table",
                "reference_column",
                "field_validator",
                "field_function",
            ]
        ]

        fields["label"] = (
            fields["column_name"]
            .str.replace("_id", "_")
            .str.replace("_", " ")
            .str.strip()
            .str.title()
        )
        column_name_mapping = {
            "field_id": "id",
            "field_component": "component",
            "field_required": "required",
            "field_label": "label",
            "field_min": "min",
            "field_max": "max",
            "field_response_choice": "response_choice",
            "field_type": "type",
            "field_order": "order",
            "character_maximum_length": "maximum_length",
            "field_placeholder": "placeholder",
            "field_multiple": "multiple",
            "field_value": "value",
            "field_label_length": "label_length",
            "field_parent": "parent",
            "field_position": "position",
            "field_default_css": "default_css",
            "field_custom_css": "custom_css",
            "field_use_css": "use_css",
            "field_validator": "validator",
            "field_function": "function",
        }

        fields.rename(columns=column_name_mapping, inplace=True)
        fields["repeated"] = False
        fields["auto"] = True
        fields["required"] = (
            fields["required"].replace("", None).fillna(False).astype(bool)
        )
        fields["multiple"] = (
            fields["multiple"].replace("", None).fillna(False).astype(bool)
        )
        fields["maximum_length"] = pd.to_numeric(
            fields["maximum_length"], errors="coerce"
        ).astype("Int64")
        fields["numeric_precision"] = pd.to_numeric(
            fields["numeric_precision"], errors="coerce"
        ).astype("Int64")

        relation_data = fields[fields["response_choice"] == "relation"]
        relation_data = (
            pd.to_numeric(relation_data["type"], errors="coerce")
            .astype("Int64")
            .dropna()
            .to_list()
        )

        field_type.extend(relation_data)

        fields = fields.sort_values(by="order")
        text_area_rows = fields[fields["type"] == "text_area"]
        other_rows = fields[fields["type"] != "text_area"]
        fields = pd.concat([other_rows, text_area_rows])

        fields["order"] = range(1, len(fields) + 1)
        fields = fields.sort_values(by="order")
        fields = fields.to_dict(orient="records")
        is_internal_submenu = [i for i in fields if i["type"] == "1"]
        if is_internal_submenu:
            nest_data["name"] = nest_data["name"].split(" ")[0] + " Profile"

        first_element, second_element = fields[::2], fields[1::2]

        generated_strings = set()
        form_final_data = []
        for lists in zip_longest(first_element, second_element, fillvalue=None):
            first_element = lists[0]
            second_element = lists[1]

            first_element_type = first_element["type"]

            if second_element:
                second_element_type = second_element["type"]
            else:
                second_element_type = None

            if (
                first_element["table_name"].lower()
                == first_element["reference_table"].lower()
            ):
                first_element["required"] = False

            if (
                second_element
                and second_element["table_name"].lower()
                == second_element["reference_table"].lower()
            ):
                second_element["required"] = False

            if first_element["maximum_length"]:
                first_element["max"] = first_element["maximum_length"]

            if second_element and second_element["maximum_length"]:
                second_element["max"] = second_element["maximum_length"]

            if first_element["validator"]:
                try:
                    operator, operand = first_element["validator"].strip().split(",")
                    if operand.lower() == "now":
                        value = operand
                        value_type = "keyword"
                        operand = []
                    else:
                        operand = [
                            operand,
                        ]
                        value = ""
                        value_type = None

                    operator = operator.strip()
                    relation = Validator_mapper_data.get(operator)
                    first_element["validator"] = {
                        "BxYMJsNpPYy": {
                            "tid": "BxYMJsNpPYy",
                            "conditions": {
                                "uFlbwNilLhP": {
                                    "tid": "uFlbwNilLhP",
                                    "value": value,
                                    "column": "",
                                    "relation": relation,
                                    "targetedColumn": operand,
                                    "value_type": value_type,
                                }
                            },
                            "conditions_mapping": [
                                {
                                    "id": "ipulQXWCSeKG",
                                    "gate": "and",
                                    "type": "local",
                                    "group": ["uFlbwNilLhP"],
                                }
                            ],
                        }
                    }
                except:
                    print("error in validator", code, first_element["validator"])
                    first_element["validator"] = None

            if second_element and second_element["validator"]:
                try:
                    operator, operand = second_element["validator"].strip().split(",")
                    operator = operator.strip()
                    if operand.lower() == "now":
                        value = operand
                        value_type = "keyword"
                        operand = []
                    else:
                        operand = [
                            operand,
                        ]
                        value = ""
                        value_type = None

                    relation = Validator_mapper_data.get(operator)
                    second_element["validator"] = {
                        "BxYMJsNpPYy": {
                            "tid": "BxYMJsNpPYy",
                            "conditions": {
                                "uFlbwNilLhP": {
                                    "tid": "uFlbwNilLhP",
                                    "column": "",
                                    "relation": relation,
                                    "targetedColumn": operand,
                                    "value": value,
                                    "value_type": value_type,
                                }
                            },
                            "conditions_mapping": [
                                {
                                    "id": "ipulQXWCSeKG",
                                    "gate": "and",
                                    "type": "local",
                                    "group": ["uFlbwNilLhP"],
                                }
                            ],
                        }
                    }
                except:
                    print("error in validator", code, second_element["validator"])
                    second_element["validator"] = None

            if first_element_type in ["text_area", "richtext"]:
                random_string = generate_random_string(
                    generated_strings=generated_strings
                )
                generated_strings.add(random_string)
                first_element["label_length"] = 12
                first_element["form_group"] = random_string

            if second_element_type and second_element_type in ["text_area", "richtext"]:
                random_string = generate_random_string(
                    generated_strings=generated_strings
                )
                generated_strings.add(random_string)
                second_element["label_length"] = 12
                second_element["form_group"] = random_string

            random_string = generate_random_string(generated_strings=generated_strings)
            generated_strings.add(random_string)

            if first_element_type not in ["text_area", "richtext"]:
                first_element["label_length"] = 6
                first_element["form_group"] = random_string

            if second_element_type and second_element_type not in [
                "text_area",
                "richtext",
            ]:
                second_element["label_length"] = 6
                second_element["form_group"] = random_string

            form_final_data.append(first_element)
            if second_element:
                form_final_data.append(second_element)

        nest_data["fields"] = form_final_data
        form_obj.append(nest_data)

    final_data["form_obj"] = form_obj
    print("Completed parse form data")

    global_obj = []
    relation_obj = []

    schema_name = tuple(
        schema_name,
    )
    field_type = tuple(
        field_type,
    )
    sql_query = f"select field_id, column_name, id, field_value as name from dalfin.global_relation where schema_name in {schema_name} order by field_id;"
    results, column_names = ssh_dalfin.get_data(sql_query)
    data = [dict(zip(column_names, row)) for row in results]
    data = pd.DataFrame(data)
    data.replace({np.nan: None}, inplace=True)

    grouped = data.groupby("field_id")
    for field_id, group_data in grouped:
        fields = group_data[["id", "name"]]

        global_obj.append(
            {
                "id": field_id,
                "name": group_data["column_name"].iloc[0],
                "options": fields.to_dict(orient="records"),
            }
        )

    sql_query = f"select id, submenu_code as form_id, field_id as field, column_name as name, multiple, reference_column, separator from dalfin.form_form_relation where schema_name in {schema_name} and id in {field_type} order by field_id;"
    results, column_names = ssh_dalfin.get_data(sql_query)
    for row in results:
        relation_data = dict(zip(column_names, row))
        relation_data["form_id"] = submenu_code_mapper.get(
            relation_data["form_id"].upper().replace("_", "-"), None
        )
        relation_obj.append(relation_data)

    final_data["global_obj"] = global_obj
    final_data["relation_obj"] = relation_obj
    print("Completed parse global and relation data")

    ssh_dalfin.close_db()

    # Specify the file path where you want to save the JSON file
    file_path = f"combined_dataset.json"

    # Writing JSON data to a file
    with open(file_path, "w") as json_file:
        json.dump(final_data, json_file)


if __name__ == "__main__":
    get_data_in_json()
