import json

import numpy as np
import pandas as pd
from database_query.ssh_db import SSH


def get_data_in_json(database):
    ssh_dalfin = SSH(database)
    final_data = {}

    sql_query = "select * from public.combined_dataset;"
    results, column_names = ssh_dalfin.get_data(sql_query)
    data = [dict(zip(column_names, row)) for row in results]
    data = pd.DataFrame(data)
    data.replace({np.nan: None}, inplace=True)

    submenu_code_mapper = {i['submenu_code'] : i['submenu_id'] for i in data[['submenu_id', 'submenu_code']].to_dict(orient='records')}

    # Selecting specific columns
    selected_columns = ['industry_id', 'industry_code', 'industry_name']
    dataframe = data[selected_columns]
    dataframe = dataframe.drop_duplicates()
    column_name_mapping = {
        'industry_id': 'id',
        'industry_code': 'code',
        'industry_name': 'name',
    }
    dataframe.rename(columns=column_name_mapping, inplace=True)
    dataframe = dataframe.to_dict(orient='records')
    final_data['industry_json'] = dataframe

    # Selecting specific columns
    selected_columns = ['system_id', 'system_code', 'system_name', 'system_description', 'industry_id']
    dataframe = data[selected_columns]
    dataframe = dataframe.drop_duplicates()
    column_name_mapping = {
        'system_id': 'id',
        'system_code': 'code',
        'system_name': 'name',
        'system_description': 'description',
    }
    dataframe.rename(columns=column_name_mapping, inplace=True)
    dataframe = dataframe.to_dict(orient='records')
    final_data['system_obj'] = dataframe

    # Selecting specific columns
    selected_columns = ['menu_id', 'menu_code', 'menu_name', 'menu_description', 'menu_is_pinned', 'menu_element', 'system_id']
    dataframe = data[selected_columns]
    dataframe = dataframe.drop_duplicates()
    dataframe = dataframe.sort_values(by='menu_id')
    dataframe['order_index'] = range(1, len(dataframe)+1)
    column_name_mapping = {
        'menu_id': 'id',
        'menu_code': 'code',
        'menu_name': 'name',
        'menu_description': 'description',
        'menu_is_pinned': 'is_pinned',
        'menu_element': 'element',
    }
    dataframe.rename(columns=column_name_mapping, inplace=True)
    dataframe = dataframe.to_dict(orient='records')
    final_data['menu_obj'] = dataframe

    # Selecting specific columns
    selected_columns = ['submenu_id', 'submenu_code', 'submenu_name', 'submenu_description', 'submenu_element',
        'submenu_layout', 'submenu_is_pinned', 'submenu_parent_id', 'submenu_show_in_sidebar', 'submenu_action', 'menu_id',
        'field_id', 'column_name', 'field_component', 'submenu_order_index',
        'field_response_choice', 'field_type', 'field_order',
        'column_default', 'field_required', 'character_maximum_length',
        'numeric_precision', 'tab_no', 'field_label', 'field_min', 'field_max',
        'tab_name', 'field_placeholder', 'field_multiple', 'field_value',
        'form_group', 'form_ordering', 'field_label_length',
        'field_parent', 'field_position', 'field_default_css', 'reference_table', 'reference_column',
        'field_custom_css', 'field_use_css', 'index_field', 'field_validator', 'field_function']
    dataframe = data[selected_columns]
    dataframe = dataframe.groupby('submenu_code')
    form_obj = []
    for _, group_data in dataframe:
        nest_data = group_data[['submenu_id', 'submenu_code', 'submenu_name', 'submenu_description', 'submenu_element',
        'submenu_layout', 'submenu_is_pinned', 'submenu_parent_id', 'submenu_show_in_sidebar', 'menu_id', 'submenu_order_index',
        'submenu_action']]
        column_name_mapping = {
            'submenu_id': 'id',
            'submenu_code': 'code',
            'submenu_name': 'name',
            'submenu_description': 'description',
            'submenu_is_pinned': 'is_pinned',
            'submenu_element': 'element',
            'submenu_layout': 'layout',
            'submenu_parent_id': 'parent_id',
            'submenu_show_in_sidebar': 'show_in_sidebar',
            'submenu_action': 'action',
            'submenu_order_index': 'order_index',
        }
        nest_data = nest_data.drop_duplicates()
        nest_data.rename(columns=column_name_mapping, inplace=True)
        nest_data = nest_data.to_dict(orient='records')[0]
        nest_data['parent_id'] = submenu_code_mapper.get(nest_data['parent_id'], None)

        fields = group_data[['field_id', 'column_name', 'field_component',
        'field_response_choice', 'field_type', 'field_order',
        'column_default', 'field_required', 'character_maximum_length',
        'numeric_precision', 'tab_no', 'field_label', 'field_min', 'field_max',
        'tab_name', 'field_placeholder', 'field_multiple', 'field_value',
        'form_group', 'form_ordering', 'field_label_length',
        'field_parent', 'field_position', 'field_default_css', 
        'field_custom_css', 'field_use_css', 'index_field', 'reference_table', 'reference_column',
        'field_validator', 'field_function']]

        fields['label'] = fields['column_name'].str.replace('_id', '_').str.replace('_', ' ').str.strip().str.title()
        column_name_mapping = {
            'field_id': 'id',
            'field_component': 'component',
            'field_required': 'required',
            'field_label': 'label',
            'field_min': 'min',
            'field_max': 'max',
            'field_response_choice': 'response_choice',
            'field_type': 'type',
            'field_order': 'order',
            'character_maximum_length': 'maximum_length',
            'field_placeholder': 'placeholder',
            'field_multiple': 'multiple',
            'field_value': 'value',
            'field_label_length': 'label_length',
            'field_parent': 'parent',
            'field_position': 'position',
            'field_default_css': 'default_css',
            'field_custom_css': 'custom_css',
            'field_use_css': 'use_css',
            'field_validator': 'validator',
            'field_function': 'function',
        }

        fields.rename(columns=column_name_mapping, inplace=True)
        fields['repeated'] = False
        fields['maximum_length'] = pd.to_numeric(fields['maximum_length'], errors='coerce').astype('Int64')
        fields['numeric_precision'] = pd.to_numeric(fields['numeric_precision'], errors='coerce').astype('Int64')

        fields = fields.sort_values(by='order')
        fields['order'] = range(1, len(fields)+1)
        nest_data['fields'] = fields.to_dict(orient='records')
        form_obj.append(nest_data)

    final_data['form_obj'] = form_obj

    global_obj = []
    relation_obj = []

    ssh_dalfin = SSH()
    sql_query = f"select field_id, column_name, id, field_value from dalfin.global_relation order by field_id;"
    results, column_names = ssh_dalfin.get_data(sql_query)
    data = [dict(zip(column_names, row)) for row in results]
    data = pd.DataFrame(data)
    data.replace({np.nan: None}, inplace=True)

    grouped = data.groupby('field_id')
    for field_id, group_data in grouped:
        fields = group_data[['id', 'field_value']]
        
        global_obj.append({
            'id' : field_id,
            'name' : group_data['column_name'].iloc[0], 
            'options' : fields.to_dict(orient='records')
        })

    sql_query = f"select id, submenu_code as form_id, field_id as field, column_name as name, multiple, reference_column, separator from dalfin.form_form_relation order by field_id;"
    results, column_names = ssh_dalfin.get_data(sql_query)
    for row in results:
        relation_data = dict(zip(column_names, row))
        relation_data['form_id'] = submenu_code_mapper.get(relation_data['form_id'].upper().replace('_', '-'), None)
        relation_obj.append(relation_data)

    final_data['global_obj'] = global_obj
    final_data['relation_obj'] = relation_obj

    ssh_dalfin.close_db()

    final_data["internal_obj"] = [
        {
            "id": 1,
            "name": "Organization User",
            "module": "OrganizationUser",
            "field": "full_name"
        }
    ]

    # Specify the file path where you want to save the JSON file
    database = database.lower()
    file_path = f"{database}_combined_dataset.json"

    # Writing JSON data to a file
    with open(file_path, 'w') as json_file:
        json.dump(final_data, json_file)
