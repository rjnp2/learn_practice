SELECT establish_aerospace();
SELECT establish_agriculture();
SELECT establish_automotive();
SELECT establish_biotechnology();
SELECT establish_commerce();
SELECT establish_construction();
SELECT establish_education();
SELECT establish_electronics();
SELECT establish_energy();
SELECT establish_environmental_management();
SELECT establish_fashion();
SELECT establish_finance();
SELECT establish_food();
SELECT establish_government();
SELECT establish_health();
SELECT establish_hospitality();
select establish_insurance();
SELECT establish_it();
select establish_legal();
SELECT establish_manufacturing();
select establish_maritime();
select establish_media();
SELECT establish_mining();
select establish_pet();
SELECT establish_petroleum();
SELECT establish_publishing();
SELECT establish_realEstate();
SELECT establish_retail();
SELECT establish_sports();
SELECT establish_telecommunications();
SELECT establish_tourism();
SELECT establish_transportation();

CREATE TABLE dalfin.combined_dataset AS
SELECT 
    field_id,
    schema_name,
    industry_id,
    industry_code,
    industry_name,
    DENSE_RANK() OVER (ORDER BY system_code) AS system_id,
    system_code,
    system_name,
    system_description,
    DENSE_RANK() OVER (ORDER BY system_code,menu_code) AS menu_id,
    menu_name,
    menu_description,
    menu_code,
    menu_is_pinned,
    menu_element,
    menu_order_index,
    DENSE_RANK() OVER (ORDER BY system_code,menu_code, submenu_code) AS submenu_id,
    submenu_name,
    submenu_description,
    submenu_code,
    submenu_is_pinned,
    submenu_order_index,submenu_element,submenu_layout,
    submenu_parent_id,submenu_show_in_sidebar,
    submenu_action,
    table_name,
    table_description,
    (CASE
      WHEN column_name = 'data_of_birth' then 'date_of_birth'
      else column_name
    end)::text as column_name,
    field_type,
    field_order,
    (CASE
      WHEN field_label = 'Data Of Birth' then 'Date Of Birth'
      else field_label
    end)::text as field_label,
    (CASE
      WHEN field_placeholder = 'Enter Data Of Birth' then 'Enter Date Of Birth'
      else field_placeholder
    end)::text as field_placeholder,
    (CASE
      WHEN column_default is not null and field_type in ('date', 'datetime', 'time') then 'now'
      WHEN column_default is not null and field_type = 'checkbox' then column_default
      else null
    end)::text as column_default,
    (CASE
      WHEN reference_table != 'No' and reference_table != table_name then true
      WHEN field_type = 'text' then true
      else false
    end)::boolean as field_required,
    field_min,
    field_max,
    character_maximum_length,
    numeric_precision,
    reference_table,
    reference_column,
    field_multiple,
    form_group,
    form_ordering,
    field_label_length,
    field_parent,
    field_position,
    field_use_css,
    field_default_css,
    field_custom_css,
    field_component,
    field_response_choice,
    field_value,
    index_field,
    (CASE
      WHEN column_name = 'date_of_birth' then '<,now'
      WHEN column_name = 'establishment_date' then '<,now'
      WHEN column_name in ('hire_date','date_of_hire','join_date') then '<=,now'
      WHEN column_name = 'license_expiry_date' then '>,now'
      else field_validator
    end)::text as field_validator,
    field_function,
    tab_no,
    tab_name
FROM 
(
    SELECT * from dalfin.aerospace_combined_dataset
    UNION ALL
    SELECT * from dalfin.agriculture_combined_dataset
    UNION ALL
    SELECT * from dalfin.automotive_combined_dataset
    UNION ALL
    SELECT * from dalfin.biotechnology_combined_dataset
    UNION ALL
    SELECT * from dalfin.commerce_combined_dataset
    UNION ALL
    SELECT * from dalfin.construction_combined_dataset
    UNION ALL
    SELECT * from dalfin.education_combined_dataset
    UNION ALL
    SELECT * from dalfin.electronics_combined_dataset
    UNION ALL
    SELECT * from dalfin.energy_combined_dataset
    UNION ALL
    SELECT * from dalfin.environmental_management_combined_dataset
    UNION ALL
    SELECT * from dalfin.fashion_combined_dataset
    UNION ALL
    SELECT * from dalfin.finance_combined_dataset
    UNION ALL
    SELECT * from dalfin.food_combined_dataset
    UNION ALL
    SELECT * from dalfin.government_combined_dataset
    UNION ALL
    SELECT * from dalfin.health_combined_dataset
    UNION ALL
    SELECT * from dalfin.hospitality_combined_dataset
    UNION ALL
    SELECT * from dalfin.insurance_combined_dataset
    UNION ALL
    SELECT * from dalfin.it_combined_dataset
    UNION ALL
    SELECT * from dalfin.legal_combined_dataset
    UNION ALL
    SELECT * from dalfin.manufacturing_combined_dataset
    UNION ALL
    SELECT * from dalfin.maritime_combined_dataset
    UNION ALL
    SELECT * from dalfin.media_combined_dataset
    UNION ALL
    SELECT * from dalfin.mining_combined_dataset
    UNION ALL
    SELECT * from dalfin.pet_combined_dataset
    UNION ALL
    SELECT * from dalfin.petroleum_combined_dataset
    UNION ALL
    SELECT * from dalfin.publishing_combined_dataset
    UNION ALL
    SELECT * from dalfin.realestate_combined_dataset
    UNION ALL
    SELECT * from dalfin.retail_combined_dataset
    UNION ALL
    SELECT * from dalfin.sports_combined_dataset
    UNION ALL
    SELECT * from dalfin.telecommunications_combined_dataset
    UNION ALL
    SELECT * from dalfin.tourism_combined_dataset
    UNION ALL
    SELECT * from dalfin.transportation_combined_dataset
)
