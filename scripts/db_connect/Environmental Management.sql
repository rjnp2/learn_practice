CREATE EXTENSION IF NOT EXISTS dblink;

CREATE OR REPLACE FUNCTION establish_Environmental_Management() RETURNS void AS $$
BEGIN
  PERFORM dblink_connect('Environmental_Management_combined', 'host=localhost port=5433 dbname=Environmental_Management user=admin password=CQ9irmBF8l7lxxJ');
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE VIEW dalfin.Environmental_Management_combined_dataset
AS SELECT * FROM dblink(
    'Environmental_Management_combined'::text, 
    'select * from public.combined_dataset'::text
    ) remote_menu(
    field_id integer, 
    schema_name text,
    industry_id integer,
    industry_code text,
    industry_name text,
    system_id integer,
    system_code text,
    system_name text,
    system_description text,
    menu_id integer,
    menu_name text,
    menu_description text,
    menu_code text,
    menu_is_pinned boolean, 
    menu_element text,
    menu_order_index integer,
    submenu_id integer,
    submenu_name text,
    submenu_description text,
    submenu_code text, 
    submenu_is_pinned boolean,
    submenu_order_index integer,
    submenu_element text,
    submenu_layout text,
    submenu_parent_id text,
    submenu_show_in_sidebar boolean, 
    submenu_action text,
    table_name text,
    table_description text,
    column_name text,
    field_type text,
    field_order integer,
    field_label text,
    field_placeholder text,
    column_default text,
    field_required boolean,
    field_min integer,
    field_max integer,
    character_maximum_length integer,
    numeric_precision integer,
    reference_table text,
    reference_column text,
    field_multiple boolean,
    form_group text,
    form_ordering integer,
    field_label_length integer,
    field_parent integer,
    field_position text,
    field_use_css text,
    field_default_css integer,
    field_custom_css json,
    field_component text,
    field_response_choice text,
    field_value text, 
    index_field text,
    field_validator text,
    field_function text,
    tab_no integer,
    tab_name text
)
ORDER BY industry_id;

SELECT establish_Environmental_Management();

select * from dalfin.Environmental_Management_combined_dataset bcd ;