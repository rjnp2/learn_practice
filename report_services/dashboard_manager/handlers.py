from sqlalchemy import or_
from sqlalchemy.orm import Session, subqueryload

from technical.endpoints.master_services.user import models as user_orm
from technical.imports.handler_import import (
    CustomPage,
    Depends,
    HTTPException,
    InferringRouter,
    JSONResponse,
    Request,
    cbv,
    get_db,
    get_tenant_code,
    jsonable_encoder,
    oauth2,
    ordering_join,
    paginate,
    schema_context,
)

from .models import DashboardChartMapper, DashboardManager
from .repository import DashboardManagerRepository
from .schema import (
    BulkDeleteDashboardManager,
    BulkDeleteDashboardMapperManager,
    DashboardManagerSchema,
    GetDashboardManagerDetailSchema,
    GetDashboardManagerSchema,
    SetDefaultDashboardManager,
    SetPositionDashboardManager,
)

router = InferringRouter()
lower = ["name", "description"]
exclude = [
    "created_by",
]

headers = {
    "id": "Id",
    "name": "Package Mapper Name",
    "description": "Description",
    "created_at": "Created At",
    "created_by": "Created By",
}
repo = DashboardManagerRepository()


@cbv(router=router)
class DashboardManagerAPIView:

    def __init__(
        self,
        request: Request,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ) -> None:
        # ValidatePermission(db, current_user, request)
        self.tenant_code = "public"
        self.organization = current_user.org_id
        self.current_user = current_user
        if not self.organization:
            raise HTTPException(400, detail={"message": "Access Denied"})

    @staticmethod
    def filter_data(
        db,
        queryset,
        search_query=None,
        sort_by=None,
        is_layout=None,
        is_default=None,
        position=[],
    ):
        if search_query:
            queryset = queryset.where(
                or_(
                    DashboardManager.name.ilike("%" + search_query + "%"),
                    DashboardManager.description.ilike("%" + search_query + "%"),
                )
            )

        if is_layout is not None:
            queryset = queryset.where(DashboardManager.is_layout == is_layout)

        if is_default is not None:
            queryset = queryset.where(DashboardManager.is_default == is_default)

        if position:
            queryset = queryset.where(DashboardManager.position == position)

        sort_by = sort_by or "-created_at"
        queryset = ordering_join(
            {},
            DashboardManager,
            queryset,
            lower=lower,
            exclude=exclude,
            sort_by=sort_by,
        )
        return queryset.all()

    @router.get("/", response_model=CustomPage[GetDashboardManagerSchema])
    async def get(
        self,
        system_id: int,
        db: Session = Depends(get_db),
        q: str = None,
        sortBy: str = None,
        is_layout: bool = None,
        is_default: bool = None,
        position: list[int] = [],
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(DashboardManager)
                .where(
                    DashboardManager.system_id == system_id,
                )
                .outerjoin(DashboardManager.created_obj)
            )

            queryset = self.filter_data(
                db=db,
                queryset=queryset,
                search_query=q,
                sort_by=sortBy,
                is_layout=is_layout,
                is_default=is_default,
                position=position,
            )
            return paginate(
                queryset,
                headers=headers,
                exclude=exclude,
            )

    @router.get("/{id}", response_model=GetDashboardManagerDetailSchema)
    async def retrieve(
        self,
        id: int,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(DashboardManager)
                .filter(DashboardManager.id == id)
                .outerjoin(DashboardManager.created_obj)
                .options(
                    subqueryload(DashboardManager.chart_data).subqueryload(
                        DashboardChartMapper.chart_obj
                    ),
                )
            )

            queryset = queryset.first()
            if queryset is None:
                raise HTTPException(status_code=404, detail="Id not found")

            return queryset

    @router.post("/")
    async def post(
        self,
        dashboard_manager: DashboardManagerSchema,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = await repo.save(
                db=db,
                dashboard_manager=dashboard_manager,
                current_user=current_user,
                organization=self.organization,
            )

            return JSONResponse(
                {
                    "message": "Dashboard saved successfully",
                    "data": data,
                }
            )

    @router.put("/set-default")
    async def set_default(
        self,
        dashboard_manager: SetDefaultDashboardManager,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.set_default(db=db, dashboard_manager=dashboard_manager)
            return JSONResponse({"message": "successfully updated default value."})

    @router.put("/set-position")
    async def set_position(
        self,
        dashboard_manager: list[SetPositionDashboardManager],
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.set_position(db=db, dashboard_manager=dashboard_manager)
            return JSONResponse({"message": "successfully updated position."})

    @router.put("/{id}")
    async def put(
        self,
        dashboard_manager: DashboardManagerSchema,
        id: int,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = await repo.update(
                db=db,
                dashboard_manager=dashboard_manager,
                current_user=current_user,
                id=id,
            )
            return JSONResponse(
                {"message": "Dashboard saved successfully", "data": data}
            )

    @router.delete("/")
    async def delete(
        self,
        dashboard_manager: BulkDeleteDashboardManager,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.delete(db=db, dashboard_manager=dashboard_manager)
            return JSONResponse({"message": "Deleted successfully"})

    @router.delete("/items/")
    async def delete_item(
        self,
        dashboard_manager: BulkDeleteDashboardMapperManager,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.delete_item(db=db, dashboard_manager=dashboard_manager)
            return JSONResponse({"message": "Deleted successfully"})
