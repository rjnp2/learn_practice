import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

from technical.settings.database import Base, BaseModel


class DashboardManager(BaseModel, Base):
    __tablename__ = "dashboard_manager"
    __table_args__ = {"schema": "public"}

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    name = sa.Column(sa.String(200), nullable=False)
    description = sa.Column(sa.Text(), nullable=True)

    layer_data = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default={},
        server_default="{}",
    )

    is_layout = sa.Column(sa.Boolean, default=False)
    is_default = sa.Column(sa.Boolean, default=False)
    dashboard_position = sa.Column(sa.Integer, default=0)

    position = sa.Column(sa.Integer, default=0)
    #     JSONB(astext_type=sa.Text()),
    #     nullable=False,
    #     default=[],
    #     server_default="[]",
    # )

    system_id = sa.Column(sa.ForeignKey("public.system.id"), nullable=False)
    organization_id = sa.Column(sa.ForeignKey("public.organization.id"), nullable=False)

    created_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)
    updated_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)

    created_obj = sa.orm.relationship("User", foreign_keys=[created_by])
    updated_obj = sa.orm.relationship("User", foreign_keys=[updated_by])

    chart_data = sa.orm.relationship(
        "DashboardChartMapper", back_populates="dashboard_obj"
    )


class DashboardChartMapper(Base):
    __tablename__ = "dashboard_chart_mapper"
    __table_args__={"schema":"public"}

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    chart_id = sa.Column(sa.ForeignKey("public.chart_manager.id"), nullable=False)
    dashboard_id = sa.Column(
        sa.ForeignKey("public.dashboard_manager.id"), nullable=False
    )

    label = sa.Column(sa.String(200), nullable=False)
    fields = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default={},
        server_default="{}",
    )

    dashboard_obj = sa.orm.relationship("DashboardManager", back_populates="chart_data")
    chart_obj = sa.orm.relationship("ChartManager", back_populates="mapped_charts")
