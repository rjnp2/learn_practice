from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from datetime import datetime, timezone

from .models import DashboardChartMapper, DashboardManager
from .schema import (
    BulkDeleteDashboardManager,
    BulkDeleteDashboardMapperManager,
    DashboardManagerSchema,
    SetPositionDashboardManager,
    SetDefaultDashboardManager,
)


class DashboardManagerRepository:
    def __init__(self) -> None:
        pass

    async def save(
        self,
        dashboard_manager: DashboardManagerSchema,
        db: Session,
        current_user,
        organization,
    ):
        dashboard_obj = DashboardManager(
            name=dashboard_manager.name,
            description=dashboard_manager.description,
            layer_data=dashboard_manager.layer_data,
            position=dashboard_manager.position,
            is_layout=dashboard_manager.is_layout,
            system_id=dashboard_manager.system_id,
            organization_id=organization,
            created_by=current_user.id,
        )

        db.add(dashboard_obj)
        db.flush()

        chart_data = dashboard_manager.chart_data
        dash_mp_list = []
        for chart_ in chart_data:
            dash_mp_obj = DashboardChartMapper(
                chart_id=chart_.chart_id,
                dashboard_id=dashboard_obj.id,
                label=chart_.label,
                fields=chart_.fields,
            )
            db.add(dash_mp_obj)
            dash_mp_list.append(dash_mp_obj)
            db.flush()

        db.commit()

        data = jsonable_encoder(dashboard_obj)
        data["id"] = dashboard_obj.id
        data["created_at"] = str(datetime.now(timezone.utc).isoformat())
        data["updated_at"] = str(datetime.now(timezone.utc).isoformat())
        data["charts"] = jsonable_encoder(dash_mp_list)
        return data

    async def update(
        self,
        dashboard_manager: DashboardManagerSchema,
        id: int,
        db: Session,
        current_user,
    ):
        dashboard_obj = db.query(DashboardManager).get(id)
        if not dashboard_obj:
            raise HTTPException(400, detail={"message": "Dashboard Id doesnot exist."})

        dashboard_obj.name = dashboard_manager.name
        dashboard_obj.description = dashboard_manager.description

        dashboard_obj.layer_data = dashboard_manager.layer_data
        dashboard_obj.position = dashboard_manager.position
        dashboard_obj.is_layout = dashboard_manager.is_layout

        dashboard_obj.system_id = dashboard_manager.system_id
        dashboard_obj.updated_by = current_user.id
        db.add(dashboard_obj)
        db.flush()

        chart_data = dashboard_manager.chart_data
        dash_mp_list = []
        for chart_ in chart_data:
            chart_id = chart_.id

            if chart_id:
                dash_mp_obj = (
                    db.query(DashboardChartMapper)
                    .where(
                        DashboardChartMapper.id == chart_id,
                        DashboardChartMapper.dashboard_id == dashboard_obj.id,
                    )
                    .first()
                )
                if not dash_mp_obj:
                    raise HTTPException(
                        400,
                        detail={
                            "message": f"Dashoard item {chart_id} Id doesnot exist."
                        },
                    )

                dash_mp_obj.label = chart_.label
                dash_mp_obj.fields = chart_.fields

            else:
                dash_mp_obj = DashboardChartMapper(
                    chart_id=chart_.chart_id,
                    dashboard_id=dashboard_obj.id,
                    label=chart_.label,
                    fields=chart_.fields,
                )

            db.add(dash_mp_obj)
            dash_mp_list.append(dash_mp_obj)
            db.flush()

        db.add(dashboard_obj)
        db.commit()
        data = jsonable_encoder(dashboard_obj)
        data["id"] = dashboard_obj.id
        data["charts"] = jsonable_encoder(dash_mp_list)
        return data

    async def set_default(
        self,
        dashboard_manager: SetDefaultDashboardManager,
        db: Session,
    ):
        dashboard_objs = (
            db.query(DashboardManager)
            .where(DashboardManager.id.in_(dashboard_manager.ids))
            .all()
        )
        if not dashboard_objs:
            raise HTTPException(400, detail={"message": "Dashboard Ids doesnot exist."})

        for dashboard_obj in dashboard_objs:
            dashboard_obj.is_default = not dashboard_obj.is_default
            db.add(dashboard_obj)
        db.commit()

    async def set_position(
        self,
        dashboard_manager: list[SetPositionDashboardManager],
        db: Session,
    ):
        for dashboard in dashboard_manager:
            dashboard_obj = (
                db.query(DashboardManager)
                .where(DashboardManager.id == dashboard)
                .first()
            )
            if not dashboard_obj:
                raise HTTPException(
                    400, detail={"message": "Dashboard Id doesnot exist."}
                )

            dashboard_obj.dashboard_position = dashboard.position
            db.add(dashboard_obj)
        db.commit()

    async def delete(self, dashboard_manager: BulkDeleteDashboardManager, db: Session):
        db.query(DashboardManager).filter(
            DashboardManager.id.in_(dashboard_manager.config_ids)
        ).delete(synchronize_session=False)
        db.commit()

    async def delete_item(
        self, dashboard_manager: BulkDeleteDashboardMapperManager, db: Session
    ):
        pakage_mapper_queryset = db.query(DashboardChartMapper).where(
            DashboardChartMapper.id.in_(dashboard_manager.config_ids),
            DashboardChartMapper.dashboard_id == dashboard_manager.id,
        )
        pakage_mapper_queryset.delete()
        db.commit()
