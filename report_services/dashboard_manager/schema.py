from datetime import datetime as date

from pydantic import BaseModel, Field, validator
import typing
from sqlalchemy.dialects.postgresql import JSONB

from technical.endpoints.report_services.chart_manager.schema import (
    ChartManagerRetrieveSchema,
)
from technical.utility.user_schema import UserInfo


class DashboardChartMapperSchema(BaseModel):
    id: int | None = None
    chart_id: int
    dashboard_id: int | None = None
    label: str | None = None

    fields: dict | None = {}

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class DashboardChartMapperDetailsSchema(BaseModel):
    id: int
    chart_obj: ChartManagerRetrieveSchema
    dashboard_id: int
    chart_id: int
    label: str | None = None

    fields: list | dict

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class BaseDashboardManagerSchema(BaseModel):
    name: str = Field(..., min_length=5, max_length=50)
    description: str | None = None

    is_layout: bool = False
    is_default: bool = False
    dashboard_position: int = 0

    position: typing.Optional[int]

    system_id: int

    class Config:
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class DashboardManagerSchema(BaseDashboardManagerSchema):
    chart_data: list[DashboardChartMapperSchema]
    layer_data: list[dict] = []

    # @validator("chart_data")
    # def check_chart_data_length(cls, value):
    #     if len(value) == 0:
    #         raise ValueError("chart_data must contain at least one chart.")
    #     return value

    class Config:
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class GetDashboardManagerSchema(BaseDashboardManagerSchema):
    id: int

    organization_id: int

    created_obj: UserInfo
    created_at: date
    updated_obj: UserInfo | None = None
    updated_at: date

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class GetDashboardManagerDetailSchema(BaseDashboardManagerSchema):
    id: int

    organization_id: int

    chart_data: list[DashboardChartMapperDetailsSchema]
    layer_data: list | dict = []

    created_obj: UserInfo
    created_at: date
    updated_obj: UserInfo | None = None
    updated_at: date

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class BulkDeleteDashboardManager(BaseModel):
    config_ids: list[int]


class SetDefaultDashboardManager(BaseModel):
    ids: list[int]


class SetPositionDashboardManager(BaseModel):
    id: int
    position: int


class BulkDeleteDashboardMapperManager(BaseModel):
    id: int
    config_ids: list[int]
