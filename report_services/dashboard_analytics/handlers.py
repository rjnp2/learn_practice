import ast

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, subqueryload

from technical.configurations.exceptions import BadRequest_400
from technical.endpoints.master_services.user import models as user_orm
from technical.endpoints.report_services.dashboard_manager.models import (
    DashboardChartMapper,
    DashboardManager,
)
from technical.imports.handler_import import (
    Depends,
    HTTPException,
    InferringRouter,
    Request,
    cbv,
    get_db,
    get_tenant_code,
    oauth2,
    schema_context,
)
from technical.utility.raw_sql_manager import create_raw_query
from technical.utility.views_tables import check_table_exist

router = InferringRouter()


@cbv(router=router)
class DashboardAnalyticsAPIView:
    def __init__(
        self,
        request: Request,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ) -> None:
        # ValidatePermission(db, current_user, request)
        self.tenant_code = "public"
        self.organization = current_user.org_id
        self.current_user = current_user
        if not self.organization:
            raise HTTPException(400, detail={"message": "Access Denied"})

    @router.get("/{id}")
    async def get(
        self,
        id: int,
        global_filters: str | None = "{}",
        db: Session = Depends(get_db),
    ):
        try:
            global_filters = ast.literal_eval(global_filters)
            if type(global_filters) != list:
                raise BadRequest_400(
                    message="Global filters data must be dict.",
                )
        except:
            global_filters = {}

        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(DashboardManager)
                .filter(DashboardManager.id == id)
                .outerjoin(DashboardManager.created_obj)
                .options(
                    subqueryload(DashboardManager.chart_data).subqueryload(
                        DashboardChartMapper.chart_obj
                    ),
                )
            )

            queryset = queryset.first()
            if not queryset:
                raise BadRequest_400(message="Id doesnot exist in dashboard table.")

            final_data = {}
            for chart_da in queryset.chart_data:
                chart_obj = chart_da.chart_obj
                chart_obj_id = chart_obj.id

                form_name = f"package_{chart_obj.package_id}"
                raw_query = check_table_exist(
                    table_name=form_name,
                    schema_name=self.tenant_code,
                )
                exists = db.execute(raw_query).fetchone()[0]
                if not exists:
                    final_data[chart_obj_id] = {
                        "message": "There is issues in package or chart, please check or verify this chart or package."
                    }
                    continue

                query_text = create_raw_query(
                    db=db,
                    chart_obj=chart_obj,
                    schema_name=self.tenant_code,
                    global_filters=global_filters,
                )

                if query_text is None:
                    final_data[chart_obj_id] = {
                        "message": "Error occurred while fetching data from the package or retrieving analytics chart. Please check the chart or package from their respective page."
                    }

                elif query_text == "no_filter":
                    final_data[chart_obj_id] = []

                else:
                    try:
                        data = db.execute(query_text).fetchall()
                        final_data[chart_obj_id] = jsonable_encoder(data)
                    except Exception as e:
                        db.rollback()
                        final_data[chart_obj_id] = {
                            "message": "Error occurred while fetching data from the package or retrieving analytics chart. Please check the chart or package from their respective page."
                        }
            return final_data
