from datetime import datetime as date
from enum import Enum

from pydantic import BaseModel, Field
from sqlalchemy.dialects.postgresql import JSONB

from technical.utility.user_schema import UserInfo


class ChartType(str, Enum):
    # card group
    scorecard = "scoreCard"

    # card group
    table = "table"

    # min-max group
    gauge = "gauge"
    column_range = "columnRange"

    # pie group with operation
    pie_chart = "pieChart"
    doughnut_chart = "doughnut"
    donut_plus_pie_chart = "donutPlusPie"
    semi_donut_chart = "semiDonut"
    bar_chart = "barChart"
    column_chart = "columnChart"
    rotated_bar_chart = "rotatedBarChart"
    radial_bar_chart = "radialBarChart"
    stacked_column = "stackedColumn"
    negative_bar_chart = "negativeBarChart"
    multi_data = "multiData"
    tree_map = "treeMap"
    funnel = "funnel"

    # line with trunc group
    time_series = "timeSeries"

    # line group
    spider = "spider"
    line_chart = "lineChart"
    area_chart = "areaChart"
    smooth_line_chart = "smoothLineChart"
    scatter_plot = "scatterPlot"
    bubble_plot = "bubblePlot"
    bell_curve = "bellCurve"
    heat_map = "heatMap"


class OperationChoice(str, Enum):
    sum = "sum"
    count = "count"
    avg = "avg"


class FilterSchema(BaseModel):
    operand: str
    operator: str
    value: str | int


class FieldDataSchema(BaseModel):
    filters: list[FilterSchema] | None = []
    x_axis: list[str]
    y_axis: list[str] | None = []
    limit: int | None
    operation: OperationChoice | None
    sorting: str | None
    interval: str | None


class ChartManagerSchema(BaseModel):
    name: str = Field(..., min_length=5, max_length=50)
    description: str | None

    package_id: int
    system_id: int
    chart_type: ChartType
    field_data: FieldDataSchema

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class ChartManagerRetrieveSchema(ChartManagerSchema):
    id: int

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class GetChartManagersSchema(ChartManagerSchema):
    id: int
    organization_id: int

    created_obj: UserInfo
    created_at: date
    updated_obj: UserInfo | None = None
    updated_at: date

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class BulkDeleteChartManager(BaseModel):
    config_ids: list[int]
