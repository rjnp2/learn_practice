from sqlalchemy import or_
from sqlalchemy.orm import Session

from technical.configurations.exceptions import BadRequest_400
from technical.endpoints.master_services.user import models as user_orm
from technical.endpoints.report_services.package.models import Package
from technical.imports.handler_import import (
    CustomPage,
    Depends,
    HTTPException,
    InferringRouter,
    JSONResponse,
    Request,
    cbv,
    get_db,
    get_tenant_code,
    jsonable_encoder,
    oauth2,
    ordering_join,
    paginate,
    schema_context,
)
from technical.utility.raw_sql_manager import create_raw_query
from technical.utility.views_tables import check_table_exist, get_column_data

from .models import ChartManager
from .repository import ChartManagerRepository
from .schema import BulkDeleteChartManager, ChartManagerSchema, GetChartManagersSchema

router = InferringRouter()
lower = ["name", "description"]
exclude = [
    "created_by",
]

headers = {
    "id": "Id",
    "name": "Chart Name",
    "description": "Description",
    "created_at": "Created At",
    "created_by": "Created By",
}
repo = ChartManagerRepository()


def flatten_list(data, final_field_data=None):
    if final_field_data is None:
        final_field_data = []

    for item in data:
        if isinstance(item, list):
            final_field_data.extend(item)
        else:
            final_field_data.append(item)
    return final_field_data


@cbv(router=router)
class ChartManagerAPIView:

    def __init__(
        self,
        request: Request,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ) -> None:
        # ValidatePermission(db, current_user, request)
        self.tenant_code = "public"
        self.organization = current_user.org_id
        self.current_user = current_user
        if not self.organization:
            raise HTTPException(400, detail={"message": "Access Denied"})

    @staticmethod
    def filter_data(
        queryset,
        package_id=None,
        search_query=None,
        sort_by=None,
        chart_type=None,
    ):
        if search_query:
            queryset = queryset.where(
                or_(
                    ChartManager.name.ilike("%" + search_query + "%"),
                    ChartManager.description.ilike("%" + search_query + "%"),
                )
            )
        if package_id is not None:
            queryset = queryset.where(ChartManager.package_id == package_id)

        if chart_type is not None:
            queryset = queryset.where(ChartManager.chart_type == chart_type)

        sort_by = sort_by or "-created_at"
        queryset = ordering_join(
            {}, ChartManager, queryset, lower=lower, exclude=exclude, sort_by=sort_by
        )
        return queryset.all()

    def validated_data(self, db, data):
        package_id = data.package_id
        field_data = data.field_data
        filters = field_data.filters

        package_obj = db.query(Package).get(package_id)
        if not package_obj:
            raise BadRequest_400(message="Package id doesnot exists")

        package_id = f"package_{package_obj.id}"

        raw_query = get_column_data(table_name=package_id, schema_name=self.tenant_code)
        data = db.execute(raw_query).fetchall()
        if not data:
            raise BadRequest_400(
                message="Package views was not created properly, please update or save this package."
            )

        if not field_data.x_axis:
            raise BadRequest_400(message="X-axis is not found for this chart")

        fields = [i[0] for i in data]
        final_field_data = []
        final_field_data = flatten_list(field_data.y_axis, final_field_data)
        final_field_data = flatten_list(field_data.x_axis, final_field_data)
        field_data = [i for i in final_field_data if i and i not in fields]

        if field_data:
            field_data = ",".join(field_data)
            raise BadRequest_400(
                message=f"{field_data} fields doesn't exist in Package."
            )

        for filte_ in filters:
            operand = filte_.operand
            if operand not in fields:
                raise BadRequest_400(
                    message=f"{operand} operand field doesn't exist in Package."
                )

    @router.get("/", response_model=CustomPage[GetChartManagersSchema])
    async def get(
        self,
        system_id: int,
        db: Session = Depends(get_db),
        q: str = None,
        package_id: int = None,
        sortBy: str = None,
        chart_type: str = None,
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(ChartManager)
                .where(ChartManager.system_id == system_id)
                .outerjoin(ChartManager.created_obj)
            )

            queryset = self.filter_data(
                queryset=queryset,
                package_id=package_id,
                search_query=q,
                sort_by=sortBy,
                chart_type=chart_type,
            )
            return paginate(
                queryset,
                headers=headers,
                exclude=exclude,
            )

    @router.get("/{id}", response_model=GetChartManagersSchema)
    async def retrieve(
        self,
        id: int,
        db: Session = Depends(get_db),
        package_id: int = None,
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(ChartManager)
                .where(
                    ChartManager.id == id,
                    ChartManager.package_id == package_id,
                )
                .outerjoin(ChartManager.created_obj)
            ).first()
            if not queryset:
                raise HTTPException(400, detail={"message": "Id doesnot exists."})
            return queryset

    @router.get("/analytic/{id}")
    async def get_analytic(
        self,
        id: int,
        system_id: int,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            chart_obj = (
                db.query(ChartManager)
                .where(ChartManager.system_id == system_id, ChartManager.id == id)
                .first()
            )
            if not chart_obj:
                raise HTTPException(400, detail={"message": "Id doesnot exists"})

            form_name = f"package_{chart_obj.package_id}"
            raw_query = check_table_exist(
                table_name=form_name,
                schema_name=self.tenant_code,
            )

            exists = db.execute(raw_query).fetchone()[0]
            if not exists:
                raise BadRequest_400(
                    message="Package views was not created properly, please update or save this package."
                )

            query_text = create_raw_query(
                db=db,
                chart_obj=chart_obj,
                schema_name=self.tenant_code,
            )
            if query_text is None:
                raise BadRequest_400(
                    message="Error occurred while fetching data from the package or retrieving analytics chart. Please check the chart or package from their respective page."
                )

            elif query_text == "no_filter":
                return []

            try:
                data = db.execute(query_text).fetchall()
                return jsonable_encoder(data)
            except Exception as e:
                raise BadRequest_400(
                    message="Error occurred while fetching data from the package or retrieving analytics chart. Please check the chart or package from their respective page."
                )

    @router.post("/")
    async def post(
        self,
        chart_manager: ChartManagerSchema,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            self.validated_data(db, chart_manager)
            data = await repo.save(
                db=db,
                chart_manager=chart_manager,
                current_user=current_user,
                organization=self.organization,
                tenant_code=self.tenant_code,
            )

            return JSONResponse(
                {
                    "message": "Created successfully",
                    "data": data,
                },
                status_code=201,
            )

    @router.put("/{id}")
    async def put(
        self,
        chart_manager: ChartManagerSchema,
        id: int,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            self.validated_data(db, chart_manager)
            data = await repo.update(
                db=db,
                chart_manager=chart_manager,
                current_user=current_user,
                id=id,
                tenant_code=self.tenant_code,
            )
            return JSONResponse({"message": "Updated successfully", "data": data})

    @router.delete("/")
    async def delete(
        self,
        chart_manager: BulkDeleteChartManager,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.delete(db=db, chart_manager=chart_manager)
            return jsonable_encoder({"message": "Deleted successfully"})
