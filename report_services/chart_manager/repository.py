from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from technical.endpoints.report_services.chart_manager import models as orm
from technical.utility.raw_sql_manager import create_raw_query
from technical.configurations.exceptions import BadRequest_400

from .schema import BulkDeleteChartManager, ChartManagerSchema


class ChartManagerRepository:
    def __init__(self) -> None:
        pass

    async def save(
        self,
        chart_manager: ChartManagerSchema,
        db: Session,
        current_user,
        organization,
        tenant_code,
    ):
        field_data = chart_manager.field_data.dict()
        field_data["filters"] = [i.dict() for i in chart_manager.field_data.filters]

        chart_obj = orm.ChartManager(
            name=chart_manager.name,
            description=chart_manager.description,
            package_id=chart_manager.package_id,
            chart_type=chart_manager.chart_type,
            field_data=field_data,
            system_id=chart_manager.system_id,
            organization_id=organization,
            created_by=current_user.id,
        )

        db.add(chart_obj)
        data = jsonable_encoder(chart_obj)

        query_text = create_raw_query(
            db=db,
            chart_obj=chart_obj,
            schema_name=tenant_code,
        )
        if query_text is None:
            raise BadRequest_400(
                message="Error occurred while fetching data from the package or retrieving analytics chart. Please check the chart or package from their respective page."
            )

        try:
            db.execute(query_text).fetchone()
        except Exception as e:
            raise BadRequest_400(
                message="Error occurred while fetching data from the package or retrieving analytics chart. Please check the chart or package from their respective page."
            )

        db.commit()
        data["id"] = chart_obj.id
        return data

    async def update(
        self,
        chart_manager: ChartManagerSchema,
        id: int,
        db: Session,
        current_user,
        tenant_code,
    ):
        chart_obj = db.query(orm.ChartManager).get(id)
        if not chart_obj:
            raise HTTPException(400, detail={"message": "Id doesnot exists"})

        field_data = chart_manager.field_data.dict()
        field_data["filters"] = [i.dict() for i in chart_manager.field_data.filters]

        chart_obj.name = chart_manager.name
        chart_obj.description = chart_manager.description
        chart_obj.chart_type = chart_manager.chart_type
        chart_obj.field_data = field_data
        chart_obj.package_id = chart_manager.package_id
        chart_obj.system_id = chart_manager.system_id
        chart_obj.updated_by = current_user.id
        db.add(chart_obj)

        data = jsonable_encoder(chart_manager)
        data["id"] = id

        query_text = create_raw_query(
            db=db,
            chart_obj=chart_obj,
            schema_name=tenant_code,
        )
        if query_text is None:
            raise BadRequest_400(
                message="Error occurred while fetching data from the package or retrieving analytics chart. Please check the chart or package from their respective page."
            )

        try:
            db.execute(query_text).fetchone()
        except Exception as e:
            raise BadRequest_400(
                message="Error occurred while fetching data from the package or retrieving analytics chart. Please check the chart or package from their respective page."
            )

        db.commit()
        return data

    async def delete(self, chart_manager: BulkDeleteChartManager, db: Session):
        pakage_mapper_queryset = db.query(orm.ChartManager).where(
            orm.ChartManager.id.in_(chart_manager.config_ids)
        )
        pakage_mapper_queryset.delete()
        db.commit()
