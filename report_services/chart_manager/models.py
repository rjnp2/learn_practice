import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

from technical.settings.database import Base, BaseModel


class ChartManager(BaseModel, Base):
    __tablename__ = "chart_manager"
    __table_args__={"schema":"public"}

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    name = sa.Column(sa.String(200), nullable=False)
    description = sa.Column(sa.Text(), nullable=True)

    package_id = sa.Column(sa.ForeignKey("public.package.id"), nullable=False)
    chart_type = sa.Column(sa.String(20),nullable=False)

    field_data = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )

    system_id = sa.Column(sa.ForeignKey("public.system.id"), nullable=False)
    organization_id = sa.Column(sa.ForeignKey("public.organization.id"), nullable=False)
    
    created_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)
    updated_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)

    created_obj = sa.orm.relationship("User", foreign_keys=[created_by])
    updated_obj = sa.orm.relationship("User", foreign_keys=[updated_by])
    
    package_obj = sa.orm.relationship("Package", foreign_keys=[package_id])
    mapped_charts = sa.orm.relationship("DashboardChartMapper", back_populates="chart_obj")

