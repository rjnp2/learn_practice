from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from technical.endpoints.report_services.package import models as orm
from technical.utility.views_manager import (
    sql_combine_view,
)

from .schema import BulkDeletePackage, PackageSchema


class PackageRepository:
    def __init__(self) -> None:
        pass

    async def save(
        self,
        package: PackageSchema,
        db: Session,
        current_user,
        organization,
        schema_name,
    ):
        # field_data = [item for item in package.field_data if item["fields"]]
        field_data = [item.dict() for item in package.field_data if item.fields]

        package_obj = orm.Package(
            name=package.name,
            description=package.description,
            field_data=field_data,
            meta=package.meta,
            system_id=package.system_id,
            organization_id=organization,
            created_by=current_user.id,
        )
        db.add(package_obj)
        db.flush()
        data = jsonable_encoder(package_obj)

        view_id = data["id"]
        query_text = sql_combine_view(
            db=db, schema_name=schema_name, field=field_data, view_id=view_id
        )
        db.execute(query_text)
        db.flush()

        db.commit()
        data["id"] = view_id
        return data

    async def update(
        self, package: PackageSchema, id: int, db: Session, current_user, schema_name
    ):
        package_obj = db.query(orm.Package).get(id)
        if not package_obj:
            raise HTTPException(400, detail={"message": "Id doesnot exists"})

        # field_data = [item for item in package.field_data if item["fields"]]
        field_data = [item.dict() for item in package.field_data if item.fields]

        package_obj.name = package.name
        package_obj.description = package.description
        package_obj.field_data = field_data
        package_obj.system_id = package.system_id
        package_obj.meta = package.meta
        package_obj.updated_by = current_user.id
        data = jsonable_encoder(package)
        data["id"] = id
        db.flush()

        query_text = sql_combine_view(
            db=db, schema_name=schema_name, field=field_data, view_id=id
        )
        db.execute(query_text)

        db.commit()
        return data

    async def delete(
        self,
        schema_name: str,
        packages: BulkDeletePackage,
        db: Session,
    ):
        package_ids = db.query(orm.Package).where(
            orm.Package.id.in_(packages.config_ids)
        )
        package_ids.delete()
        for i in packages.config_ids:
            view_name = f"{schema_name}.package_{i}"
            raw_query = f"DROP VIEW IF EXISTS {view_name} cascade;"
            db.execute(raw_query)
            db.flush()
        db.commit()
