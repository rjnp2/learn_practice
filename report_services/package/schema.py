from datetime import datetime as date

from pydantic import BaseModel, Field
from sqlalchemy.dialects.postgresql import JSONB

from technical.utility.user_schema import UserInfo


class FieldDataSchema(BaseModel):
    form_id: int | str | None
    fields: list | None
    joiner: dict | None


class PackageSchema(BaseModel):
    name: str = Field(..., min_length=5, max_length=50)
    description: str | None = None
    meta: dict | None = {}

    field_data: list[FieldDataSchema]

    system_id: int

    class Config:
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class GetPackagesSchema(PackageSchema):
    id: int

    organization_id: int

    created_obj: UserInfo
    created_at: date

    updated_obj: UserInfo | None
    updated_at: date

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class BulkDeletePackage(BaseModel):
    config_ids: list[int]
