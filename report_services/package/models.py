import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

from technical.settings.database import Base, BaseModel


class Package(BaseModel, Base):
    __tablename__ = "package"
    __table_args__={"schema":"public"}

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    name = sa.Column(sa.String(200), nullable=False)
    description = sa.Column(sa.Text(), nullable=True)

    field_data = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )

    meta = sa.Column(sa.JSON, nullable=True)

    system_id = sa.Column(sa.ForeignKey("public.system.id"), nullable=False)
    organization_id = sa.Column(sa.ForeignKey("public.organization.id"), nullable=False)

    created_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)
    updated_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)

    created_obj = sa.orm.relationship("User", foreign_keys=[created_by])
    updated_obj = sa.orm.relationship("User", foreign_keys=[updated_by])
