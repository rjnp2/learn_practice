from sqlalchemy import or_
from sqlalchemy.orm import Session

from technical.configurations.exceptions import BadRequest_400
from technical.endpoints.master_services.user import models as user_orm
from technical.endpoints.org_services.form.models import Form
from technical.imports.handler_import import (
    CustomPage,
    Depends,
    HTTPException,
    InferringRouter,
    JSONResponse,
    Request,
    cbv,
    get_db,
    get_tenant_code,
    jsonable_encoder,
    oauth2,
    ordering_join,
    paginate,
    schema_context,
)
from technical.utility.views_manager import sql_create_global_view, sql_create_user_view
from technical.utility.views_tables import (
    check_table_exist,
    get_column_data,
    get_package_data,
)

from .models import Package
from .repository import PackageRepository
from .schema import BulkDeletePackage, GetPackagesSchema, PackageSchema

router = InferringRouter()
lower = ["name", "description"]
exclude = [
    "created_by",
]

headers = {
    "id": "Id",
    "name": "Package Name",
    "description": "Description",
    "created_at": "Created At",
    "created_by": "Created By",
}
repo = PackageRepository()


@cbv(router=router)
class PackageAPIView:

    def __init__(
        self,
        request: Request,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ) -> None:
        # ValidatePermission(db, current_user, request)
        self.tenant_code = get_tenant_code(db, current_user, request)
        self.organization = current_user.org_id
        self.current_user = current_user
        if not self.organization:
            raise HTTPException(400, detail={"message": "Access Denied"})

    @staticmethod
    def filter_data(
        db,
        queryset,
        search_query=None,
        sort_by=None,
    ):
        if search_query:
            queryset = queryset.where(
                or_(
                    Package.name.ilike("%" + search_query + "%"),
                    Package.description.ilike("%" + search_query + "%"),
                )
            )

        sort_by = sort_by or "-created_at"
        queryset = ordering_join(
            {}, Package, queryset, lower=lower, exclude=exclude, sort_by=sort_by
        )
        return queryset.all()

    def get_form_table_data(self, db, form_id):
        if form_id not in [
            "form_department",
            "form_organization_user",
            "form_global",
        ]:
            queryset = db.query(Form.id, Form.name).filter(Form.id == form_id).first()
            if not queryset:
                raise HTTPException(
                    400,
                    detail={"message": f"{form_id} doesnot exist."},
                )
            form_id = queryset.name
        return form_id

    def validate_field_data(self, db, field_data: list):
        filtered_data = list(
            filter(
                lambda x: x.get("joiner") is None or len(x.get("joiner")) == 0,
                field_data,
            )
        )
        if len(filtered_data) > 1:
            filtered_data = [str(i["form_id"]) for i in filtered_data]
            filtered_data = ",".join(filtered_data)

            raise HTTPException(
                400,
                detail={
                    "message": f"It seems that there exist multiple forms which have no joiner data in these {filtered_data} forms."
                },
            )

        field_data = sorted(
            field_data,
            key=lambda x: (
                len(x.get("joiner", [])) if x.get("joiner") is not None else 0
            ),
        )
        for data in field_data:
            form_id = data.get("form_id")
            if not data or not form_id:
                raise HTTPException(
                    400,
                    detail={
                        "message": "The data format is invalid because either the data or form_id is empty."
                    },
                )

            fields = data.get("fields")
            if not fields:
                raise HTTPException(
                    400, detail={"message": "The fields are empty inside data."}
                )

            fields = set([i.lower() for i in data["fields"]])
            if form_id == "form_organization_user":
                form_fields = set(["id", "full_name"])

            elif form_id == "form_department":
                form_fields = set(["id", "name"])

            elif form_id == "form_global":
                form_fields = set(["option_id", "option_name"])

            else:
                raw_query = get_column_data(
                    table_name=f"table_{form_id}",
                    schema_name=self.tenant_code,
                )
                form_fields = [i[0] for i in db.execute(raw_query).fetchall()]

                tabl_name = f"jt_{form_id}_"
                raw_query = f"""SELECT table_name
                    FROM information_schema.tables
                    WHERE table_name ILIKE '{tabl_name}%'
                    and table_schema = '{self.tenant_code}';
                """
                form_fields.extend(
                    [
                        i[0].replace(tabl_name, "")
                        for i in db.execute(raw_query).fetchall()
                    ]
                )
                form_fields = set(form_fields)

            difference_set = fields - form_fields
            if difference_set:
                fields = ",".join([str(i) for i in difference_set])
                name = self.get_form_table_data(db=db, form_id=form_id)
                raise HTTPException(
                    400,
                    detail={
                        "message": f"It seems that the {fields} field does not exist in the {name} form."
                    },
                )

            joiner = data.get("joiner")
            if not joiner:
                continue

            ## to --> own form
            to = joiner.get("to")
            if not to:
                raise HTTPException(
                    400,
                    detail={
                        "message": "It seems that there is no 'to' key in the Joiner data."
                    },
                )

            ## --> another form
            from_ = joiner.get("from")
            if not from_:
                raise HTTPException(
                    400,
                    detail={
                        "message": "It seems that there is no 'from' key in the Joiner data."
                    },
                )

            nested_form_id = joiner.get("form_id")
            if not nested_form_id:
                raise HTTPException(
                    400,
                    detail={
                        "message": "It seems that there is no 'form_id' key in the Joiner data."
                    },
                )

            if to.lower() not in form_fields:
                name = self.get_form_table_data(db=db, form_id=form_id)
                raise HTTPException(
                    400,
                    detail={
                        "message": f"It seems that the {to} field ('to') inside joiner data does not exist in the {name} form."
                    },
                )

            if to.lower() not in fields:
                name = self.get_form_table_data(db=db, form_id=form_id)
                raise HTTPException(
                    400,
                    detail={
                        "message": f"It seems that the {to} field ('to') inside joiner data does not exist in the fields of the {name} form."
                    },
                )

            fields = set([i.lower() for i in data["fields"]])
            if nested_form_id == "form_organization_user":
                nested_form_fields = set(["id", "full_name"])

            elif nested_form_id == "form_department":
                nested_form_fields = set(["id", "name"])

            elif nested_form_id == "form_global":
                nested_form_fields = set(["option_id", "option_name"])

            else:
                raw_query = get_column_data(
                    table_name=f"table_{nested_form_id}",
                    schema_name=self.tenant_code,
                )
                nested_form_fields = [i[0] for i in db.execute(raw_query).fetchall()]

                tabl_name = f"jt_{nested_form_id}_"
                raw_query = f"""SELECT table_name
                    FROM information_schema.tables
                    WHERE table_name ILIKE '{tabl_name}%'
                    and table_schema = '{self.tenant_code}';
                """
                nested_form_fields.extend(
                    [
                        i[0].replace(tabl_name, "")
                        for i in db.execute(raw_query).fetchall()
                    ]
                )
                nested_form_fields = set(nested_form_fields)

            if from_.lower() not in nested_form_fields:
                name = self.get_form_table_data(db=db, form_id=nested_form_id)
                raise HTTPException(
                    400,
                    detail={
                        "message": f"It seems that the {from_} field ('from') inside joiner data does not exist in the {name} form."
                    },
                )

            nested_form_fields = [
                i for i in field_data if i.get("form_id") == nested_form_id
            ]
            if nested_form_fields:
                nested_form_fields = nested_form_fields[0].get("fields", [])

            if from_.lower() not in nested_form_fields:
                name = self.get_form_table_data(db=db, form_id=nested_form_id)
                raise HTTPException(
                    400,
                    detail={
                        "message": f"It seems that the {from_} field ('from') inside joiner data does not exist in the fields of the {name} form."
                    },
                )

    @router.get("/", response_model=CustomPage[GetPackagesSchema])
    async def get(
        self,
        system_id: int,
        db: Session = Depends(get_db),
        q: str = None,
        current_user: user_orm.User = Depends(oauth2.get_current_user),
        sortBy: str = None,
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(Package)
                .where(
                    Package.system_id == system_id,
                )
                .outerjoin(Package.created_obj)
            )

            queryset = self.filter_data(
                db=db,
                queryset=queryset,
                search_query=q,
                sort_by=sortBy,
            )

            return paginate(
                queryset,
                headers=headers,
                exclude=exclude,
            )

    @router.get("/{id}", response_model=GetPackagesSchema)
    async def retrieve(
        self,
        id: int,
        system_id: int,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(Package)
                .where(Package.system_id == system_id, Package.id == id)
                .outerjoin(Package.created_obj)
            )

            return queryset.first()

    @router.get("/data/{id}")
    async def get_data(
        self,
        id: int,
        system_id: int,
        db: Session = Depends(get_db),
        limit: int = 10,
        offset: int = 0,
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(Package)
                .where(
                    Package.system_id == system_id,
                    Package.id == id,
                )
                .first()
            )

            if not queryset:
                raise HTTPException(400, detail={"message": "Id doesnot exist"})

            table_name = f"package_{queryset.id}"
            raw_query = check_table_exist(
                table_name=table_name,
                schema_name="public",
            )
            exist = db.execute(raw_query).fetchone()[0]
            if not exist:
                raise BadRequest_400(
                    message="Package views was not created properly, please update or save this package."
                )

            package_id = f"package_{queryset.id}"
            raw_query = get_package_data(
                package_id=package_id,
                schema_name="public",
                limit=limit,
                offset=offset,
            )
            try:
                data = db.execute(raw_query).fetchall()
            except:
                raise BadRequest_400(
                    message="Package views was not created properly, please update or save this package."
                )

            data = jsonable_encoder(data)

            if len(data) == 0:
                raw_query = get_column_data(
                    table_name=f"{package_id}",
                    schema_name="public",
                )
                data = [
                    {i[0]: None for i in db.execute(raw_query).fetchall()},
                ]

            return data

    @router.post("/")
    async def post(
        self,
        package: PackageSchema,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        query_text = check_table_exist(table_name="form_global", schema_name="public")
        exists = db.execute(query_text).fetchone()[0]
        if not exists:
            query_text = sql_create_global_view(schema_name="public")

            db.execute(query_text)
            db.flush()

        query_text = check_table_exist(
            table_name="form_organization_user", schema_name="public"
        )
        exists = db.execute(query_text).fetchone()[0]
        if not exists:
            query_text = sql_create_user_view(
                schema_name="public",
            )
            db.execute(query_text)
            db.flush()

        query_text = check_table_exist(
            table_name="form_department", schema_name="public"
        )
        exists = db.execute(query_text).fetchone()[0]
        if not exists:
            query_text = sql_create_user_view(
                schema_name="public",
            )
            db.execute(query_text)
            db.flush()

        db.commit()

        with schema_context("public", db):
            name = package.name.lower()
            query = (
                db.query(Package)
                .filter(
                    Package.system_id == package.system_id,
                    Package.name.ilike(name),
                )
                .first()
            )
            if query:
                raise BadRequest_400(message="The package name must be unique.")

            # field_data = [item for item in package.field_data if item["fields"]]
            field_data = [i.dict() for i in package.field_data if i.fields]

            self.validate_field_data(
                db=db,
                field_data=field_data,
            )

            data = await repo.save(
                db=db,
                package=package,
                current_user=current_user,
                organization=self.organization,
                schema_name=self.tenant_code,
            )
            return JSONResponse(
                {
                    "message": "Package saved successfully",
                    "data": data,
                },
                status_code=201,
            )

    @router.put("/{id}")
    async def put(
        self,
        package: PackageSchema,
        id: int,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        query_text = check_table_exist(table_name="form_global", schema_name="public")
        exists = db.execute(query_text).fetchone()[0]
        if not exists:
            query_text = sql_create_global_view(schema_name="public")

            db.execute(query_text)
            db.flush()

        query_text = check_table_exist(
            table_name="form_organization_user", schema_name="public"
        )
        exists = db.execute(query_text).fetchone()[0]
        if not exists:
            query_text = sql_create_user_view(
                schema_name="public",
            )
            db.execute(query_text)
            db.flush()

        query_text = check_table_exist(
            table_name="form_department", schema_name="public"
        )
        exists = db.execute(query_text).fetchone()[0]
        if not exists:
            query_text = sql_create_user_view(
                schema_name="public",
            )
            db.execute(query_text)
            db.flush()

        db.commit()

        with schema_context("public", db):
            name = package.name.lower()
            query = (
                db.query(Package)
                .filter(
                    Package.system_id == package.system_id,
                    Package.id != id,
                    Package.name.ilike(name),
                )
                .first()
            )
            if query:
                raise BadRequest_400(message="The package name must be unique.")

            # field_data = [item for item in package.field_data if item["fields"]]
            field_data = [i.dict() for i in package.field_data if i.fields]

            self.validate_field_data(
                db=db,
                field_data=field_data,
            )

            data = await repo.update(
                db=db,
                package=package,
                current_user=current_user,
                id=id,
                schema_name=self.tenant_code,
            )
            return JSONResponse({"message": "Package saved successfully", "data": data})

    @router.delete("/")
    async def delete(
        self,
        packages: BulkDeletePackage,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.delete(
                db=db, packages=packages, schema_name="public"
            )
            return jsonable_encoder(
                {
                    "message": "Deleted successfully",
                }
            )
