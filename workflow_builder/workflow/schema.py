from datetime import datetime

from pydantic import BaseModel, Field
from pydantic import BaseModel, Field, validator

from sqlalchemy.dialects.postgresql import JSONB

from technical.utility.user_schema import UserInfo


class WorkFlowBaseSchema(BaseModel):
    title: str = Field(..., min_length=5)
    description: str | None = None

    system_id: int

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class WorkFlowSchema(WorkFlowBaseSchema):
    field_data: dict = {}
    input_data: dict = {}
    elements: list | dict = {}

    @validator("field_data")
    def check_field_data_length(cls, value):
        if len(value) == 0:
            raise ValueError("field_data must contain at least one item.")

        if "start" not in value:
            raise ValueError("field_data must contains start node.")

        return value

    @validator("elements")
    def check_elements_length(cls, value):
        if len(value) == 0:
            raise ValueError("elements must contain at least one item.")
        return value


class GetWorkFlowsSchema(WorkFlowBaseSchema):
    id: int

    is_enabled: bool = False
    organization_id: int

    created_obj: UserInfo
    created_at: datetime

    updated_obj: UserInfo | None
    updated_at: datetime


class GetWorkFlowsListSchema(WorkFlowBaseSchema):
    id: int
    field_data: dict = {}


class GetWorkFlowsDetailSchema(GetWorkFlowsSchema):
    field_data: dict = {}
    elements: list[dict] = []


class BulkDeleteWorkFlow(BaseModel):
    config_ids: list[int]


class StatusWorkFlow(BaseModel):
    id: int
    system_id: int
    is_enabled: bool = False


class ValidateSignature(BaseModel):
    client_id: str
    signature: str


class ExecutorSchema(BaseModel):
    trigger_id: int | None = None
    trigger_log_id: int | None = None
    input_data: dict = {}
