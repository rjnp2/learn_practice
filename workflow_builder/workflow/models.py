import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

from technical.settings.database import Base, BaseModel


class Workflow(BaseModel, Base):
    __tablename__ = "workflow"
    __table_args__ = {"schema": "public"}

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    title = sa.Column(sa.String(200), nullable=False)
    description = sa.Column(sa.Text(), nullable=True)

    is_enabled = sa.Column(sa.Boolean, default=False)

    field_data = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )

    elements = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )

    system_id = sa.Column(sa.ForeignKey("public.system.id"), nullable=False)
    organization_id = sa.Column(sa.ForeignKey("public.organization.id"), nullable=False)

    created_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=False)
    updated_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)

    created_obj = sa.orm.relationship("User", foreign_keys=[created_by])
    updated_obj = sa.orm.relationship("User", foreign_keys=[updated_by])

    triggers = sa.orm.relationship("Trigger", back_populates="workflow_obj")
