from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi.responses import JSONResponse
import hmac
import hashlib
import base64
import json
import requests
from urllib.parse import urljoin
from fastapi import Request

from technical.common.system_control import system_access
from technical.endpoints.master_services.user import models as user_orm
from technical.imports.handler_import import (
    InferringRouter,
    Request,
    cbv,
    Depends,
    get_db,
    oauth2,
    get_tenant_code,
    HTTPException,
    ordering_join,
    schema_context,
    paginate,
    CustomPage,
    jsonable_encoder,
    BadRequest_400,
)

from .repository import WorkflowRepository
from .models import Workflow
from .schema import (
    WorkFlowSchema,
    GetWorkFlowsSchema,
    BulkDeleteWorkFlow,
    StatusWorkFlow,
    ValidateSignature,
    ExecutorSchema,
)
from technical.settings.settings import settings_instance
from technical.utility.subdomain_manager import get_db_domain

from technical.endpoints.workflow_services.trigger.transerve_workflow import (
    transerve_workflow_using_bfs,
)
from technical.endpoints.workflow_services.trigger.models import Trigger, TriggerLog
from technical.endpoints.workflow_services.workflow.validate_credentials import (
    validate_creds,
)


router = InferringRouter()

repo = WorkflowRepository()

lower = ["title", "description"]
exclude = [
    "created_by",
]
headers = {
    "id": "Id",
    "title": "Workflow Title",
    "description": "Description",
    "created_at": "Created At",
    "created_by": "Created By",
}
SECRET_TOKEN = "626CqdR7vhFPAizXb1TBTVHkZJvKWYkA"


@cbv(router=router)
class WorkFlowAPIView:

    def __init__(
        self,
        request: Request,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ) -> None:
        system_access(current_user, request, db)
        self.tenant_code = get_tenant_code(db, current_user)
        self.db_code = get_db_domain(request=request)
        self.organization = current_user.org_id
        self.current_user = current_user
        if not self.organization:
            raise HTTPException(400, detail={"message": "Access Denied"})

    @staticmethod
    def filter_data(
        db,
        queryset,
        search_query=None,
        sort_by=None,
        archived=None,
    ):
        if search_query:
            queryset = queryset.where(
                or_(
                    Workflow.title.ilike("%" + search_query + "%"),
                    Workflow.description.ilike("%" + search_query + "%"),
                )
            )

        if archived is not None:
            queryset = queryset.where(Workflow.deleted == archived)

        sort_by = sort_by or "-created_at"
        queryset = ordering_join(
            {}, Workflow, queryset, lower=lower, exclude=exclude, sort_by=sort_by
        )
        return queryset.all()

    @router.get("/", response_model=CustomPage[GetWorkFlowsSchema])
    async def get(
        self,
        system_id: int,
        db: Session = Depends(get_db),
        q: str = None,
        current_user: user_orm.User = Depends(oauth2.get_current_user),
        archived: bool = False,
        sortBy: str = None,
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(Workflow)
                .where(
                    Workflow.system_id == system_id,
                )
                .outerjoin(Workflow.created_obj)
            )

            queryset = self.filter_data(
                db=db,
                queryset=queryset,
                search_query=q,
                sort_by=sortBy,
                archived=archived,
            )

            return paginate(
                queryset,
                headers=headers,
                exclude=exclude,
            )

    @router.get("/{id}/")
    async def retrieve(
        self,
        id: int,
        system_id: int,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(Workflow)
                .where(Workflow.system_id == system_id, Workflow.id == id)
                .outerjoin(Workflow.created_obj)
            ).first()

            if not queryset:
                raise BadRequest_400(message="Id doesnot exist.")

            return queryset

    @router.post("/executor/{id}/")
    async def executor_workflow(
        self,
        id: int,
        system_id: int,
        schema_code: ExecutorSchema,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        trigger_id = schema_code.trigger_id
        trigger_log_id = schema_code.trigger_log_id
        input_data = schema_code.input_data

        with schema_context(self.tenant_code, db):
            workflow_obj = (
                db.query(Workflow).where(
                    Workflow.system_id == system_id,
                    Workflow.id == id,
                    Workflow.deleted == False,
                )
            ).first()

            if not workflow_obj:
                raise BadRequest_400(message="Id doesnot exist.")

            if not workflow_obj.is_enabled:
                raise BadRequest_400(message="This workflow is disabled.")

            current_node = "start"
            if trigger_log_id:
                trigger_log_obj = db.query(TriggerLog).where(
                    TriggerLog.id == trigger_log_id,
                    TriggerLog.workflow_id == id,
                    TriggerLog.status.in_(["ask_for_input", "current", "ask_current"]),
                )

                if trigger_id:
                    trigger_log_obj = trigger_log_obj.where(
                        TriggerLog.trigger_id == trigger_id,
                    )

                trigger_log_obj = trigger_log_obj.first()
                if trigger_log_obj:
                    trigger_log_id = trigger_log_obj.id
                else:
                    trigger_log_id = None

            workflow_obj = jsonable_encoder(workflow_obj)
            trigger_message = "The Workflow is triggered from the UI."
            log_data, status = await transerve_workflow_using_bfs(
                workflow_instance=workflow_obj,
                start_node=current_node,
                db=db,
                schema_name=self.db_code,
                event_dict=input_data,
                trigger_id=trigger_id,
                trigger_log_id=trigger_log_id,
                trigger_message=trigger_message,
                current_user_id=current_user.id,
            )

            if status:
                db.commit()
                raise HTTPException(status_code=200, detail=log_data)

            db.rollback()
            status_code = 400
            if log_data.get("status") in [
                "ask_for_input",
                "modal_action",
                "current",
                "ask_current",
            ]:
                status_code = 210

            raise HTTPException(status_code=status_code, detail=log_data)

    @router.post("/single-executor/{id}/")
    async def single_executor_workflow(
        self,
        id: int,
        system_id: int,
        block_key: str,
        trigger_log_id: int,
        trigger_id: int = None,
        input_data: dict = {},
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            workflow_obj = (
                db.query(Workflow).where(
                    Workflow.system_id == system_id,
                    Workflow.id == id,
                    Workflow.deleted == False,
                )
            ).first()

            if not workflow_obj:
                raise BadRequest_400(message="Id doesnot exist.")

            if not workflow_obj.is_enabled:
                raise BadRequest_400(message="This workflow is disabled.")

            trigger_log_obj = (
                db.query(TriggerLog).where(
                    TriggerLog.id == trigger_log_id,
                    TriggerLog.trigger_id == trigger_id,
                    TriggerLog.workflow_id == id,
                    TriggerLog.status == "ask_for_input",
                )
            ).first()

            if not trigger_log_obj:
                raise BadRequest_400(
                    message="This trigger log isnot found with this id."
                )

            trigger_log_id = trigger_log_obj.id
            trigger_log = trigger_log_obj.log
            individual_trigger_log = trigger_log.get(block_key)
            individual_input_data = individual_trigger_log.get("input_data", {})
            individual_input_data.update(input_data)

            individual_input_data = {block_key: individual_input_data}

            workflow_obj = jsonable_encoder(workflow_obj)
            trigger_message = "The Workflow is triggered from the UI."
            log_data, status = await transerve_workflow_using_bfs(
                workflow_instance=workflow_obj,
                start_node=block_key,
                db=db,
                schema_name=self.db_code,
                event_dict=individual_input_data,
                trigger_id=trigger_id,
                trigger_log_id=trigger_log_id,
                trigger_message=trigger_message,
                current_user_id=current_user.id,
            )

            log_status = log_data.get("status")
            if status:
                db.commit()
                raise HTTPException(status_code=200, detail=log_data)

            db.rollback()
            status_code = 400
            if log_status in [
                "ask_for_input",
                "modal_action",
                "current",
                "ask_current",
            ]:
                status_code = 210

            raise HTTPException(status_code=status_code, detail=log_data)

    @router.post("/validate/")
    async def validate_workflow(
        self,
        workflow_obj: dict,
        input_data: dict = {},
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        if "title" not in workflow_obj:
            raise BadRequest_400(message="Title key is missing.")

        if "field_data" not in workflow_obj:
            raise BadRequest_400(message="Field data key is missing.")

        trigger_message = "The workflow is validated from the UI."
        with schema_context(self.tenant_code, db):
            log_data, status = await transerve_workflow_using_bfs(
                workflow_instance=workflow_obj,
                start_node="start",
                db=db,
                schema_name=self.db_code,
                is_validate=True,
                event_dict=input_data,
                trigger_message=trigger_message,
                current_user_id=current_user.id,
            )
            db.rollback()
            if status:
                raise HTTPException(status_code=200, detail=log_data)

            log_status = log_data.get("status")
            status_code = 400
            if log_status in [
                "ask_for_input",
                "modal_action",
                "current",
                "ask_current",
            ]:
                status_code = 210

            raise HTTPException(status_code=status_code, detail=log_data)

    @router.post("/validate-credentials/")
    async def validate_credentials(
        self,
        service: str,
        config: dict,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        status = await validate_creds(config, service)
        if status:
            return {"status": status, "message": "Validated Sucessfully"}
        else:
            return {"status": status, "message": "Couldn't Validate Credentials"}

    def generate_signature(self, secret, data):
        secret_bytes = secret.encode("utf-8")
        data_bytes = data.encode("utf-8")
        digest = hmac.new(secret_bytes, data_bytes, hashlib.sha256).digest()
        return base64.b64encode(digest).decode("utf-8")

    @router.post("/create-signature/")
    async def create_signature(
        self,
        client_id: str,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        signature = self.generate_signature(SECRET_TOKEN, client_id)
        return {"signature": signature, "message": "Signature Generated Sucessfully"}

    @router.post("/generate-wh-api/")
    async def generate_wh_api(
        self,
        client_id: str,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        backend_base_url = settings_instance.BACKEND_BASE_URL
        webhook_url = urljoin(
            backend_base_url, f"external-hook/{self.db_code}/{client_id}/"
        )

        return JSONResponse(
            status_code=200,
            content={"message": "Signature  verified", "url": webhook_url},
        )

    @router.post("/duplicate/{id}/")
    async def duplicate_trigger(
        self,
        id: int,
        system_id: int,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            workflow_obj = (
                db.query(Workflow).where(
                    Workflow.system_id == system_id,
                    Workflow.id == id,
                    Workflow.deleted == False,
                )
            ).first()

            if not workflow_obj:
                raise BadRequest_400(message="Id doesnot exist.")

            add_workflow_obj = Workflow(
                title=f"copy : {workflow_obj.title}",
                description=workflow_obj.description,
                field_data=workflow_obj.field_data,
                elements=workflow_obj.elements,
                system_id=workflow_obj.system_id,
                organization_id=self.organization,
                created_by=current_user.id,
            )
            db.add(add_workflow_obj)
            db.flush()

            trigger_objs = (
                db.query(Trigger).where(
                    Trigger.system_id == system_id, Trigger.workflow_id == id
                )
            ).all()

            for trigger_obj in trigger_objs:
                add_trigger_obj = Trigger(
                    title=f"copy : {trigger_obj.title}",
                    description=trigger_obj.description,
                    is_enabled=trigger_obj.is_enabled,
                    trigger_type=trigger_obj.trigger_type,
                    trigger_method=trigger_obj.trigger_method,
                    source=trigger_obj.source,
                    source_type=trigger_obj.source_type,
                    config=trigger_obj.config,
                    meta=trigger_obj.meta,
                    location=trigger_obj.location,
                    workflow_id=add_workflow_obj.id,
                    system_id=trigger_obj.system_id,
                    form_id=trigger_obj.form_id,
                    organization_id=self.organization,
                    created_by=current_user.id,
                )
                db.add(add_trigger_obj)
                db.flush()

            db.commit()
            return {"message": "Successfully duplicated workflow and triggers."}

    @router.post("/", status_code=201)
    async def post(
        self,
        workflow: WorkFlowSchema,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = await repo.save(
                db=db,
                workflow=workflow,
                current_user=current_user,
                organization=self.organization,
            )

            return {
                "message": "workflow saved successfully",
                "data": data,
            }

    @router.put("/restore/")
    async def restore(
        self,
        workflow: BulkDeleteWorkFlow,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.restore(db=db, workflow=workflow)
            return {
                "message": "Restored successfully",
            }

    @router.put("/enable-disable/")
    async def enable_disable(
        self,
        workflows: list[StatusWorkFlow],
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            for workflow in workflows:
                workflow_obj = (
                    db.query(Workflow).where(
                        Workflow.system_id == workflow.system_id,
                        Workflow.id == workflow.id,
                    )
                ).first()

                if not workflow_obj:
                    raise BadRequest_400(message=f"{workflow.id} id doesnot exist.")

                workflow_obj.is_enabled = workflow.is_enabled

                if not workflow.is_enabled:
                    trigger_objs = (
                        db.query(Trigger).where(
                            Trigger.system_id == workflow.system_id,
                            Trigger.workflow_id == workflow.id,
                        )
                    ).all()

                    for trigger_obj in trigger_objs:
                        trigger_obj.is_enabled = False

            db.commit()
            return {
                "message": "Update successfully",
            }

    @router.put("/{id}/")
    async def put(
        self,
        workflow: WorkFlowSchema,
        id: int,
        is_validated: bool = True,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        log_data = {}
        if workflow.field_data and is_validated:
            with schema_context(self.tenant_code, db):
                queryset = {"title": workflow.title, "field_data": workflow.field_data}

                trigger_message = "This is a test while saving the workflow."
                log_data, status = await transerve_workflow_using_bfs(
                    workflow_instance=queryset,
                    start_node="start",
                    db=db,
                    schema_name=self.db_code,
                    is_validate=True,
                    event_dict=workflow.input_data,
                    trigger_message=trigger_message,
                    current_user_id=current_user.id,
                )
                db.rollback()
                if not status:
                    log_status = log_data.get("status")
                    status_code = 400
                    if log_status in [
                        "ask_for_input",
                        "modal_action",
                        "current",
                        "ask_current",
                    ]:
                        status_code = 210
                    raise HTTPException(status_code=status_code, detail=log_data)

        with schema_context(self.tenant_code, db):
            data = await repo.update(
                db=db,
                workflow=workflow,
                id=id,
                current_user=current_user,
            )
            return {
                "message": "workflow saved successfully",
                "data": data,
                "log_data": log_data,
            }

    @router.delete("/")
    async def delete(
        self,
        workflow: BulkDeleteWorkFlow,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.delete(
                db=db,
                workflow=workflow,
            )
            return {
                "message": "Deleted successfully",
            }


def verify_signature(secret, data, received_signature):
    secret_bytes = secret.encode("utf-8")
    data_bytes = data.encode("utf-8")
    expected_signature = hmac.new(secret_bytes, data_bytes, hashlib.sha256).digest()
    expected_signature_b64 = base64.b64encode(expected_signature).decode("utf-8")
    return hmac.compare_digest(expected_signature_b64, received_signature)


@router.post("/validate-signature/")
async def validate_signature(request: Request, data: ValidateSignature):
    client_id = data.get("client_id", "")
    signature = data.get("signature", "")
    if not verify_signature(SECRET_TOKEN, client_id, signature):
        return JSONResponse(
            status_code=403,
            content={"message": "Signature not verified"},
        )
    return JSONResponse(
        status_code=200,
        content={"message": "Signature  verified"},
    )


@router.post("/wh/")
async def wh_test(request: Request):
    body = await request.body()

    body = json.loads(body)
    data = {
        "data": {
            "client_id": body.get("client_id", ""),
            "signature": "Uvw7c1bMuA2fHe2KVIu6OH/aHWxRcNJhtASy6r0Wow0=",
        }
    }
    url = "https://api.dev.dalfin.ai/api/v1/workflow/validate-signature/"
    response = requests.post(url, data=data)
    return {"message": "Signature  verified", "response": response}
