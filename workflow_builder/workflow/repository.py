from sqlalchemy.orm import Session
from technical.imports.handler_import import BadRequest_400, jsonable_encoder

from .models import Workflow
from technical.endpoints.workflow_services.trigger.models import Trigger
from .schema import BulkDeleteWorkFlow, WorkFlowSchema


class WorkflowRepository:
    def __init__(self) -> None:
        pass

    async def save(
        self,
        workflow: WorkFlowSchema,
        db: Session,
        current_user,
        organization,
    ):
        workflow_obj = Workflow(
            title=workflow.title,
            description=workflow.description,
            is_enabled=True,
            field_data=workflow.field_data,
            elements=workflow.elements,
            system_id=workflow.system_id,
            organization_id=organization,
            created_by=current_user.id,
        )
        db.add(workflow_obj)

        db.commit()
        data = jsonable_encoder(workflow_obj)
        data["id"] = workflow_obj.id
        return data

    async def update(
        self, workflow: WorkFlowSchema, id: int, db: Session, current_user
    ):
        workflow_obj = db.query(Workflow).get(id)
        if not workflow_obj:
            raise BadRequest_400(message="Id doesnot exists")

        workflow_obj.title = workflow.title
        workflow_obj.description = workflow.description

        workflow_obj.field_data = workflow.field_data or workflow_obj.field_data
        workflow_obj.elements = workflow.elements or workflow_obj.elements

        workflow_obj.updated_by = current_user.id
        data = jsonable_encoder(workflow_obj)
        db.commit()
        return data

    async def delete(
        self,
        workflow: BulkDeleteWorkFlow,
        db: Session,
    ):
        workflow_ids = (
            db.query(Workflow)
            .where(Workflow.id.in_(workflow.config_ids), Workflow.deleted == False)
            .all()
        )
        for workflow in workflow_ids:
            workflow.deleted = True
            triggers = db.query(Trigger).where(
                Trigger.workflow_id == workflow.id, Trigger.deleted == False
            )
            triggers.update({"deleted": True}, synchronize_session=False)

        db.commit()

    async def restore(
        self,
        workflow: BulkDeleteWorkFlow,
        db: Session,
    ):
        workflow_ids = (
            db.query(Workflow)
            .where(Workflow.id.in_(workflow.config_ids), Workflow.deleted == True)
            .all()
        )
        for workflow in workflow_ids:
            workflow.deleted = False
        db.commit()
