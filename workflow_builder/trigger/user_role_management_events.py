from technical.configurations.hash import Hash
from technical.endpoints.master_services.user.models import User as user_orm
from technical.endpoints.tenant_services.organization_user.models import (
    OrganizationUser,
    OrganizationUserRole,
)
from technical.endpoints.workflow_services.trigger.utils import (
    get_data_from_config,
)


def check_user_condition(db, conditions_mapping, conditions):
    final_filter_data = []
    junction = []

    for conditions_map in conditions_mapping:
        gate = conditions_map.get("gate").lower()

        if gate is None:
            return {"error_message": f"gate is not provided in conditions_mapping."}

        groups = conditions_map.get("group")
        if groups is None:
            return {"error_message": f"groups is not provided in conditions_mapping."}

        filter_data = []
        for group in groups:
            condition_data = conditions.get(group)

            if condition_data is None:
                return {"error_message": f"{group} is not found in conditions data."}

            column = condition_data.get("column")
            if column is None:
                return {
                    "error_message": f"column is not provided in conditions_mapping."
                }

            value = condition_data.get("value")
            if value is None:
                return {
                    "error_message": f"value is not provided in conditions_mapping."
                }

            operator = condition_data.get("operator").lower().strip().replace("_", " ")
            if operator is None:
                return {
                    "error_message": f"operator is not provided in conditions_mapping."
                }

            operator = operator.lower().strip().replace("_", " ")
            if operator not in [
                "=",
                "!=",
                "in",
                "not in",
                "like",
                "not like",
                ">",
                ">=",
                "<",
                "<=",
                "between",
            ]:
                return {"error_message": f"Invalid operator."}

            if operator == "between":
                start_item = value[0]
                end_item = value[-1]
                value = f"'{start_item}' and '{end_item}'"

            elif operator in ["like", "not like"]:
                value = f"'%{value}%'"

            elif operator in ["in", "not in"]:
                value = ",".join(f"'{i}'" for i in value)
                value = "{" + value + "}"

            else:
                value = f"'{value}'"

            if column in ["id", "login_id", "full_name", "is_active"]:
                filter_data.append(f'"{column}" {operator} {value}')

            elif column in ["country_id", "user_department_id"]:
                filter_data.append(f'organization__user."{column}" {operator} {value}')
                junction.append(
                    f"""
                    join public.organization__user 
                    on puser.user_id = organization__user.id
                """
                )

            elif column in ["role_id"]:
                filter_data.append(f'organization__user."{column}" {operator} {value}')
                junction.append(
                    f"""
                    join public.organization__user 
                    on puser.user_id = organization__user.id

                    join public.organization__user_role 
                    on organization__user.user_id = organization__user_role.org_user_id
                """
                )

        if gate == "and":
            filter_data = " and ".join(filter_data)

        elif gate == "or":
            filter_data = " or ".join(filter_data)

        else:
            return {"error_message": f"gate is not invalid in conditions_mapping."}

        final_filter_data.append(filter_data)

    final_filter_data = " and ".join(final_filter_data)
    if final_filter_data:
        final_filter_data = f"where {final_filter_data}"

    junction = " \n ".join(junction)

    query_text = f"""
        SELECT id 
        FROM public.user
        {junction}
        {filter_data};
    """
    output_data = [i[0] for i in db.execute(query_text).fetchall()]
    return {
        "output_data": output_data,
    }


async def create_a_user(
    config,
    db,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    login_id_config = config.get("login_id")
    full_name_config = config.get("full_name")
    password_config = config.get("password")
    organization_config = config.get("organization")
    record_created_by_config = config.get("record_created_by")
    user_department_id_config = config.get("user_department_id")
    country_id_config = config.get("country_id")
    domain_id_config = config.get("domain_id")
    role_id_config = config.get("role_id")

    input_column = []
    is_ask_for_input = False

    login_id_config = get_data_from_config(
        db=db,
        data_config=login_id_config,
        event_dict=event_dict,
        data_name="login_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = login_id_config.get("error_message")
    is_ask_for_input = login_id_config.get("is_ask_for_input")
    login_id = login_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    role_id_config = get_data_from_config(
        db=db,
        data_config=role_id_config,
        event_dict=event_dict,
        data_name="role_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = role_id_config.get("error_message")
    is_ask_for_input = role_id_config.get("is_ask_for_input")
    role_id = role_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    organization_config = get_data_from_config(
        db=db,
        data_config=organization_config,
        event_dict=event_dict,
        data_name="organization",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = organization_config.get("error_message")
    is_ask_for_input = organization_config.get("is_ask_for_input")
    organization = organization_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    full_name_config = get_data_from_config(
        db=db,
        data_config=full_name_config,
        event_dict=event_dict,
        data_name="full_name",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = full_name_config.get("error_message")
    is_ask_for_input = full_name_config.get("is_ask_for_input")
    full_name = full_name_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    password_config = get_data_from_config(
        db=db,
        data_config=password_config,
        event_dict=event_dict,
        data_name="password",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = password_config.get("error_message")
    is_ask_for_input = password_config.get("is_ask_for_input")
    password = password_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    user_department_id_config = get_data_from_config(
        db=db,
        data_config=user_department_id_config,
        event_dict=event_dict,
        data_name="user_department_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = user_department_id_config.get("error_message")
    is_ask_for_input = user_department_id_config.get("is_ask_for_input")
    user_department_id = user_department_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    country_id_config = get_data_from_config(
        db=db,
        data_config=country_id_config,
        event_dict=event_dict,
        data_name="country_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = country_id_config.get("error_message")
    is_ask_for_input = country_id_config.get("is_ask_for_input")
    country_id = country_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    record_created_by_config = get_data_from_config(
        db=db,
        data_config=record_created_by_config,
        event_dict=event_dict,
        data_name="record_created_by",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = record_created_by_config.get("error_message")
    is_ask_for_input = record_created_by_config.get("is_ask_for_input")
    record_created_by = record_created_by_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    domain_id_config = get_data_from_config(
        db=db,
        data_config=domain_id_config,
        event_dict=event_dict,
        data_name="domain_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = domain_id_config.get("error_message")
    is_ask_for_input = domain_id_config.get("is_ask_for_input")
    domain_id = domain_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    try:
        user_obj = user_orm(
            full_name=full_name,
            login_id=login_id,
            type_id=3,
            tenant_code=db.execute("SELECT current_schema();").fetchone()[0],
            password_hash=Hash.bcrypt(password),
            is_active=True,
            domain_id=domain_id,
        )
        db.add(user_obj)
        db.flush()

        organization_user_obj = OrganizationUser(
            organization_id=organization,
            created_by=record_created_by,
            user_id=user_obj.id,
            user_department_id=user_department_id,
            country_id=country_id,
        )
        db.add(organization_user_obj)
        db.flush()

        user_role = OrganizationUserRole(org_user_id=user_obj.id, role_id=role_id)
        db.add(user_role)

        value = {
            "id": user_obj.id,
            "full_name": full_name,
            "login_id": login_id,
            "organization": organization,
            "record_created_by": record_created_by,
            "user_department_id": user_department_id,
            "role_id": role_id,
            "country_id": country_id,
        }
        return {
            "input_column": input_column,
            "output_data": value,
        }

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}


async def update_a_user(
    config,
    db,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    login_id_config = config.get("login_id")
    role_id_config = config.get("role_id")
    record_id_config = config.get("record_id")
    full_name_config = config.get("full_name")
    password_config = config.get("password")
    user_department_id_config = config.get("user_department_id")
    country_id_config = config.get("country_id")

    input_column = []
    is_ask_for_input = False

    login_id_config = get_data_from_config(
        db=db,
        data_config=login_id_config,
        event_dict=event_dict,
        data_name="login_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = login_id_config.get("error_message")
    is_ask_for_input = login_id_config.get("is_ask_for_input")
    login_id = login_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    role_id_config = get_data_from_config(
        db=db,
        data_config=role_id_config,
        event_dict=event_dict,
        data_name="role_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = role_id_config.get("error_message")
    is_ask_for_input = role_id_config.get("is_ask_for_input")
    role_id = role_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    record_id_config = get_data_from_config(
        db=db,
        data_config=record_id_config,
        event_dict=event_dict,
        data_name="record_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = record_id_config.get("error_message")
    is_ask_for_input = record_id_config.get("is_ask_for_input")
    record_id = record_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    full_name_config = get_data_from_config(
        db=db,
        data_config=full_name_config,
        event_dict=event_dict,
        data_name="full_name",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = full_name_config.get("error_message")
    is_ask_for_input = full_name_config.get("is_ask_for_input")
    full_name = full_name_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    password_config = get_data_from_config(
        db=db,
        data_config=password_config,
        event_dict=event_dict,
        data_name="password",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = password_config.get("error_message")
    is_ask_for_input = password_config.get("is_ask_for_input")
    password = password_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    user_department_id_config = get_data_from_config(
        db=db,
        data_config=user_department_id_config,
        event_dict=event_dict,
        data_name="user_department_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = user_department_id_config.get("error_message")
    is_ask_for_input = user_department_id_config.get("is_ask_for_input")
    user_department_id = user_department_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    country_id_config = get_data_from_config(
        db=db,
        data_config=country_id_config,
        event_dict=event_dict,
        data_name="country_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = country_id_config.get("error_message")
    is_ask_for_input = country_id_config.get("is_ask_for_input")
    country_id = country_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    try:
        user_obj = db.query(user_orm).where(user_orm.id == record_id).first()

        if not user_obj:
            return {"error_message": f"Id doesn't exists in this {workflow_title}."}

        user_obj.full_name = full_name
        user_obj.login_id = login_id
        user_obj.password_hash = Hash.bcrypt(password)
        db.flush()

        organization_user_obj = (
            db.query(OrganizationUser)
            .where(OrganizationUser.user_id == user_obj.id)
            .first()
        )

        if not organization_user_obj:
            return {"error_message": f"Id doesn't exists in this {workflow_title}."}

        organization_user_obj.user_department_id = user_department_id
        organization_user_obj.country_id = country_id
        db.flush()

        db.query(OrganizationUserRole).where(
            OrganizationUserRole.org_user_id == user_obj.id
        ).delete()

        user_role = OrganizationUserRole(org_user_id=user_obj.id, role_id=role_id)
        db.add(user_role)

        value = {
            "record_id": record_id,
            "full_name": full_name,
            "login_id": login_id,
            "user_department_id": user_department_id,
            "role_id": role_id,
            "country_id": country_id,
        }
        return {
            "input_column": input_column,
            "output_data": value,
        }

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}


async def assign_role_to_user(
    config,
    db,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    record_id_config = config.get("record_id")
    role_id_config = config.get("role_id")

    input_column = []
    is_ask_for_input = False

    record_id_config = get_data_from_config(
        db=db,
        data_config=record_id_config,
        event_dict=event_dict,
        data_name="record_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = record_id_config.get("error_message")
    is_ask_for_input = record_id_config.get("is_ask_for_input")
    record_id = record_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    role_id_config = get_data_from_config(
        db=db,
        data_config=role_id_config,
        event_dict=event_dict,
        data_name="role_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = role_id_config.get("error_message")
    is_ask_for_input = role_id_config.get("is_ask_for_input")
    role_id = role_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    try:
        for id in record_id:
            user_obj = db.query(user_orm).where(user_orm.id == id).first()

            if not user_obj:
                return {"error_message": f"Id doesn't exists in this {workflow_title}."}

            user_role = OrganizationUserRole(org_user_id=user_obj.id, role_id=role_id)
            db.add(user_role)
            db.flush()

        value = {
            "record_id": record_id,
            "role_id": role_id,
        }
        return {
            "input_column": input_column,
            "output_data": value,
        }

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}


async def deactivate_a_user(
    config,
    db,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    record_id_config = config.get("record_id")
    status_config = config.get("status")
    input_column = []
    is_ask_for_input = False

    record_id_config = get_data_from_config(
        db=db,
        data_config=record_id_config,
        event_dict=event_dict,
        data_name="record_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        check_condition=check_user_condition,
        **kwargs,
    )
    error_message = record_id_config.get("error_message")
    is_ask_for_input = record_id_config.get("is_ask_for_input")
    record_id = record_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    status_config = get_data_from_config(
        db=db,
        data_config=status_config,
        event_dict=event_dict,
        data_name="status",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = status_config.get("error_message")
    is_ask_for_input = status_config.get("is_ask_for_input")
    status = status_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    value = {
        "record_id": record_id,
        "status": bool(status),
    }

    try:
        for id in record_id:
            user_obj = db.query(user_orm).where(user_orm.id == id).first()
            if not user_obj:
                return {"error_message": f"Id doesn't exists in this {workflow_title}."}

            user_obj.is_active = bool(status)
            db.flush()

        return {
            "input_column": input_column,
            "output_data": value,
        }

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}
