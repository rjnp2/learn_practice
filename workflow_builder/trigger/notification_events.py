from fastapi import WebSocket
from twilio.rest import Client
import httpx
from technical.configurations.mail import Email
from technical.imports.handler_import import schema_context, Depends
from technical.endpoints.master_services.user.models import User as user_orm
from datetime import datetime, timezone
from technical.endpoints.org_services.notification_message.models import (
    NotificationMessage,
)
from technical.endpoints.org_services.notification_message.manager import (
    ConnectionManager,
)
from technical.endpoints.workflow_services.trigger.utils import (
    get_data_from_config,
    get_list_data_from_config,
)

manager = ConnectionManager()


async def send_email_notification(
    db,
    config,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    to_email_config = config.get("to_email") or []
    cc_email_config = config.get("cc_email") or []
    bcc_email_config = config.get("bcc_email") or []
    subject_config = config.get("subject")
    body_config = config.get("body")
    attachments_config = config.get("attachment") or []

    input_column = []
    is_ask_for_input = False

    to_email_config = get_list_data_from_config(
        db=db,
        data_config=to_email_config,
        event_dict=event_dict,
        data_name="to_email",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = to_email_config.get("error_message")
    is_ask_for_input = to_email_config.get("is_ask_for_input")
    to_email = to_email_config.get("output_data")

    if error_message:
        return {
            "error_message": error_message,
        }

    cc_email_config = get_list_data_from_config(
        db=db,
        data_config=cc_email_config,
        event_dict=event_dict,
        data_name="cc_email",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = cc_email_config.get("error_message")
    is_ask_for_input = cc_email_config.get("is_ask_for_input")
    cc_email = cc_email_config.get("output_data")

    if error_message:
        return {
            "error_message": error_message,
        }

    bcc_email_config = get_list_data_from_config(
        db=db,
        data_config=bcc_email_config,
        event_dict=event_dict,
        data_name="bcc_email",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = bcc_email_config.get("error_message")
    is_ask_for_input = bcc_email_config.get("is_ask_for_input")
    bcc_email = bcc_email_config.get("output_data")

    if error_message:
        return {
            "error_message": error_message,
        }

    subject_config = get_data_from_config(
        db=db,
        data_config=subject_config,
        event_dict=event_dict,
        data_name="subject",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = subject_config.get("error_message")
    is_ask_for_input = subject_config.get("is_ask_for_input")
    subject = subject_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    body_config = get_data_from_config(
        db=db,
        data_config=body_config,
        event_dict=event_dict,
        data_name="body",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = body_config.get("error_message")
    is_ask_for_input = body_config.get("is_ask_for_input")
    body = body_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    attachments_config = get_list_data_from_config(
        db=db,
        data_config=attachments_config,
        event_dict=event_dict,
        data_name="attachments",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = attachments_config.get("error_message")
    is_ask_for_input = attachments_config.get("is_ask_for_input")
    attachments = attachments_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    elif not to_email and not cc_email and not bcc_email:
        return {
            "error_message": f"It seems that the configuration data is invalid, as at least one of the to, cc, or bcc email addresses must be present in this {workflow_title}."
        }

    elif not subject or not body:
        return {
            "error_message": f"It seems that config data is invalid i.e subject or body is empty in this {workflow_title}."
        }

    try:
        email = Email(
            to_email=to_email,
            cc_email=cc_email,
            bcc_email=bcc_email,
            subject=subject,
            attachments=attachments,
            body=body,
        )
        if attachments:
            email.send_email_with_attachment()
        else:
            email.sendMail()

        data = {
            "subject": subject,
            "body": body,
            "to_email": to_email,
            "cc_email": cc_email,
            "bcc_email": bcc_email,
            "attachments": attachments,
        }
        return {
            "input_column": input_column,
            "output_data": data,
        }
    except Exception as e:
        return {"error_message": f"An error occurred while sending mail due to {e}"}


async def notification_function(
    db,
    config,
    workflow_title,
    event_dict={},
    websocket: WebSocket = Depends(),
    all_event_dict={},
    *args,
    **kwargs,
):
    subject_config = config.get("subject")
    body_config = config.get("body")
    user_ids_config = config.get("user_ids")
    organization_config = config.get("organization")

    is_ask_for_input = False
    input_column = []

    subject_config = get_data_from_config(
        db=db,
        data_config=subject_config,
        event_dict=event_dict,
        data_name="subject",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = subject_config.get("error_message")
    is_ask_for_input = subject_config.get("is_ask_for_input")
    subject = subject_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    body_config = get_data_from_config(
        db=db,
        data_config=body_config,
        event_dict=event_dict,
        data_name="body",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = body_config.get("error_message")
    is_ask_for_input = body_config.get("is_ask_for_input")
    body = body_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    organization_config = get_data_from_config(
        db=db,
        data_config=organization_config,
        event_dict=event_dict,
        data_name="organization",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = organization_config.get("error_message")
    is_ask_for_input = organization_config.get("is_ask_for_input")
    organization = organization_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    user_ids_config = get_list_data_from_config(
        db=db,
        data_config=user_ids_config,
        event_dict=event_dict,
        data_name="user_ids",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = user_ids_config.get("error_message")
    is_ask_for_input = user_ids_config.get("is_ask_for_input")
    user_ids = user_ids_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    elif not subject or not body or not user_ids or not organization:
        return {
            "error_message": f"It seems that config data is invalid i.e subject or body or user_ids or organization is empty in this {workflow_title}."
        }

    try:
        with schema_context("public", db):
            user_obj = db.query(user_orm).filter(user_orm.id.in_(user_ids))

        if not user_obj:
            return {
                "error_message": f"It seems that user not found in this {workflow_title}."
            }

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}

    try:
        with schema_context("public", db):
            for user in user_obj:
                await manager.notification(user.id, subject, websocket)

                notification_message_obj = NotificationMessage(
                    user_id=user.id,
                    subject=subject,
                    content=body,
                    is_read=False,
                    created_at=datetime.now(timezone.utc),
                    organization_id=organization,
                )
                db.add(notification_message_obj)
                db.flush()

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}

    data = {
        "subject": subject,
        "body": body,
        "user_ids": user_ids,
        "created_at": str(datetime.now(timezone.utc)),
        "notification_id": notification_message_obj.id,
    }
    return {
        "input_column": input_column,
        "output_data": data,
    }


async def send_sms_mail(
    db,
    config,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    account_sid_config = config.get("account_sid")
    auth_token_config = config.get("auth_token")
    admin_phone_number_config = config.get("admin_phone_number")
    sms_message_config = config.get("sms_message")
    to_config = config.get("to")

    is_ask_for_input = False
    input_column = []

    account_sid_config = get_data_from_config(
        db=db,
        data_config=account_sid_config,
        event_dict=event_dict,
        data_name="account_sid",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = account_sid_config.get("error_message")
    is_ask_for_input = account_sid_config.get("is_ask_for_input")
    account_sid = account_sid_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    auth_token_config = get_data_from_config(
        db=db,
        data_config=auth_token_config,
        event_dict=event_dict,
        data_name="auth_token",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = auth_token_config.get("error_message")
    is_ask_for_input = auth_token_config.get("is_ask_for_input")
    auth_token = auth_token_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    admin_phone_number_config = get_data_from_config(
        db=db,
        data_config=admin_phone_number_config,
        event_dict=event_dict,
        data_name="admin_phone_number",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = admin_phone_number_config.get("error_message")
    is_ask_for_input = admin_phone_number_config.get("is_ask_for_input")
    admin_phone_number = admin_phone_number_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    sms_message_config = get_data_from_config(
        db=db,
        data_config=sms_message_config,
        event_dict=event_dict,
        data_name="sms_message",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = sms_message_config.get("error_message")
    is_ask_for_input = sms_message_config.get("is_ask_for_input")
    sms_message = sms_message_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    to_config = get_list_data_from_config(
        db=db,
        data_config=to_config,
        event_dict=event_dict,
        data_name="to",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = to_config.get("error_message")
    is_ask_for_input = to_config.get("is_ask_for_input")
    to = to_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    if not account_sid and not auth_token and not admin_phone_number:
        return {
            "error_message": f"It seems that config data is invalid i.e Account SSID or Auth Token or Admin Phone Number is empty in this {workflow_title}."
        }

    elif not sms_message and not to:
        return {
            "error_message": f"It seems that config data is invalid i.e SMS Message or Recepient Phone Numbers or both is empty in this {workflow_title}."
        }

    try:
        client = Client(account_sid, auth_token)
        for recipient in to:
            client.messages.create(
                body=sms_message, from_=admin_phone_number, to=recipient
            )

        data = {
            "sms_message": sms_message,
            "to": to,
            "admin_phone_number": admin_phone_number,
        }
        return {
            "input_column": input_column,
            "output_data": data,
        }

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}


async def send_webhook_notification(
    db,
    config,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    url_config = config.get("url")
    subject_config = config.get("subject")
    content_config = config.get("content")
    secret_config = config.get("secret")
    client_id_config = config.get("client_id")

    is_ask_for_input = False
    input_column = []

    url_config = get_data_from_config(
        db=db,
        data_config=url_config,
        event_dict=event_dict,
        data_name="url",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = url_config.get("error_message")
    is_ask_for_input = url_config.get("is_ask_for_input")
    url = url_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    subject_config = get_data_from_config(
        db=db,
        data_config=subject_config,
        event_dict=event_dict,
        data_name="subject",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = subject_config.get("error_message")
    is_ask_for_input = subject_config.get("is_ask_for_input")
    subject = subject_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    content_config = get_data_from_config(
        db=db,
        data_config=content_config,
        event_dict=event_dict,
        data_name="content",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = content_config.get("error_message")
    is_ask_for_input = content_config.get("is_ask_for_input")
    content = content_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    secret_config = get_data_from_config(
        db=db,
        data_config=secret_config,
        event_dict=event_dict,
        data_name="secret",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = secret_config.get("error_message")
    is_ask_for_input = secret_config.get("is_ask_for_input")
    secret = secret_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    client_id_config = get_data_from_config(
        db=db,
        data_config=client_id_config,
        event_dict=event_dict,
        data_name="client_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = client_id_config.get("error_message")
    is_ask_for_input = client_id_config.get("is_ask_for_input")
    client_id = client_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    if not url and not secret and not client_id:
        return {
            "error_message": f"It seems that config data is invalid i.e URL and Secret Key are empty in this {workflow_title}."
        }

    elif not subject and not content:
        return {
            "error_message": f"It seems that config data is invalid i.e Subject or Content or both is empty in this {workflow_title}."
        }

    data = {
        "status": 200,
        "client_id": client_id,
        "subject": subject,
        "content": content,
    }

    try:
        async with httpx.AsyncClient() as client:
            response = await client.post(
                url, json=data, headers={"Authorization": secret}
            )
            response.raise_for_status()
            return {
                "input_column": input_column,
                "output_data": data,
            }
    except Exception as e:
        return {"error_message": f"An error occurred: {e}"}
