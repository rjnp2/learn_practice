import sys

from sqlalchemy.orm import Session
from technical.imports.handler_import import BadRequest_400, jsonable_encoder

from fastapi import BackgroundTasks
from crontab import CronTab

from .models import Trigger
from .schema import BulkDeleteTrigger, TriggerSchema, BulkUpdateTriggerSchema
from .create_sch_triggers_file import create_python_code
from .run_python_file import run_background_process
from .create_file_triggers_file import create_file_python_code


def remove_cron_job(name):
    cron = CronTab(user=True)
    for job in cron:
        if name in job.comment:
            cron.remove(job)
    cron.write()


def add_cron_job(command, schedule, log_dir, name):
    python_env_location = sys.prefix
    command = f"{python_env_location}/bin/python3.11 {command} >> {log_dir} 2>&1"

    cron = CronTab(user=True)
    job = cron.new(command=command, comment=name)
    job.setall(schedule)
    cron.write()


class TriggerRepository:
    def __init__(self) -> None:
        pass

    async def save(
        self,
        trigger: TriggerSchema,
        db: Session,
        current_user,
        organization,
        schema,
        background_tasks: BackgroundTasks,
    ):
        trigger_obj = Trigger(
            title=trigger.title,
            description=trigger.description,
            trigger_type=trigger.trigger_type,
            trigger_method=trigger.trigger_method,
            source=trigger.source,
            source_type=trigger.source_type,
            icon=trigger.icon,
            is_enabled=True,
            config=trigger.config,
            meta=trigger.meta,
            location=trigger.location,
            workflow_id=trigger.workflow_id,
            system_id=trigger.system_id,
            form_id=trigger.form_id,
            organization_id=organization,
            created_by=current_user.id,
        )
        db.add(trigger_obj)
        db.flush()

        data = jsonable_encoder(trigger_obj)
        data_id = data["id"]

        log_data = None
        if trigger.trigger_type.lower() == "trg_sch":
            if trigger.config.get("cronterval") is None:
                raise BadRequest_400(
                    message="cronterval of schedular trigger is empty."
                )

            file_name, log_dir = create_python_code(
                schema_name=schema,
                work_id=trigger.workflow_id,
                name=data_id,
                trigger_type=trigger.trigger_type,
            )
            name = f"trigger_{schema}_{data_id}"
            schedule = trigger.config.get("cronterval")
            remove_cron_job(name)
            add_cron_job(
                command=file_name, schedule=schedule, log_dir=log_dir, name=name
            )

        elif trigger.trigger_type.lower() == "trg_file":
            if trigger.config.get("folder_location") is None:
                raise BadRequest_400(
                    message="folder_location of schedular trigger is empty."
                )

            file_name = create_file_python_code(
                schema_name=schema,
                work_id=trigger.workflow_id,
                folder_location=trigger.config.get("folder_location"),
                name=data_id,
                method=trigger.trigger_method,
                trigger_type=trigger.trigger_type,
            )
            background_tasks.add_task(run_background_process, file_name)

        db.commit()
        return data, log_data

    async def update(
        self,
        triggers: list[BulkUpdateTriggerSchema],
        db: Session,
        current_user,
        schema,
        background_tasks: BackgroundTasks,
    ):
        final_data = []
        for trigger in triggers:
            id = trigger.id
            trigger_obj = db.query(Trigger).get(id)
            if not trigger_obj:
                raise BadRequest_400(message="Id doesnot exists")

            trigger_obj.title = trigger.title
            trigger_obj.description = trigger.description

            trigger_obj.icon = trigger.icon
            trigger_obj.trigger_type = trigger.trigger_type
            trigger_obj.trigger_method = trigger.trigger_method

            trigger_obj.source = trigger.source
            trigger_obj.source_type = trigger.source_type

            trigger_obj.config = trigger.config
            trigger_obj.meta = trigger.meta

            trigger_obj.location = trigger.location

            trigger_obj.workflow_id = trigger.workflow_id
            trigger_obj.system_id = trigger.system_id
            trigger_obj.form_id = trigger.form_id

            trigger_obj.updated_by = current_user.id

            if trigger.trigger_type.lower() == "trg_sch":
                if trigger.config.get("cronterval") is None:
                    raise BadRequest_400(
                        message="cronterval of schedular trigger is empty."
                    )

                file_name, log_dir = create_python_code(
                    schema_name=schema,
                    work_id=trigger.workflow_id,
                    name=id,
                    trigger_type=trigger.trigger_type,
                )
                schedule = trigger.config.get("cronterval")
                name = f"trigger_{schema}_{id}"
                remove_cron_job(name)
                add_cron_job(
                    command=file_name, schedule=schedule, log_dir=log_dir, name=name
                )

            elif trigger.trigger_type.lower() == "trg_file":
                if trigger.config.get("folder_location") is None:
                    raise BadRequest_400(
                        message="folder_location of schedular trigger is empty."
                    )

                file_name = create_file_python_code(
                    schema_name=schema,
                    work_id=trigger.workflow_id,
                    folder_location=trigger.config.get("folder_location"),
                    name=id,
                    method=trigger.trigger_method,
                    trigger_type=trigger.trigger_type,
                )
                background_tasks.add_task(run_background_process, file_name)

            data = jsonable_encoder(trigger_obj)

            data["id"] = id
            final_data.append(data)
            db.flush()

        db.commit()
        return final_data

    async def delete(
        self,
        schema,
        trigger: BulkDeleteTrigger,
        db: Session,
    ):
        workflow_ids = (
            db.query(Trigger)
            .where(Trigger.id.in_(trigger.config_ids), Trigger.deleted == False)
            .all()
        )
        for trigger_ in workflow_ids:
            trigger_.deleted = True
        db.commit()

        for trig_id in trigger.config_ids:
            name = f"trigger_{schema}_{trig_id}"
            remove_cron_job(name)

    async def restore(
        self,
        schema,
        trigger: BulkDeleteTrigger,
        db: Session,
    ):
        trigger_ids = (
            db.query(Trigger)
            .where(Trigger.id.in_(trigger.config_ids), Trigger.deleted == True)
            .all()
        )

        final_data = []
        for trigger in trigger_ids:
            trigger.deleted = False

            file_name, log_dir = create_python_code(
                schema_name=schema,
                work_id=trigger.workflow_id,
                name=trigger.id,
                trigger_type=trigger.trigger_type,
            )
            schedule = trigger.config.get("cronterval")
            name = f"trigger_{schema}_{trigger.id}"
            final_data.append((file_name, schedule, log_dir, name))

        db.commit()

        for data in final_data:
            file_name, schedule, log_dir, name = data
            remove_cron_job(name)
            add_cron_job(
                command=file_name, schedule=schedule, log_dir=log_dir, name=name
            )
