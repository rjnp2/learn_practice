import os


def create_file_python_code(
    schema_name, work_id, folder_location, name, method, trigger_type
):
    trigger_id = name
    name = f"trigger_{name}"

    python_code = f"""
import asyncio
import logging
import os
import sys
from pathlib import Path

import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# Get the directory path of the current script
current_script_path = os.path.dirname(os.path.realpath(__file__))

# Navigate to the parent directory of the current script
parent_directory = os.path.abspath(os.path.join(current_script_path, os.pardir))
parent_directory = os.path.abspath(os.path.join(parent_directory, os.pardir))
parent_directory = os.path.abspath(os.path.join(parent_directory, os.pardir))

try:
    # Change the current working directory to the parent directory
    sys.path.append(parent_directory)

    # Now import the necessary modules from the new location
    from technical.imports.handler_import import (
        get_db,
        schema_context,
        jsonable_encoder,
    )
    from technical.endpoints.workflow_services.trigger.transerve_workflow import (
        transerve_workflow_using_bfs,
    )
    from technical.endpoints.workflow_services.workflow.models import Workflow
    from technical.endpoints.workflow_services.trigger.models import Trigger
    from technical.settings.database import Database, switch_database
except Exception as e:
    raise ValueError(f"Error changing directory or importing modules: {{e}}")

# Set up logging
log_file_path = os.path.join(current_script_path, "{name}.log")
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    handlers=[logging.FileHandler(log_file_path), logging.StreamHandler(sys.stdout)],
)
logger = logging.getLogger(__name__)

async def execute_workflow(schema_name, work_id, trigger_message, event_dict):
    try:
        db = switch_database(schema_name)

        with schema_context('public', db):
            queryset = (
                db.query(Trigger).where(
                    Trigger.id == {trigger_id},
                    Trigger.deleted == False,
                    Trigger.is_enabled == True,
                )
            ).first()

        if not queryset:
            observer.stop()
            db.rollback()
            db.close()
            return


        with schema_context('public', db):
            queryset = (
                db.query(Workflow).where(
                    Workflow.id == work_id,
                    Workflow.deleted == False,
                    Workflow.is_enabled == True,
                )
            ).first()

        if not queryset:
            logger.error(f"No workflow found with ID {work_id}")
            observer.stop()
            db.close()
            return

        queryset = jsonable_encoder(queryset)
        event_dict = {{ 'start':event_dict}}
        workflow_instance = {{
            "workflow_instance": queryset,
            "start_node": "start",
            "db": db,
            "schema_name": schema_name,
            "trigger_id":{trigger_id},
            "trigger_message":trigger_message,
            "event_dict":event_dict,
        }}

        log_data, status = await transerve_workflow_using_bfs(**workflow_instance)
        if not status:
            observer.stop()
            db.rollback()
            db.close()
            logger.info("Workflow execution failed.")
            return

        logger.info("Workflow execution completed successfully.")
        db.commit()
        db.close()

    except Exception as e:
        logger.error(f"Error during workflow execution: {{str(e)}}")
        observer.stop()
        db.rollback()
        db.close()

class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        schema_name = '{schema_name}'
        work_id = {work_id}
        method = '{method}'

        file_location = os.path.dirname(event.src_path)
        file_name = os.path.basename(event.src_path)
        file_extension = Path(file_name).suffix
        file_size = os.path.getsize(event.src_path)

        trigger_message = f'"{{file_name}}" has been {{event.event_type}} at the {{file_location}} location.'

        event_dict = {{
            'trigger_id':f"{trigger_type}_{trigger_id}",
            'method':method,
            'file_name':file_name,
            'file_location':file_location,
            'file_extension':file_extension,
            'file_size':file_size,
        }}

        if event.is_directory:
            return None

        elif event.event_type == 'created' and method == 'add':
            # Event for file creation
            print(f"Received created event - {{event.src_path}}")
            asyncio.run(execute_workflow(schema_name, work_id, trigger_message, event_dict))

        elif event.event_type == 'modified' and method == 'mod':
            # Event for file modification
            print(f"Received modified event - {{event.src_path}}")
            asyncio.run(execute_workflow(schema_name, work_id, trigger_message, event_dict))

        elif event.event_type == 'deleted' and method == 'del':
            # Event for file deletion
            print(f"Received deleted event - {{event.src_path}}")
            asyncio.run(execute_workflow(schema_name, work_id, trigger_message, event_dict))

if __name__ == "__main__":
    directory_to_path = '{folder_location}'
    event_handler = Handler()
    observer = Observer()

    observer.schedule(event_handler, directory_to_path, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
        print("Observer Stopped")

    observer.join()
    """

    python_code = python_code.strip()

    script_dir = f"trigger_files"
    os.makedirs(script_dir, exist_ok=True)

    script_dir = f"{script_dir}/{schema_name}"
    os.makedirs(script_dir, exist_ok=True)

    script_dir = f"{script_dir}/files"
    os.makedirs(script_dir, exist_ok=True)

    script_dir = f"{script_dir}/{name}.py"
    with open(script_dir, mode="w") as f:
        f.write(python_code)
    return script_dir
