import os


def create_python_code(schema_name, work_id, name, trigger_type):
    trigger_id = name
    name = f"trigger_{name}"

    python_code = f"""
import asyncio
import logging
import os
import sys
from datetime import datetime
from crontab import CronTab

# Set up logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Get the directory path of the current script
current_script_path = os.path.dirname(os.path.realpath(__file__))

# Navigate to the parent directory of the current script (3 levels up)
parent_directory = os.path.abspath(
    os.path.join(current_script_path, os.pardir, os.pardir, os.pardir)
)

try:
    # Change the current working directory to the parent directory
    os.chdir(parent_directory)
    sys.path.append(parent_directory)

    # Now import the necessary modules from the new location
    from technical.imports.handler_import import (
        get_db,
        schema_context,
        jsonable_encoder,
    )
    from technical.endpoints.workflow_services.trigger.transerve_workflow import (
        transerve_workflow_using_bfs,
    )
    from technical.endpoints.workflow_services.workflow.models import Workflow
    from technical.settings.database import Database, switch_database
except Exception as e:
    raise ValueError(f"Error changing directory or importing modules: {{e}}")

def remove_cron_job(name):
    cron = CronTab(user=True)
    for job in cron:
        if name in job.comment:
            cron.remove(job)
    cron.write()

async def execute_workflow(schema_name, work_id, trigger_message):
    try:
        db = switch_database(schema_name)

        with schema_context('public', db):
            queryset = (
                db.query(Workflow).where(
                    Workflow.id == work_id,
                    Workflow.deleted == False,
                    Workflow.is_enabled == True,
                )
            ).first()

        if not queryset:
            logger.error(f"No workflow found with ID {work_id}")
            name = f"trigger_{schema_name}_{trigger_id}"
            remove_cron_job(name)
            
            db.rollback()
            db.close()
            return

        current_time = str(datetime.now().strftime("%H:%M:%S"))
        trigger_message = f'"{trigger_id}" scheduler trigger has been running at the {{current_time}} datetime.'
        queryset = jsonable_encoder(queryset)

        event_dict = {{ 
            'start':{{
                'trigger_id':f"{trigger_type}_{trigger_id}",
            }}
        }}
        workflow_instance = {{
            "workflow_instance": queryset,
            "start_node": "start",
            "db": db,
            "schema_name": schema_name,
            "trigger_id":{trigger_id},
            "trigger_message":trigger_message,
            event_dict=event_dict,
        }}

        log_data, status = await transerve_workflow_using_bfs(**workflow_instance)

        if not status:
            db.rollback()
            db.close()
            logger.info("Workflow execution failed.")
            return

        logger.info("Workflow execution completed successfully.")
        db.commit()
        db.close()

    except Exception as e:
        logger.error(f"Error during workflow execution: {{str(e)}}")
        db.rollback()
        db.close()

if __name__ == "__main__":
    asyncio.run(execute_workflow(
        schema_name = '{schema_name}',
        work_id= {work_id}
    ))
    """

    python_code = python_code.strip()

    script_dir = f"trigger_files"
    os.makedirs(script_dir, exist_ok=True)

    script_dir = f"{script_dir}/{schema_name}"
    os.makedirs(script_dir, exist_ok=True)

    script_dir = f"{script_dir}/scheduler"
    os.makedirs(script_dir, exist_ok=True)

    log_dir = f"{script_dir}/{name}.log"
    log_dir = os.path.abspath(log_dir)

    script_dir = f"{script_dir}/{name}.py"
    script_dir = os.path.abspath(script_dir)

    with open(script_dir, mode="w") as f:
        f.write(python_code)

    return script_dir, log_dir
