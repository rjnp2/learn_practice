from .record_management_events import (
    update_exist_records,
)
from technical.settings.database import switch_database


async def request_approval(
    config,
    db,
    schema_name,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    new_db = switch_database(schema_name)
    event_outcome = await update_exist_records(
        config=config,
        db=new_db,
        workflow_title=workflow_title,
        event_dict=event_dict,
        all_event_dict=all_event_dict,
        *args,
        **kwargs,
    )

    if (
        event_outcome
        and type(event_outcome) == dict
        and not event_outcome.get("error_message")
    ):
        new_db.commit()
    else:
        new_db.rollback()
    new_db.close()
    return event_outcome


async def modify_approval(
    db,
    config,
    schema_name,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    new_db = switch_database(schema_name)
    event_outcome = await update_exist_records(
        config=config,
        db=new_db,
        workflow_title=workflow_title,
        event_dict=event_dict,
        all_event_dict=all_event_dict,
        *args,
        **kwargs,
    )

    if (
        event_outcome
        and type(event_outcome) == dict
        and not event_outcome.get("error_message")
    ):
        new_db.commit()
    else:
        new_db.rollback()
    new_db.close()
    return event_outcome


async def forward_approval(
    db,
    config,
    schema_name,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    new_db = switch_database(schema_name)
    event_outcome = await update_exist_records(
        config=config,
        db=new_db,
        workflow_title=workflow_title,
        event_dict=event_dict,
        all_event_dict=all_event_dict,
        *args,
        **kwargs,
    )

    if (
        event_outcome
        and type(event_outcome) == dict
        and not event_outcome.get("error_message")
    ):
        new_db.commit()
    else:
        new_db.rollback()

    new_db.close()
    return event_outcome
