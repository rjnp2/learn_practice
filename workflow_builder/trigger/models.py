import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB

from technical.settings.database import Base, BaseModel


class Trigger(BaseModel, Base):
    __tablename__ = "triggers"
    __table_args__ = {"schema": "public"}

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    title = sa.Column(sa.String(200), nullable=False)
    description = sa.Column(sa.Text(), nullable=True)

    is_enabled = sa.Column(sa.Boolean, default=False)

    icon = sa.Column(sa.String, nullable=True)

    trigger_type = sa.Column(sa.String(200), nullable=False)
    trigger_method = sa.Column(sa.String(200), nullable=False)

    source = sa.Column(sa.String(200), nullable=False)
    source_type = sa.Column(sa.String(200), nullable=True)

    config = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )

    meta = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )

    location = sa.Column(sa.String(200), nullable=True)
    file_location = sa.Column(sa.Text, nullable=True)

    workflow_id = sa.Column(sa.ForeignKey("public.workflow.id"), nullable=False)

    system_id = sa.Column(sa.ForeignKey("public.system.id"), nullable=False)
    form_id = sa.Column(sa.ForeignKey("public.form.id"), nullable=True)
    organization_id = sa.Column(sa.ForeignKey("public.organization.id"), nullable=False)

    created_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=False)
    updated_by = sa.Column(sa.ForeignKey("public.user.id"), nullable=True)

    created_obj = sa.orm.relationship("User", foreign_keys=[created_by])
    updated_obj = sa.orm.relationship("User", foreign_keys=[updated_by])

    workflow_obj = sa.orm.relationship("Workflow", back_populates="triggers")


class TriggerLog(Base):
    __tablename__ = "trigger_logs"
    __table_args__ = {"schema": "public"}

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)

    trigger_id = sa.Column(sa.ForeignKey("public.triggers.id"), nullable=True)
    workflow_id = sa.Column(sa.ForeignKey("public.workflow.id"), nullable=False)

    record_id = sa.Column(sa.Integer(), nullable=True)

    status = sa.Column(sa.String(200), nullable=False)

    logs = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )

    input_data = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )

    instance_data = sa.Column(
        JSONB(astext_type=sa.Text()),
        nullable=False,
        default=[],
        server_default="[]",
    )
    created_at = sa.Column(
        sa.TIMESTAMP(timezone=True), default=sa.func.now(), onupdate=sa.func.now()
    )

    workflow_obj = sa.orm.relationship("Workflow", foreign_keys=[workflow_id])
    trigger_obj = sa.orm.relationship("Trigger", foreign_keys=[trigger_id])
