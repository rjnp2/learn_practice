import json
from fastapi.encoders import jsonable_encoder
from datetime import datetime
from technical.endpoints.org_services.form.models import Form
from technical.configurations.sequence import new_seq_function
from technical.utility.views_tables import (
    get_data_details,
    sql_insert_record,
    sql_insert_record_junction,
    sql_update_record,
)
from technical.endpoints.workflow_services.trigger.utils import (
    get_data_from_config,
)


def get_query_data(db, conditions, template_id, schema_name, form_name):
    filter_data = []
    having_data = []
    junction = []
    for condition_data in conditions:
        column = condition_data.get("column")
        if column is None:
            return {"error_message": f"column is not provided in conditions_mapping."}

        value = condition_data.get("value")
        if value is None:
            return {"error_message": f"value is not provided in conditions_mapping."}

        operator = condition_data.get("operator").lower().strip().replace("_", " ")
        if operator is None:
            return {"error_message": f"operator is not provided in conditions_mapping."}

        operator = operator.lower().strip().replace("_", " ")

        response_choice = condition_data.get("response_choice")
        if response_choice is None:
            return {
                "error_message": f"response_choice is not provided in conditions_mapping."
            }

        if operator not in [
            "=",
            "!=",
            "in",
            "not in",
            "like",
            "not like",
            ">",
            ">=",
            "<",
            "<=",
            "between",
        ]:
            return {"error_message": f"Invalid operator."}

        if isinstance(value, list) and response_choice.startswith(
            "internal", "relation", "global"
        ):
            value = ",".join(f"'{i}'" for i in value)

        elif operator == "between":
            start_item = value[0]
            end_item = value[-1]
            value = f"'{start_item}' and '{end_item}'"

        elif operator in ["like", "not like"]:
            value = f"'%{value}%'"

        else:
            value = f"'{value}'"

        if response_choice == "default":
            filter_data.append(f'"{column}" {operator} {value}')

        elif response_choice.startswith("global"):
            filter_data.append(f'"{column}" && ARRAY[{value}]')

        elif response_choice.startswith(("internal", "relation")):
            jt = f"jt_{template_id}_{column}"
            having_data.append(
                f"ARRAY[{value}]::bigint[] @> ARRAY_AGG(DISTINCT {jt}.table_2)::bigint[]"
            )
            junction.append(
                f"""join {schema_name}.{jt}
                on {form_name}.id = {jt}.table_1
            """
            )
        else:
            return {"error_message": f"Invalid condition"}

    filter_data = " and ".join(filter_data)
    if filter_data:
        filter_data = f"where {filter_data}"

    having_data = " and ".join(having_data)
    if having_data:
        having_data = f"having {having_data}"

    junction = " \n ".join(junction)

    try:
        raw_query = f"""
            select id from {schema_name}.{form_name} 
            {junction}
            {filter_data}
            GROUP BY {form_name}.id
            {having_data};
        """
        record_ids = db.execute(raw_query).fetchall()
        record_ids = [i[0] for i in record_ids]
    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}

    return {
        "output_data": record_ids,
    }


def get_form_fields(id, db, workflow_title):
    try:
        form_field = db.query(Form).get(id)
        if not form_field:
            return {
                "error_message": f"Template id not found in {workflow_title} workflow."
            }
    except Exception as e:
        message = f"An error occurred due to {e}"
        return {"error_message": message}

    final_data = {}
    for i in form_field.fields:
        id = i.get("id")
        final_data[id] = {
            "label": i.get("label"),
            "deleted": i.get("deleted", False),
            "required": bool(i.get("required")),
        }
    return {"field_data": final_data, "form_name": form_field.name}


def data_convert_validation(template_data, field_data, workflow_title):
    column_data = []
    value_data = []
    junction_table_data = []

    for field_dat in template_data:
        if type(field_dat) != dict:
            return {
                "error_message": f"Type of template_data must be list of object/dict in {workflow_title} workflow."
            }

        value = field_dat.get("value")
        field_id = str(field_dat.get("id")).lower()

        field_data_id = field_data.get(field_id, {})
        label = field_data_id.get("label") or field_id
        required = field_data_id.get("required", False)
        deleted = field_data_id.get("deleted", False)
        response_choice = field_dat.get("response_choice").lower()

        if deleted:
            if response_choice.startswith(("global")):
                field_dat["value"] = "{-}"
            continue

        if value in [None, "", []] and required:
            return {"error_message": f"value of {label} is required."}

        elif value in ["", "", None, []]:
            if response_choice.startswith(("global")):
                field_dat["value"] = "{-}"

            continue

        field_type = str(field_dat.get("type")).lower()
        if response_choice.startswith(("global")):
            value = ",".join([str(i) for i in value if i]) or "-"
            value = "{" + value + "}"

        elif response_choice.startswith(("internal", "relation")):
            data = {
                "id": field_dat.get("id"),
                "value": field_dat.get("value"),
            }
            junction_table_data.append(data)

        elif field_type == "number":
            try:
                value = float(value)
            except:
                return {
                    "error_message": f"Value of {label} data must be float in {workflow_title} workflow."
                }

        elif field_type == "number-int":
            try:
                value = int(value)
            except:
                return {
                    "error_message": f"Value of {label} data must be integer in {workflow_title} workflow."
                }

        elif field_type in ["checkbox", "switch"]:
            try:
                value = bool(value)
            except:
                return {
                    "error_message": f"Value of {label} data must be boolean in {workflow_title} workflow."
                }

        elif field_type == "datetime":
            try:
                value = datetime.datetime.fromisoformat(value.replace("Z", "+00:00"))
                value = str(value)
            except:
                return {
                    "error_message": f"Value of {label} data must be datetime with format %Y-%m-%d %H:%M:%S in {workflow_title} workflow."
                }

        elif field_type == "date":
            try:
                value = datetime.datetime.strptime(value, "%Y-%m-%d").date()
                value = str(value)
            except:
                return {
                    "error_message": f"Value of {label} data must be datetime with format %Y-%m-%d in {workflow_title} workflow."
                }

        elif field_type == "time":
            try:
                value = value.split(":")
                if len(value) == 3:
                    value = value[:-1]
                value = ":".join(value)
                value = datetime.datetime.strptime(value, "%H:%M").time()
                value = str(value)
            except:
                return {
                    "error_message": f"Value of {label} data must be time in {workflow_title} workflow."
                }

        if isinstance(value, list):
            value = json.dumps(value)

        elif isinstance(value, dict):
            value = json.dumps(value)

        field_dat["value"] = value
        field_id = f'"{field_id}"'
        if not response_choice.startswith(("internal", "relation")):
            column_data.append(field_id)
            value_data.append(value)

    return {
        "column_data": column_data,
        "value_data": value_data,
        "junction_table_data": junction_table_data,
    }


async def create_new_records(
    config,
    db,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    template_id_config = config.get("template_id")
    user_id_config = config.get("user_id")
    organization_config = config.get("organization")
    schema_name_config = config.get("schema_name")
    template_data_config = config.get("template_data")

    input_column = []
    is_ask_for_input = False

    template_id_config = get_data_from_config(
        db=db,
        data_config=template_id_config,
        event_dict=event_dict,
        data_name="template_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = template_id_config.get("error_message")
    is_ask_for_input = template_id_config.get("is_ask_for_input")
    template_id = template_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    schema_name_config = get_data_from_config(
        db=db,
        data_config=schema_name_config,
        event_dict=event_dict,
        data_name="schema_name",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = schema_name_config.get("error_message")
    is_ask_for_input = schema_name_config.get("is_ask_for_input")
    schema_name = schema_name_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    user_id_config = get_data_from_config(
        db=db,
        data_config=user_id_config,
        event_dict=event_dict,
        data_name="user_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = user_id_config.get("error_message")
    is_ask_for_input = user_id_config.get("is_ask_for_input")
    user_id = user_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    organization_config = get_data_from_config(
        db=db,
        data_config=organization_config,
        event_dict=event_dict,
        data_name="organization",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = organization_config.get("error_message")
    is_ask_for_input = organization_config.get("is_ask_for_input")
    organization = organization_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if not template_id:
        return {
            "error_message": f"It seems that config data is invalid i.e template id is empty in this {workflow_title}."
        }

    output = get_form_fields(id=template_id, db=db, workflow_title=workflow_title)
    message = output.get("message")
    if message:
        return {"error_message": message}

    field_data = output.get("field_data")
    form_name = output.get("form_name")
    if field_data is None or form_name is None:
        return {"error_message": "Error encountered while getting form data."}

    template_data_config = get_data_from_config(
        db=db,
        data_config=template_data_config,
        event_dict=event_dict,
        data_name="template_data",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_template_data=True,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = template_data_config.get("error_message")
    is_ask_for_input = template_data_config.get("is_ask_for_input")
    template_data = template_data_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    if (
        not template_id
        or not user_id
        or not schema_name
        or not template_data
        or not not organization
    ):
        return {
            "error_message": f"It seems that config data is invalid i.e template id or user id or schema name or organization or template data is empty in this {workflow_title}."
        }

    try:
        seq = await new_seq_function(
            form_id=template_id,
            db_code=kwargs.get("schema_name"),
            schema_name=schema_name,
            organization=organization,
        )

        output = data_convert_validation(
            template_data=template_data,
            field_data=field_data,
            workflow_title=workflow_title,
        )

        message = output.get("message")
        if message:
            return {"error_message": message}

        column_data = output.get("column_data")
        value_data = output.get("value_data")
        junction_table_data = output.get("junction_table_data")
        if column_data is None or value_data is None or junction_table_data is None:
            return {"error_message": "Error encountered while parsing template data."}

        column_data.extend(["uid", "meta", "is_draft", "user_id", "created_by"])
        value_data.extend(
            [
                seq,
                "{}",
                False,
                user_id,
                user_id,
            ]
        )
        my_dict = {
            f"val{k}".replace('"', ""): v for k, v in zip(column_data, value_data)
        }

        value_d = [f":val{i}".replace('"', "") for i in column_data]
        value_d = ", ".join(value_d)
        value_d = f"({value_d})"

        column_data = ", ".join(column_data)
        raw_query = sql_insert_record(
            form_id=template_id,
            schema_name=schema_name,
            cols=column_data,
            vals=value_d,
        )
        templates_obj = db.execute(raw_query, my_dict)
        value = jsonable_encoder(templates_obj.fetchone())
        db.flush()

        column_data_junc = ", ".join(["table_1", "table_2"])
        for data in junction_table_data:
            value[data.get("id")] = data.get("value", [])

            for val in data.get("value"):
                value_data = f'({value["id"]}, {val})'
                junction_table = "jt_{0}_{1}".format(template_id, data.get("id"))
                raw_query = sql_insert_record_junction(
                    table_name=junction_table,
                    schema_name=schema_name,
                    cols=column_data_junc,
                    vals=value_data,
                )
                db.execute(raw_query)
                db.flush()

        value["template_id"] = template_id
        value["user_id"] = user_id
        value["schema_name"] = schema_name

        return {"output_data": value, "input_column": input_column}
    except Exception as e:
        message = f"An error occurred due to {e}"
        return {"error_message": message}


async def update_exist_records(
    config,
    db,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    template_id_config = config.get("template_id")
    user_id_config = config.get("user_id")
    schema_name_config = config.get("schema_name")
    template_data_config = config.get("template_data")
    record_id_config = config.get("record_id")

    input_column = []
    is_ask_for_input = False
    is_current_data = False
    is_ask_current_data = False

    template_id_config = get_data_from_config(
        db=db,
        data_config=template_id_config,
        event_dict=event_dict,
        data_name="template_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        is_ask_current_data=is_ask_current_data,
        **kwargs,
    )
    error_message = template_id_config.get("error_message")
    is_ask_for_input = is_ask_for_input or template_id_config.get("is_ask_for_input")
    is_current_data = template_id_config.get("is_current_data")
    is_ask_current_data = template_id_config.get("is_ask_current_data")
    template_id = template_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    schema_name_config = get_data_from_config(
        db=db,
        data_config=schema_name_config,
        event_dict=event_dict,
        data_name="schema_name",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        is_ask_current_data=is_ask_current_data,
        **kwargs,
    )
    error_message = schema_name_config.get("error_message")
    is_ask_for_input = is_ask_for_input or schema_name_config.get("is_ask_for_input")
    is_current_data = schema_name_config.get("is_current_data")
    is_ask_current_data = schema_name_config.get("is_ask_current_data")
    schema_name = schema_name_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    user_id_config = get_data_from_config(
        db=db,
        data_config=user_id_config,
        event_dict=event_dict,
        data_name="user_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        is_ask_current_data=is_ask_current_data,
        **kwargs,
    )
    error_message = user_id_config.get("error_message")
    is_ask_for_input = is_ask_for_input or user_id_config.get("is_ask_for_input")
    is_current_data = user_id_config.get("is_current_data")
    is_ask_current_data = user_id_config.get("is_ask_current_data")
    user_id = user_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    record_id_config = get_data_from_config(
        db=db,
        data_config=record_id_config,
        event_dict=event_dict,
        data_name="record_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        is_ask_current_data=is_ask_current_data,
        **kwargs,
    )
    error_message = record_id_config.get("error_message")
    is_ask_for_input = is_ask_for_input or record_id_config.get("is_ask_for_input")
    is_current_data = record_id_config.get("is_current_data")
    is_ask_current_data = record_id_config.get("is_ask_current_data")
    record_id = record_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    output = get_form_fields(id=template_id, db=db, workflow_title=workflow_title)

    message = output.get("message")
    field_data = output.get("field_data")
    form_name = output.get("form_name")

    if message:
        return {"error_message": message}

    elif field_data is None or form_name is None:
        return {"error_message": "Error encountered while getting form data."}

    template_data_config = get_data_from_config(
        db=db,
        data_config=template_data_config,
        event_dict=event_dict,
        data_name="template_data",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_template_data=True,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        is_ask_current_data=is_ask_current_data,
        **kwargs,
    )
    error_message = template_data_config.get("error_message")
    is_ask_for_input = is_ask_for_input or template_data_config.get("is_ask_for_input")
    is_current_data = template_data_config.get("is_current_data")
    is_ask_current_data = template_data_config.get("is_ask_current_data")
    template_data = template_data_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_current_data:
        return {"error_message": "ask_current", "input_column": input_column}

    if is_current_data:
        return {"error_message": "current", "input_column": input_column}

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    if (
        not template_id
        or not user_id
        or not schema_name
        or not template_data
        or not record_id
    ):
        return {
            "error_message": f"It seems that config data is invalid i.e template id or user id or schema name or record id or template data is empty in this {workflow_title}."
        }

    output = data_convert_validation(
        template_data=template_data,
        field_data=field_data,
        workflow_title=workflow_title,
    )

    message = output.get("message")
    if message:
        return {"error_message": message}

    column_data = output.get("column_data")
    value_data = output.get("value_data")
    junction_table_data = output.get("junction_table_data")
    if column_data is None or value_data is None or junction_table_data is None:
        return {"error_message": "Error encountered while parsing template data."}

    try:
        raw_query = get_data_details(
            table_name=f"table_{template_id}",
            schema_name=schema_name,
            record_id=record_id,
        )
        inspection_data = db.execute(raw_query).fetchone()
        if inspection_data:
            inspection_data = jsonable_encoder(inspection_data)
        else:
            return {"error_message": f"Id doesn't exists in this {workflow_title}."}

        column_data.extend(["updated_by"])
        value_data.extend(
            [
                user_id,
            ]
        )
        my_dict = {
            f"val{k}".replace('"', ""): v for k, v in zip(column_data, value_data)
        }
        my_dict["data_id"] = record_id

        column_dat = [f":val{i}".replace('"', "") for i in column_data]
        set_clause = [f"{i} = {j}" for i, j in zip(column_data, column_dat)]
        set_clause = ", ".join(set_clause)

        raw_query = sql_update_record(
            form_id=template_id,
            schema_name=schema_name,
            set_clause=set_clause,
        )
        templates_obj = db.execute(raw_query, my_dict)
        value = jsonable_encoder(templates_obj.fetchone())
        db.flush()

        column_data_junc = ", ".join(["table_1", "table_2"])
        for data in junction_table_data:
            value[data.get("id")] = data.get("value", [])
            junction_table = "jt_{0}_{1}".format(template_id, data.get("id"))

            tbl_query = f"""
                DELETE FROM {schema_name}.{junction_table}
                WHERE tbl.table_1 = {record_id};
            """
            db.execute(tbl_query)

            for val in data.get("value"):
                value_data = f'({value["id"]}, {val})'
                raw_query = sql_insert_record_junction(
                    table_name=junction_table,
                    schema_name=schema_name,
                    cols=column_data_junc,
                    vals=value_data,
                )
                db.execute(raw_query)
                db.flush()

        value["template_id"] = template_id
        value["user_id"] = user_id
        value["schema_name"] = schema_name
        value["record_id"] = record_id

        return {"output_data": value, "input_column": input_column}
    except Exception as e:
        message = f"An error occurred due to {e}"
        return {"error_message": message}


async def delete_records(
    config,
    db,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    template_id_config = config.get("template_id")
    schema_name_config = config.get("schema_name")
    record_id_config = config.get("record_id")

    input_column = []
    is_ask_for_input = False
    is_current_data = False

    template_id_config = get_data_from_config(
        db=db,
        data_config=template_id_config,
        event_dict=event_dict,
        data_name="template_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = template_id_config.get("error_message")
    is_ask_for_input = template_id_config.get("is_ask_for_input")
    is_current_data = template_id_config.get("is_current_data")
    template_id = template_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    schema_name_config = get_data_from_config(
        db=db,
        data_config=schema_name_config,
        event_dict=event_dict,
        data_name="schema_name",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = schema_name_config.get("error_message")
    is_ask_for_input = schema_name_config.get("is_ask_for_input")
    is_current_data = schema_name_config.get("is_current_data")
    schema_name = schema_name_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if not template_id or not schema_name or not record_id:
        return {
            "error_message": f"It seems that config data is invalid i.e template_id is empty in this {workflow_title}."
        }

    form_name = f"table_{template_id}"
    record_id_config = get_data_from_config(
        db=db,
        data_config=record_id_config,
        event_dict=event_dict,
        data_name="record_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        check_condition=get_query_data,
        template_id=template_id,
        schema_name=schema_name,
        form_name=form_name,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = record_id_config.get("error_message")
    is_ask_for_input = record_id_config.get("is_ask_for_input")
    is_current_data = record_id_config.get("is_current_data")
    record_id = record_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    if is_current_data:
        return {"error_message": "current", "input_column": input_column}

    if type(record_id) != list:
        record_id = [
            record_id,
        ]

    record_id_str = ", ".join(map(str, record_id))

    try:
        raw_query = f"""
            UPDATE {schema_name}.{form_name} 
            set deleted = false
            where id in ({record_id_str})
        """
        db.execute(raw_query)
    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}

    value = {
        "schema_name": schema_name,
        "template_id": template_id,
        "record_id": record_id,
        "status": True,
    }
    return {"output_data": value, "input_column": input_column}


async def restore_records(
    config,
    db,
    workflow_title,
    all_event_dict={},
    event_dict={},
    *args,
    **kwargs,
):
    template_id_config = config.get("template_id")
    schema_name_config = config.get("schema_name")
    record_id_config = config.get("record_id")

    input_column = []
    is_ask_for_input = False
    is_current_data = False

    template_id_config = get_data_from_config(
        db=db,
        data_config=template_id_config,
        event_dict=event_dict,
        data_name="template_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = template_id_config.get("error_message")
    is_ask_for_input = template_id_config.get("is_ask_for_input")
    is_current_data = template_id_config.get("is_current_data")
    template_id = template_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    schema_name_config = get_data_from_config(
        db=db,
        data_config=schema_name_config,
        event_dict=event_dict,
        data_name="schema_name",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = schema_name_config.get("error_message")
    is_ask_for_input = schema_name_config.get("is_ask_for_input")
    is_current_data = schema_name_config.get("is_current_data")
    schema_name = schema_name_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if not template_id or not schema_name or not record_id:
        return {
            "error_message": f"It seems that config data is invalid i.e template_id is empty in this {workflow_title}."
        }

    form_name = f"table_{template_id}"
    record_id_config = get_data_from_config(
        db=db,
        data_config=record_id_config,
        event_dict=event_dict,
        data_name="record_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        check_condition=get_query_data,
        template_id=template_id,
        schema_name=schema_name,
        form_name=form_name,
        **kwargs,
    )
    error_message = record_id_config.get("error_message")
    is_ask_for_input = record_id_config.get("is_ask_for_input")
    is_current_data = record_id_config.get("is_current_data")
    record_id = record_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    if is_current_data:
        return {"error_message": "current", "input_column": input_column}

    if type(record_id) != list:
        record_id = [
            record_id,
        ]

    record_id_str = ", ".join(map(str, record_id))

    try:
        raw_query = f"""
            UPDATE {schema_name}.{form_name} 
            set deleted = false
            where id in ({record_id_str})
        """
        db.execute(raw_query)
    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}

    value = {
        "schema_name": schema_name,
        "template_id": template_id,
        "record_id": record_id,
        "status": False,
    }
    return {"output_data": value, "input_column": input_column}


async def duplicate_record(
    config,
    db,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    template_id_config = config.get("template_id")
    organization_config = config.get("organization")
    record_id_config = config.get("record_id")
    schema_name_config = config.get("schema_name")

    input_column = []
    is_ask_for_input = False
    is_current_data = False

    template_id_config = get_data_from_config(
        db=db,
        data_config=template_id_config,
        event_dict=event_dict,
        data_name="template_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = template_id_config.get("error_message")
    is_ask_for_input = template_id_config.get("is_ask_for_input")
    is_current_data = template_id_config.get("is_current_data")
    template_id = template_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    organization_config = get_data_from_config(
        db=db,
        data_config=organization_config,
        event_dict=event_dict,
        data_name="organization",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = organization_config.get("error_message")
    is_ask_for_input = organization_config.get("is_ask_for_input")
    is_current_data = organization_config.get("is_current_data")
    organization = organization_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    schema_name_config = get_data_from_config(
        db=db,
        data_config=schema_name_config,
        event_dict=event_dict,
        data_name="schema_name",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = schema_name_config.get("error_message")
    is_ask_for_input = schema_name_config.get("is_ask_for_input")
    is_current_data = schema_name_config.get("is_current_data")
    schema_name = schema_name_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    record_id_config = get_data_from_config(
        db=db,
        data_config=record_id_config,
        event_dict=event_dict,
        data_name="record_id",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        is_current_data=is_current_data,
        **kwargs,
    )
    error_message = record_id_config.get("error_message")
    is_ask_for_input = record_id_config.get("is_ask_for_input")
    is_current_data = record_id_config.get("is_current_data")
    record_id = record_id_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    if is_current_data:
        return {"error_message": "current", "input_column": input_column}

    if not template_id or not organization or not record_id or not schema_name:
        return {
            "error_message": f"It seems that config data is invalid i.e template_id or organization or record_id or schema_name is empty in this {workflow_title}."
        }

    try:
        raw_query = get_data_details(
            table_name=f"table_{template_id}",
            schema_name=schema_name,
            record_id=record_id,
        )
        inspection_data = db.execute(raw_query).fetchone()
        if inspection_data:
            inspection_data = jsonable_encoder(inspection_data)
        else:
            return {"error_message": f"Id doesn't exists in this {workflow_title}."}

        original_id = inspection_data.pop("id")
        inspection_data["uid"] = await new_seq_function(
            form_id=template_id,
            db_code=kwargs.get("schema_name"),
            schema_name=schema_name,
            organization=organization,
        )

        column_data = list(inspection_data.keys())
        value_data = list(inspection_data.values())

        my_dict = {
            f"val{k}".replace('"', ""): (
                json.dumps(v) if isinstance(v, (list, dict)) else v
            )
            for k, v in zip(column_data, value_data)
        }

        value_d = [f":val{i}".replace('"', "") for i in column_data]
        value_d = ", ".join(value_d)
        value_d = f"({value_d})"

        column_data = ", ".join([f'"{i}"' for i in column_data])
        raw_query = sql_insert_record(
            form_id=template_id,
            schema_name=schema_name,
            cols=column_data,
            vals=value_d,
        )

        templates_obj = db.execute(raw_query, my_dict)
        value = jsonable_encoder(templates_obj.fetchone())
        db.flush()

        column_data_junc = ", ".join(["table_1", "table_2"])
        table_name = f"table_{template_id}"
        sql_query = f"""
        SELECT
            tc.table_name AS junction_table
        FROM
            information_schema.table_constraints AS tc
            JOIN information_schema.key_column_usage AS kcu
                ON tc.constraint_name = kcu.constraint_name
                AND tc.table_schema = kcu.table_schema
            JOIN information_schema.constraint_column_usage AS ccu
                ON ccu.constraint_name = tc.constraint_name
                AND ccu.table_schema = tc.table_schema
        WHERE
            tc.constraint_type = 'FOREIGN KEY'
            AND ccu.table_name = '{table_name}'
            AND ccu.table_schema = '{schema_name}';
        """

        junction_tables = db.execute(sql_query).fetchall()
        junction_tables = jsonable_encoder(junction_tables)

        for data in junction_tables:
            data = data["junction_table"]
            raw_query = f"""
                select table_2 FROM {schema_name}.{data} tbl
                WHERE tbl.table_1 = {original_id};
            """
            old_value = db.execute(raw_query).fetchall()
            old_value = jsonable_encoder(old_value)

            for val in old_value:
                val = val["table_2"]
                value_data = f'({value["id"]}, {val})'
                raw_query = sql_insert_record_junction(
                    table_name=data,
                    schema_name=schema_name,
                    cols=column_data_junc,
                    vals=value_data,
                )
                db.execute(raw_query)
                db.flush()

        value["schema_name"] = schema_name
        return {"output_data": value, "input_column": input_column}

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}
