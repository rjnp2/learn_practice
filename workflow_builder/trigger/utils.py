from technical.endpoints.workflow_services.variables.models import Variables
from technical.endpoints.master_services.user.models import User as user_orm
import re

# Define the pattern
variable_pattern = r"@\{va\{\d+\}va\}"
data_carry_pattern = r"@\{dc\{[a-zA-Z0-9_]+#[a-zA-Z0-9_]+\}dc\}"


def get_variable_value(db, variable, is_multiple=False):
    config_to_data = (
        db.query(Variables)
        .where(Variables.id == variable, Variables.deleted == False)
        .first()
    )

    if is_multiple:
        variable_value = (
            config_to_data.value if config_to_data and config_to_data.value else []
        )

    else:
        variable_value = (
            config_to_data.value[0] if config_to_data and config_to_data.value else ""
        )

    return variable_value


def get_data_from_config(
    db,
    data_config,
    event_dict,
    data_name,
    input_column,
    all_event_dict,
    workflow_title,
    is_ask_for_input=False,
    is_current_data=False,
    is_ask_current_data=False,
    **kwargs,
):
    if type(data_config) == dict:
        data_type = data_config.get("type", "").lower()
        check_condition = kwargs.get("check_condition")
        is_dynamic_check_condition = kwargs.get("is_dynamic_check_condition", False)
        is_template_data = kwargs.get("is_template_data", False)

        if data_type == "static":
            data = data_config.get("static", "")

        elif data_type == "dynamic":
            data = data_config.get("static", "")

            data_matches = re.findall(variable_pattern, data)
            for data_match in data_matches:
                replace_data_match = (
                    data_match.replace("@{va{", "").replace("}va}", "").strip()
                )

                ind_data = get_variable_value(
                    db=db, variable=replace_data_match, is_multiple=False
                )
                data = data.replace(data_match, ind_data)

            data_matches = re.findall(data_carry_pattern, data)
            for data_match in data_matches:
                replace_data_match = (
                    data_match.replace("@{dc{", "").replace("}dc}", "").strip()
                )
                node_data, key_data = replace_data_match.split("#")
                node_data = node_data.strip()

                key_data = key_data.strip()

                prev_node = all_event_dict.get(node_data)
                ind_data = prev_node.get(key_data)
                data = data.replace(data_match, ind_data)

        elif is_template_data and data_type == "variable":
            variable = data_config.get("variable")

            data = []
            for key, value in variable.items():
                variable_type = value.get("variable_type").lower()

                if variable_type == "static":
                    ind_data = data_config.get("static", "")

                elif variable_type == "variable":
                    variable = value.get("variable")
                    is_multiple = value.get("response_choice", "").startswith(
                        ("internal", "global", "relation")
                    )
                    ind_data = get_variable_value(
                        db=db, variable=variable, is_multiple=is_multiple
                    )

                else:
                    return {
                        "error_message": f"It seems that variable of {data_name} is not valid in this {workflow_title}."
                    }

                data.append(
                    {
                        "id": key,
                        "value": ind_data,
                        "response_choice": value.get("response_choice"),
                        "type": value.get("type"),
                    }
                )

        elif data_type == "variable":
            variable = data_config.get("variable")
            data = get_variable_value(db=db, variable=variable, is_multiple=False)

        elif data_type == "ask_for_input":
            data = event_dict.get(data_name)
            input_column.append(data_name)
            if not data:
                is_ask_for_input = True

        elif data_type == "ask_current":
            data = event_dict.get(data_name)
            input_column.append(data_name)

            if not data:
                is_ask_current_data = True

        elif data_type == "current":
            data = event_dict.get(data_name)
            input_column.append(data_name)
            if not data:
                is_current_data = True

        elif data_type == "login_user":
            data = kwargs.get("current_user_id")

        elif is_dynamic_check_condition and data_type == "filter":
            if check_condition is None:
                return {"error_message": f"check_condition is not provided in config."}

            conditions = data_config.get("conditions")
            if conditions is None:
                return {"error_message": f"condition is not provided in config."}

            template_id = kwargs.get("template_id")
            if template_id is None:
                return {"error_message": f"template_id is not provided in config."}

            schema_name = kwargs.get("schema_name")
            if schema_name is None:
                return {"error_message": f"schema_name is not provided in config."}

            form_name = kwargs.get("form_name")
            if form_name is None:
                return {"error_message": f"form_name is not provided in config."}

            try:
                data = check_condition(
                    db=db,
                    conditions=conditions,
                    template_id=template_id,
                    schema_name=schema_name,
                    form_name=form_name,
                )

                error_message = data.get("error_message")
                data = data.get("output_data")
                if error_message:
                    return {
                        "error_message": error_message,
                    }
            except:
                return {"error_message": f"Invalid condition function."}

        elif not is_dynamic_check_condition and data_type == "filter":
            if check_condition is None:
                return {"error_message": f"check_condition is not provided in config."}

            conditions = data_config.get("conditions")
            if conditions is None:
                return {"error_message": f"condition is not provided in config."}

            conditions_mapping = data_config.get("conditions_mapping")
            if conditions_mapping is None:
                return {
                    "error_message": f"conditions_mapping is not provided in config."
                }

            try:
                data = check_condition(
                    db=db, conditions_mapping=conditions_mapping, conditions=conditions
                )

                error_message = data.get("error_message")
                data = data.get("output_data")
                if error_message:
                    return {
                        "error_message": error_message,
                    }
            except:
                return {"error_message": f"Invalid condition function."}

        elif data_type == "all_users":
            data = db.query(user_orm).all()
            data = [i.id for i in data]

        elif not is_template_data and data_type == "data_carry":
            data_carry = data_config.get("data_carry", {})
            node_data = data_carry.get("node")
            key_data = data_carry.get("key")

            prev_node = all_event_dict.get(node_data)
            data = prev_node.get(key_data)

        elif is_template_data and data_type == "data_carry":
            data_carry = data_config.get("data_carry", {})

            data = []
            for key, value in data_carry.items():
                node_data = value.get("node")
                key_data = value.get("key")
                prev_node = all_event_dict.get(node_data)

                data.append(
                    {
                        "id": key,
                        "value": prev_node.get(key_data),
                        "response_choice": value.get("response_choice"),
                        "type": value.get("type"),
                    }
                )

        elif data_type == "set_trigger_message":
            data = kwargs.get("trigger_message", "This is system generated message.")

        else:
            return {
                "error_message": f"It seems that type of {data_name} is not valid in this {workflow_title}."
            }

    else:
        return {
            "error_message": f"It seems that the configuration is invalid because the {data_name} must be a dictionary in this {workflow_title}."
        }

    return {
        "output_data": data,
        "is_ask_for_input": is_ask_for_input,
        "is_current_data": is_current_data,
        "is_ask_current_data": is_ask_current_data,
    }


def get_list_data_from_config(
    db,
    data_config,
    event_dict,
    data_name,
    input_column,
    all_event_dict,
    workflow_title,
    is_ask_for_input=False,
    is_current_data=False,
    is_ask_current_data=False,
    **kwargs,
):
    data = []

    if type(data_config) == list:
        for ind_data_config in data_config:
            data = get_data_from_config(
                db=db,
                data_config=ind_data_config,
                event_dict=event_dict,
                data_name=data_name,
                input_column=input_column,
                all_event_dict=all_event_dict,
                workflow_title=workflow_title,
                is_ask_for_input=is_ask_for_input,
                is_current_data=is_current_data,
                is_ask_current_data=is_ask_current_data,
                **kwargs,
            )
            error_message = data.get("error_message")
            is_ask_for_input = data.get("is_ask_for_input")
            is_current_data = data.get("is_current_data")
            is_ask_current_data = data.get("is_ask_current_data")
            output_data = data.get("output_data")
            if error_message:
                return {
                    "error_message": error_message,
                }

            if type(output_data) == list:
                data.extend(output_data)
            else:
                data.append(output_data)

    else:
        return {
            "error_message": f"It seems that the configuration is invalid because the {data_name} must be a dictionary in this {workflow_title}."
        }

    return {
        "output_data": data,
        "is_ask_for_input": is_ask_for_input,
        "is_current_data": is_current_data,
        "is_ask_current_data": is_ask_current_data,
    }
