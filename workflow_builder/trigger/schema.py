from datetime import datetime as date
from enum import Enum

from pydantic import BaseModel, Field
from sqlalchemy.dialects.postgresql import JSONB

from technical.utility.user_schema import UserInfo
from technical.endpoints.workflow_services.workflow.schema import (
    GetWorkFlowsListSchema,
)
import typing


class TriggerType(str, Enum):
    scheduled_trigger = "TRG_SCH"
    file_trigger = "TRG_FILE"
    db_form_trigger = "DB_FORM_TRG"
    db_trigger = "TRG_DB"
    ui_button_click_trigger = "TRG_BTN"
    ui_field_entry_trigger = "TRG_FLD"
    ui_button_hover_trigger = "TRG_HVR"
    api_trigger = "TRG_API"
    webhook_trigger = "TRG_WBH"
    process_timeout_trigger = "TRG_OUT"
    auth_trigger = "TRG_AUTH"


class SourceChoice(str, Enum):
    static = "static"
    dynamic = "dynamic"
    empty = ""


class TriggerBaseSchema(BaseModel):
    title: str = Field(..., min_length=5)
    description: str = ""

    trigger_type: TriggerType
    trigger_method: str = ""

    icon: str | None = None

    source: SourceChoice
    source_type: str | None

    location: str | None = None

    workflow_id: int
    system_id: int
    form_id: int | None = None

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class TriggerSchema(TriggerBaseSchema):
    config: dict = {}
    meta: list | dict = []


class BulkUpdateTriggerSchema(TriggerBaseSchema):
    id: int
    config: dict = {}
    meta: list | dict = []


class GetTriggersSchema(TriggerBaseSchema):
    id: int

    is_enabled: bool = False
    organization_id: int
    config: dict = {}
    logs: typing.Optional[list] = []

    trigger_json: dict = {}

    created_obj: UserInfo
    created_at: date

    updated_obj: UserInfo | None
    workflow_obj: GetWorkFlowsListSchema
    updated_at: date


class BulkDeleteTrigger(BaseModel):
    config_ids: list[int]


class StatusTriggerSchema(BaseModel):
    id: int
    system_id: int
    is_enabled: bool = False


class InputDataSchema(BaseModel):
    input_data: dict = {}
    data_dict: dict = {}


class TriggerFkSchema(BaseModel):
    id: int
    title: str

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class WorkFkSchema(BaseModel):
    id: int
    title: str

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }


class TriggerLogsSchema(BaseModel):
    id: int
    status: str
    record_id: typing.Optional[int]
    logs: dict = {}
    input_data: dict = {}
    instance_data: dict = {}
    created_at: date

    workflow_obj: WorkFkSchema
    trigger_obj: TriggerFkSchema | None = None

    class Config:
        orm_mode = True
        use_enum_values = True
        json_encoders = {
            JSONB: lambda v: v,
        }
