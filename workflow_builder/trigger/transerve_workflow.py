from collections import deque
from datetime import datetime, timezone
from technical.endpoints.workflow_services.trigger.models import TriggerLog
from technical.imports.handler_import import schema_context
from fastapi.encoders import jsonable_encoder
from technical.settings.database import switch_database

from .user_role_management_events import (
    create_a_user,
    update_a_user,
    assign_role_to_user,
    deactivate_a_user,
)

from .notification_events import (
    send_email_notification,
    notification_function,
    send_sms_mail,
    send_webhook_notification,
)

from .record_management_events import (
    create_new_records,
    update_exist_records,
    delete_records,
    restore_records,
    duplicate_record,
)

from .script_events import execute_script, execute_sequence_script

from .user_interation_events import redirect_response, display_alert

from .conditions import check_condition
from .approval_events import modify_approval, forward_approval, request_approval
from .external_integration_events import api_event

event_mapper = {
    "evnt_noti_mail": send_email_notification,
    "evnt_noti_noti": notification_function,
    "evnt_noti_sms": send_sms_mail,
    "evnt_noti_webhook": send_webhook_notification,
    "evnt_script_code": execute_script,
    "evnt_script_seq": execute_sequence_script,
    "evnt_ux_alert": display_alert,
    "evnt_ux_rdrct": redirect_response,
    "evnt_rcrd_add": create_new_records,
    "evnt_rcrd_updt": update_exist_records,
    "evnt_rcrd_del": delete_records,
    "evnt_rcrd_rest": restore_records,
    "evnt_rcrd_dup": duplicate_record,
    "evnt_user_mgmt_add": create_a_user,
    "evnt_user_mgmt_updt": update_a_user,
    "evnt_user_mgmt_assign": assign_role_to_user,
    "evnt_user_mgmt_deact": deactivate_a_user,
    "evnt_dyn_act_mod": modify_approval,
    "evnt_dyn_act_for": forward_approval,
    "evnt_dyn_act_req": request_approval,
    "evnt_integ_ext_api": api_event,
}

event_run_once = (
    "evnt_noti_mail",
    "evnt_noti_noti",
    "evnt_noti_sms",
    "evnt_noti_webhook",
    "evnt_script_code",
    "evnt_script_seq",
    "evnt_ux_alert",
    "evnt_ux_rdrct",
    "evnt_integ_ext_api",
)


def save_trigger_log(
    schema_name,
    trigger_id,
    workflow_id,
    status,
    log_data,
    input_data,
    instance_data,
    trigger_log_id=None,
):
    new_db = switch_database(schema_name)

    try:
        with schema_context("public", new_db):
            if trigger_log_id:
                trigger_log_obj = (
                    new_db.query(TriggerLog).where(
                        TriggerLog.id == trigger_log_id,
                    )
                ).first()

                trigger_log_obj.status = str(status)
                trigger_log_obj.logs = log_data
                trigger_log_obj.input_data = input_data
                trigger_log_obj.instance_data = instance_data
                new_db.commit()
                new_db.close()
                return trigger_log_id

            else:
                record_id = input_data.get("start", {}).get("record_id")
                trigger_log = TriggerLog(
                    trigger_id=trigger_id,
                    workflow_id=workflow_id,
                    status=str(status),
                    logs=log_data,
                    input_data=input_data,
                    record_id=record_id,
                    instance_data=instance_data,
                )
                new_db.add(trigger_log)
                new_db.commit()
                trigger_log_id = trigger_log.id
                new_db.close()
                return trigger_log_id
    except Exception as e:
        new_db.rollback()
        new_db.close()


async def transerve_workflow_using_bfs(
    workflow_instance,
    start_node,
    db,
    schema_name,
    is_validate=False,
    event_dict={},
    *args,
    **kwargs,
):
    queue = deque()

    workflow_instance_title = workflow_instance["title"]
    workflow = workflow_instance["field_data"]
    queue.append(start_node)

    trigger_message = kwargs.get("trigger_message")
    trigger_log_id = kwargs.get("trigger_log_id")

    if trigger_log_id:
        with schema_context("public", db):
            trigger_log_obj = (
                db.query(TriggerLog).where(
                    TriggerLog.id == trigger_log_id,
                )
            ).first()
            if trigger_log_obj:
                log_data = trigger_log_obj.logs
            else:
                log_data = {}
    else:
        log_data = {}

    current_user_id = kwargs.get("current_user_id") or workflow_instance["created_by"]
    log_data["trigger_message"] = trigger_message
    workflow_element = workflow_instance["elements"]
    log_data["elements"] = workflow_element
    log_data["field_data"] = workflow

    log_data_input_data = log_data.get("input_data", {})
    log_data_input_data.update(event_dict)
    event_dict = log_data_input_data

    trigger_data = event_dict.get("start", {})

    final_result = True
    while queue:
        return_now = False
        current_node_key = queue.popleft()
        individual_log_data = {}

        output_data = None
        log_data["current_node"] = current_node_key

        current_node = workflow[current_node_key]

        individual_event_dict = event_dict.get(current_node_key, {})

        input_column = individual_event_dict.get("input_column") or []

        next_node = current_node["next"]
        block = current_node.get("block", "").lower()
        title = current_node.get("title", "")
        unmod_code = current_node.get("code", "")
        code = unmod_code.lower()
        config = current_node.get("config")
        modal_action = config.get("modal_action")

        message = individual_event_dict.get("message") or "completed"
        status = individual_event_dict.get("status") or "success"
        start_time_gmt = datetime.now(timezone.utc)

        if block == "start":
            message = "Started"

        elif block == "end":
            message = "Workflow completed"

        elif block == "condition":
            if code in ["cndn_bin", "cndn_dom"]:
                new_next_node = []

                for next_nod in next_node:
                    condition_node = workflow[next_nod]

                    if condition_node.get("block", "").lower() != "logic":
                        continue

                    logic_label = condition_node.get("title", "")
                    config = condition_node.get("config", {})

                    condition_match = check_condition(
                        db=db,
                        schema_name=schema_name,
                        condition_config=config,
                        data_dict=event_dict.get("condition"),
                        trigger_data=trigger_data,
                    )

                    error_message = condition_match.get("error_message")
                    condition_output = condition_match.get("condition_output")

                    if error_message:
                        status = "error"
                        return_now = True
                        message = f"{error_message} in {logic_label} logic"
                        new_next_node = []
                        break

                    if condition_output or not config:
                        new_next_node.append(
                            next_nod,
                        )
                        message = f'Activated "{logic_label}" logic'
                        break

                if return_now:
                    pass

                elif not new_next_node:
                    message = f"No logic is activated"
                    status = "error"

                next_node = new_next_node

            elif code == "cndn_seq":
                new_next_node = []
                message = []

                for next_nod in next_node:
                    condition_node = workflow[next_nod]

                    if condition_node.get("block", "").lower() != "logic":
                        continue

                    logic_label = condition_node.get("title", "")
                    config = condition_node["config"]

                    condition_match = check_condition(
                        db=db,
                        schema_name=schema_name,
                        condition_config=config,
                        data_dict=event_dict.get("condition"),
                        trigger_data=trigger_data,
                    )

                    error_message = condition_match.get("error_message")
                    condition_output = condition_match.get("condition_output")

                    if error_message:
                        status = "error"
                        return_now = True
                        message = f"{error_message} in {logic_label} logic"
                        new_next_node = []
                        break

                    if condition_output or not config:
                        message.append(logic_label)
                        new_next_node.append(next_nod)

                if return_now:
                    pass

                elif not new_next_node:
                    message = f"No logic is activated"
                    status = "error"

                elif new_next_node:
                    logics_message = "logics" if len(message) > 1 else "logic"
                    message = ", ".join(message)
                    message = f'Activated "{message}" {logics_message}'

                else:
                    message = f"No logic is activated"
                    status = "error"
                    new_next_node = []

                next_node = new_next_node

            else:
                status = "error"
                message = f'invalid "{code}" code type'
                next_node = None

        elif block == "event":
            if modal_action:
                is_completed = individual_event_dict.get("is_completed")
                if not is_completed:
                    status = "modal_action"
                    message = "Exceution paused. User action required"
                    return_now = True

                else:
                    message = "Event triggered successfully"
                    status = "success"

            elif config:

                individual_event_status = individual_event_dict.get("status")
                if code in event_run_once and individual_event_status:
                    continue

                event_function = event_mapper.get(code)
                if event_function is not None:
                    event_outcome = await event_function(
                        config=config,
                        workflow_title=workflow_instance_title,
                        is_validate=is_validate,
                        db=db,
                        schema_name=schema_name,
                        event_dict=individual_event_dict,
                        trigger_message=trigger_message,
                        current_user_id=current_user_id,
                        all_event_dict=event_dict,
                    )

                    message = "Event triggered successfully"

                    if event_outcome and type(event_outcome) != dict:
                        status = "error"
                        return_now = True
                        message = "Outcome of this events is not valid dict."

                    elif event_outcome and type(event_outcome) == dict:
                        error_message = event_outcome.get("error_message")
                        input_column = event_outcome.get("input_column")
                        output_data = event_outcome.get("output_data", {})
                        individual_event_dict.update(output_data)

                        if error_message:
                            status = "error"
                            return_now = True
                            message = error_message

                            if error_message.lower() == "ask_for_input":
                                message = "Exceution paused. User input required"
                                status = "ask_for_input"

                            if error_message.lower() == "ask_current":
                                message = "Exceution paused. User input required"
                                status = "ask_current"

                            elif error_message.lower() == "current":
                                message = "Exceution paused. User input required"
                                status = "current"

                else:
                    status = "error"
                    message = f"It seems that event doesnot exist with {code} code in this {workflow_instance_title}."
                    return_now = True

            else:
                return_now = True
                status = "error"
                message = f"It seems that config data doesnot exist in this {workflow_instance_title}."

        elif block == "frontend_event":
            is_completed = individual_event_dict.get("is_completed")
            if not is_completed:
                status = "modal_action"
                message = "Exceution paused. User action required"
                return_now = True

            else:
                message = "Event triggered successfully"
                status = "success"

        elif block == "logic":
            message = "Successfully matched"
            next_node = [
                next_nod
                for next_nod in next_node
                if workflow[next_nod].get("block", "").lower() != "logic"
            ]

        else:
            status = "error"
            message = f'invalid "{block}" block type'
            next_node = None

        end_time_gmt = datetime.now(timezone.utc)

        if next_node:
            queue.extend(next_node)

        time_difference = end_time_gmt - start_time_gmt

        individual_log_data["start_date"] = start_time_gmt.strftime("%Y-%m-%d %H:%M:%S")
        individual_log_data["end_date"] = end_time_gmt.strftime("%Y-%m-%d %H:%M:%S")
        individual_log_data["input_column"] = input_column
        individual_log_data["config"] = config
        individual_log_data["block"] = block
        individual_log_data["code"] = unmod_code
        individual_log_data["title"] = title
        individual_log_data["total_seconds"] = str(time_difference.total_seconds())
        individual_log_data["status"] = status
        individual_log_data["message"] = message.capitalize()
        individual_log_data["input_data"] = individual_event_dict
        log_data[current_node_key] = individual_log_data
        event_dict[current_node_key] = individual_event_dict
        log_data["input_data"] = event_dict
        log_data["status"] = status

        if return_now:
            final_result = False
            break

    if not is_validate:
        trigger_log_id = save_trigger_log(
            schema_name=schema_name,
            trigger_id=kwargs.get("trigger_id"),
            trigger_log_id=trigger_log_id,
            workflow_id=workflow_instance["id"],
            status=status,
            log_data=log_data,
            input_data=event_dict,
            instance_data={},
        )
        log_data["trigger_log_id"] = trigger_log_id

    return log_data, final_result


async def run_transerve_workflow_using_bfs_bg(triggers, tenant_code, *args, **kwargs):
    trigger_message = kwargs.get("trigger_message")
    path = kwargs.get("path", "")
    method = kwargs.get("method", "")
    status_code = kwargs.get("status_code", 200)
    time_difference = kwargs.get("time_difference", 0)
    current_user_id = kwargs.get("current_user_id")

    db = switch_database(tenant_code)

    event_dict = kwargs.get("event_dict", {})

    for trigger in triggers:
        config = trigger.get("config")
        trigger_type = trigger.get("trigger_type")
        trigger_id = trigger.get("id")

        if trigger["trigger_type"] in ["TRG_API"]:
            api_url = config.get("api_url", "")
            if api_url != path:
                continue

            trigger_message = f'"{api_url}" was requested with the {method} method, and the response status code is {str(status_code).lower()}.'

        elif trigger["trigger_type"] in ["TRG_OUT"]:
            api_url = config.get("api_url", "")
            time_limit = config.get("time_limit")

            if api_url != path and time_limit is None:
                continue

            try:
                time_limit = float(time_limit)
            except:
                continue

            if time_limit >= time_difference:
                continue

            event_dict = {"start": {"time_limit": time_limit}}

            trigger_message = (
                f'"{api_url}" took {time_difference} to complete its execution.'
            )

        start_node = event_dict["start"]
        start_node["trigger_id"] = f"{trigger_type}_{trigger_id}"
        event_dict["start"] = start_node

        workflow_obj = jsonable_encoder(trigger["workflow_obj"])
        if workflow_obj["deleted"]:
            continue

        if not workflow_obj["is_enabled"]:
            continue

        try:
            with schema_context("public", db):
                _, status = await transerve_workflow_using_bfs(
                    workflow_instance=workflow_obj,
                    start_node="start",
                    db=db,
                    schema_name=tenant_code,
                    trigger_id=trigger["id"],
                    trigger_message=trigger_message,
                    current_user_id=current_user_id,
                    event_dict=event_dict,
                )
                if status:
                    db.commit()
                else:
                    db.rollback()
        except Exception as e:
            print(e)
            db.rollback()
