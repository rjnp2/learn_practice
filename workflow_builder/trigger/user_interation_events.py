from fastapi import WebSocket
from technical.imports.handler_import import Depends
from technical.endpoints.org_services.notification_message.manager import (
    ConnectionManager,
)
from fastapi.responses import RedirectResponse
from technical.endpoints.workflow_services.trigger.utils import (
    get_data_from_config,
)

manager = ConnectionManager()


async def redirect_response(
    db,
    config,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    url_config = config.get("url")

    input_column = []
    is_ask_for_input = False

    url_config = get_data_from_config(
        db=db,
        data_config=url_config,
        event_dict=event_dict,
        data_name="url",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = url_config.get("error_message")
    is_ask_for_input = url_config.get("is_ask_for_input")
    url = url_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    elif not url:
        return {
            "error_message": f"It seems that config data is invalid i.e URL is empty in this {workflow_title}."
        }
    return RedirectResponse(url=url)


async def display_alert(
    db,
    config,
    workflow_title,
    websocket: WebSocket = Depends(),
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    value_config = config.get("value")

    input_column = []
    is_ask_for_input = False

    input_column = []
    value_config = get_data_from_config(
        db=db,
        data_config=value_config,
        event_dict=event_dict,
        data_name="value",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = value_config.get("error_message")
    is_ask_for_input = value_config.get("is_ask_for_input")
    value = value_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    elif not value:
        return {
            "error_message": f"It seems that config data is invalid i.e alert value is empty in this {workflow_title}."
        }

    await manager.notification("workflow", value, websocket)

    value = {
        "value": value,
    }
    return {
        "input_column": input_column,
        "output_data": value,
    }
