from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Query
from sqlalchemy import desc, func

from sqlalchemy.orm import Session, subqueryload
from technical.common.system_control import system_access
from technical.endpoints.master_services.user import models as user_orm
from technical.imports.handler_import import (
    InferringRouter,
    cbv,
    Request,
    Depends,
    get_db,
    oauth2,
    get_tenant_code,
    ordering_join,
    schema_context,
    JSONResponse,
    BadRequest_400,
    HTTPException,
    CustomPage,
    paginate,
)
from fastapi import BackgroundTasks
from technical.utility.subdomain_manager import get_db_domain

from .repository import TriggerRepository
from .models import Trigger, TriggerLog
from .schema import (
    TriggerSchema,
    GetTriggersSchema,
    BulkDeleteTrigger,
    BulkUpdateTriggerSchema,
    StatusTriggerSchema,
    TriggerLogsSchema,
)

router = InferringRouter()

repo = TriggerRepository()

lower = ["title", "description"]
exclude = [
    "created_by",
]
headers = {
    "id": "Id",
    "title": "Trigger Title",
    "description": "Description",
    "created_at": "Created At",
    "created_by": "Created By",
}


@cbv(router=router)
class TriggerAPIView:

    def __init__(
        self,
        request: Request,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ) -> None:
        system_access(current_user, request, db)
        self.tenant_code = get_tenant_code(db, current_user)
        self.db_code = get_db_domain(request=request)
        self.organization = current_user.org_id
        self.current_user = current_user
        if not self.organization:
            raise BadRequest_400(message="Access Denied")

    @staticmethod
    def filter_data(
        db,
        queryset,
        search_query=None,
        trigger_type=None,
        trigger_method=None,
        location=None,
        form_id=None,
        workflow_id=None,
        sort_by=None,
        is_enabled=None,
        archived=None,
    ):
        if search_query:
            queryset = queryset.where(
                or_(
                    Trigger.title.ilike("%" + search_query + "%"),
                    Trigger.description.ilike("%" + search_query + "%"),
                )
            )

        if trigger_type:
            queryset = queryset.where(Trigger.trigger_type.in_(trigger_type))

        if is_enabled:
            queryset = queryset.where(Trigger.is_enabled == is_enabled)

        if trigger_method:
            queryset = queryset.where(Trigger.trigger_method == trigger_method)

        if location:
            queryset = queryset.where(Trigger.location == location)

        if archived is not None:
            queryset = queryset.where(Trigger.deleted == archived)

        if workflow_id:
            queryset = queryset.where(Trigger.workflow_id == workflow_id)

        if form_id:
            queryset = queryset.where(Trigger.form_id == form_id)
            for instance in queryset.filter(
                Trigger.config.has_key("approval_type")
            ).all():
                logs = (
                    db.query(
                        TriggerLog.id,
                        TriggerLog.status,
                        TriggerLog.record_id,
                        func.jsonb_path_query_first(
                            TriggerLog.logs, "$.current_node"
                        ).label("current_node"),
                    )
                    .filter(
                        TriggerLog.workflow_id == instance.workflow_id,
                        TriggerLog.trigger_id == instance.id,
                        TriggerLog.record_id.isnot(None),
                    )
                    .order_by(TriggerLog.id)
                    .all()
                )
                logs_encode = jsonable_encoder(logs)
                instance.logs = logs_encode

        sort_by = sort_by or "-created_at"
        queryset = ordering_join(
            {}, Trigger, queryset, lower=lower, exclude=exclude, sort_by=sort_by
        )
        return queryset.all()

    @router.get("/", response_model=list[GetTriggersSchema])
    async def get(
        self,
        system_id: int,
        db: Session = Depends(get_db),
        q: str = None,
        sortBy: str = None,
        trigger_type: list[str] = Query(None),
        trigger_method: str = None,
        location: str = None,
        workflow_id: int = None,
        archived: bool = False,
        form_id: int = None,
        is_enabled: bool = None,
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(Trigger)
                .where(
                    Trigger.system_id == system_id,
                )
                .outerjoin(Trigger.created_obj)
                .options(subqueryload(Trigger.workflow_obj))
            )

            queryset = self.filter_data(
                db=db,
                queryset=queryset,
                search_query=q,
                sort_by=sortBy,
                trigger_type=trigger_type,
                trigger_method=trigger_method,
                location=location,
                workflow_id=workflow_id,
                form_id=form_id,
                archived=archived,
                is_enabled=is_enabled,
            )

            return queryset

    @router.get("/logs/", response_model=CustomPage[TriggerLogsSchema])
    async def retrieve_logs(
        self,
        trigger_id: int = None,
        workflow_id: int = None,
        status: str = None,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            queryset = db.query(TriggerLog).options(
                subqueryload(TriggerLog.workflow_obj),
                subqueryload(TriggerLog.trigger_obj),
            )
            trigger_counts = db.query(
                TriggerLog.trigger_id, func.count(TriggerLog.trigger_id).label("count")
            ).group_by(TriggerLog.trigger_id)

            null_trigger_counts = db.query(TriggerLog).filter(
                TriggerLog.trigger_id.is_(None)
            )

            if trigger_id is not None:
                queryset = queryset.where(TriggerLog.trigger_id == trigger_id)
                null_trigger_counts = null_trigger_counts.where(
                    TriggerLog.trigger_id == trigger_id
                )
                trigger_counts = trigger_counts.where(
                    TriggerLog.trigger_id == trigger_id
                )

            if workflow_id is not None:
                queryset = queryset.where(TriggerLog.workflow_id == workflow_id)
                null_trigger_counts = null_trigger_counts.where(
                    TriggerLog.workflow_id == workflow_id
                )
                trigger_counts = trigger_counts.where(
                    TriggerLog.workflow_id == workflow_id
                )

            if status is not None:
                queryset = queryset.where(TriggerLog.status == status)
                null_trigger_counts = null_trigger_counts.where(
                    TriggerLog.status == status
                )
                trigger_counts = trigger_counts.where(TriggerLog.status == status)

            queryset = queryset.order_by(desc(TriggerLog.created_at)).all()

            trigger_counts = {i.trigger_id: i.count for i in trigger_counts.all()}
            trigger_counts[None] = null_trigger_counts.count()

            return paginate(
                queryset,
                headers=trigger_counts,
                exclude=[],
            )

    @router.get("/logs/{id}")
    async def retrieve_single_logs(
        self,
        id: int,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(TriggerLog)
                .filter(TriggerLog.id == id)
                .options(
                    subqueryload(TriggerLog.workflow_obj),
                    subqueryload(TriggerLog.trigger_obj),
                )
                .first()
            )
            if not queryset:
                raise BadRequest_400(message="Id doesnot exist.")

            return jsonable_encoder(queryset)

    @router.get("/{id}/")
    async def retrieve(
        self,
        id: int,
        system_id: int,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            queryset = (
                db.query(Trigger)
                .where(
                    Trigger.system_id == system_id,
                    Trigger.id == id,
                    Trigger.deleted == False,
                )
                .outerjoin(Trigger.created_obj)
            ).first()
            if not queryset:
                raise BadRequest_400(message="Id doesnot exist.")

            return queryset

    @router.post("/", status_code=201)
    async def post(
        self,
        trigger: TriggerSchema,
        background_tasks: BackgroundTasks,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data, log_data = await repo.save(
                db=db,
                trigger=trigger,
                current_user=current_user,
                organization=self.organization,
                schema=self.db_code,
                background_tasks=background_tasks,
            )

            return JSONResponse(
                {
                    "message": "workflow saved successfully",
                    "data": data,
                    "detail": log_data,
                }
            )

    @router.put("/enable-disable/")
    async def enable_disable(
        self,
        triggers: list[StatusTriggerSchema],
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            for trigger in triggers:
                trigger_obj = (
                    db.query(Trigger).where(
                        Trigger.system_id == Trigger.system_id,
                        Trigger.id == trigger.id,
                        Trigger.deleted == False,
                    )
                ).first()

                if not trigger_obj:
                    raise BadRequest_400(message=f"{trigger.id} id doesnot exist.")

                trigger_obj.is_enabled = trigger.is_enabled

            db.commit()
            return {
                "message": "Update successfully",
            }

    @router.put("/restore/")
    async def restore(
        self,
        trigger: BulkDeleteTrigger,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.restore(db=db, trigger=trigger, schema=self.tenant_code)
            return {
                "message": "Restored successfully",
            }

    @router.put("/bulk-update/")
    async def put(
        self,
        triggers: list[BulkUpdateTriggerSchema],
        background_tasks: BackgroundTasks,
        db: Session = Depends(get_db),
        current_user: user_orm.User = Depends(oauth2.get_current_user),
    ):
        with schema_context(self.tenant_code, db):
            data = await repo.update(
                db=db,
                triggers=triggers,
                current_user=current_user,
                schema=self.db_code,
                background_tasks=background_tasks,
            )
            return JSONResponse(
                {"message": "workflow saved successfully", "data": data}
            )

    @router.delete("/")
    async def delete(
        self,
        trigger: BulkDeleteTrigger,
        db: Session = Depends(get_db),
    ):
        with schema_context(self.tenant_code, db):
            _ = await repo.delete(db=db, trigger=trigger, schema=self.db_code)
            return {
                "message": "Deleted successfully",
            }
