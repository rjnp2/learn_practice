import requests

from technical.endpoints.workflow_services.trigger.utils import (
    get_data_from_config,
)


async def api_event(
    db,
    config,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    url_config = config.get("url")
    headers_config = config.get("headers")
    payload_config = config.get("payload")
    params_config = config.get("params")
    method_config = config.get("method")

    input_column = []
    is_ask_for_input = False

    url_config = get_data_from_config(
        db=db,
        data_config=url_config,
        event_dict=event_dict,
        data_name="url",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = url_config.get("error_message")
    is_ask_for_input = url_config.get("is_ask_for_input")
    url = url_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    headers_config = get_data_from_config(
        db=db,
        data_config=headers_config,
        event_dict=event_dict,
        data_name="headers",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = headers_config.get("error_message")
    is_ask_for_input = headers_config.get("is_ask_for_input")
    headers = headers_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    payload_config = get_data_from_config(
        db=db,
        data_config=payload_config,
        event_dict=event_dict,
        data_name="payload",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = payload_config.get("error_message")
    is_ask_for_input = payload_config.get("is_ask_for_input")
    payload = payload_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    params_config = get_data_from_config(
        db=db,
        data_config=params_config,
        event_dict=event_dict,
        data_name="params",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = params_config.get("error_message")
    is_ask_for_input = params_config.get("is_ask_for_input")
    params = params_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    method_config = get_data_from_config(
        db=db,
        data_config=method_config,
        event_dict=event_dict,
        data_name="method",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = method_config.get("error_message")
    is_ask_for_input = method_config.get("is_ask_for_input")
    method = method_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    elif not url or not headers or not payload or not params or not method:
        return {
            "error_message": f"It seems that the configuration data is invalid, as url or headers or payload or params or method is empty in this {workflow_title}."
        }

    method = method.lower()
    try:
        if method == "get":
            response = requests.get(url, headers=headers, params=params)

        elif method == "post":
            response = requests.post(url, headers=headers, json=payload, params=params)

        elif method == "put":
            response = requests.put(url, headers=headers, json=payload, params=params)

        elif method == "delete":
            response = requests.delete(url, headers=headers, params=params)

        elif method == "patch":
            response = requests.patch(url, headers=headers, json=payload, params=params)

        elif method == "options":
            response = requests.options(url, headers=headers, params=params)

        else:
            response = requests.head(url, headers=headers, params=params)

        data = {
            "response_data": response.json(),
            "method": method,
            "url": url,
            "headers": headers,
            "payload": payload,
            "params": params,
        }
        return {"output_data": data, "input_column": []}

    except Exception as e:
        return {"error_message": f"An error occurred due to {e}"}
