import subprocess
from technical.endpoints.workflow_services.trigger.utils import (
    get_data_from_config,
)


async def execute_script(
    db,
    config,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    code_config = config.get("code")
    language_config = config.get("language").lower()

    input_column = []
    is_ask_for_input = False
    code_config = get_data_from_config(
        db=db,
        data_config=code_config,
        event_dict=event_dict,
        data_name="code",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = code_config.get("error_message")
    is_ask_for_input = code_config.get("is_ask_for_input")
    code = code_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    language_config = get_data_from_config(
        db=db,
        data_config=language_config,
        event_dict=event_dict,
        data_name="language",
        input_column=input_column,
        all_event_dict=all_event_dict,
        workflow_title=workflow_title,
        is_ask_for_input=is_ask_for_input,
        **kwargs,
    )
    error_message = language_config.get("error_message")
    is_ask_for_input = language_config.get("is_ask_for_input")
    language = language_config.get("output_data")
    if error_message:
        return {
            "error_message": error_message,
        }

    if is_ask_for_input:
        return {"error_message": "ask_for_input", "input_column": input_column}

    elif not code and not language:
        return {
            "error_message": f"It seems that config data is invalid i.e code and language is empty in this {workflow_title}.",
        }

    code = code.strip()

    if language == "python":
        try:
            exec_globals = {}
            exec_locals = {}
            exec(code, exec_globals, exec_locals)
            output = exec_locals.get("output", "")
        except Exception as e:
            return {"error_message": f"An error occurred: {e}"}

    elif language == "bash":
        try:
            result = subprocess.run(
                ["bash", "-c", code], capture_output=True, text=True
            )
            output = result.stdout
            error = result.stderr
        except Exception as e:
            return {"error_message": f"An error occurred: {e}"}

    else:
        return {
            "error_message": f"It seems that {language} language value inside config data is invalid choice in this {workflow_title}."
        }


async def execute_sequence_script(
    config,
    workflow_title,
    event_dict={},
    all_event_dict={},
    *args,
    **kwargs,
):
    if not config:
        return {
            "error_message": f"It seems that config data is invalid i.e config is empty in this {workflow_title}."
        }

    for script in config:
        outcome = await execute_script(
            script,
            workflow_title,
            event_dict=event_dict,
            all_event_dict=all_event_dict,
            *args,
            **kwargs,
        )

        error_message = outcome.get("error_message")
        if error_message:
            return {
                "error_message": error_message,
            }
