import subprocess
import sys


def run_background_process(script_path):
    python_env_location = f"{sys.prefix}/bin/python3.11"

    try:
        subprocess.Popen([python_env_location, script_path], shell=False)
        print(f"Started background process for script: {script_path}")
    except Exception as e:
        print(f"Error starting background process: {e}")
