from sqlalchemy import text


def form_condition_checker(groups, conditions, source, data_dict, db):
    result_data = []
    for group in groups:
        filter_data = ""
        having_data = ""
        junction = ""

        condition_data = conditions.get(group)
        if condition_data is None:
            return {"error_message": f"{group} is not found in conditions data."}

        column = condition_data.get("column")
        if column is None:
            return {"error_message": f"column is not provided in conditions_mapping."}

        form_id = condition_data.get("form_id")
        if form_id is None:
            return {"error_message": f"form_id is not provided in conditions_mapping."}

        f_schema_name = condition_data.get("schema")
        if f_schema_name is None:
            return {
                "error_message": f"f_schema_name is not provided in conditions_mapping."
            }

        value = condition_data.get("value")
        if value is None:
            return {"error_message": f"value is not provided in conditions_mapping."}

        operator = condition_data.get("operator").lower().strip().replace("_", " ")
        if operator is None:
            return {"error_message": f"operator is not provided in conditions_mapping."}

        operator = operator.lower().strip().replace("_", " ")

        response_choice = condition_data.get("response_choice")
        if response_choice is None:
            return {
                "error_message": f"response_choice is not provided in conditions_mapping."
            }

        if source.lower() == "static":
            form_name = form_id
            response_choice = "default"

        elif source.lower() == "dynamic":
            form_name = f"table_{form_id}"

        else:
            return {"error_message": f"Source is not valide input."}

        # n_type = condition_data["type"]

        if operator not in [
            "=",
            "!=",
            "in",
            "not in",
            "like",
            "ilike",
            "not like",
            "not ilike",
            # "startwith",
            # "endwith",
            # "istartwith",
            # "iendwith",
            ">",
            ">=",
            "<",
            "<=",
            "between",
        ]:
            return {"error_message": f"Invalid {operator} operator for {group}."}

        if isinstance(value, list) and response_choice.startswith(
            ("internal", "relation", "global")
        ):
            value = ",".join(f"'{i}'" for i in value)

        elif operator == "between":
            start_item = value[0]
            end_item = value[-1]
            value = f"'{start_item}' and '{end_item}'"

        elif operator in ["like", "not like", "ilike", "not ilike"]:
            value = f"'%{value}%'"

        elif operator in [
            "startwith",
        ]:
            operator = "like"
            value = f"'%{value}'"

        elif operator in [
            "endwith",
        ]:
            operator = "like"
            value = f"'{value}%'"

        elif operator in [
            "istartwith",
        ]:
            operator = "ilike"
            value = f"'%{value}'"

        elif operator in [
            "iendwith",
        ]:
            operator = "ilike"
            value = f"'{value}%'"

        else:
            value = f"'{value}'"

        if response_choice == "default":
            filter_data = f'"{column}" {operator} {value}'

        elif response_choice.startswith("global"):
            filter_data = f'"{column}" && ARRAY[{value}]'

        elif response_choice.startswith(("internal", "relation")):
            jt = f"jt_{form_id}_{column}"
            having_data = f"ARRAY[{value}]::bigint[] @> ARRAY_AGG(DISTINCT {jt}.table_2)::bigint[]"
            junction = f"""join {f_schema_name}.{jt} {jt} 
                on {form_name}.id = {jt}.table_1
                """
        else:
            return {"error_message": f"Invalid condition"}

        if filter_data:
            filter_data += " and deleted=false"
        else:
            filter_data += "deleted=false"

        if (
            data_dict
            and data_dict.get("id")
            and data_dict.get("form_id")
            and data_dict.get("form_id") == form_id
        ):
            filter_data += f" and id={data_dict.get('id')}"

        if form_name == "organization__user":
            junction = f"join public.user puser on {form_name}.user_id = puser.id"

        if having_data:
            having_data = f"having {having_data}"

        raw_query = f"""
            SELECT EXISTS (
                SELECT 1 FROM {f_schema_name}.{form_name} 
                {junction}
                where {filter_data}
                GROUP BY {form_name}.id
                {having_data}
            );
        """
        raw_query = text(raw_query)
        exist = db.execute(raw_query).fetchone()[0]
        result_data.append(exist)

    return {"condition_output": result_data}


def check_trigger_condition(groups, conditions, trigger_data):
    if not trigger_data:
        return {"error_message": f"Trigger data is empty in conditions data."}

    result_data = []
    for group in groups:
        condition_data = conditions.get(group)
        if condition_data is None:
            return {"error_message": f"{group} is not found in conditions data."}

        column = condition_data.get("column")
        if column is None:
            return {"error_message": f"column is not provided in conditions_mapping."}

        value = condition_data.get("value")
        if value is None:
            return {"error_message": f"value is not provided in conditions_mapping."}

        instance_value = trigger_data.get(column)
        if instance_value is None:
            return {"error_message": f"{column} value is not present in trigger_data."}

        operator = condition_data.get("operator")
        if operator is None:
            return {"error_message": f"operator is not provided in conditions_mapping."}

        operator = operator.lower().strip().replace("_", " ")

        if operator not in [
            "=",
            "!=",
            "in",
            "not in",
            "like",
            "ilike",
            "not like",
            "not ilike",
            ">",
            ">=",
            "<",
            "<=",
            "between",
        ]:
            return {"error_message": f"Invalid {operator} operator for {group}."}

        if operator == "=":
            operator = "=="

        elif operator == "like":
            result_data.append(str(value).lower() in str(instance_value).lower())

        elif operator == "not ilike":
            result_data.append(str(value).lower() not in str(instance_value).lower())

        elif operator == "between":
            start_item = value[0]
            end_item = value[-1]
            result_data.append(
                instance_value <= start_item and instance_value >= end_item
            )
        else:
            result_data.append(eval(f"{instance_value} {operator} {value}"))

    return {"condition_output": result_data}


def check_condition(db, condition_config, *args, **kwargs):
    if not condition_config:
        return {"condition_output": False}

    trigger_data = kwargs.get("trigger_data", {})
    data_dict = kwargs.get("data_dict", {})
    source = condition_config.get("source")

    conditions_mapping = condition_config.get("conditions_mapping")
    if conditions_mapping is None:
        return {"error_message": f"conditions_mapping is not provided in config."}

    conditions = condition_config.get("conditions")
    if conditions is None:
        return {"error_message": f"conditions is not provided in config."}

    combined_result_data = []
    for conditions_map in conditions_mapping:
        gate = conditions_map.get("gate")

        if gate is None:
            return {"error_message": f"gate is not provided in conditions_mapping."}

        trigger_id = conditions_map.get("trigger_id", "")
        if trigger_id and trigger_id != trigger_data.get("trigger_id", ""):
            continue

        groups = conditions_map.get("group")
        if groups is None:
            return {"error_message": f"groups is not provided in conditions_mapping."}

        if trigger_id:
            condition_output = check_trigger_condition(
                groups=groups, conditions=conditions, trigger_data=trigger_data
            )
        else:
            condition_output = form_condition_checker(
                groups=groups,
                conditions=conditions,
                source=source,
                data_dict=data_dict,
                db=db,
            )

        error_message = condition_output.get("error_message")
        if error_message:
            return {"error_message": error_message}

        result_data = condition_output.get("condition_output")

        if gate.lower() == "and":
            result_data = all(result_data)

        elif gate.lower() == "or":
            result_data = any(result_data)

        combined_result_data.append(result_data)

    return {"condition_output": all(combined_result_data)}
